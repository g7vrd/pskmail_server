#! /usr/bin/perl -w
# ARQ module rflinkserver.pl by PA0R. This module is part of the PSK_ARQ suite of
# programs. Rflinkserver contains the server protocol engine which adds an arq layer

# Rflinkserver.pl includes logic for the server.
# to keyboard oriented protocols like PSK31, PSK63, MFSK, MT63 etc.
# This program is published under the GPL license.
#   Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014
#       Rein Couperus PA0R (rein@couperus.com)
#
# *    rflinkserver.pl is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    (at your option) any later version.
# *    the Free Software Foundation; either version 2 of the License, or
# *
# *    rflinkserver.pl is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.3
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#Last change: 270614 DL8JF


use lib "/usr/local/share/pskmail_server";
use Mail::POP3Client;
# use Net::SMTP;
# use Net::SMTP::TLS;
use Email::Send::SMTP::Gmail;
use DB_File;
use IO::Handle;
use MIME::Base64;
use MIME::Lite;
use POSIX qw(strftime);
use Time::Local;
use Email::LocalDelivery;
use Email::Folder;
use IO::Select;
use File::Temp;

use LWP;
my $period = 30;
my $debug_mail = 0;
my $monitor = 0;
my $opensystem = 1;
my $Use_ssl = 0;
my $email_limit = 20000;
my $Emails_enabled = 1;
my $Web_enabled = 1;
my $BigEarserverport = "10148";
my $select = IO::Select->new();
my @memories = ();
my $recxtail = "";
my $debug2 = 0;
my $outfreq = 0;
my $Transfer = 0;
$PTTCOMMAND = 0;

#statistics
my $Messagecount = 0;
my $statlog = "$ENV{HOME}/.pskmail/stats.log";
my $Email_up = 0;
my $Email_down = 0;
my $Local_up = 0;
my $Local_down = 0;
my $File_up = 0;
my $File_down = 0;
my $Bulls_sent = 0;

$Defaultmode = "PSK500R";
$NOINTERNET = 0;

use arq;

$SIG{INT} = \&catcher;

my ($ServerCall, $Inputfile, $output, $dbfile, $dbpass, $dbaddress, $relay, $smtpauthlevel, $smtphelo, $smtptlsuser, $smtptlspass, $smtptlsport, $Max_retries, $Maxidle, $Shortidle, $Maxwebpage, $nosession, $commandcall, $Aprs_connect, $Aprs_beacon, $Aprs_port, $Aprs_address, @prefixes, $posit_time, $scheduler, @schedule, $rigtype, $rigrate, $rigdevice, $scanner, $qrgfile, $freq_offset, @freq_corrections, @Beaconarray, $traffic_qrg, $bulletinmode);

my $TxInputfile = "";
$dbfile = "$ENV{HOME}/.pskmail/rflinkusers/rflink_users.db";
$output = ">$ENV{HOME}/.pskmail/gmfsk_autofile"; # deprecated 
$TxInputfile = "$ENV{HOME}/.pskmail/TxInputfile";
$Inputfile = "$ENV{HOME}/.pskmail/gMFSK.log"; # deprecated 

$traffic_qrg = 0;
$nosession = "none";
$posit_time = 10; #deprecated 
my $traffic = "";
$bulletinmode = "MFSK32";
$BigEar = "";
$MARS = 0;
my $pingout = "No internet connection";
my @ping = ();
my $txtimer = 0;

#`echo "none" > $ENV{HOME}/.pskmail/PSKmailsession`;


if (!-d "$ENV{HOME}/.pskmail/transfer") {
	`mkdir ~/.pskmail/transfer`;
}
if (!-d "$ENV{HOME}/.pskmail/bulletins") {
	`mkdir ~/.pskmail/bulletins`;
}
if (!-d "$ENV{HOME}/.pskmail/repeats") {
	`mkdir ~/.pskmail/repeats`;
}

eval {
	if ($NOINTERNET == 0) {
		@ping = `ping -c 1 pskmail.org 2>/dev/null`;
	}
};

if($ping[1] && $ping[1] =~ /(time=\d*\.*\d* ms)/) {
	$pingout = "Ping " . $1;
	`touch $ENV{HOME}/.pskmail/.internet`;
	print "Internet connection o.k.\n";
} else {
	if (-e "$ENV{HOME}/.pskmail/.internet") {
		`rm ~/.pskmail/.internet`;
		print "No internet connection\n";
	}
}



if (-e "$ENV{HOME}/.pskmail/debug") {
	$debug2 = 1;
}

if (-e "$ENV{HOME}/.pskmail/pskmailrc.pl") {
	eval `cat $ENV{HOME}/.pskmail/pskmailrc.pl`
	or die "No config file: $@\n";

}
if (-e "$ENV{HOME}/.pskmail/.manual_config") {
	my $config = `cat $ENV{HOME}/.pskmail/.manual_config`;
	chomp $config;
	($ServerCall,$relay,$Aprs_beacon) = split ",", $config;
	`rm $ENV{HOME}/.pskmail/.manual_config`;
}
if (-e "$ENV{HOME}/.pskmail/qrg/memtable") {
	open ($fh1, "$ENV{HOME}/.pskmail/qrg/memtable");
	my @memtable = <$fh1>;
	close $fh1;

	foreach $mline (@memtable) {
		chomp $mline;
			my ($nr,$fr) = split "," , $mline;
			if ($fr && $fr =~ /^\d.*/){
				$memories[$nr] = $fr;
			}
	}
}

	`touch $ENV{HOME}/.pskmail/aprslog`;
	`echo "0" > $ENV{HOME}/.pskmail/.band`;

my $BE = 0; #Bigear available?

print "Beacon:" . $Aprs_beacon . "\n";

if ($Aprs_beacon !~ /(\d\d)\d\d\.\d\d([NS])P(\d\d\d)\d\d\.\d\d([EW])&.*/ &&
	$Aprs_beacon !~ /(\d\d)\d\d\.\d\d([NS])\/(\d\d\d)d\d\.\d\d([EW]).*/) {
	logprint("Wrong APRS beacon format. Please adjust pskmailrc.pl\n");
	print ("Latitude format is: ddmm.mm, padded with leading and trailing zeros\n");
	print ("e.g.: 4420.34 or 0400.11 or 0000.10\n\n");
	print ("Longitude format is: dddmm.mm, padded with leading and trailing zeros\n");
	print ("e.g.: 14420.34 or 00400.11 or 00000.10\n\n");
	print "Please stop and restart\n";
} else {
	my $lt = $1;
	my $ln = $3;
	print "POS=$1$2:$3$4\n";
	if ($2 eq "S") {
		$lt *= -1;
	}
	if ($4 eq "W") {
		$ln *= -1;
	}
	if ($lt < 71 && $lt > 35 && $ln > -20 && $ln < 20) {
		$BE = 1;
		print "Geo area is valid\n";
	} else {
		print "Outside BigEar geo area...\n";
	}
}

if (-e "$ENV{HOME}/.pskmail/scheduler.pl") {
	eval `cat $ENV{HOME}/.pskmail/scheduler.pl` or die "Could not interpret scheduler file\n";
		# get the schedule if there is one...
	$scheduler = 1;
}

my $modem = "PSK500R";

setmode ($modem);

### connect to BigEar server...

	sendfreqs(); # send frequency table to pskmail.org

#=head1

print "BigEar serverport:", $BigEarserverport, "\n";

if ($BE == 1 && $BigEarserverport eq "10148" && $MARS != 1) {
	$BigEar = bigearconnect ();
#	print "Trying to connect to $BigEar\n";
}

if ($BigEar) {
	print "Connected to PSKBigEar\n";
} else {
	print "BigEar not available\n";
}

if (-e "$ENV{HOME}/.pskmail/.idcd") {
	`rm $ENV{HOME}/.pskmail/.idcd`;
}
#=cut

my ($reader, $writer);
pipe($reader, $writer);
$writer->autoflush(1);

my $ConnectStatus = "Listening";
my $TextFromFile = "";
my $Status = "Listen";
my $RxStatus = "";
my $Systemstatus = "";
my $Retries = 0;
my $RxReady = 0;
my $outputstring = "";
my $oldprediction = "";
my $connectsecond = 0;

my $Version = "2.2.2";
my $Pop_host = 	"";	# pop3 provider
my $Pop_user = 	"";	# pop user
my $Pop_pass = 	"";	# pop password
my $attachment = ""; # attachment file name
my $address = 	""; 	# set return address for SMTP....
my $mailuser = 	"$ENV{HOME}/.pskmail/.mailuser"; 	# data store where the mail is received with fetchmail...
my $findupasswd = "";	# password for APRS
my $key = "0";
my $prm = "0";

my $Usercalls = "";
my $usrmessage = "";
my $call = "";
my $dummy = 0;		# dummy variable
my $mymailnumber = 0;
my @msgarray = ();
#scheduler
my $systime = 0;
my $lasthour = 0;
#scanner
my $oldhour = 0;
my $oldmins = 0;
my @freqs = ();
my @modes = ();
my @freqhrs = ();
my @rigmod = ();
my @scancls = ();
my $transfer_time = 10;

# beacon
my $Beacon_sent = 0;
my @Beacons_sent = qw (0 0 0 0 0);	# Keeps track of beacons, have they been sent this time around ?

# RSID
my $rsidtx_sent = 0;
my $txID_off = 0;
my @freqtable = ();
# These were preset to PSK500R and USB. That is not right as it messes up memory scan
my $pskmodes;
my $rigmodes;



		if ($scanner && -e "$ENV{HOME}/.pskmail/qrg/freqs.txt") {
			
			open ($fh, "$ENV{HOME}/.pskmail/qrg/freqs.txt") or die "Cannot open the scanner file!";
			@freqhrs = <$fh>;
			close ($fh);

			my $storeline = "";
			my $j = 0;
			for ($i = 0; $i < scalar @freqhrs; $i++) {
				if ($freqhrs[$i] =~ /^(\d+,\d+,\d+,\d+,\d+).*/) {
					$freqtable[$j] = $1;
					$j++;
				} elsif ($freqhrs[$i] =~ /[USBFM]+,[USBFM]+,[USBFM]+,[USBFM]+,[USBFM]+/) {
					$rigmodes = $freqhrs[$i];
				} elsif ($freqhrs[$i] =~ /[DMINEXCWIAPSKTHORMF0-9]+,[DMINEXCWIAPSKTHORMF0-9]+,[DMINEXCWIAPSKTHORMF0-9]+,[DMINEXCWIAPSKTHORMF0-9]+,[DMINEXCWIAPSKTHORMF0-9]+/) {
					$pskmodes = $freqhrs[$i];
				}
			}

			$hour = (time / 3600) % 24;		#/
			if ($j > 23) {
				$cat = $freqtable[$hour];
			} else {
				$cat = $freqtable[0];
			}
			print "Scanning: $cat", "\n";
			print "Offset = $freq_offset minute(s)\n";
			print "Pskmodes:$pskmodes.\n";

			chomp $cat;
			@freqs = split (",", $cat);
			@scanfreqs = @freqs;
			# Added a check, error if trying to split when there are no rigmodes.
			if ($rigmodes) { @rigmod = split (",", $rigmodes);}


			if (-e "$ENV{HOME}/.pskmail/qrg/scancalls") {
				my $scancalls = `cat $ENV{HOME}/.pskmail/qrg/scancalls`;
				@scancls = split (",", $scancalls);

				print $scancalls, "\n";
			}
			### send freq. table to pskmail.org
			send_freq_table();
		} elsif ($scanner eq "F") {
			print "No frequency file ~/.pskmail/qrg/freqs.txt\n";
			exit;
		}

if ($scanner && $scanner eq "M") {
	if (-e "$ENV{HOME}/.pskmail/qrg/memtable") {
		; # is o.k.
	} else {
		print "Memory scan needs a file ~/.pskmail/qrg/memtable\n";
		exit;
	}
}

$ConnectStatus = "Listening";


# open aprs write port

BEGIN { @f = split //, "/-35753=?=357"x2; }

#my ($udp_ipaddr,$udp_portaddr);

$error = `echo "none" > $ENV{HOME}/.pskmail/PSKmailsession`;
$error = `touch zb`;

initialize();

	open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
	print SESSIONDATA $nosession;
	close SESSIONDATA;

#get users from data base
	getusercalls();

{
	my @shortcallusers = split (" ", ($ServerCall . " " . $Usercalls));
	open (CALLS, ">$ENV{HOME}/.pskmail/calls.txt");
	foreach my $call (@shortcallusers) {
		print CALLS $call, "\n";
	}
	close CALLS;
}

#=head1
########### fork off a listner for BigEar server ##############
 	my $bigearline;

	if ($bigearpid = fork)	{
		# this is the parent, continuing below
	} else {

		$svr = "";

		# listens for bigear, writes to pipe.
		die "cannot fork the bigear listener: $!\n" unless defined $bigearpid;

		while (1) {
			if ($BigEar) {
					eval {
					local $SIG{ALRM} = sub {die "alarm"};
					alarm 2;
					eval {
						$bigearline = <$BigEar>;
					};
					alarm 0;
				};
				alarm 0;
				if ($@ && $@ =~ /alarm/) {
					print "Timeout reading BigEar\n";
				} elsif ($@) {
					print "$@,\n";
				}
			}

			if ($bigearline) {
				my $freq = 0;
#print $bigearline;
				if ($bigearline !~ /$ServerCall/) {

					if ($bigearline =~ /<(.*)>\s%U(\d*)/) {
						$svr = $1;

						$freq = $2;
						if ($freq eq "") {
							$freq = 0;
						}
						open (FRE, "$ENV{HOME}/.pskmail/.band");
						my $srvfreq = <FRE>;
						close FRE;
						chomp $srvfreq;
#printf ("UP:%d, %s\n", $freq, $svr);
						if ($freq == 0 || $freq < ($srvfreq + 100) && $freq > ($srvfreq - 100)) {
#print "DCD!\n";
							open (IDCD, ">$ENV{HOME}/.pskmail/.idcd");
							print IDCD $svr;
							close IDCD;
						}
					} elsif ($bigearline =~ /<(.*)>\s%D/ && $svr eq $1) {
						if (-e "$ENV{HOME}/.pskmail/.idcd") {
							`rm $ENV{HOME}/.pskmail/.idcd`;
						}
#printf ("DWN:%d, %s\n", $freq, $svr);						

					}

				}

				$bigearline = "";
			}

			if (-e "$ENV{HOME}/.pskmail/.bigear") {
				my $packet = `cat $ENV{HOME}/.pskmail/.bigear`;
				`rm $ENV{HOME}/.pskmail/.bigear`;
				if ($BigEar) {
					print $BigEar $packet or die "noprint";
				} else {
					sleep 1;
					$BigEar = bigearconnect();
					if ($BigEar) {
						$BigEar->autoflush(1);
						print "Reconnected to BigEar\n";
					}
				}

			}
			select (undef, undef, undef, 0.1);
		}
	}
#=cut


if ($pid = fork) {
	close $writer;
my $ok = 0;
my $bulname = "";
my @bs = ();	
###############################################################
#   service loop
###############################################################
	while (1) {
#print "SERVICELOOP\n";
		$ok = 0;
		if (-e "$ENV{HOME}/.pskmail/.auth_ok") {
			$ok = 1;
		}
		my $readline= "";
		eval {
			local $SIG{ALRM} = sub { die "timeout" };
			alarm 10;
			eval {
				$readline = <$reader>;
			};
		};
		alarm 0;
		die if $@ && $@ !~ /timeout/;
		
		if (-e "$ENV{HOME}/.pskmail/.mailmessage") {
			$readline = `cat $ENV{HOME}/.pskmail/.mailmessage`;
			`rm $ENV{HOME}/.pskmail/.mailmessage`;
		}

##### send bulletin ########################################
		$session = get_session();
				
		if ($session eq "none") {
				$bs = `ls $ENV{HOME}/.pskmail/bulletins`;
			
				if ($bs ne "") {
					@bss = split " ", $bs;
					$bulname = shift @bss;
					print $bulname, "\n";
					sleep 20;
				}
		
				if ($bulname ne "" && -e "$ENV{HOME}/.pskmail/bulletins/$bulname") {
							
		print "", $bulname, "\n";
		$outf = `cat "$ENV{HOME}/.pskmail/bulletins/$bulname"`;
		print $outf, "\n";
		
					my ($bulletintext, $filetime) = getbulletin ("$ENV{HOME}/.pskmail/bulletins/$bulname");	
						
					my $dt = `date -u`;
					$bulletintext = "ZCZC\nQTC de $ServerCall \n$dt\n\n" . $bulletintext . "NNNN\n";
					
		print $bulletintext, "\n";
					
					sendmode($bulletinmode);
					
					sendamp ($bulletintext);
		
					print "Deleting bulletin:" , "$ENV{HOME}/.pskmail/bulletins/$bulname", "\n";			
					
					addstat ("bulletin");
				}
		}
##### send long bulletin ########################################

		if (-e "$ENV{HOME}/.pskmail/pskdownload/longbulletin") {
			my $bulletintext = `cat $ENV{HOME}/.pskmail/pskdownload/longbulletin`;
			my $dt = `date -u`;
			$bulletintext = "ZCZC\nQTC de $ServerCall \n$dt\n\n" . $bulletintext . "NNNN\n";
			my $bullmodem = "MT63-500";
			setmode ($bullmodem);
			sendmodemcommand ("<txrsid>ON</txrsid>");
			$bullmodem = "PSK500R";
			sendlongbulletin ($bulletintext);
			setmode ($bullmodem);
			addstat ("longbulletin");
			`rm $ENV{HOME}/.pskmail/pskdownload/longbulletin`;
		}
##### send fleetcodes ########################################

		if (-e "$ENV{HOME}/.pskmail/pskdownload/fleetcode") {
			my $bulletintext = `cat $ENV{HOME}/.pskmail/pskdownload/fleetcode`;
			my $dt = `date -u`;
			$bulletintext = "ZFZF\nQTC de $ServerCall \n$dt\n\n" . $bulletintext . "NNNN\n";
			sendbulletin ($bulletintext);
			addstat ("fleetcode");
			`rm $ENV{HOME}/.pskmail/pskdownload/fleetcode`;
		}
############################################################
#   Any traffic waiting?

			if (int (time()/60) % 5 == 3) { $Transfer = 0;} 
		
			if (int (time()/60) % 5 == 4 && $Transfer == 0)  {			## 5 mins aprs beacon min. 4 ...
							# now do what we want....
				eval {
					if ($NOINTERNET == 0) {
						@ping = `ping -c 1 pskmail.org 2>/dev/null`;
					}
				};
				
				if($ping[1] && $ping[1] =~ /(time=\d*\.*\d* ms)/) {
					$pingout = "Ping " . $1;
					`touch $ENV{HOME}/.pskmail/.internet`;
#					print "Internet connection o.k.\n";
				} else {
					if (-e "$ENV{HOME}/.pskmail/.internet") {
						`rm ~/.pskmail/.internet`;
#						print "No internet connection\n";
					}
				}
																	
				if (-e "$ENV{HOME}/.pskmail/.internet") {		
					my $serverslist = `cat $ENV{HOME}/.pskmail/.internetcalls`;  # see if there is traffic
					@servers = split "\n", $serverslist;
					foreach my $server (@servers) {
#						print "SER:", $server, "\n";
						if ($server =~ /(\d+\s\d+:\d+)\s([A-Z0-9\-]+)/) {
							$meat{$2}++;
						}
					}
		
					my $result2 = "";
					my $result3 = "";
					my $result1 = query ("~listmail $ServerCall");
	
					my @fls = split "\n", $result1;
					$result1 = "";
					if ($fls[0] ne "-end-") {
						$result2 = query ("~getmail $fls[0]");
											
							if (length($result2) > 10) {
#								print "MAIL:\n", $result2;
								## put the mail away...
								storemail ($result2);
								
								$result3 = query("~deletemail $fls[0]");	
							} else {
								$result2 = "";
								$fls[0] = "";
							}
					}
				}
									
				$session = get_session();
				
				if ($session eq "none") {
					my @trafficwaiting = `ls -l ~/.pskmail/transfer`;
					
					if (@trafficwaiting > 1 ) {
						print "TRAFFIC\n";
						my $nroffiles = @trafficwaiting;
						my $peer;
						if ($trafficwaiting[1] =~ /.*\d+:\d+\s(.*)/) {
							$traffic = "$ENV{HOME}/.pskmail/transfer/" . $1;

							my $peer = "";
							$transferheaderline = `cat $traffic | head -n 1 `;
							print $transferheaderline, "\n";
							if ($transferheaderline =~ />FM:([A-Z0-9\-]+):([A-Z0-9\-]+):.*/) {
								$peer = $2;
							}
							
							$Transfer = 1;
							settxrsid(1);
							setrxrsid(1);
							my $info = newtransferconnectblock($peer, $ServerCall, $1);
		#					print "INFO:", $info, "\n";
							sendtransfer($info);
							`echo $traffic >> $ENV{HOME}/.pskmail/filetransfer`;
						}
					}
						
				}
			}

					
############################################################
		if ($readline) {
			chomp($readline);
			if ($monitor) {
				print $readline, "\n";
			}
#print "READLINE:", $readline, "\n";
			$_ = $readline;
			if (m/~TEST/) {
				if (-e "$ENV{HOME}/.pskmail/testfile") {	#/
					`cat $ENV{HOME}/.pskmail/testfile >> $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "Sorry, no testfile" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/Message\ssent.*/ && -e "$ENV{HOME}/.pskmail/filetransfer") {
				`echo "~QUIT" >> $ENV{HOME}/.pskmail/TxInputfile`;
				`touch $ENV{HOME}/.pskmail/.endtransfer`;
			} elsif (m/^(\S+)\s\d+\.\d+\.\d+\-\d{2}.*/m) {
####### switch frequency #################################
				print "OK, Server=", $1,"\n";
							`cat $traffic >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
			} elsif (m/~QSY!/) {
####### switch frequency #################################
					if ($traffic_qrg) {
						sleep 10;
						$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "F" $traffic_qrg`;
						print "QSY to $traffic_qrg!\n";
					}
			} elsif (m/~QSY (\d+)/) {
####### switch frequency #################################
					my $goodfreq = 0;
					my $qsyfreq = $1;
print $qsyfreq, "\n";
					if ($qsyfreq) {
print "freq ok, " . $qsyfreq . "\n";

					if ($scanner eq "M") {
						@freqs = memtofreq ($hour);
					}
						if ($qsyfreq > 1838250 && $qsyfreq < 1849750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 1838250 && $freqs[$i] < 1849750) {
									$goodfreq = 1;
								}
							}
						} elsif ($qsyfreq > 3580250 && $qsyfreq < 3599750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 3580250 && $freqs[$i] < 3599750) {
									$goodfreq = 1;
								}
							}

						} elsif ($qsyfreq > 7040250 && $qsyfreq < 7049750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 7040250 && $freqs[$i] < 7049750) {
									$goodfreq = 1;
								}
							}


						} elsif ($qsyfreq > 10140250 && $qsyfreq < 10148500) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 10140250 && $freqs[$i] < 10148500) {
									$goodfreq = 1;
								}
							}

						} elsif ($qsyfreq > 14070250 && $qsyfreq < 14098750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 14070250 && $freqs[$i] < 14098750) {
									$goodfreq = 1;
								}
							}

						} elsif ($qsyfreq > 18095250 && $qsyfreq < 18108750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 18095250 && $freqs[$i] < 18108750) {
									$goodfreq = 1;

								}
							}
						} elsif ($qsyfreq > 21070250 && $qsyfreq < 21109750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 21070250 && $freqs[$i] < 21109750) {
									$goodfreq = 1;
								}
							}
						} elsif ($qsyfreq > 24915250 && $qsyfreq < 24928750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 24915250 && $freqs[$i] < 24928750) {
									$goodfreq = 1;
								}
							}
						} elsif ($qsyfreq > 28070250 && $qsyfreq < 28149750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 28070250 && $freqs[$i] < 28149750) {
									$goodfreq = 1;
								}
							}
						} elsif ($qsyfreq > 50100250 && $qsyfreq < 50499750) {
							for ($i = 0; $i < 5; $i++) {
								if ($freqs[$i] > 50100250 && $freqs[$i] < 50499750) {
									$goodfreq = 1;
								}
							}
						} else {
							print "Invalid frequency:" , $qsyfreq, "\n";
						}
						if ($scanner eq "F" && $goodfreq == 1) {
								`echo "QSY to $1!\n" > $ENV{HOME}/.pskmail/TxInputfile`;
								$qsyfreq += $freq_corrections[0];
								sleep 10;
								$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "F" $qsyfreq`;
								print "QSY to $1!\n";
						} elsif ($scanner eq "M") {
								`echo "trying QSY to $1!\n" > $ENV{HOME}/.pskmail/TxInputfile`;
								$qsyfreq = freqtomem ($qsyfreq);
								if ($qsyfreq) {
									sleep 10;
									$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "E" $qsyfreq`;
									print "QSY to $1!\n";
								} else {
									`echo "Sorry, frequency not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
								}
						} else  {
							`echo "Frequency out of range..." > $ENV{HOME}/.pskmail/TxInputfile`;
						}
					}
			} elsif (m/~STATUS?/) {
####### get server status #################################

					my $uptime = `uptime`;
					my $mem = `df | grep sda`;
					if ($mem =~ /(\d+%)/) { $mem = "MM = " . $1 . "\n";}
					my $pingout = "No internet connection";
					my @ping = ();
					eval {
						@ping = `ping -c 1 pskmail.org 2>/dev/null`;
					};
					if($ping[1] && $ping[1] =~ /(time=\d*\.*\d* ms)/) {
						$pingout = "Ping " . $1;
					}
					`echo "\nServer status:\n$uptime$mem$pingout\n" > $ENV{HOME}/.pskmail/TxInputfile`;

			} elsif (m/~BEACONTEST/) {
print "BEACONTEST>>>\n";
#					set_txstatus("TxPositBeacon");
					set_txstatus("TxPositBeacon");
					send_frame;
					sleep (5);

			} elsif (m/~CHANNELS/) {
#######  send list of channels to client #############################
				if (-e "$ENV{HOME}/.pskmail/memories.pm" && $scanner eq "M") {
					my @memfreqs = ();
					my @memnrs = ();


			    	my $frqs = "Scanning memories: ";
					for ($i = 0; $i < 5; $i++) {
						$frqs .= $freqs[$i];
						$frqs .= ",";
					}
					`echo "$frqs" >> $ENV{HOME}/.pskmail/TxInputfile`;

					`echo "Channels open for summoning:" >> $ENV{HOME}/.pskmail/TxInputfile`;

					open MEM, "$ENV{HOME}/.pskmail/memories.pm";
					`echo "Memories:" >> $ENV{HOME}/.pskmail/TxInputfile`;
					while (my $line = <MEM>) {

						if ($line =~ /\s==\s(\d\d\d\d\d\d\d\d*)/) {
							push @memfreqs, $1;
						}
						if ($line =~ /sfreq = (\d\d*)/) {
							push @memnrs, $1;
						}
					}
					close MEM;
					for ($i = 0; $i < $#memfreqs; $i++) {
						$memout = sprintf ("%d = %d", $memnrs[$i], $memfreqs[$i]);
						`echo $memout >> $ENV{HOME}/.pskmail/TxInputfile`;
					}

					`echo "Listen first!!\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				} elsif (defined @frequencytable) {
					my $frqs = "Scanning: ";
					for ($i = 0; $i < 5; $i++) {
						$frqs .= $frequencytable[$i];
						$frqs .= ",";
					}
					`echo "$frqs" > $ENV{HOME}/.pskmail/TxInputfile`;
				} elsif (-e "$ENV{HOME}/.pskmail/validfreqs.pm") {
					open FMEM, "$ENV{HOME}/.pskmail/validfreqs.pm";

					my $frqs = "Scanning: ";
					for ($i = 0; $i < 5; $i++) {
						$frqs .= $freqs[$i];
						$frqs .= ",";
					}

					`echo "$frqs" > $ENV{HOME}/.pskmail/TxInputfile`;

					`echo "Channels open for summoning:" >> $ENV{HOME}/.pskmail/TxInputfile`;
					while (my $line = <FMEM>) {
						print $line;
						if ($line =~ /\s==\s(\d\d\d\d\d\d\d\d*)/) {
							print $1, "\n";

							`echo $1 >> $ENV{HOME}/.pskmail/TxInputfile`;
						}
					}
					close FMEM;
					`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "Listen first!!\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				} elsif ($scanner eq "F") {
					my $frqs = "Scanning: ";
					for ($i = 0; $i < 5; $i++) {
						$frqs .= $freqs[$i];
						$frqs .= ",";
					}

					`echo "All narrowband digital (500Hz) channels for R1 allowed" > $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "$frqs" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "Listen first!!\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "Sorry, no scan\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				}

			} elsif (m/~ROUTES/) {
####### send routes table to client  #################################
				if (-e "$ENV{HOME}/.pskmail/routes") {
					my $routes = "";
					my @routelines = `cat $ENV{HOME}/.pskmail/routes`;
					my @sortedlist = sort { $a cmp $b } @routelines;
					foreach my $line (@sortedlist) {
						if ($line =~ /(\S+) (\S+) (\d+)/) {
							my $rec = $3;
							my $tim = time();
							my $dur = int (($tim - $rec) / 60);
							if ($dur < 10080) {
								my $add = sprintf ("%s -> %s %d\n", $1,$2,$dur);
								$routes .= $add;
							}
						}
					}

					`echo "$routes" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "Sorry, no routes available" > $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~GETBEACONS/) {
####### switch beacon on #################################
				my $call = get_session();
					my $beacons = getbeacons($call);
					`echo "$beacons" > $ENV{HOME}/.pskmail/TxInputfile`;

			}elsif (m/~QTC\?/) {
####### get list of headers #################################
				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					getuserdata(get_session());

					if ($Pop_host) {

						`echo "Trying your mail" > $ENV{HOME}/.pskmail/TxInputfile`;

						eval {
							list_mail() ; # get mail from pop server
						};
						if ($@) {
							`echo "Cannot get the mail headers...$@" > $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							logprint ("QTC received...\n");
							if ($monitor && $debug_mail) {
								print `cat $ENV{HOME}/.pskmail/mailheaders`;
							}
							if (-s "$ENV{HOME}/.pskmail/mailheaders" < 10) {
								`echo "No mail" > $ENV{HOME}/.pskmail/TxInputfile`;
							}elsif (-e "$ENV{HOME}/.pskmail/mailheaders") {
								my $hdrlength = length (`cat $ENV{HOME}/.pskmail/mailheaders`);
								`echo "Your mail:" >> $ENV{HOME}/.pskmail/TxInputfile`;
								my $headers = `cat $ENV{HOME}/.pskmail/mailheaders >> $ENV{HOME}/.pskmail/TxInputfile`;
								`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
								unlink "$ENV{HOME}/.pskmail/mailheaders";
							}
							addstat ("getheaders");
						}
					} else {
							`echo "Cannot get your mail!" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				}
			}elsif (m/~QTC (\d*)\+/) {
####### get list of headers from nr. #########################
				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					my $starthdr = $1;
					getuserdata(get_session());

					print "Req. QTC from $starthdr\n";

					if ($Pop_host) {
						eval {
							list_mail() ; # get mail from pop server
						};
						if ($@) {
							`echo "Cannot get the mail headers...$@" > $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							logprint ("QTC received...\n");
							if ($monitor && $debug_mail) {
								print `cat $ENV{HOME}/.pskmail/mailheaders`;
							}
							if (-s "$ENV{HOME}/.pskmail/mailheaders" < 10) {
								`echo "No mail" > $ENV{HOME}/.pskmail/TxInputfile`;
							}elsif (-e "$ENV{HOME}/.pskmail/mailheaders") {
								my $hdrlines = `cat $ENV{HOME}/.pskmail/mailheaders | wc -l`;

								my $newhdrs = $hdrlines - $starthdr;

								if ($newhdrs > 1) {
									my $headers = `tail -n $newhdrs $ENV{HOME}/.pskmail/mailheaders`;
									my $hdrlength = length ($headers);
									my $ident = sprintf("Your mail: %d", $hdrlength);
									`echo "$ident" >> $ENV{HOME}/.pskmail/TxInputfile`;
									`echo "$headers" >> $ENV{HOME}/.pskmail/TxInputfile`;
									`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
									unlink "$ENV{HOME}/.pskmail/mailheaders";
									addstat ("getheaders");
								} else {
									my $hdrs = $hdrlines - 2;
									`echo "Sorry, only $hdrs mails" >> $ENV{HOME}/.pskmail/TxInputfile`;
								}
							}
						}
					} else {
							`echo "Don't know how get your mail!" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				}
			} elsif (m/~MAIL/) {
####### get ALL mail #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					getuserdata(get_session());
					if ($Pop_host) {
						eval {
							read_mail(1 ... $Count) ; # get mail from pop server
						};
						if ($@) {
							`echo "Cannot get your mail: $@ > $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							filter($mailuser, 0);
							if ($debug_mail) {`cat $ENV{HOME}/.pskmail/mailfile`;}
							`mv $ENV{HOME}/.pskmail/mailfile $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
						}
					} else {
							`echo "Cannot get your mail!" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				}
			} elsif (m/~LISTLOCAL(.*)/) {
####### list local mails #################################


				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					my $call = get_session();

					if (-e "$ENV{HOME}/.pskmail/localmail/$call") {
						my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
						my $mails = "";
						my $count = 0;

						for my $msg ($folder->messages) {
							$count++;
							print $count, " ", $msg->header("From"), " ", $msg->header("Subject"), "\n";
							$mails .= $count . " ". $msg->header("From") . " " . $msg->header("Subject") . "\n";
						}
						`echo "Local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$mails" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
						addstat ("getheaders");
					} else {
						`echo "No local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				}

			} elsif (m/~DELETELOCAL (.*)/) {
####### delete local mails #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					my $call = get_session();
					my $msgnr = 1 unless $1;
					my @numbers = split / /, $1;

					if (-e "$ENV{HOME}/.pskmail/localmail/$call") {
						my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
						my $mails = "";
						my $count = 0;
						for my $msg ($folder->messages) {
							$count++;
							my $del = 1;
							for $msgnr (@numbers) {
								if ($count == $msgnr) {
									$del = 0;
								}
							}
							if ($del) {
								$mails .= "From $call " .
								$msg->header("Date") . "\n" .
								$msg->as_string . "\n\n";
							}
						}
						open (MAILS, ">", "$ENV{HOME}/.pskmail/localmail/$call");
						print MAILS $mails;
						close (MAILS);
						`echo "Mails $1 deleted..." >> $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						`echo "No local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				}
			} elsif (m/~READLOCAL (.*)/) {
####### read local mails #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					my $call = get_session();
					my $msgnr = 1 unless $1;
					my @numbers = split / /, $1;


					if (-e "$ENV{HOME}/.pskmail/localmail/$call") {
						my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
						my $mails = "";
						my $count = 0;

						for my $msg ($folder->messages) {
							$count++;
							for $msgnr (@numbers) {
								if ($count == $msgnr) {
									$mails .= "Message $count for $call:\n" .
									"From: ". $msg->header("From") .
									"\nSubject: " .
									$msg->header("Subject") . "\n" .
									"Date: " .
									$msg->header("Date") . "\n\n" .
									$msg->body . "\n" ;
								}
							}
						}
						$mails =~ s/\r\n/\n/g;
						`echo "$mails" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "\n-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						addstat ("getlocalmail");
					} else {
						`echo "No local mail for $call:\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				}
			} elsif (m/~READPAQ(.*)/ && $Emails_enabled ) {
####### get single mails #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					getuserdata(get_session());
					my @numbers = split / /, $1;
					eval {
						read_mail(@numbers);
					};
					if ($Emails_enabled == 0) {
						`echo "sorry, no internet email on this server" > $ENV{HOME}/.pskmail/TxInputfile`;
					} elsif ($@) {
						`echo "Cannot get your mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						filter($mailuser, 0);
						`/usr/local/share/pskmail_server/paq864 $ENV{HOME}/.pskmail/mailfile`;
						$size_file = `cat $ENV{HOME}/.pskmail/mailfile.864`;
						$ident = "~PAQ864RD " . sprintf("%d", length($size_file));
						`echo "$ident" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`cat mailfile.864 >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($debug_mail) {`cat $ENV{HOME}/.pskmail/mailfile`;}
						unlink "$ENV{HOME}/.pskmail/mailfile.864";
						addstat ("getmail");
					}
				}
			} elsif (m/~READZIP\s*(\d*)/ && $Emails_enabled) {
####### get single mails compressed #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					my $mailnr = 0;
					$mailnr = $1;
					print "mailnr=",$mailnr, "\n";
					if ($mailnr) {
						getuserdata(get_session());
						my @numbers = split / /, $1;

						eval {
							read_mail(@numbers);
						};
						if ($Emails_enabled == 0) {
							`echo "sorry, no internet email on this server" > $ENV{HOME}/.pskmail/TxInputfile`;
						} elsif ($@) {
							`echo "Cannot get your mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							filter($mailuser, 0);
							`gzip $ENV{HOME}/.pskmail/mailfile`;
							$size_file = `cat $ENV{HOME}/.pskmail/mailfile.gz`;
							my $b64file = encode_base64 ($size_file);

							if (getprotocol()) {
								my $server = $ServerCall;
								my $call = get_session();
								my $lqrb = length($b64file);
								my $filenm = " ";

								my $tempname = mktemp(XXXXXX);
								if ( -d "$ENV{HOME}/.pskmail/pending/$call") {
									;
								} else {
									mkdir "$ENV{HOME}/.pskmail/pending/$call";
								}

								my $ListBundle = ">FM:" . $server . ":" . $call . ":" . $tempname .
												":m:" . $filenm . ":" . $lqrb;

								`echo "$ListBundle" >> $ENV{HOME}/.pskmail/pending/Transactions`;

								`echo "$b64file" >> $ENV{HOME}/.pskmail/pending/$call/$tempname`;

								`echo "$ListBundle" >> $ENV{HOME}/.pskmail/TxInputfile`;
								`echo "$b64file" >> $ENV{HOME}/.pskmail/TxInputfile`;
								`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
							} else {
								$ident = "~ZIPPED64 " . sprintf("%d", length($b64file));
								`echo "$ident" >> $ENV{HOME}/.pskmail/TxInputfile`;
								`echo "$b64file" >> $ENV{HOME}/.pskmail/TxInputfile`;
								`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
							}
						}

							if ($debug_mail && -e "$ENV{HOME}/.pskmail/mailfile") {
								`cat $ENV{HOME}/.pskmail/mailfile`;
							}
							unlink "$ENV{HOME}/.pskmail/mailfile.gz";
							addstat ("getmail");

					} else {
						`echo "Which mail number?\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				}

			} elsif (m/~READ (\d*)/ && $Emails_enabled) {
####### get single mails #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

				getuserdata(get_session());

				if ($Pop_host) {

					my @numbers = ();
					my $number	= 0;

					if ($1) {
						$number = $1;
					} else {
						`echo "Which mail number?" > $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "Enter number and click READ" > $ENV{HOME}/.pskmail/TxInputfile`;
					}
					$numbers[0] = $number;
					eval {
						read_mail(@numbers);
					};
					if ($Emails_enabled == 0) {
						`echo "sorry, no internet email on this server" > $ENV{HOME}/.pskmail/TxInputfile`;
					} elsif ($@) {
						`echo "Cannot get your mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						filter($mailuser, 0);
						my $msglen = -s "$ENV{HOME}/.pskmail/mailfile";
						if ($msglen > $email_limit) {
							`head -c $email_limit mailfile`;
							`echo "Your msg: $msglen" >> $ENV{HOME}/.pskmail/TxInputfile`;
							`cat $ENV{HOME}/.pskmail/mailfile >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "[mail truncated]" >> $ENV{HOME}/.pskmail/TxInputfile`;

						} else {
							`echo "Your msg: $msglen" >> $ENV{HOME}/.pskmail/TxInputfile`;
							if ( -e "$ENV{HOME}/.pskmail/mailfile") {
								`cat $ENV{HOME}/.pskmail/mailfile >> $ENV{HOME}/.pskmail/TxInputfile`;
							}
						}
							`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;

						if ($debug_mail && -e "$ENV{HOME}/.pskmail/mailfile") {
							`cat $ENV{HOME}/.pskmail/mailfile`;
						}
						if ( -e "$ENV{HOME}/.pskmail/mailfile") {
							unlink "$ENV{HOME}/.pskmail/mailfile";
						}
						addstat ("getmail");
					}
				}
				}
			} elsif (m/~KEEP(.*)/) {
####### send mail to archive #################################
####### changer subject to [Mailarchive]date #################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

				getuserdata(get_session());
				if ($Pop_host) {

					my @numbers = split / /, $1;
					eval {
						read_mail(@numbers);
					};
					if ($@) {
						`echo "Cannot get your mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						filter($mailuser, 1);

						@message = ();

						my $call = get_session();
						getuserdata($call);

						my $subject = "[Mailarchive] " . `date`;
						my $to = $address;

						push @message, $to;
						push @message, $address;
						push @message, $subject;

						open ($MA, "$ENV{HOME}/.pskmail/mailfile");
						my @body = <$MA>;
						close ($MA);
						foreach my $messageline (@body) {
							chomp $messageline;
						}
						push @message, @body;

						eval {
							$apath = 'none';
							send_mail($apath,@message);	# send the mail via smtp
						};
						if ($@) {
							`echo "Error sending mail : $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							`echo "\nArchive $1 sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						}
						@message = ();
						unlink "$ENV{HOME}/.pskmail/mailfile";
					}
				}
				}
			} elsif (m/~GETFILE (.*)/) {
####### get file from ./pskdownload dir #################################
				my $grb = `cat $ENV{HOME}/.pskmail/pskdownload/$1`;
				$grb =~ s/\r\n/\n/g;
				`echo "$grb" > $ENV{HOME}/.pskmail/TxInputfile`;
				`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
				addstat ("getfile");
			} elsif (m/~GETBIN (.*)/) {
####### get file from ./pskdownload dir #################################
				my $filenm = $1;

				my $grb = "";
				if (-e "$ENV{HOME}/.pskmail/pskdownload/$1") {
					if (substr ($filenm, -2) ne "gz" && substr ($filenm, -3) ne "bz2") {
						`gzip -f -9 $ENV{HOME}/.pskmail/pskdownload/$filenm`;
						$filenm .= ".gz";
						$grb = `cat $ENV{HOME}/.pskmail/pskdownload/$filenm`;
						`gunzip -f $ENV{HOME}/.pskmail/pskdownload/$filenm`;
					} else {
						$grb = `cat $ENV{HOME}/.pskmail/pskdownload/$filenm`;
					}
					$grb = encode_base64 ($grb);
					$lqrb = length ($grb);

					if (getprotocol()) {
	# here store message

						my $server = $ServerCall;
						my $call = get_session();

						my $tempname = mktemp(XXXXXX);
						if ( -d "$ENV{HOME}/.pskmail/pending/$call") {
							;
						} else {
							mkdir "$ENV{HOME}/.pskmail/pending/$call";
						}


						my $Bundlestr = ">FM:" . $tempname . ":f:" . $filenm . ":" . $lqrb;

						my $ListBundle = ">FM:" . $server . ":" . $call . ":" . $tempname .
										":f:" . $filenm . ":" . $lqrb;

						`echo "$ListBundle" >> $ENV{HOME}/.pskmail/pending/Transactions`;

						`echo "$grb" >> $ENV{HOME}/.pskmail/pending/$call/$tempname`;
	#					`echo "-end-" >> $ENV{HOME}/.pskmail/pending/$call/$tempname`;

						`echo "$ListBundle" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						`echo "Your file:$filenm $lqrb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
					addstat ("getfile");
				} else {
					`echo "File not found!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~FA:(.*)/) {
				my $Transaction = $1;
				my $Tlines = "";
				my @TRS = ();
		
				if (-e "$ENV{HOME}/.pskmail/filetransfer") {
					my $rmfile = "$ENV{HOME}/.pskmail/transfer/" . $Transaction;
					unlink($rmfile);
					print "Removing $Transaction from transfer list\n";
					`rm $ENV{HOME}/.pskmail/filetransfer`;
#					disconnect();
				}
				
				my $call = get_session();

				if (-e "$ENV{HOME}/.pskmail/pending/$call/$Transaction") {
					`rm $ENV{HOME}/.pskmail/pending/$call/$Transaction`;
					print "Deleting transaction $Transaction\n";
				}
				if (-e "$ENV{HOME}/.pskmail/pending/Transactions") {				
					$Tlines = `cat $ENV{HOME}/.pskmail/pending/Transactions`;
					@TRS = split "\n", $Tlines;
				}

				if (-e "$ENV{HOME}/.pskmail/pending/Transactions"){
					`rm $ENV{HOME}/.pskmail/pending/Transactions`;
				}

		# rewrite transactions list
				foreach my $TR (@TRS) {
					if ($TR !~ /$Transaction/) {
						`echo "$TR" >> $ENV{HOME}/.pskmail/pending/Transactions`;
					}
				}
				
			
			} elsif (m/~FN:(.*)/) {
				my $Transaction = $1;
				my $call = get_session();

				if (-e "$ENV{HOME}/.pskmail/pending/$call/$Transaction") {
					`rm $ENV{HOME}/.pskmail/pending/$call/$Transaction`;
					print "Deleting transaction $Transaction\n";
				}
				my $Tlines = `cat $ENV{HOME}/.pskmail/pending/Transactions`;
				my @TRS = split "\n", $Tlines;

				if (-e "$ENV{HOME}/.pskmail/pending/Transactions"){
					`rm $ENV{HOME}/.pskmail/pending/Transactions`;
				}
		# rewrite transactions list
				foreach my $TR (@TRS) {
					if ($TR !~ /$Transaction/) {
						`echo "$TR" >> $ENV{HOME}/.pskmail/pending/Transactions`;
					}
				}
				`echo "\nTransaction aborted...\n" > $ENV{HOME}/.pskmail/TxInputfile`;
			} elsif (m/~Mp(\d+),(.*)/) {
				my $pw = make_secret ($1, $2);
				$pw = md5_base64($pw);
				my $cl = get_session();
				my $rec = getuserdata($cl);
#print "RECORD:", $rec,"\n";
				if (length($findupasswd) < 3 || $findupasswd eq "none" || $findupasswd eq "NONE") {
					$ok = 0;
					`touch "$ENV{HOME}/.pskmail/.auth_ok"`;
#					`echo "No password set..." >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "No authentication... set password ??" >> $ENV{HOME}/.pskmail/TxInputfile`;
				} elsif (-e "$ENV{HOME}/.pskmail/.auth_ok") {
					`rm "$ENV{HOME}/.pskmail/.auth_ok"`;
					$ok = 0;
					$pw = getp($cl, $pw);
					$ok = $pw;
					if ($ok) {
						`touch "$ENV{HOME}/.pskmail/.auth_ok"`;
						`echo "OK..." >> $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						`echo "No authentication... set password ??" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				} else {
					`touch "$ENV{HOME}/.pskmail/.auth_ok"`;
					`echo "Auth ok..." >> $ENV{HOME}/.pskmail/TxInputfile`;
					$ok = 1;
				}
				print "OK:" , $ok, "\n";
			} elsif (m/~RESETRECORD/) {
				my $cl = get_session();
						tie (%db, "DB_File", $dbfile)
							or die "Cannot open user database\n";
							delete $db{$cl};
						untie %db;
						`echo "Mail record deleted..." >> $ENV{HOME}/.pskmail/TxInputfile`;
			} elsif (m/~Msp(\d+),(.*)/) {

#				if ($ok > 0) {
					my $pw = make_secret ($1, $2);
					my $cl = get_session();
					getuserdata($cl);
					@val = split ",",$pw;
					if (!defined $Pop_pass || $Pop_pass eq "") { $Pop_pass = "none"; }

					if ($val[0] eq $Pop_pass || $Pop_pass eq "none"
							|| setp( $Pop_pass) eq "none" || $Pop_pass eq "\n"){

						tie (%db, "DB_File", $dbfile)
							or die "Cannot open user database\n";
						my @values = split "," ,$db{$cl};
						$findupassword = md5_base64($val[1]);

						my $record = (shift @values || "none") . ",";
						$record .= (shift @values || "none") . ",";;
						$record .= (shift @values || "none") . ",";
						$record .= (shift @values || "smtp_host") . ",";
						$record .= (shift @values || "none") . ",";
						$record .= (shift @values || "none") . ",";
						$record .= ($findupassword || "none") ;

						$db{$cl} = $record;
						$record = $db{$cl};
						untie %db;
						`echo "Session password changed!" >> $ENV{HOME}/.pskmail/TxInputfile`;
						$ok = 1;
					} else {
						`echo "Error, no mail password...\nPassword not changed!" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
#				} else {
#					`echo "Sorry, not allowed...\nPassword not changed!" >> $ENV{HOME}/.pskmail/TxInputfile`;
#				}

			} elsif (m/~GETIAC/) {
####### get IAC fleetcode file #################################
				my $filenm = "http://weather.noaa.gov/pub/data/raw/as/asxx21.egrr..txt";
				if (-e "$ENV{HOME}/.pskmail/.fleetcode") {
					`rm $ENV{HOME}/.pskmail/.fleetcode`;
					print "Deleting .fleetcode\n";
				}
				`wget http://weather.noaa.gov/pub/data/raw/as/asxx21.egrr..txt -O $ENV{HOME}/.pskmail/.fleetcode `;
				$firstline = `head -n 1 $ENV{HOME}/.pskmail/.fleetcode`;

				my $asstime = "000000";
				if ($firstline =~ /ASXX21\sEGRR\s(\w\w\w\w\w\w)/) {
					$asstime = $1;
				}

				my ($year, $month, $day) = (localtime)[5,4,3];
				my $filedate = sprintf("_%04d%02d%02d", $year+1900, $month+1, $day);
				my $filename = "$ENV{HOME}/.pskmail/pskdownload/ASXXX_" . $asstime . $filedate;
				my $tname = "ASXXX_" . $asstime . $filedate . ".gz";

				`mv $ENV{HOME}/.pskmail/.fleetcode $filename`;

				my $grb = "";

				if (-e "$filename") {
					`gzip -f $filename`;
					$filename .= ".gz";
					$grb = `cat $filename`;
					$grb = encode_base64 ($grb);
					$lqrb = length ($grb);
					`rm $filename`;

					if (getprotocol()) {
	# here store message

						my $server = $ServerCall;
						my $call = get_session();

						my $tempname = mktemp(XXXXXX);
						if ( -d "$ENV{HOME}/.pskmail/pending/$call") {
							;
						} else {
							mkdir "$ENV{HOME}/.pskmail/pending/$call";
						}


						my $Bundlestr = ">FM:" . $tempname . ":f:" . $tname . ":" . $lqrb;

						my $ListBundle = ">FM:" . $server . ":" . $call . ":" . $tempname .
										":f:" . $tname . ":" . $lqrb;

						`echo "$ListBundle" >> $ENV{HOME}/.pskmail/pending/Transactions`;

						`echo "$grb" >> $ENV{HOME}/.pskmail/pending/$call/$tempname`;

						`echo "$ListBundle" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						`echo "Your file:$filenm $lqrb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
					addstat ("getfile");
				} else {
					`echo "File not found!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~GET2IAC/) {
####### get IAC fleetcode file #################################
				my $filenm = "http://weather.noaa.gov/pub/data/raw/fs/fsxx21.egrr..txt";
				if (-e "$ENV{HOME}/.pskmail/.fleetcode") {
					`rm $ENV{HOME}/.pskmail/.fleetcode`;
					print "Deleting .fleetcode\n";
				}
				`wget http://weather.noaa.gov/pub/data/raw/fs/fsxx21.egrr..txt -O $ENV{HOME}/.pskmail/.fleetcode `;
				$firstline = `head -n 1 $ENV{HOME}/.pskmail/.fleetcode`;

				my $asstime = "000000";
				if ($firstline =~ /FSXX21\sEGRR\s(\w\w\w\w\w\w)/) {
					$asstime = $1;
				}

				my ($year, $month, $day) = (localtime)[5,4,3];
				my $filedate = sprintf("_%04d%02d%02d", $year+1900, $month+1, $day);
				my $filename = "$ENV{HOME}/.pskmail/pskdownload/FSXXX_" . $asstime . $filedate;
				my $tname = "FSXXX_" . $asstime . $filedate . ".gz";

				`mv $ENV{HOME}/.pskmail/.fleetcode $filename`;

				my $grb = "";

				if (-e "$filename") {
					`gzip -f $filename`;
					$filename .= ".gz";
					$grb = `cat $filename`;
					$grb = encode_base64 ($grb);
					$lqrb = length ($grb);
					`rm $filename`;

					if (getprotocol()) {
	# here store message

						my $server = $ServerCall;
						my $call = get_session();

						my $tempname = mktemp(XXXXXX);
						if ( -d "$ENV{HOME}/.pskmail/pending/$call") {
							;
						} else {
							mkdir "$ENV{HOME}/.pskmail/pending/$call";
						}


						my $Bundlestr = ">FM:" . $tempname . ":f:" . $tname . ":" . $lqrb;

						my $ListBundle = ">FM:" . $server . ":" . $call . ":" . $tempname .
										":f:" . $tname . ":" . $lqrb;

						`echo "$ListBundle" >> $ENV{HOME}/.pskmail/pending/Transactions`;

						`echo "$grb" >> $ENV{HOME}/.pskmail/pending/$call/$tempname`;

						`echo "$ListBundle" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						`echo "Your file:$filenm $lqrb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
					addstat ("getfile");
				} else {
					`echo "File not found!" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~STOP:(.*)/) {
				if ($1) {
					my $Transaction = $1;
					my $call = get_session();

					if (-e "$ENV{HOME}/.pskmail/pending/$call/$Transaction") {
						`rm $ENV{HOME}/.pskmail/pending/$call/$Transaction`;
						print "Deleting transaction $Transaction\n";
					}
					my $Tlines = `cat $ENV{HOME}/.pskmail/pending/Transactions`;
					my @TRS = split "\n", $Tlines;

					if (-e "$ENV{HOME}/.pskmail/pending/Transactions") {
						`rm $ENV{HOME}/.pskmail/pending/Transactions`;
					}
			# rewrite transactions list
					foreach my $TR (@TRS) {
						if ($TR !~ /$Transaction/) {
							`echo "$TR" >> $ENV{HOME}/.pskmail/pending/Transactions`;
						}
					}
				}

				`touch $ENV{HOME}/.pskmail/.abort_trans`;

			} elsif (m/~FA:(.*)/) {
				my $Transaction = $1;
				my $call = get_session();

				if (-e "$ENV{HOME}/.pskmail/pending/$call/$Transaction") {
					`rm $ENV{HOME}/.pskmail/pending/$call/$Transaction`;
					print "Deleting transaction $Transaction\n";
				}
				my $Tlines = `cat $ENV{HOME}/.pskmail/pending/Transactions`;
				my @TRS = split "\n", $Tlines;
				
				if (-e "$ENV{HOME}/.pskmail/pending/Transactions") {
					`rm $ENV{HOME}/.pskmail/pending/Transactions`;
				}
		# rewrite transactions list
				foreach my $TR (@TRS) {
					if ($TR !~ /$Transaction/) {
						`echo "$TR" >> $ENV{HOME}/.pskmail/pending/Transactions`;
					}
				}
print "calling stop function\n";
				`touch $ENV{HOME}/.pskmail/.abort_trans`;
				set_txstatus("RESET_TXQueue");
				send_frame("");

			} elsif (m/~FO5:([A-Z0-9\-]+):([A-Z0-9\-]+):([A-Za-z0-9\-]+):(\w):(.+):(\d+)/) {

				if ($4 eq "s" || $4 eq "u" || $4 eq "b") {
#					if ($2 eq $ServerCall) {
						my $tempname = $3;
						if (-e "$ENV{HOME}/.pskmail/incoming/$tempname") {
							my $tempcontent = `cat $ENV{HOME}/.pskmail/incoming/$tempname`;
							my $templength = length($tempcontent);
							`echo "~FY:$tempname:$templength" >> $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							`echo "~FY:$tempname:0" >> $ENV{HOME}/.pskmail/TxInputfile`;
						}

#					}
				}

			} elsif (m/\>FM:([A-Z0-9\-]+):([A-Z0-9\-]+):([A-Za-z0-9\-]+):(\w):(.*):(\d+)/) {
####### receive compressed  file #################################

	print $1, "\n";
	print $2, "\n";
	print $3, "\n";
	print $4, "\n";
	print $5, "\n";
	print $6, "\n";
	

							
				my $h_from = $1;
				my $h_to = $2;
				my $h_tmp = $3;
				my $h_type = $4;
				my $h_xxx = $5;
				my $uploadfile = $5;
				my $h_length = $6;
print "TYPE:", $h_type, "\n";
print "NAME:", $h_xxx, "\n";			
			my $tempfile = $3;
			if ($4 eq "s") {
				`echo "Receiving email" > $ENV{HOME}/.pskmail/TxInputfile`;


				my $mailstatus = "receive";
				my $cmail = "";
				my $pmail = "";

				logprint ("Receiving compressed mail \n");

				@body = ();
				my $partial = 0;

				if (!-e "$ENV{HOME}/.pskmail/incoming/") {
					`mkdir $ENV{HOME}/.pskmail/incoming/`;
				} elsif (-e "$ENV{HOME}/.pskmail/incoming/$tempfile") {
					$cmail = `cat $ENV{HOME}/.pskmail/incoming/$tempfile`;
					$partial = 1;
				}

				while ($mailstatus eq "receive") {
					$readline = <$reader>;

					if ($readline) {

						if ($readline =~ /.*~ABORTSEND.*/) {
							last;
						}

						if (get_session() eq "none") {
							last;
						}

						if (substr($readline, 0, 5) eq "-end-") {
#print "end of the mail....\n";
							last;
						}

						reset_idle();

						if ($monitor) {
							print $readline;
						}

						$cmail .= $readline;

						if ($partial == 0) {
							open (ADDPENDING, ">>", "$ENV{HOME}/.pskmail/incoming/$tempfile");
							print ADDPENDING $readline;
#							print "Adding:" . $readline . "\n";
							close ADDPENDING;
						}
					}

				}
				my $cpcmail = $cmail;

				$cmail = decode_base64 ($cmail);

				open (CMAIL, ">", "$ENV{HOME}/.pskmail/compressed.gz");
				print CMAIL $cmail;
				close CMAIL;

				$error = `gunzip -f $ENV{HOME}/.pskmail/compressed.gz`;

my $testvar = `cat $ENV{HOME}/.pskmail/compressed`;


				open (CMAIL, "<", "$ENV{HOME}/.pskmail/compressed");

				my @cmsg = ();
				@cmsg = <CMAIL>;
				$dummy = shift @cmsg;
				$dummy = shift @cmsg;

				foreach $readline (@cmsg){

					if ($readline) {

						reset_idle();
						chomp($readline);
						if ($monitor) {
							print $readline, "\n";
						}

						if ($readline =~ /^\.$/) {
							$mailstatus = "end_of_mail";
							@message = ();

							my $call = get_session();
							getuserdata($call);

							push @message, $to;
							push @message, $address;
							push @message, $subject;
							if ($to !~ /saildocs/){
								push @message, "PSKmail message from $address";
							}
							push @message, @body;
							
							if ($to =~ /\b[\w.-]+@[\w.-]+[.]+\w{2,4}\b/) { # internet mail
								eval {
									if (length($attachment) == 0) {
										$apath = 'none';
										send_mail($apath,@message);	# send the mail via smtp
									} else {
										push @message, $attachment;
										send_MIME(@message);    # send MIME message via smtp
										$attachment = "";
									}
								};

								if ($@) {
									`echo "Error sending mail : $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
								} else {
									`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
									`echo "~FA:$tempfile\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
									addstat ("sendmail");
								}
								@message = ();
								$mailstatus = "end_of_mail";


							} elsif ($to =~ /([A-Z0-9\-]+)\@$ServerCall/) { # local mbox
								`echo "Receiving local mail" > $ENV{HOME}/.pskmail/TxInputfile`;
								print "Local mbox $1\n";
								if (-e "$ENV{HOME}/.pskmail/localmail/$1") {
									print "Local mbox $1 exists\n";
								} else {
									`touch "$ENV{HOME}/.pskmail/localmail/$1"`;
									print "Local mbox $1 made\n";
									`echo "Local mailbox made" > $ENV{HOME}/.pskmail/TxInputfile`;
								}
								$error = shift @body;
								
								my $mydate = `date`;
								my $msg =
									"To: $to" . "\n" .
									"From: $call" . "\n" .
									"Subject: " . $subject . "\n" .
									"Date: $mydate" . "\n" .
									join ("\n", @body) . "\n\n";

								@delivered_to = ();
								@boxes = ("$ENV{HOME}/.pskmail/localmail/$1");
								@delivered_to = Email::LocalDelivery->deliver($msg, @boxes);

								@message = ();
								`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
								`echo "~FA:$tempfile\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

								$mailstatus = "end_of_mail";
								addstat ("sendlocalmail");
							} elsif ($to =~ /(.*)\@([A-Z0-9-]+)/) { # mbox at other server;
								my $check = checkserver($2);
#								print "CHECK=", $check, "\n";
								if ($check) {
									`echo "Receiving local mail for $2" > $ENV{HOME}/.pskmail/TxInputfile`;
									my $mydate = `date`;
									my $msg =
										"To: $to" . "\n" .
										"From: $call\@$ServerCall" . "\n" .
										"Subject: " . $subject . "\n" .
										"Date: $mydate" . "\n" .
										join ("\n", @body) . "\n\n";
	
## send $msg file to server
									if (!-d "$ENV{HOME}/.pskmail/transfer") {
										`mkdir ~/.pskmail/transfer`;
									}
								
								
									my $hdr = ">FM:$h_from:$2:$h_tmp:$h_type:$h_xxx:$h_length" . "\n";
									my $transitmsg = $hdr . $cpcmail;
									if (-e "$ENV{HOME}/.pskmail/.internet") {
										# try to send via internet portal
										sendlocalmail($transitmsg, $h_tmp);
										
									} else {
										open (TRMAIL, ">", "$ENV{HOME}/.pskmail/transfer/$h_tmp");
										print TRMAIL $transitmsg;
										close TRMAIL;
									}
										
									@message = ();
									`echo "\nWill try to send message to $2...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
									`echo "~FA:$tempfile\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
									
									`rm $ENV{HOME}/.pskmail/incoming/$tempfile`;
	
									$mailstatus = "end_of_mail";
									addstat ("sendtransitmail");
								} else {
									`echo "\nServer unknown\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
								}

							} 
						} elsif ($readline =~ /~ABORTSEND/) {	# in case we are stuck in a loop
							# stop.....
							$to = "";
							$subject  = "";
							$attachment = "";
							@body = ();
							last;
						}
						elsif ($readline =~ /Subject: /) {
							# add subject
							my $subjectline = substr($readline, 9);
							$subject = $subjectline ;
						}
						elsif ($readline =~ /To: /) {

							# write  To:

							$to = substr($readline, 4);

						} elsif ($readline =~ /Your attachment: filename=/) {

							$attachment = substr($readline, 26);
#							print $attachment, "\n";
							push @body, $readline;

						} else {
							push @body, $readline;
						}
					}
					if ($mailstatus eq "end_of_mail") {
						last;
					}

				}	# end mail receive
				
					addstat ("sendmail");

				if (-e "$ENV{HOME}/.pskmail/compressed") {
					`rm $ENV{HOME}/.pskmail/compressed`;
				}

				print "End of mail receive...\n";
#				`echo "~FA:$tempfile\n" >> $ENV{HOME}/.pskmail/TxInputfile`;


				close CMAIL;
				$mailstatus = "none";
# receive upload file
			  } elsif ($4 eq "u" || $4 eq "b") {

				if ($4 eq "b") {
					print "Receiving bulletin $h_xxx\n";
				} else {
					print "Receiving file\n";
				}

				$uploadfile = $h_xxx;
				$tempfile = $3;

				`echo "Receiving file" > $ENV{HOME}/.pskmail/TxInputfile`;

				my $mailstatus = "receive";
				my $cmail = "";
				my $pmail = "";

				logprint ("Receiving compressed mail \n");

				@body = ();
				my $partial = 0;

				if (!-e "$ENV{HOME}/.pskmail/incoming/") {
					`mkdir $ENV{HOME}/.pskmail/incoming/`;
				} elsif (-e "$ENV{HOME}/.pskmail/incoming/$tempfile") {
					$cmail = `cat $ENV{HOME}/.pskmail/incoming/$tempfile`;
					$partial = 1;
				}

				my $ready = 0;

				while ($mailstatus eq "receive") {

					if (get_session() eq "none") {
						last;
					}

					$readline = <$reader>;

					if ($readline) {

						if ($readline =~ /.*~ABORTSEND.*/) {
							last;
						}

						if ($readline =~ /~QUIT/) {
							last;
						}



						if (substr($readline, 0, 5) eq "-end-") {
							$ready = 1;
							$mailstatus = "end_of_file";
							last;
						}

						reset_idle();

						if ($monitor) {
#							print $readline;
						}

						$cmail .= $readline;

						if ($partial == 0) {
							open (ADDPENDING, ">>", "$ENV{HOME}/.pskmail/incoming/$tempfile");
							print ADDPENDING $readline;
							print "Adding:" . $readline . "\n";
							close ADDPENDING;
						}
					}

				}

				if ($ready) {
					$cmail = decode_base64 ($cmail);

					open (CMAIL, ">", "$ENV{HOME}/.pskmail/compressed.gz");
					print CMAIL $cmail;
					close CMAIL;

					$error = `gunzip -f $ENV{HOME}/.pskmail/compressed.gz`;
print $error, "\n";					

					my $testvar = `cat $ENV{HOME}/.pskmail/compressed`;
print "FILE:", $uploadfile, "\n";
					my $upload = "mv $ENV{HOME}/.pskmail/compressed $ENV{HOME}/.pskmail/uploads/" . $h_xxx;
					
					my $bulletin = "mv $ENV{HOME}/.pskmail/compressed $ENV{HOME}/.pskmail/bulletins/" . $h_xxx;
					
					if ($4 eq "b") {
						`$bulletin`;
					} else {
						`$upload`;
					}

					`rm $ENV{HOME}/.pskmail/incoming/$tempfile`;

					my $announcement = "~FA:" . $tempfile;
#					$announcement .= "File " . $uploadfile . " stored\n";

					`echo "$announcement" >> $ENV{HOME}/.pskmail/TxInputfile`;
					addstat ("sendfile");

				} else {
					`echo "File $uploadfile not stored" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			  }
			} elsif (m/~FY:(\w+):(\d+)/) {
				my $Transaction = $1;
				my $Frombyte = $2;
				my $Bundlestr = "";
				my $call = get_session();
				if (-e "$ENV{HOME}/.pskmail/pending/Transactions") {
					my $TRSs = `cat $ENV{HOME}/.pskmail/pending/Transactions`;
					my @TRS = split "\n", $TRSs;
					foreach $TRline (@TRS) {
						if ($TRline =~ /$Transaction/) {
							$Bundlestr = $TRline;
						}
					}

					$grb = `tail -c +$Frombyte $ENV{HOME}/.pskmail/pending/$call/$Transaction`;

					`echo "$Bundlestr" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "$grb" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~HRDSERVERS/) {
				getheard();
			} elsif (m/~LISTFILES/) {
####### list files from ./pskdownload dir #################################
				my $filelist = `ls -l $ENV{HOME}/.pskmail/pskdownload`;
				@filelines = split ("\n", $filelist);
				$filelist = "";
				foreach $fileline (@filelines) {
					if ($fileline =~ /.*1(.*) (.*) (.*) (.*) (.*)/) {
						$filelist = $filelist . $5 . " " . $3 . " " .  $4 . " " . $2 . "\n";
					}
				}
				my $flistlength = length($filelist);
				my $fflist = sprintf("\nYour_files: %d", $flistlength);
				my $grb = `echo "$fflist" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$grb = `echo "$filelist" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$grb = `echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
				$filelist = "";
			} elsif (m/~TGET (.*)/ && $Web_enabled) {
####### dump web page with elinks #################################
				my $URL = $1;
				my $webpage = "";
				my $begin = "";
				my $end = "";
				if ($URL =~ /^(\S+)\s+begin:(\S.+)\s+end:(\S.+)/) {
					$URL = $1;
					$begin = $2;
					$end = $3;
				}

				if ($Web_enabled) {
					if ($monitor) {
						print "Trying $URL\n";
					}
					`echo "Trying $URL\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

					eval {
						$webpage =  `elinks -dump -no-references -no-numbering $URL 2>&1` or die "No page";
	#					$webpage =  `lynx -dump -connect_timeout=10 $URL 2>&1` or die "No page";
					};

					if ($webpage =~ /^Alert!/m) { $webpage = "Timeout!\n"; }

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "404\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {

						if ($begin) {
			print "\nin begin...\n\n";
							my @shortpage = split "\n", $webpage;
							my $restpage = "";
							my $bg = 0;
							my $en = 0;
							foreach my $pgline (@shortpage) {
								if ($pgline =~ /$begin/i) {
									$bg = 1;
								}
								if ($pgline =~ /$end/i) {
									$bg = 0;
									$en = 1;
								} elsif ($bg && $en == 0) {
									$restpage .= $pgline;
									$restpage .= "\n";
								}

							}
							$webpage = $restpage;
						}

						if (length $webpage > $Maxwebpage) {
							$webpage = substr ($webpage, 0, $Maxwebpage);
							$webpage .= "\n-truncated -\n\n";
						}
						my $weblength = length ($webpage);
						`echo "Your wwwpage: $weblength" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
					addstat ("getweb");

				} else {
						`echo "Sorry, no web access from this server..."  >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~TGETZIP (.*)/) {
####### dump web page with elinks #################################
				my $URL = $1;
				my $webpage = "";
				my $begin = "";
				my $end = "";
				if ($URL =~ /^(\S+)\s+begin:(\S.+)\s+end:(\S.+)/) {
					$URL = $1;
					$begin = $2;
					$end = $3;
				}

				if ($Web_enabled) {
					if ($monitor) {
						print "Trying $URL\n";
					}
					`echo "Trying $URL\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

					eval {
						$webpage =  `elinks -dump -no-references -no-numbering $URL 2>&1` or die "No page";
	#					$webpage =  `lynx -dump -connect_timeout=10 $URL 2>&1` or die "No page";
					};




					if ($webpage =~ /^Alert!/m) { $webpage = "Timeout!\n"; }

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "404\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {


						if ($begin) {
							my @shortpage = split "\n", $webpage;
							my $restpage = "";
							my $bg = 0;
							my $en = 0;
							foreach my $pgline (@shortpage) {
								if ($pgline =~ /$begin/i) {
									$bg = 1;

								}
								if ($pgline =~ /$end/i) {
									$bg = 0;
									$en = 1;
								} elsif ($bg && $en == 0) {
									$restpage .= $pgline;
									$restpage .= "\n";
								}

							}
							$webpage = $restpage;
						}

						if (length $webpage > $Maxwebpage) {
							$webpage = substr ($webpage, 0, $Maxwebpage);
							$webpage .= "\n-truncated -\n\n";
						}


						if (-e "$ENV{HOME}/.pskmail/webpagetmp.gz") {
							`rm $ENV{HOME}/.pskmail/webpagetmp.gz`;
						}
						open (WEBPAGE, ">$ENV{HOME}/.pskmail/webpagetmp");
						print WEBPAGE $webpage;
						close WEBPAGE;
						`gzip -f -9 $ENV{HOME}/.pskmail/webpagetmp`;
						$size_file = `cat $ENV{HOME}/.pskmail/webpagetmp.gz`;
						my $b64file = encode_base64 ($size_file);

						my $server = $ServerCall;
						my $call = get_session();

						my $tempname = mktemp(XXXXXX);
						if ( -d "$ENV{HOME}/.pskmail/pending/$call") {
							;
						} else {
							mkdir "$ENV{HOME}/.pskmail/pending/$call";
						}


=head
						$ident = "~TGET64 " . sprintf("%d", length($b64file));

						`echo "$ident" >> $ENV{HOME}/.pskmail/TxInputfile`;

						`echo "$b64file" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
=cut
						if (getprotocol()) {
							my $filenm = " ";
							my $lqrb = length($b64file);
							my $ListBundle = ">FM:" . $server . ":" . $call . ":" . $tempname .
											":w:" . $filenm . ":" . $lqrb;

							`echo "$ListBundle" >> $ENV{HOME}/.pskmail/pending/Transactions`;

							`echo "$b64file" >> $ENV{HOME}/.pskmail/pending/$call/$tempname`;

							`echo "$ListBundle" >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "$b64file" >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
						} else {
							$ident = "~TGET64 " . sprintf("%d", length($b64file));
							`echo "$ident" >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "$b64file" >> $ENV{HOME}/.pskmail/TxInputfile`;
							`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						}
						addstat ("getweb");

					}

					my $dummy =`rm $ENV{HOME}/.pskmail/webpage*`;

				} else {
						`echo "Sorry, no web access from this server..."  >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~GETFREQ\s*(.*)/) {
				my $fcall = $1;
				$fcall .= "info";
				`lynx -dump http://pskmail.org/store/$fcall > $ENV{HOME}/.pskmail/frfile`;
				`gzip -f $ENV{HOME}/.pskmail/frfile`;
				$size_file = `cat $ENV{HOME}/.pskmail/frfile.gz`;
				my $b64file = encode_base64 ($size_file);

				if (getprotocol()) {
					my $server = $ServerCall;
					my $call = get_session();
					my $lqrb = length($b64file);
					my $filenm = $1 . "info";

					my $tempname = mktemp(XXXXXX);
					if ( -d "$ENV{HOME}/.pskmail/pending/$call") {
						;
					} else {
						mkdir "$ENV{HOME}/.pskmail/pending/$call";
					}

					my $ListBundle = ">FM:" . $server . ":" . $call . ":" . $tempname .
									":q:" . $filenm . ":" . $lqrb;

					`echo "$ListBundle" >> $ENV{HOME}/.pskmail/pending/Transactions`;

					`echo "$b64file" >> $ENV{HOME}/.pskmail/pending/$call/$tempname`;

					`echo "$ListBundle" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "$b64file" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;	
				}			
				
			} elsif (m/~GETPOS\s*(.*)/) {
####### get position from findu.com  #################################
				my $poscall = "";
				my $webpage = "";

				if (-e "$ENV{HOME}/.pskmail/.internet") {
					if (defined $1 && $1) {
						$poscall = $1;
					} else {
						$poscall = get_session();
					}
					my $URL = "http://hermes.esrac.ele.tue.nl/position.php?call=" . $poscall;

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 30;
						eval {
							@web =  `lynx -dump $URL` or die "No page";
							$webpage = join "", @web;
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = ""; }
				}
				if ($webpage) {
					`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
					`echo "Cannot get position\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~GETMSG\s*(\d*)/) {
####### get messages from findu.com  #################################
				`echo "Getting your messages" > $ENV{HOME}/.pskmail/TxInputfile`;

				my $msgnr = "";

				if (-e "$ENV{HOME}/.pskmail/.internet"){
					if ( $1 && $1 > 0) {
						$msgnr = $1;
					} else {
						$msgnr = 10;
					}
					my $call = get_session();
					my $to_me = 0;
					my $webpage = "";

					@msgs = `lynx -dump  http://www.db0anf.de/app/aprs/stations/messages-$call`;
					$printflg = 0;



					my $j = $msgnr;

					foreach $line (@msgs) {
						if ($line =~ /APRS Messages:/) {
							$printflg = 1;
						} elsif ($line =~ /Total number of message records for/) {
							$printflg = 0;
						} elsif ($printflg) {
							if ($line =~ /\[\d+\](\S+)/) {
								if ($1 ne $call) {
									$to_me = 1;
								} else {
									$to_me = 0;
								}
							}
							if ($to_me && $line =~ /\[\d+\](\S+)\s*\[\d+\](\S+)\s+\d\d(\d\d)-(\d\d)-(\d\d\s\d\d:\d\d):\d\d(\s.*)/) {
								my $fr = $1 . "      ";
								$fr = substr ($fr, 0, 10);
					print "CHR|" . substr($6, 1, 2) . "|\n";
								if (substr ($6, 1, 2) ne ":") {
#									print $fr . $3 . $4 . $5 . $6 ."\n";

									$webpage .= $fr . $3 . $4 . $5 . $6 ."\n";
								}

							} else {
								if ($to_me) {
#									print substr ($line, 22);
									$webpage = substr ($webpage, 0, length($webpage) -1);
									if ($line =~ /\s*(.*)/) {
										$line = " " . $1 . "\n";
									}
									$webpage .= $line;
								}
							}
						}

					}
#					print "WEBPAGE:\n" . $webpage;

					my @pg = ();
					my @pg2 = ();
					@pg = split "\n", $webpage;
					@pg2 = reverse @pg;

					$webpage = "";
					my $k = 0;

					for ($k = 0; $k < $msgnr; $k++) {
						my $ln = pop @pg;
						if ($ln) {
							$webpage .= $ln ;
							$webpage .= "\n";
						}
					}
#					print "WEBPAGE2:\n" . $webpage;

					`echo "From       Date  Time  Message " > $ENV{HOME}/.pskmail/TxInputfile `;
					`echo "$webpage" >> $ENV{HOME}/.pskmail/TxInputfile`;


				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETNEAR/) {
####### get near stations from findu.com  #################################
					`echo "Getting aprs stations" > $ENV{HOME}/.pskmail/TxInputfile`;

				if (-e "$ENV{HOME}/.pskmail/.internet"){
					my $URL = "http://www.findu.com/cgi-bin/near.cgi?call=" . get_session();
					my $webpage = "";

					@all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 30;
						eval {
							@all =  `lynx -dump $URL 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						`echo "APRS Stations near you \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;



						foreach $line (@all) {
							my $icon = 0;
print $line;
							if ($line =~ /Call callbook msg/) {
									$meat = 1;
							} elsif ($line =~ /Overall rate:/) {
									$meat= 0;
							} elsif ($line =~ /\s+\[\d+\]\[([PS]\d+)\.GIF\]\s+(.*)/) {
								$position = $1 . "," . $2 ;
							} elsif ($line =~ /(\-*\d+\.\d+)\s(\-*\d+\.\d+)\s\d+\.\d+\s\w*\s*00:(\d+)(:\d+:\d+)/) {

								if ($3 < 1) {
									$position = $position . "," . $1 . "," . $2  ."\n";
									$webpage .= $position;
								} else {
									$position = "";
								}
							}


						}


						my $weblength = length ($webpage);
						`echo "Your wwwpage: $weblength" >> $ENV{HOME}/.pskmail/TxInputfile`;

						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

						if ($monitor) {
							print $webpage;
						}
					}
					addstat ("getweb");
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}


			} elsif (m/~GETTIDE\s*(\d+)/) {
####### get tideinfo from findu.com  #################################
				`echo "Getting the tide data" > $ENV{HOME}/.pskmail/TxInputfile`;


				my $msgnr = "";

				if (-e "$ENV{HOME}/.pskmail/.internet"){
					if ( $1 && $1 > 0) {
						$msgnr = $1;
					} else {
						$msgnr = 1229;
					}

					my $URL = "http://www.findu.com/cgi-bin/tide.cgi?tide=" . $msgnr;
					my $webpage = "";
	 				my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 30;
						eval {
							@all =  `lynx -dump $URL 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						my $count = 11;
						my $webpage = "";
						foreach $line (@all) {
							if ($line =~ /Tide at/ || $line =~ /\d+\-\d+\-\d+\s/) {
								$webpage .= $line;
								$count--;
								if ($count == 0) {
									last;
								}
							}
						}


						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}

			} elsif (m/~GETTIDESTN\s*(\d*)/) {
####### get tidestations from findu.com  #################################
					`echo "Getting list of tide references" > $ENV{HOME}/.pskmail/TxInputfile`;

				if (-e "$ENV{HOME}/.pskmail/.internet"){
					my $URL = "http://www.findu.com/cgi-bin/tidestation.cgi?call=" . get_session();
					my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 30;
						eval {
							@all =  `lynx -dump $URL 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "";
						foreach $line (@all) {
							if ($line =~ /(\d+\.\d+)\s(\d+\s)\[\d+\](.*)\s\d+\.\d+\s\d+\.\d+$/) {
								$webpage .=  $2 . $3 . ", " . $1 . "\n";
							}
						}


						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETSERVERS\s*(\d*)/) {
####### get server freqs from wiki  #################################
					`echo "Getting list from the web" > $ENV{HOME}/.pskmail/TxInputfile`;

				if (-e "$ENV{HOME}/.pskmail/.internet"){
					my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 20;
						eval {
							@all =  `elinks -dump -no-references -no-numbering http://pskmail.wikispaces.com/PSKmailservers | grep Active` or die "No page";
						};
					};
					alarm 0;
					$webpage = "";
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "";
						foreach $line (@all) {
							if ($line =~ /(\s*[A-Z0-9\-]+)\s+.*Active(.*)/ && length($line) < 90) {
								chomp $line;
								$line .= "\n";
								my $cl = substr($line, 0, 9);
								my $fr = substr($line, 27, 10);
								my $md = substr ($line, 78);
								$webpage .= $cl . "   " . $fr . "   " . $md ;
							} elsif ($line =~ /(\S+)\s+(\d+\.\d*)\s*(\d+\.\d*)\s*(\d+\.\d*)\s*(\d+\.\d*)\s*(\d+\.\d*)/) {
								$webpage .= $1 . " " . $2  . " " . $3 . " " . $4 . " " . $5 . " " . $6 . "\n";
							}
						}


						my $weblength = length ($webpage);
						`echo "Your wwwpage: $weblength" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETNEWS\s*(\d*)/) {
####### get server freqs from wiki  #################################
					`echo "Getting latest news from the pskmail wiki" > $ENV{HOME}/.pskmail/TxInputfile`;

				if (-e "$ENV{HOME}/.pskmail/.internet"){
					my @all = ();

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 20;
						eval {
							@all =  `elinks -dump -no-references -no-numbering http://pskmail.wikispaces.com/NEWS` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "Pskmail News \n\n";
						foreach $line (@all) {
							if ($line =~ /notify me/) {
								$valid = 1;
							} elsif ($line =~ /--------/) {
								$valid = 0;
							} elsif ($valid && $line !~ /Edit This Page/) {
								$webpage .= $line;
							}
						}
						$webpage .= "\n-end-\n";

						my $weblength = length ($webpage);
						`echo "Your wwwpage: $weblength" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
			} elsif (m/~GETWWV/) {
####### get WWV data from NOAA  #################################

			my $wwvout;

				`echo "QRX..., getting your info! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				`wget -O $ENV{HOME}/.pskmail/pskdownload/wwv.txt http://www.swpc.noaa.gov/ftpdir/latest/wwv.txt`;

				my @wwv = `cat $ENV{HOME}/.pskmail/pskdownload/wwv.txt`;

				for ($i = 7; $i < 9; $i++) {
					$wwvout .= $wwv[$i];
				}
				`echo "$wwvout \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

			} elsif (m/~GETCAMP (\d+\.\d+)\s*(\d+\.\d+)/) {
####### get camp sites from findu.com  #################################
`echo "Sorry, this info is not available anymore from the DARC.de website!! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

=head
				if (-e "$ENV{HOME}/.pskmail/.internet"){
					my @all = ();

					`echo "QRX..., getting your info! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

					eval {
						local $SIG{ALRM} = sub { die "timeout" };
						alarm 30;
						eval {
							@all =  `lynx -dump "http://www.darc.de/echolink-bin/ba.pl?sel=latlondec\&latdegdec=$1\&londegdec=$2" 2>&1` or die "No page";
						};
					};
					alarm 0;
					if ($@ && $@ =~ /timeout/){
						$webpage = "Cannot connect\n"
					}
					if ($webpage =~ /^Alert!/m) { $webpage = "Cannot connect\n";}

					if ($@ && $@ =~ /No page/) {
						if ($monitor) {
							print "404\n";
						}
						`echo "Sorry, page not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					} else {
						$webpage = "";
						foreach $line (@all) {
							if ($line =~ /^Refer/) {
								last;
						} elsif ($line =~ /Google/) {
								last;
						} elsif ($line =~ /\[1\](.*)/) {
								$line = "\n" . $1 . "\n";
							} elsif ($line =~ /^\s*\[\d+\](\d+).(\d+).(\d+).\s(\w)\s(\d+).(\d+).(\d+).\s(\w)(.*)/) {
								$latval = $1 + ($2 + $3/100)/60;
								$lonval = $5 + ($6 + $7/100)/60;
								if ($4 ne "N") {
									$latval *= -1;
								}
								if ($8 ne "O") {
									$lonval *= -1;
								}
								$latprint = sprintf("\n%f, ",$latval);
								$lonprint = sprintf("%f", $lonval);
								$line = $latprint . $lonprint . $9 . "\n";
							} elsif ($line =~ /womo-sp/) {
								$line = "";
							} elsif ($line =~ /Campingplatz/) {
								$line = "";
							}
							$webpage .= $line;
							$count++;
							if ($count > 100) {
								$count = 0;
								last;
							}

						}


						my $weblength = length ($webpage);
						`echo "Your wwwpage: $weblength" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "$webpage \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

						if ($monitor) {
							print $webpage;
						}
					}
				} else {
					`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				}
=cut
			} elsif (m/~GETRELAYS (\-*\d+\.\d+)\s*(\-*\d+\.\d+)/) {
####### get VHF/UHF Relays from DARC.de  #################################

`echo "Sorry, this info is not available anymore from the DARC.de website!! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

=head
				`echo "QRX..., getting your info! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				`wget -O relais http://www.darc.de/echolink-bin/relais.pl?sel=latlondec\\&latdegdec=$1\\&londegdec=$2\\&printas=psk` ;

				`cat relais >> $ENV{HOME}/.pskmail/TxInputfile`;
				`echo "-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
=cut
			} elsif (m/~MSG (\S+)\s+(\S*\@\S*)\s(.*)/) {
####### send aprs mail #################################
					my $call = $1;
					getuserdata($call);
					$to = $2;
					my $aprsmsg = $3;
					print "APRS Email to:", $to, "\n";
					if ($to =~ /(.*)\@$ServerCall/) { # local mbox
						print "Local mbox $1\n";
						if (-e "$ENV{HOME}/.pskmail/localmail/$1") {
							print "Local mbox $1 exists\n";
						} else {
							`touch "$ENV{HOME}/.pskmail/localmail/$1"`;
							print "Local mbox $1 made\n";
						}
						my $mydate = `date`;
						my $msg =
							"From $call $mydate" .
							"To: $to" . "\n" .
							"From: $address" . "\n" .
							"Subject: " . "PSKaprs message from $call" . "\n" .
							"Date: $mydate" . "\n" .
							$aprsmsg . "\n\n";

						my @boxes = ("$ENV{HOME}/.pskmail/localmail/$1");
						my @delivered_to = Email::LocalDelivery->deliver($msg, @boxes);

						@message = ();
					} else {
						$subject = "PSKaprs message from $call";
						@message = ();

							if ($to =~ /\S+\@(\S+)\.\S+/) {
								if ($1 eq "tweetymail" ) {
									$subject = "";
								}									
								$apath = 'none';
								
								push @message, $to;
								push @message, $address;
								push @message, $subject;
								push @message, $aprsmsg;

								logprint ("sending mail message to $to\n");
								
								eval {
								if ($1 eq "tweetymail") {
									if ($address ne "none" && length ($address) > 0) {
										send_tweety (@message);	# send the mail via pskmail.org
										}
									} else {
										$apath = 'none';
										send_mail($apath,@message);	# send the mail via smtp
									}
								};
								if ($@) {
									logprint ("Could not send aprs email message:$@\n");
								}

						}
					}

				} elsif (m/~TWEET (.*)/) {
					`echo "Sending your update" > $ENV{HOME}/.pskmail/TxInputfile`;
					my $call = get_session();
					my $cmd = sprintf ("curl -u %s -d %cstatus=!pskmail %s:%s%c http://identi.ca/api/statuses/update.xml",  "pskmailposter:useCurl", 34, $call, $1, 34);
					my $result = `$cmd`;	
					addstat ("sendtweet");
				} elsif (m/~GETUPDATE/) {
####### get identi.ca updates #################################

				`echo "QRX..., getting your updates! \n\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

				@myxml = `curl -u pskmailposter:useCurl http://identi.ca/api/statuses/home_timeline.xml`;

				my $i = 0;
				foreach $line (@myxml) {
#					print "LINE:", $line;
					if ($line =~ /\<screen_name\>(.*)\<\/screen_name\>/) {

#						`echo "$1\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
						$i++;
						if ($i > 10) {
							last;
						}
					} elsif ($line =~ /\<text\>(.*)\<\/text\>/) {
						print $1, "\n";
						my $out = $1;
						$out =~ s/!pskmail//;
						$out .= "\n";
						`echo "$out" >> $ENV{HOME}/.pskmail/TxInputfile`;

					}

				}
				`echo "\n-end-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				addstat ("gettweet");

			} elsif (m/~POSITION (\d+\.\d+) (\d+\.\d+)/) {
####### send position to findu.com #################################
					`echo "Sending position to findu" > $ENV{HOME}/.pskmail/TxInputfile`;

				if (-e "$ENV{HOME}/.pskmail/.internet") {

					my $call = get_session();
					getuserdata($call);
					if ($findupasswd) {

						my $pos_string = "call=$call&passwd=$findupasswd&lat=$1&lon=$2\n";

						$to = "posit\@findu.com";
						$subject = "none";
						@message = ();
						push @message, $to;
						push @message, $address;
						push @message, $subject;
						push @message, $pos_string;

						eval {
							$apath = 'none';
							send_mail($apath,@message);	# send the mail via smtp
						};
						if ($@) {
							`echo "Cannot send position: $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
						 } else {
							`echo "\nPosition sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
							@message = ();
						}
					} else {
						`echo "Sorry, no password on file!\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
					}
				} else {
						`echo "Sorry, internet not available\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				}

			} elsif (m/~QUIT/) {
####### Quit connect  #################################
#					`echo ">Quit received" > $ENV{HOME}/.pskmail/TxInputfile`;

				logprint ("Disconnect received\n");
#print "DISCON:", get_streamid, "\n";
				disconnect();
				
				if (-e "$ENV{HOME}/.pskmail/.tx.lck") {
					`rm $ENV{HOME}/.pskmail/.tx.lck`;
				}

				$ConnectStatus = "Disconnected";

				open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
				print SESSIONDATA $nosession;
				close SESSIONDATA;
				
#				disconnect();

			} elsif (m/~DELETE(.*)/) {
####### delete message(s) from POP #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

				my @numbers = split / /, $1;
				eval {
					delete_mail(@numbers);
				};
				if ($@) {
					`echo "Cannot delete mail: $@" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
#					filter($mailuser, 0);
					`echo "Mail $1 deleted...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
				}
				}
			} elsif (m/~TELNET (\S+)\s(\d+)/) {
####### telnet agent #################################
print "Starting telnet agent\n";
				if ($1 && $2) {
				  	`echo "Starting telnet agent\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					telnetagent($1, $2, 0);
				} else {
					`echo "Invalid telnet address...\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				}
			} elsif (m/~PASSx(\d+)\,(.*)$/)  {

				if ($ok == 0) {
					`echo "sorry, wrong auth password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {
						$telnetpassw = make_secret ($1, $2);
				}

			} elsif (m/~NODE/) {
####### telnet agent #################################
print "Entering packet node\n\n";
				`echo "Entering packet node" > $ENV{HOME}/.pskmail/TxInputfile`;

				my $telnetmode = 1;

				telnetagent("db0fhn.efi.fh-nuernberg.de", 8023, $telnetmode);

			} elsif (m/~RECx(\d+)\,(.*)/) {
####### Update user database for CALL #################################

					`echo "\nReceiving record\n" > $ENV{HOME}/.pskmail/TxInputfile`;

				$recxtail = $2;
#				chomp $strt;

				while (length($recxtail) > 0) {
					my $in = <$reader>;
					chomp $in;
					$recxtail .= $in;
					if ($recxtail =~ /.*Q/) {
						$recxtail = substr($recxtail, 0, length($recxtail) -1);
						last;
					}
				}

				$r = make_secret ($1, $recxtail);

#				$strt = "";

				my $call = get_session();

				eval {
					my $rec = decode_base64($r);
					my @newvalues = split ("," , $rec);

					tie (%db, "DB_File", $dbfile)
						or die "Cannot open user database\n";

						my @values = ();
						my $record_valid = 0;

						if ($db{$call}) {
							@values = split /,/ ,$db{$call};
							$record_valid = 1;
						}

					my $record = (shift @newvalues) . ",";
					$record .= (shift @newvalues) . ",";
					$record .= (setp (shift @newvalues)) . ",";
					$record .= ("smtp_host,");
					$record .= (shift @newvalues) . ",";

					if ($record_valid) {
						$record .= "$ENV{HOME}/.pskmail/.mailuser,";
						$record .= $values[6] . ",";
					} else {
						$record .= "$ENV{HOME}/.pskmail/.mailuser,";
						$record .= "none";
					}


					$db{$call} = $record;


					untie %db;
				};
				if ($@) {
					print "Error in database: $@\n";
				} else {
					print "Updating database for $call\n";
					`echo "Updated database for $call" >> $ENV{HOME}/.pskmail/TxInputfile`;
					$ok = 1;
				}

			} elsif (m/~RECx(.*)$/) {
####### Update user database for CALL #################################
					`echo "Receiving record" > $ENV{HOME}/.pskmail/TxInputfile`;

				my $call = get_session();

				eval {
					my $rec = decode_base64($1);
					my @values = split (/,/ , $rec);

					$Pop_host = shift @values;
					my $record = $Pop_host . ",";
					$Pop_user = shift @values;
					$record .= ($Pop_user . ",");
					$Pop_pass = (setp (shift @values));
					$record .= ($Pop_pass . ",");
					$record .= ("smtp_host,");
					$address = shift @values;
					$record .= ($address . ",");
#					$mailuser = ".mailuser";
					$record .= ($mailuser . ",");
					$findupasswd = shift @values;
					chomp $findupasswd;
					$record .= ($findupasswd . ",");

					tie (%db, "DB_File", $dbfile)
						or die "Cannot open user database\n";

					$db{$call} = $record;

					untie %db;
				};
				if ($@) {
					print "Error in database: $@\n";
				} else {
					print "Updating database for $call\n";
					`echo "Updated database for $call" >> $ENV{HOME}/.pskmail/TxInputfile`;
					$ok = 1;
				}
			} elsif (m/~CSEND/ && $ok) {
####### receive compressed email message file #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

				`echo "Receiving email" > $ENV{HOME}/.pskmail/TxInputfile`;

				my $mailstatus = "receive";
				my $cmail = "";

				logprint ("Receiving compressed mail \n");

				@body = ();

				while ($mailstatus eq "receive") {
					$readline = <$reader>;

					if ($readline) {

						if (substr($readline, 0, 5) eq "-end-") {
							last;
						}

						reset_idle();

						if ($monitor) {
#							print $readline;
						}

						$cmail .= $readline;

					}

				}
#				print $cmail;

				$cmail = decode_base64 ($cmail);

				open (CMAIL, ">", "$ENV{HOME}/.pskmail/compressed.gz");
				print CMAIL $cmail;
				close CMAIL;

				$error = `gunzip $ENV{HOME}/.pskmail/compressed.gz`;

my $testvar = `cat $ENV{HOME}/.pskmail/compressed`;


				open (CMAIL, "<", "$ENV{HOME}/.pskmail/compressed");

				my @cmsg = ();
				@cmsg = <CMAIL>;
				$dummy = shift @cmsg;
				$dummy = shift @cmsg;
				my $address_override = "";

				foreach $readline (@cmsg){

					if ($readline) {

						reset_idle();
						chomp($readline);
						if ($monitor) {
#							print $readline, "\n";
						}

						if ($readline =~ /^\.$/) {
							$mailstatus = "end_of_mail";
							@message = ();

							my $call = get_session();
							getuserdata($call);
							if ($address_override) {
								$address = $address_override;
							}
							push @message, $to;
							push @message, $address;
							push @message, $subject;
							if ($to !~ /saildocs/){
								push @message, "PSKmail message from $address";
							}
							push @message, @body;

							if ($to =~ /(.*)\@$ServerCall/) { # local mbox

								`echo "Receiving local mail" > $ENV{HOME}/.pskmail/TxInputfile`;
								print "Local mbox $1\n";
								if (-e "$ENV{HOME}/.pskmail/localmail/$1") {
									print "Local mbox $1 exists\n";
								} else {
									`touch "$ENV{HOME}/.pskmail/localmail/$1"`;
									print "Local mbox $1 made\n";
									`echo "Local mailbox made" > $ENV{HOME}/.pskmail/TxInputfile`;
								}
								my $mydate = `date`;
								my $msg =
									"To: $to" . "\n" .
									"From: $call" . "\n" .
									"Subject: " . $subject . "\n" .
									"Date: $mydate" . "\n" .
									join ("\n", @body) . "\n\n";

								@delivered_to = ();
								@boxes = ("$ENV{HOME}/.pskmail/localmail/$1");
								@delivered_to = Email::LocalDelivery->deliver($msg, @boxes);

								@message = ();
								`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

								$mailstatus = "end_of_mail";
								addstat ("sendlocalmail");

							} else {						# internet mbox

								eval {
									if (length($attachment) == 0) {
										$apath = 'none';
										send_mail($apath,@message);	# send the mail via smtp
									} else {
										push @message, $attachment;
										send_MIME(@message);    # send MIME message via smtp
										$attachment = "";
									}
								};

								if ($@) {
									`echo "Error sending mail : $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
								} else {
									`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
								}
								@message = ();
								$mailstatus = "end_of_mail";
								addstat ("sendmail");
							}
						}
						elsif ($readline =~ /~ABORTSEND/) {	# in case we are stuck in a loop
							# stop.....
							$to = "";
							$subject  = "";
							$attachment = "";
							@body = ();
							last;
						}
						elsif ($readline =~ /Subject: /) {
							# add subject
							my $subjectline = substr($readline, 9);
							$subject = $subjectline ;
						}
						elsif ($readline =~ /To: /) {

							# write  To:

							$to = substr($readline, 4);

						} elsif (m/From: /) {

							# write  From:

							$address_override = substr($readline, 6);
						} elsif ($readline =~ /Your attachment: filename=/) {

							$attachment = substr($readline, 26);
#							print $attachment, "\n";
							push @body, $readline;

						} else {
							push @body, $readline;
						}
					}
					if ($mailstatus eq "end_of_mail") {
						last;
					}
					addstat ("sendmail");
				}	# end mail receive


				if (-e "$ENV{HOME}/.pskmail/compressed") {
					`rm $ENV{HOME}/.pskmail/compressed`;
				}

				print "End of mail receive...\n";

				close CMAIL;
				$mailstatus = "none";

				} # end ok...

			} elsif (m/~SEND/) {
####### send message file #################################

				if ($ok == 0) {
					`echo "sorry, wrong password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
				} else {

					`echo "Receiving email" > $ENV{HOME}/.pskmail/TxInputfile`;

				my @message = ();	# $to, $from, $subject, @body
				my $address_override = "";

				my $mailstatus = "receive";

				logprint ("Receiving mail \n");

				@body = ();

				while ($mailstatus eq "receive") {


					$readline = <$reader>;

					if ($readline) {

						reset_idle();
						chomp($readline);
						if ($monitor) {
							print $readline, "\n";
						}
						$_ = $readline;
						if (m/^\.$/) {
							$mailstatus = "end_of_mail";
							@message = ();

							my $call = get_session();


							getuserdata($call);
							if ($address_override) {
								$address = $address_override;
							}

							push @message, $to;
							push @message, $address;
							push @message, $subject;
							if ($to !~ /saildocs/){
								push @message, "PSKmail message from $address";
							}
							push @message, @body;
							
							print "TO:", $to, "\n";
							
							if ($to =~ /(\w+)\@\w+\.\w+/) { # internet mail

								eval {
									if (length($attachment) == 0) {
										$apath = 'none';
										send_mail($apath,@message);	# send the mail via smtp
									} else {
										push @message, $attachment;
										send_MIME(@message);    # send MIME message via smtp
										$attachment = "";
									}
								};

								if ($@) {
									`echo "Error sending mail : $@" >> $ENV{HOME}/.pskmail/TxInputfile`;
								} else {
									`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
									addstat ("sendmail");
								}
								@message = ();
								$mailstatus = "end_of_mail";
							} elsif ($to =~ /(.*)\@$ServerCall/) { # local mbox
								`echo "Receiving local mail" > $ENV{HOME}/.pskmail/TxInputfile`;
								print "Local mbox $1\n";
								if (-e "$ENV{HOME}/.pskmail/localmail/$1") {
									print "Local mbox $1 exists\n";
								} else {
									`touch "$ENV{HOME}/.pskmail/localmail/$1"`;
									print "Local mbox $1 made\n";
									`echo "Local mailbox made" > $ENV{HOME}/.pskmail/TxInputfile`;
								}
								my $mydate = `date`;
								my $msg =
									"From $call $mydate" .
									"To: $to" . "\n" .
									"From: $call" . "\n" .
									"Subject: " . $subject . "\n" .
									"Date: $mydate" . "\n" .
									join ("\n", @body) . "\n\n";

								my @boxes = ("$ENV{HOME}/.pskmail/localmail/$1");
								my @delivered_to = Email::LocalDelivery->deliver($msg, @boxes);

								@message = ();
								`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

								$mailstatus = "end_of_mail";
								addstat ("sendlocalmail");
							} else {
								# other server?
								
								if (checkserver($2)) {
									`echo "Receiving local mail" > $ENV{HOME}/.pskmail/TxInputfile`;
									print "mail from $1\n";

									my $mydate = `date`;
									my $msg =
										"From $call $mydate" .
										"To: $to" . "\n" .
										"From: $call" . "\n" .
										"Subject: " . $subject . "\n" .
										"Date: $mydate" . "\n" .
										join ("\n", @body) . "\n\n";
	
									#queue message for transfer
									print $msg, "\n";
	
									@message = ();
									`echo "\nMessage sent...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
	
									$mailstatus = "end_of_mail";
									addstat ("sendlocalmail");									
								} else {
									`echo "\nCould not find $2...\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
								}
							} 
						} elsif (m/~ABORTSEND/) {	# in case we are stuck in a loop
							# stop.....
							$to = "";
							$subject  = "";
							$attachment = "";
							@body = ();
							last;
						}
						elsif (m/Subject: /) {
							# add subject
							my $subjectline = substr($readline, 9);
							$subject = $subjectline ;
						}
						elsif (m/To: /) {

							# write  To:

							$to = substr($readline, 4);

						} elsif (m/From: /) {

							# write  From:

							$address_override = substr($readline, 6);
						} elsif (m/Your attachment: filename=/) {

							# write  To:

							$attachment = substr($readline, 26);
							print $attachment, "\n";
							push @body, $readline;

						} else {
							push @body, $readline;
						}
					}
				}	# end mail receive
				addstat ("sendmail");
			}

####### Command ~PROFILE ###########################
			} elsif (m/(~PROFILE.*)/) {
				# do nothing, it's ok.
####### Command ~ABORTSEND #########################
			} elsif (m/(~ABORTSEND.*)/) {
				# do nothing, it's ok.
####### Command unknown ###########################
			} elsif (m/(~\S*)/) {
				`echo "Huh?? I don't understand $1" >> $ENV{HOME}/.pskmail/TxInputfile`;
			}
			$readline = "";
			select undef, undef, undef, 0.1;
		} else {

####### scanner #################################

			if ($scanner) {
				
				scanner();	
			}
			
####### query #################################
		

####### beacon  ################################
			my $minu = (time / 60 ) % $period;		# What minute is this ?
			if ($minu>=0 && $minu <5){
				serverbeacons();
				send_freq_table();
			}

####### send aprs beacon #################################

			if ($Aprs_connect == 1) {
				if (time() - $systime >= 60 * $posit_time) {			## 10 mins aprs beacon...
					$systime = time();
					# now do what we want....

					my ($month, $hour, $min) = (gmtime) [3, 2, 1];
					$month = substr (("0" . $month), -2, 2);
					$hour = substr (("0" . $hour), -2, 2);
					$min = substr (("0" . $min), -2, 2);

					my $mytime = $month . $hour . $min . "z";
					if (-e "$ENV{HOME}/.pskmail/aprs_wx.txt") {

						my $WX = `cat "$ENV{HOME}/.pskmail/aprs_wx.txt"`;

						if (index ($WX, "_") == 0) { $WX = substr($WX, 1);}

						$MSG = "$ServerCall" . ">PSKAPR:" . "@" . $mytime . $Aprs_beacon . $WX ."\n";
					} else {
						$MSG = "$ServerCall" . ">PSKAPR:" . "@" . $mytime . $Aprs_beacon ."\n";
					}

					aprs_send ($MSG);

					# end
				}
			}
		}	# end if 'read'

	}	# end while loop

	close $reader;
	waitpid($pid, 0);
} else {
	die "Cannot fork: $!" unless defined $pid;
	close $reader;

	pskserver($ServerCall, "$Inputfile", "$output", @scancls);

	close $writer;

	$error = `killall rflinkserver.pl`; # kill all children still running.....

	exit;
}
$error = `killall rflinkserver.pl`; # kill all children still running.....

exit (1);



########################################################
sub pskserver {	#		main, server
########################################################

my ($ServerCall, $Inputfile, $output, @scancls) = @_;
$outputfile = substr ($output, 1);

my $STAT = "";
my $teststatus = "Listening";
my $startflag = 0;
my $mailcount = 0;

#DCD handler
my $DCDactive = 0;
my $DCDnew = 0;

#Asymmetric link
my $havedowngradedtx = 1;

#Adaptative timing
my $currentidle = 0;
my $havesoh = 0;


`cp zb "$Inputfile"`;
`cp zb "$outputfile"`;

		$ConnectStatus = "Listening";

while (1) {
	logprint ("\nListening to the radio\n");
#print "3436", get_rxstatus(),"\n";
	sendmodemcommand ("<rsid>ON</rsid>");

	$mailcount = 0;

	if ($startflag) {
		if (get_session() eq $nosession) {
			set_txstatus("TXDisconnect");
			send_frame();
			$startflag = 0;
#print "3445\n";
		}
	} else {
		$startflag = 1;
	}

	open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
	print SESSIONDATA "none";
	close SESSIONDATA;

	reset_arq();

		$ConnectStatus = "Listening";

	if ($ConnectStatus eq "Listening") {

		my $isloaded = 0;
		
		until (get_rxstatus() eq "Connect_req" || get_rxstatus() eq "Transfer_connect" ) {

			my $trf = get_rxstatus();
#print "3467",$trf, "\n";

			if ($trf eq "Transfer_connect" && -e "$ENV{HOME}/.pskmail/filetransfer") {
				print "TRAFFIC:", $traffic, "\n";
				set_rxstatus("Transfer_req");
				
			}

## DCD handler, resets DCD after 1 second...

			my $DCDnow = gettimeofday();
	
			if (-e "$ENV{HOME}/.pskmail/.DCD") {
				if ($DCDactive == 0) {
					$DCDnew = $DCDnow;
					$DCDactive = 1;
				} else {
					if ($DCDnow - $DCDnew > 1) {
						unlink "$ENV{HOME}/.pskmail/.DCD";
						$DCDactive = 0;
					}
				}
			}


## here call change for scanner

			if ($scancls[0]) {
				$mins = time() / 60 % 5;
				$ServerCall = $scancls[$mins];
				chomp $ServerCall;
			}

			if (get_session() eq $nosession) {
				if (get_rxqueue()) {
					my $mystring = get_rxqueue();
					# write received text to handler
					printf $writer ("%s", $mystring);
					reset_rxqueue();
				}
			}
			eval {
				local $SIG{ALRM} = sub { die "alarm!!" };
				alarm 30;
				eval {
					listening($select);
				};
				alarm 0;
			};
			alarm 0;
			die if $@ && $@ !~ /alarm!!/;

			inc_idle();
			if (get_rxstatus() eq "Poll_rx") {
				set_txstatus("TXAbortreq");
				send_frame();
			}
		}
		
		if (get_rxstatus() eq "Transfer_connect") {
			my $sessiontimer = time();
			while (1) {
					eval {
						local $SIG{ALRM} = sub { die "Alarm!!" };
						alarm 30;
						eval {
							listening($select);
						};
						alarm 0;
					};
					alarm 0;
					die if $@ && $@ !~ /Alarm!!/;

## see if we received a status
					$RxStatus = get_rxstatus();

					if ($RxStatus eq "Status_rx"){
print "3652:", $RxStatus, "\n";
						sendmodemcommand ("<rsid>ON</rsid>");
						Time::HiRes::sleep(0.2);

						settxrsid(1);

						print "OK, Status received", "\n";
						$ConnectStatus = "Connected";
###########################
					my @trafficwaiting = `ls -l ~/.pskmail/transfer`;
					
					if (@trafficwaiting > 1 ) {
						print "TRAFFIC\n";
						my $nroffiles = @trafficwaiting;
						my $peer;
						if ($trafficwaiting[1] =~ /.*\d+:\d+\s(.*)/) {
							$traffic = "$ENV{HOME}/.pskmail/transfer/" . $1;
							my $peer = "";
							$transferheaderline = `cat $traffic | head -n 1 `;
							print $transferheaderline, "\n";
							if ($transferheaderline =~ />FM:([A-Z0-9\-]+):([A-Z0-9\-]+):.*/) {
								$peer = $2;
							}
	
							sleep (10);
							
							$Transfer = 1;
							settxrsid(1);
							setrxrsid(1);

							`echo $traffic >> $ENV{HOME}/.pskmail/filetransfer`;
						}
					}
						
				}
###########################						
						
						
						while ($ConnectStatus eq "Connected"){
							eval {
								local $SIG{ALRM} = sub { die "Alarm!!" };
								alarm 30;
								eval {
									listening($select);
								};
								alarm 0;
							};
							alarm 0;
							die if $@ && $@ !~ /Alarm!!/;
							
							$RxStatus = get_rxstatus();
print "3594", $RxStatus, "\n";			
							$txqlen = gettxtqueue();
			
							if ($RxStatus eq "Abort") {
								$ConnectStatus = "Listening";
								
								last;
							} elsif ($RxStatus eq "Data"){	
								### here session timer
								$sessiontimer = time();
		print "Setting time:", $sessiontimer, "\n";
							} elsif ($RxStatus eq "Status_rx"){
			
								if ($txqlen) {
									$RxReady = 1;
									$Retries = 0;
									#Reset flag so that next downgrade starts with RX mode again
									$havedowngradedtx = 1;
								} else {
									$RxReady = 1;
									$Retries++;
									set_rxstatus ("EOT");
			
								}
							} elsif ($RxStatus eq "Poll_rx"){
									$RxReady = 0;
									$Retries++;
									set_txstatus ("TXStat");
									send_frame();
							} elsif ($RxStatus eq "Connect_req"){
								$Reconnect_possible++;
								$Retries = 0;
								if ($Reconnect_possible >= 1) {
									$Reconnect_possible = 0;
									print "Reconnect received!!\n";
			
									$ConnectStatus = "Listening";
									last;
								}
							} elsif ($RxStatus eq "Disconnect_req"){
								set_txstatus("TXDisconnect");
								send_frame();
								my $discall = get_session();
								logprint("Disconnected\n");								
								disconnect();

								reset_rxstatus;
								$ConnectStatus = "Listening";
								$Retries = $Max_retries;
	print "Bailing out....\n";							
								reset_rxstatus();
								last;
							}
							my $timecheck = time();
							my $diff = $timecheck - $sessiontimer;
			print "TIMEDIFF:", $diff, "\n";
							if ($timecheck - $sessiontimer > 300) {
								print "Bailing out...\n";
								reset_rxstatus();
								last;
							}
							if ($timecheck - $sessiontimer > 30 && -e "$ENV{HOME}/.pskmail/.endtransfer") {
								`rm $ENV{HOME}/.pskmail/.endtransfer`;
								reset_rxstatus();
								last;
							}
							
			
							my $mystring = get_rxqueue();
							printf $writer ("%s", $mystring);
							if ($mystring) {
								$Retries = 0;
							}
							reset_rxqueue();
			
							inc_idle();
			
							my $session_status = get_session();
							if ($session_status eq "none" || $session_status eq "beacon") {
								reset_rxstatus();
								last;	# outta here...
							}
							if ($RxReady == 1 ) {
;					
								if (-e "$ENV{HOME}/.pskmail/.abort_trans") {
									$txqlen = 0;
									`rm $ENV{HOME}/.pskmail/.abort_trans`;
									set_txstatus("RESET_TXQueue");
									send_frame("");
								} else {
									$RxReady = 0;
									if ($txqlen) {
										$Retries = 0;
									}
									$outputstring = gettxinput();
									set_txstatus("TXTraffic");
									send_frame($outputstring);
									if ($outputstring) {
										$Retries = 0;
									}
									$outputstring = "";
								}
							}
														
						}

						last;
					}
					sleep(10);	#check every  10 seconds
				}
				
			

#		}
				
		if (get_rxstatus() eq "Connect_req") {
			

			$ConnectStatus = "Connecting";

##Set session to "connecting" to stop the scanner to change frequency and mode while completing the connection
			open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
			print SESSIONDATA "connecting";
			close SESSIONDATA;



			my $acks_sent = 5;
## start a loop for sending acks...
			while ($acks_sent > 0) {

				# switch txID on

				sendmodemcommand ("<txrsid>ON</txrsid>");

				# degrade modem if no status received after 2 tries
				if ($acks_sent < 4  && gettxmodenumber() ne 0) {
					#TX downgrade turn (always if in symmetric mode)

						adjusttxmodem(-1);	#Downgrade by one step
						#Remember we just downgraded the TX mode
						$havedowngradedtx = 1;

				}

				# send connect_ack
				set_txstatus("TXConnect_ack");
				send_frame();

				reset_arq();


				if (getrxmodem() eq "PSK1000") {
					$Maxidle = get_maxidle(16, 5, 7);
					$Shortidle = get_maxidle(3, 5, 7);				
				} elsif (getrxmodem() eq "PSK500") {
					$Maxidle = get_maxidle(32, 5, 7);
					$Shortidle = get_maxidle(3, 5, 7);
				} elsif (getrxmodem() eq "PSK250RC3") {
					$Maxidle = get_maxidle(40, 5, 9);
					$Shortidle = get_maxidle(6, 5, 9);
				} elsif (getrxmodem() eq "PSK63RC10") {
					$Maxidle = get_maxidle(20, 5, 9);
					$Shortidle = get_maxidle(6, 5, 9);
				} elsif (getrxmodem() eq "PSK125RC4") {
					$Maxidle = get_maxidle(20, 5, 9);
					$Shortidle = get_maxidle(6, 5, 9);
				} elsif (getrxmodem() eq "PSK63RC5") {
					$Maxidle = get_maxidle(10, 5, 7);
					$Shortidle = get_maxidle(6, 5, 7);
				} elsif (getrxmodem() eq "THOR22") {
					$Maxidle = get_maxidle(64, 10, 7);
					$Shortidle = get_maxidle(6, 10, 7);
				} elsif (getrxmodem() eq "THOR8") {
					$Maxidle = get_maxidle(64, 10, 7);
					$Shortidle = get_maxidle(3, 5, 7);
				} elsif (getrxmodem() eq "THOR4") {
					$Maxidle = get_maxidle(64, 10, 7);
					$Shortidle = get_maxidle(6, 10, 7);
				} elsif (getrxmodem() eq "CTSTIA") {
					$Maxidle = get_maxidle(64, 10, 7);
					$Shortidle = get_maxidle(6, 10, 7);
				} elsif (getrxmodem() eq "DOMINOEX22") {
					$Maxidle = get_maxidle(64, 10, 4);
					$Shortidle = get_maxidle(6, 2, 4);
				} elsif (getrxmodem() eq "DOMINOEX11") {
					$Maxidle = get_maxidle(64, 10, 4);
					$Shortidle = get_maxidle(6, 2, 4);				
				} else {
					$Maxidle = get_maxidle(32, 2.5, 7);
					$Shortidle = get_maxidle(3, 2.5, 7);
				}
if ($Maxidle > 57 && getrxmodem() eq "DOMINOEX22")	{
	$Maxidle = 15;
	$Shortidle = 8;
}			
print "MAXIDLE=" . $Maxidle . "\n";



#Avoid long delays at connect time (check during timeout, not at the end				sleep int ($Maxidle * 2) + 4;
				$Maxidle += 0;
###shoud not need that extra time now
###				my $connecttimeout = ($Maxidle * 2) + 4;
## we do, PSK500 connect is not possible otherwise...
				my $connecttimeout = $Maxidle + 24;
				my $connecttimecounter = 0;
## listen for a status frame
				sleep(1); #check every second
###may need this later	 ((($currentidle > $Maxidle) && ($havesoh != 0)) || (($currentidle > $Shortidle) && ($havesoh == 0))) {
				while ($connecttimecounter <= $connecttimeout) {
					$connecttimecounter += 1;
					eval {
						local $SIG{ALRM} = sub { die "Alarm!!" };
						alarm 30;
						eval {
							listening($select);
						};
						alarm 0;
					};
					alarm 0;
					die if $@ && $@ !~ /Alarm!!/;

## see if we received a status
					$RxStatus = get_rxstatus();

					if ($RxStatus eq "Status_rx"){

						sendmodemcommand ("<rsid>OFF</rsid>");
						Time::HiRes::sleep(0.2);

						settxrsid(0);

						print "OK, Status received", "\n";
						$ConnectStatus = "Connected";
						$acks_sent = 5;

						last;
					}
					sleep(1);	#check every second
				}


				if ($ConnectStatus eq "Connected") {
					last;
				} else {
					$acks_sent--;
					print $acks_sent, " acks left...\n";
					if ($acks_sent < 1) {
						print "Aborting connect\n";
						$ConnectStatus = "Listening";
						open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
						print SESSIONDATA "connecting";
						close SESSIONDATA;
						reset_rxstatus();
						$RxStatus = "Disconnect_req";
					}
				}
			}


			my $Reconnect_possible = 0;

			if (-e "$ENV{HOME}/.pskmail/telnetactive") {	#/
				unlink "$ENV{HOME}/.pskmail/telnetactive";
			}

## we are now connected ?

			$is_session = `cat $ENV{HOME}/.pskmail/PSKmailsession`;

			if ($is_session eq "Listening") {
				print "Station not heard, listening to the radio...\n";
				$ConnectStatus = "Listening";
				next;
			}

					print $writer "~ABORTSEND\n";

					$call=get_call();

					settxrsid(1);

					if (index ($Usercalls, $call) < 0) {
						if (getuserdata ($call) ne "Unknown") {
							$Usercalls .= $call;	# add it to the list of users
							$Usercalls .= " ";
						}elsif ($opensystem) {
							$Usercalls .= $call;	# add it to the list of users anyway
							$Usercalls .= " ";
							$Pop_host = "";
							print "added call to list of known calls\n";
							$usrmessage = "pse update your record!\n";
						}
					}

					$_ = $Usercalls;

					my @callfrags = split ("\/", $call);
					foreach my $frag(@callfrags) {
						if (m/$frag/) { 	# from Usercalls list
							if (length($frag) > 3) {
								$call = $frag;
								logprint ("Call $call o.k.\n");
								open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
								print SESSIONDATA $call;  # put it away safely
								close SESSIONDATA;
								sleep(2);

								my $mailcall = get_session();
								$Pop_host = "";
								$Pop_user = "";
								$Pop_pass = "";
								$address = "";
								$findupasswd = "";

								getuserdata($mailcall);
								logprint("Connected to $mailcall\n");
							

								my $mailrec = 1;
								my $pssw = 1;
								if (length($Pop_user) < 3 ) {
									print "No mail record defined\n";
									$mailrec = 0;
								} else {
									`touch "$ENV{HOME}/.pskmail/.auth_ok"`;
								}
								if (length($findupasswd) < 3) {
									print "No session password defined\n";
									$pssw = 0;									
								}								

								$sstate = "";

								if (-e "$ENV{HOME}/.pskmail/.internet") {
									$sstate = "I";
								}

								if (-e "$ENV{HOME}/.pskmail/localmail/$mailcall") {	# see if there is local mail
									my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$call");
									my $mails = "";
									my $count = 0;

									for my $msg ($folder->messages) {
										$count++;
									}
									if ($count) {
										$sstate .= "L$count";
									}
								}

								if ($Pop_host && -e "$ENV{HOME}/.pskmail/.internet" && $Emails_enabled) {	# check if interner mail
									my $count = 0;
									$usrmessage = "";
									if ($mailrec == 0) {
										$usrmessage = "Missing user email settings, please upload!\n";
									}
									if ($pssw == 0) {
										$usrmessage .= "No password in database\n";
									}
									eval {
										local $SIG{ALRM} = sub { die "alarm"};
										alarm 20;
										eval {
											$mailcount = count_mail() ; # get mail from pop server
											print "Mail count =", $mailcount, "\n";
										};
										alarm 0;
									};

									alarm 0;
									if ($@ && $@ =~ /alarm/) {
										$usrmessage .= " Timeout reading the mail...\n";
									} else {											
											if ($mailcount > 1) {
												$usrmessage .= ($mailcount . " mails.\n");
											} elsif ($mailcount ==1) {
												$usrmessage .= ($mailcount . " email.\n");
											} elsif ($mailcount ==0) {
												$usrmessage .= ("No email.\n");
											
											} elsif ($count < 1) {
												if ($mailrec == 1){
													$usrmessage .= ("Failed to get your email, pse upload correct email settings.\n");
												} 
											} else {
												$usrmessage .= ("Could not list mail.\n");
											}
											if (-e "$ENV{HOME}/.pskmail/mailheaders") {unlink "$ENV{HOME}/.pskmail/mailheaders";}

									}
								}



											($seconds, $minutes, $hours, @rest) = gmtime();
											`echo $seconds > $ENV{HOME}/.pskmail/.connectsecond`;
											@rest = ();
											if (length($seconds < 2)) { $seconds = "0" . $seconds};
											if (length($minutes < 2)) { $minutes = "0" . $minutes};
											if (length($hours < 2)) { $hours = "0" . $hours};

											$key = getkeys();

#											if ($Transfer) {
#												$sstate .= "T" . $key;
#											} else {
												$sstate .= "M" . $key;
#											}
											
											$motd = "This text gets printed at the client terminal" . ".\n";
											`echo $motd >> $ENV{HOME}/.pskmail/TxInputfile`;

											`echo "\n$ServerCall $Version-$hours:$minutes:$seconds-$sstate>\n$usrmessage\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
											$sstate = "";

										# check if we have messages in the queue
										#
											if (-e "$ENV{HOME}/.pskmail/pending/Transactions") {
												my $TRSfile = `cat $ENV{HOME}/.pskmail/pending/Transactions`;

												my @TRS = split '\n', $TRSfile;
												my $call = get_session();
												foreach my $trline (@TRS) {

													if ($trline =~ /\>FM:([A-Z0-9\-]+):([A-Z0-9\-]+):(.*)/) {
														my $cmpcall = $2;
														my $rest = $3;
														if ($call eq $cmpcall) {
															`echo ">FO5:$ServerCall:$call:$rest" >> $ENV{HOME}/.pskmail/TxInputfile`;
														}

													}
												}
											}

											$STAT = getuserdata($mailcall);
											$STAT = "" unless $Pop_host;
											last;
										}
									} else {
										`echo "Sorry, $call not registered...\n" > $ENV{HOME}/.pskmail/TxInputfile`;
										sleep 10;
									}
								}


					$RxReady = 1;

					$Retries = 0;

			while ($ConnectStatus eq "Connected") {	# Connected loop...
#print "CONNECTEDloop\n";

#Adaptative timing: get maxidle from subroutine. Start with size of status frame first
#				open MAXIDLEDATA, "$ENV{HOME}/.pskmail/.maxidle";
#				$Maxidle = <MAXIDLEDATA>;
#				close MAXIDLEDATA;

				if (getrxmodem() eq "PSK500") {
					$Maxidle = get_maxidle(32, 5, 7);
					$Shortidle = get_maxidle(7, 5, 7); #assume 4 missing blocks avg
				} else {
					$Maxidle = get_maxidle(32, 2.5, 7);
					$Shortidle = get_maxidle(7, 2.5, 7); #assume 4 missing blocks avg
				}
				#Have we received an soh. If not just wait for a status frame.
				$havesoh = 0;
				if (-e "$ENV{HOME}/.pskmail/.soh") {
					$havesoh = -1;
				}
				$currentidle = get_idle();
#print "CURRENTIDLE:", $currentidle, "\n";
				until ((($currentidle > $Maxidle) && ($havesoh != 0)) || (($currentidle > $Shortidle) && ($havesoh == 0))) {

					
					
					if (-e "$ENV{HOME}/.pskmail/.txing") {
						sleep 1;
#print "TX-ON\n";						
					} else {
#print "IDLE++","\n";						
						inc_idle();	# update idle counter
						$txtimer = 0;

					}

					reset_rxstatus();

					eval {
						local $SIG{ALRM} = sub { die "Alarm!!" };
						alarm 60;
						eval {
							listening($select);
						};
						alarm 0;
					};
					alarm 0;
					die if $@ && $@ !~ /Alarm!!/;

					$RxStatus = get_rxstatus();

					if (check_lastblock()) {
						last;
					}

					#Have we received an soh? If not just wait for a status frame.
					$havesoh = 0;
					if (-e "$ENV{HOME}/.pskmail/.soh") {
						$havesoh = -1;
					}
					$currentidle = get_idle();
				}
				$RxStatus = get_rxstatus();

				$txqlen = gettxtqueue();

				if ($RxStatus eq "Abort") {
					$ConnectStatus = "Listening";
					last;
				} elsif ($RxStatus eq "Status_rx"){

					if ($txqlen) {
						$RxReady = 1;
						$Retries = 0;
						#Reset flag so that next downgrade starts with RX mode again
						$havedowngradedtx = 1;
					} else {
						$RxReady = 1;
						$Retries++;

					}
				} elsif ($RxStatus eq "Poll_rx"){
						$RxReady = 0;
						$Retries++;
						set_txstatus ("TXStat");
						send_frame();
				} elsif ($RxStatus eq "Connect_req"){
					$Reconnect_possible++;
					$Retries = 0;
					if ($Reconnect_possible >= 1) {
						$Reconnect_possible = 0;
						print "Reconnect received!!\n";

						$ConnectStatus = "Listening";
						last;
					}
				} elsif ($RxStatus eq "Disconnect_req"){
					disconnect();
					set_txstatus("TXDisconnect");
					send_frame();
logprint("Disconnected from $call");
					reset_rxstatus;
					$ConnectStatus = "Listening";
					$Retries = $Max_retries;
				}

				my $mystring = get_rxqueue();
				printf $writer ("%s", $mystring);
				if ($mystring) {
					$Retries = 0;
				}
				reset_rxqueue();

				inc_idle();

				my $session_status = get_session();
				if ($session_status eq "none" || $session_status eq "beacon") {
					last;	# outta here...
				}
#print "CURRENT:", $currentidle, "\n";
				if ((($currentidle > $Maxidle) && ($havesoh != 0)) || (($currentidle > $Shortidle) && ($havesoh == 0))) {	# send poll frame
					reset_idle();
					if (-e "$ENV{HOME}/.pskmail/.stat") {
						$prediction = `cat $ENV{HOME}/.pskmail/.stat`;
						chomp $prediction;
						$oldprediction = $prediction;
						print "Prediction:" . $prediction . "|\n";
						my $ckinfo = "0" . $prediction;
						my $chks = checksum($ckinfo);
						my $substitute = "<SOH>0" . $prediction . $chks . "<EOT>";
						open (MOUT, ">" , "$ENV{HOME}/.pskmail/.input");
						print MOUT $substitute;
						close MOUT;
						$substitute = "";
						$prediction = "";
						`rm $ENV{HOME}/.pskmail/.stat`;
					} else {
						# set RSID TX ON until a status block is received.
						settxrsid(1);
						setrxrsid(1);
						sendmodemcommand ("<rsid>ON</rsid>");
						sendmodemcommand ("<txrsid>ON</txrsid>");
print "$Retries Sending poll with havesoh: $havesoh, maxidle: $Maxidle, Shortidle: $Shortidle, currentidle: $currentidle \n";

						reset_idle();

						if (gettxmodenumber() == 1 && $Retries < $Max_retries -2) {
							$Retries = $Max_retries - 2; # two more retries for THOR8
						}

						$Retries++;

						#Only adjust TX mode if it is in the list
						if ($Retries >= 2  && gettxmodenumber() ne 0) {
							#TX downgrade turn (always if in symmetric mode)
							if ($havedowngradedtx eq 0 || getrxmodenumber() eq 0) {
								#Reset the retries counter
###VK2ETA test, let the counter increment	$Retries = 0;
###								`echo "$Retries" > $ENV{HOME}/.pskmail/.retries`;
								adjusttxmodem(-1);	#Downgrade by one step
								settxrsid(1);
								sendmodemcommand ("<txrsid>ON</txrsid>");
								#Remember we just downgraded the TX mode
								$havedowngradedtx = 1;
							} else {
								#If we are in symetric mode, do not adjust RX mode as it follows the TX
								if (getrxmodenumber() ne 0) {
									adjustrxmodem(-1);
									if (gettxmodem() eq "PSK500") {
										adjusttxmodem(-1); # PSK500 often fails, back to PSK500R ...
									}
									settxrsid(1);
									print "Set retry mode to TX=", gettxmodem(), " , RX=" , getrxmodem() , "\n";
									$havedowngradedtx = 0;
# Slow the counter down (1 decrement every two increments)
								$Retries --;
								`echo "$Retries" > $ENV{HOME}/.pskmail/.retries`;
								}
							}
						}
						set_txstatus("TXPoll");
						send_frame();

						$RxReady = 0;
					}
				}
				if ($RxReady == 1 ) {
				
					if (-e "$ENV{HOME}/.pskmail/.abort_trans") {
						$txqlen = 0;
						`rm $ENV{HOME}/.pskmail/.abort_trans`;
						set_txstatus("RESET_TXQueue");
						send_frame("");
					} else {
						$RxReady = 0;
						if ($txqlen) {
							$Retries = 0;
						}
						$outputstring = gettxinput();
						set_txstatus("TXTraffic");
						send_frame($outputstring);
						if ($outputstring) {
							$Retries = 0;
						}
						$outputstring = "";
					}
				}
					if ((gettxmodenumber() > 5 && $Retries >= $Max_retries * 2) ||
						($Retries >= $Max_retries)) {

						if (-e "$ENV{HOME}/.pskmail/telnetactive" && $Retries < 60) {
							# do nothing
						} else {
							set_txstatus("TXDisconnect");
							send_frame();
							$ConnectStatus = "Listening";
							reset_rxstatus;
							$ConnectStatus = "Listening";
							$Retries = 0;
						}
					}

				if ($ConnectStatus ne "Connected") {
					print "Disconnected\n";
					open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
					print SESSIONDATA $nosession;
					close SESSIONDATA;
					$ConnectStatus = "Listening";
					last;
				}
			}
## connect ended

		}

	}
}  # end while(1)
}

########################################################
sub get_serverstatus {
########################################################

		return $ConnectStatus;
}

########################################################
sub count_mail {
#######################################
my $validconnect = 0;
my $pop;
$stationname = get_session();
print "\nStationname =", $stationname, "\n";
getuserdata($stationname);

return -1 unless $Pop_host;

if ($Pop_host =~ /gmail/i || $Pop_host =~ /web.de/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}


$count = $pop->Count();

if ($count > -1) {
	$pop->Reset;
}
return $count;
}

########################################################
sub just_count_mail {
#######################################
my $validconnect = 0;
my $pop;
$stationname = shift @_;
print "\nStationname =", $stationname, "\n";

getuserdata($stationname);

return -1 unless $Pop_host;

if ($Pop_host =~ /gmail/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}


$count = $pop->Count();

if ($count > -1) {
	$pop->Reset;
}
return $count;
}

########################################################
sub list_mail {
#######################################
my $validconnect = 0;
my $hdr = "";
my $pop = "";

$stationname = get_session();

getuserdata($stationname);

return unless $Pop_host;

if ($debug_mail) {print "$Pop_host|\n";}

if ($Pop_host =~ /gmail/i || $Pop_host =~ /web.de/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}


	if ($pop->Count() > -1) {
		logprint ("Pop defined\n");
		$validconnect = 1;
	}

	if ($debug_mail) {print "$Pop_user,$Pop_pass|\n";}


	if ($validconnect) {logprint ("Authenticated\n");}

	$Count = $pop->Count;
	print "Count=$Count\n";


	if ($Count) {
		logprint ("Got message list\n");
		`echo '' > $mailuser`;
	};

	open (HFH, ">$ENV{HOME}/.pskmail/mailheaders");
#	open (MFH, "$mailuser");
	open (MFH, ">$ENV{HOME}/.pskmail/.mailuser");

	print HFH "\n";
	print MFH "\n";

	for ($i =1; $i <= $Count; $i++) {
		my (@msg, $subject, $sender, $from);
		my $headerlength = 0;

		@msg = $pop->Head($i);

		$subject = $sender = '';

		foreach $hdr (@msg) {

			$headerlength += length ($hdr);

			if ($hdr =~ /^Subject:\s+/i) 	{
				$subject = substr($hdr, 8);
				$subject =~ s/['"]//g;
			}
#'"
			if ($hdr =~ /^From:\s+/i)	{
				$sender = substr($hdr, 6);
					($from = $sender) =~ s{<.*>}{};
					if ($from =~ m{\(.*\)}) {$from = $hdr; }
					$from ||= $sender;
					$from =~ s/['"]//g;
			}
		}

		my $messagecontent = $pop->Head($i, 999);

		my $mesglength = length ($messagecontent) - ($headerlength + 100);

		my $headerline = sprintf ("%2.0d %-30.30s %-60.60s %d\n", $i, $from, $subject, $mesglength);
		print HFH $headerline;
		if ($monitor && $debug_mail) {
			print $headerline;
		}
		my $fromline = sprintf ("%-20.20s %2.0d %-55.55s ", $from, $i, $subject);

			print MFH "From $fromline $mesglength Bytes\n";
			print MFH @$messagecontent;


		$mymailnumber = $i;

	}

	print HFH "\n";
	print MFH "\n";

	close (HFH);
	close (MFH);

	$pop->Close; # keep the mail for the moment




} # end list_mail

########################################################
sub read_mail {
#######################################

my @numbers = @_;

my $fault = 0;
my $messagecontent;
my $pop;

getuserdata(get_session());

return unless $Pop_host;

if ($Pop_host =~ /gmail/i || $Pop_host =~ /web.de/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}

if (-e "$mailuser") {`rm "$mailuser"`} ;


open (MFH, ">>$mailuser");

foreach my $msgid (@numbers) {
	my (@msg, $subject, $sender, $from, $dte);

	if ($msgid !~ /\d+/) { next; }

		if ($Pop_user) {
			$messagecontent = $pop->Head($msgid, 999);
		} else {
			$messagecontent = $pop->HeadAndBody($msgid);
		}
		if ($messagecontent) {
			print MFH "\n\nFrom \n";
			print MFH $messagecontent;
		} else {
			if ($monitor) {
				print "failed \n";
			}
			`echo "Message not available\n"`;
		}

	$mymailnumber = $msgid;
}

close (MFH);

$pop->Reset; # keep the mail for the moment




} # end read_mail

#####################################################
sub delete_mail {
#####################################################

my @delmessages = @_;

getuserdata(get_session());

return unless $Pop_host;

if ($Pop_host =~ /gmail/i) {
	$Use_ssl = 1;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
								USESSL		=> $Use_ssl,
								);
} else {
	$Use_ssl = 0;
	$pop = new Mail::POP3Client (USER 		=> $Pop_user,
								PASSWORD 	=> $Pop_pass,
								HOST		=> $Pop_host,
	      						AUTH_MODE => 'PASS',
								USESSL		=> $Use_ssl,
								);
}



foreach $msgid(@delmessages) {
	if ($msgid) {
		$pop->Delete($msgid) ;
	}
}
$pop->Close;

} # end delete_mail

#####################################################
sub getuserdata {
#####################################################
	tie (%db, "DB_File", $dbfile)
		or die "Cannot open user database\n";

	tie (%visitors, "DB_File", ".visitors")
		or die "Cannot open visitors database\n";

	my $key = "";
	my $Value = "";

	$key = shift @_;

	return ("") unless $key;
	if (exists $db{$key}) {
		 $Value = $db{$key};
	} else {
		$Pop_host = "none";
		$Pop_user = "none";
		$Pop_pass = "none";
		$address = "none";
		$findupasswd = "none";

		return "";
	}
		my @values = split ("," , $Value);

	$Pop_host = $values[0];
	$Pop_user = $values[1];
	my $P = $values[2];
	$Pop_pass = setp($P);
	my $dump = $values[3];	# dump smtp server, fixed
	$address = $values[4];
	$dummy = $values[5];
	$findupasswd = $values[6];

	foreach $key (keys %visitors) {
		if (time - $visitors{$key} > 130000) {
			delete $visitors{$key};	# give them 36 hours...
			delete $db{$key};
		}
	}

	untie %db;
	untie %visitors;
	return @values;
}

#########################################################
sub getusercalls {
#########################################################

	tie (%db, "DB_File", $dbfile)
		or die "Cannot open user database\n";

	$Usercalls = "";
	foreach my $callkey (keys %db) {
		$Usercalls .= $callkey;
		$Usercalls .= " ";
	}
	untie %db;

}
#end
################################
sub filter {
################################
# in = $mailuser, out = mailfile
################################

my $inputfile = shift @_;
my $html = shift @_;

my @fromarray;
my @subjectarray;
my $datearray;
my @headerarray;
my $counter = -1;
my $line;
my @testarray = ();
@msgarray = ();
my @craps = ();

init_crap_filter ();	# fills craps array with text scraps

open (INP, $inputfile);
my @mailtest = <INP>;
close (INP);

my $quiet = 1;
my $subjdone = 0;
my $datedone = 0;
my $fromdone = 0;

my $mesgstart = 0;
foreach $line (@mailtest) {

$line =~ s/\015//g;

$_ = $line;

		if (m/^From /) {
			$quiet = 1;
			$counter++;
			$subjdone = 0;
			$datedone = 0;
			$fromdone = 0;
			$mesgstart = 1;
#			$msgarray[$counter] .= $line;
			if ($monitor) {
				print $line;	##debug
			}


		}

		next unless $mesgstart;

		if ($line eq "\n" || $line eq "\r\n") {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			$quiet = 0;

			if ($monitor) {
				print $line;	##debug
			}
		}
		if (m/^From: / && $fromdone == 0) {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			$fromdone = 1;
			if ($monitor) {
				print $line;	##debug
			}

		}
		elsif (m/^Subject:/ && $subjdone == 0) {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			if ($counter >= 0) { $subjectarray[$counter] .= $line; }
			$subjdone = 1;
			if ($monitor) {
				print $line;	##debug
			}
		}
		elsif (m/^Date:/ && $datedone == 0) {
			if ($counter >= 0) { $msgarray[$counter] .= $line; }
			$datedone = 1;
			if ($monitor) {
				print $line;	##debug
			}
		} else {
			if ($quiet == 0) {
				if ($counter >= 0) {
					$line =~ s/\n\n/\n/;
					$line =~ s/\r\n/\n/;
					$msgarray[$counter] .= $line;
				}
			if ($monitor) {
				print $line;	##debug
			}
			}
		}
}

open (OUTP, ">$ENV{HOME}/.pskmail/mailin");
for ($i = 0; $i <= $counter; $i++) {
	print OUTP $msgarray[$i];
}
close (OUTP);

open (MAILF, "$ENV{HOME}/.pskmail/mailin");		# html filter
open (MAILOUT, ">$ENV{HOME}/.pskmail/mailfile");

while (<MAILF>) {
	if ($html == 0 && m#<HTML#i ... m#</HTML#i) {
		if ($monitor) {
			print $_;
		}
	 } else {
	 	my $in = crapfilter ($_);
	 	print MAILOUT $in;
	 }
}
close (MAILOUT);
close (MAILF);

`cp $ENV{HOME}/.pskmail/mailfile $ENV{HOME}/.pskmail/mailtest`;

} #end filter

################################################
sub crapfilter {
################################################
	my 	$line = shift @_;


	if ($craps[0]) {

		$line =~ s/=\?\?Q\?//;
		$line =~ s/\?Q\?//;
		$line =~ s/\?iso-8859-1\?q\?//;
		$line =~ s/=\?iso-8859-1\?Q\?//;
		$line =~ s/=09//;
		$line =~ s/=20//;
		$line =~ s/=22//;
		$line =~ tr/\r//;
		$line =~ s/^\s+\n/\n/;

		if ($line =~ /^>/ ) {
			return ("");
		}

		foreach $scrap (@craps) {
			chomp $scrap;
			if ($line =~ /$scrap/) {
				return ("");
			}
		}

	}

	return $line;
}
#################################################################
sub init_crap_filter {	# fills array with text scraps
#################################################################
	if (-e "$ENV{HOME}/.pskmail/crapmail.txt") {
		open ($nfh, "$ENV{HOME}/.pskmail/crapmail.txt");
		@craps = <$nfh>;
		close ($nfh);
	} else {
		print "Can not find file: crapmail.txt\n";
	}
}
#################################################################

sub addstat {
	my $type = shift @_;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime(time);
		$year += 1900; 
	my $dt = $year . "-" . $mon . "-" . $mday . "," . $hour . ":" . $min . ":" . $sec;
	my $out = $dt . "," . $type;
	`echo "$out" >> $statlog`;	
	print $out, "\n";
}

############################################################
# Used to transfer emails through smtpserver.
# No internal eval block, pse use one when calling this method.
# SMTP AUTH TLS added 2008-01-10 /SM0RWO
# Rewritten to allow none, plain or TLS AUTH. 2008-10-19 /SM0RWO
# Added mandatory date header. 2008-10-19 / SM0RWO
# Added support for multiple recipients 2010-02-18 /SM0RWO
# Made the timezone setting work. 2010-06-10 /SM0RWO
# Changed to work with gmail,yahoomail like TLS, SSL , 2014-06-14 /DL8JF
sub send_mail {
############################################################
    my($apath, $to, $from, $subject, @body)= @_;
    my $smtp;

		$smtp = Email::Send::SMTP::Gmail->new( -smtp=>$relay,
											   -port=>$smtptlsport,
											   -login=>$smtptlsuser,
											   -pass=>$smtptlspass,
											   -layer=>$smtpseclayer,
											   -auth=>$smtpauthlevel,												
											   -debug=>0);												
		die "Could not open TLS connection: $!" if (! defined $smtp);
		    
##DEBUG:
logprint ("Connection open...\n");
##DEBUG END
my $body='';
$to =~ s/ //g;    
$body = join("\n",@body);
### Begin transaction ###
    $smtp->send(-from=>$from,
				-to=>$to,
				-replyto=>$from,
				-subject=>$subject, 
				-body=>$body,
				-attachments=>$apath,
				-verbose=>0);
	$smtp->bye;
print "Email sent via:",$relay,",Port:",$smtptlsport,",Seclayer:",$smtpseclayer,"\n";		
}

#####################################################
sub send_tweety {
############################################################
    my($to, $from, $subject, @body)= @_;
    my $smtp;

    		$smtp = new Net::SMTP::TLS("www.pskmail.org",
			Hello	 =>	 $smtphelo,
        		Port     =>      "587",
        		User     =>      "pi4tueserver\@pskmail.org",
        		Password =>      "test1234",
			Timeout  => 10,
			Debug    => 1);
   	        die "Could not open TLS connection: $!" if (! defined $smtp);

##DEBUG:
logprint ("Connection to tweety open...\n");
##DEBUG END

	# Create a date for the mandatory Date: header
   	my @dayofweek = (qw(Sun Mon Tue Wed Thur Fri Sat));
   	my @monthnames = (qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec));
	my $timezone = strftime("%Z", localtime());
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday) = localtime();
   	$year += 1900;
   	my $mydate = sprintf("%s, %02d %3s %04d %02d:%02d:%02d (%s)",$dayofweek[$wday],$mday,$monthnames[$mon],$year,$hour,$min,$sec,$timezone);

	### Begin transaction ###
	$smtp->mail( $from );
	# Handle single or multiple recipients separated by a comma
	my @email = split(/,/,$to);
	foreach my $pers (@email) {
		$smtp->to($pers);
	}


	# Send the data
    	$smtp->data;

    	$smtp->datasend("From: $from\n");
	$smtp->datasend("Reply-To: $from\n"); # Necessary for gmail as it rewrites the from-field.
	$smtp->datasend("To: $to\n");
	$smtp->datasend("Date: $mydate\n");
    	$smtp->datasend("Subject: $subject\n");
#	$smtp->datasend("Comments: Via PskMail.\n");
	$smtp->datasend("\r\n");
    	foreach (@body) {
        	 $smtp->datasend("$_\n");
    	}
    	$smtp->dataend;
    	$smtp->quit;
}


###########################################
sub send_MIME {
###########################################
	my($to, $from, $subject, @body)= @_;	
	my $attachment = pop @body;	
	my @attachment_array = ();
	my @text_array = ();
	my @content_array = ();
	my $fileflg = 0;
	my $msg = "";

	foreach $line (@body) {
		if ($line =~ /Your attachment/) {
			$fileflg = 1;
		} elsif ($fileflg == 0) {
			push @text_array, $line;
		} else {
			push @content_array, $line;
		}
#		printf ("%d LINE:%s\n", $fileflg, $line);
	}

	my $filecontent = join "\n", @content_array;
	my $textcontent = join "\n", @text_array;

	my $afile = substr($attachment, 1, length($attachment) -2);

	my $att = decode_base64($filecontent);


	open (ATT, ">$ENV{HOME}/.pskmail/uploads/$afile");
	print ATT $att;
	close (ATT);


#	print "\nTEXT:\n", $textcontent , "\n---------\n" ;
#	print	"FILE=" , $afile , "\n";

# now try to send it via smtp...

### Try to send the message
my $apath = "$ENV{HOME}/.pskmail/uploads/$afile";
my @message = ();
push @message, $to;
push @message, $from;
push @message, $subject;
push @message, $textcontent;
logprint ("Sending mail message to $to\n");
eval {
	send_mail($apath,@message);	# send the mail via smtp
};

if ($@) {
	logprint ("Could not send MIME email message:$@\n");
}

print "done", "\n";

`rm "$ENV{HOME}/.pskmail/uploads/$afile"`;

}

############################################################
# This is used to get the generated boundary line generated
# by MIME::Lite. That line must be placed in the header of
# the outgoing email. 2010-06-10 / SM0RWO
sub getboundary{
############################################################
	my($body)= @_;

	my $boundarystart = "--_----------=_";
	my $result="";

	if ( $body =~ m/$boundarystart(\d+)/ ) {
		$result = $boundarystart . $1;
	}
	## Remove the first two -- as those are always added for the boundary
	## Only remove if the string is bigger than two characters.
	if (length $result gt 2) {
		return substr $result, 2;
	}
	else {
		return $result;
	}
}


###########################################
sub get_session {
###########################################
	open SESSIONDATA, "$ENV{HOME}/.pskmail/PSKmailsession";
	my $session = <SESSIONDATA>;
	chomp $session;
	close SESSIONDATA;

	return $session;
}
##############################################
sub scanner {
##############################################
#print "scanner:", time(), "\n";
	my $bndfreq = 0;
	$hour = (time / 3600) % 24;	#/
	if ($hour != $oldhour) {

		if (-e "$ENV{HOME}/.pskmail/qrg/freqs.txt") {
			open ($fh, $qrgfile) or die "Cannot open the scanner file!";
			@freqhrs = <$fh>;
			close ($fh);

my $storeline = "";

			for ($i = 0; $i < @freqhrs; $i++) {
				if ($freqhrs[$i] =~ /^\d.*/) {
					$freqtable[$i] = $freqhrs[$i];
					$storeline = $freqhrs[$i];
				} elsif ($freqhrs[$i]) {
					if ($freqhrs[$i] =~ /USB.*/ ||
						$freqhrs[$i] =~ /FM.*/) {
							$pskmodes = $freqhrs[$i];
							if ($i > 0) {
								$freqtable[$i] = $storeline;
							}
					} elsif ($freqhrs[$i] =~ /PSK.*/ ||
							$freqhrs[$i] =~ /THOR.*/ ||
							$freqhrs[$i] =~ /MFSK.*/ ||
							$freqhrs[$i] =~ /CTSTIA/ ||
							$freqhrs[$i] =~ /CW/) {
								$pskmodes = $freqhrs[$i];
							}
							if ($i > 0) {
								$freqtable[$i] = $storeline;
							}
					} else {
					if (!$freqhrs && $i > 0) {
						$freqtable[$i] = $storeline;
					}
				}
			}

			$cat = $freqtable[$hour];
#			print $cat;
			chomp $cat;

			@freqs = split (",", $cat);
			@modes = split (",",$pskmodes);
		}
		$oldhour = $hour;
	}

	my $secs = time % 60;
	
	$mins = ((time / 60) + $freq_offset) % 5 ;	#/	

	if ($mins != $oldmins) {

			my $result = query("~list ALL");
#			print "RESULT:\n", $result;	
			open $fh1, ">", "$ENV{HOME}/.pskmail/.internetcalls";
			print $fh1 $result;
			close $fh1;		

		if (get_session() eq "none") {
			my $cmd = '';
			
			my $rigm = '';

			if (@rigmod) {
				$rigm = $rigmod[$mins];		##PATCH IS0GRB 11092008
			}
			if ($scanner eq "C" ) {
				$cmd = 'H'; # set channel
			} elsif ($scanner eq "M" ) {
				$cmd = 'E'; # set memory
				$outfreq = $freqs[$mins];
				$modem = $modes[$mins];
			} elsif ($scanner eq "S" ) {
				$modem = $modes[$mins];
				setmode($modem); # set fldigi mode, symmetric
				$oldmins = $mins;
				return;
			} else {
				$cmd = "F";
				$outfreq = $freqs[$mins] + $freq_corrections[$mins];;
				$modem = $modes[$mins];
				`echo $outfreq > $ENV{HOME}/.pskmail/.band`;
			}
			open $fh2, ">", "$ENV{HOME}/.pskmail/.rxmodem";
			print $fh2 $modem = $modes[$mins];;
			close $fh2;		
#print $modem, "\n";
			if ($scanner eq "M" && -e "$ENV{HOME}/.pskmail/qrg/memtable") {
				my $mem = $freqs[$mins];
				if ($memories[$mem]) {
					$memfreq = $memories[$mem];
					$bndfreq = $memfreq;
				}
			}



#			`echo "$bndfreq" > $ENV{HOME}/.pskmail/.band`;
#		print "Freq:$bndfreq.\n";
#		print "Memory:$outfreq.\n";

			if ($modem ne "CTSTIA") {
				if (-e "$ENV{HOME}/.pskmail/.ctstiamode") {
					unlink "$ENV{HOME}/.pskmail/.ctstiamode";
				}
			}
			if ($modem ne "CW") {
				sendmodemcommand ("<rsid>ON</rsid>");
			} else {
				sendmodemcommand ("<rsid>OFF</rsid>");
			}
			Time::HiRes::sleep(0.2);
			setmode($modem); # set fldigi mode
#			print "<Mode>:$modem\n";
								
			my $error = "";
			eval {
				# Set a mode if none is entered
				if (defined $rigm && $rigm ne ''){
#					print $freq_corrections[$mins], "\n";
					setworkingfreq ($outfreq  - $freq_corrections[0]);
					$error = `rigctl -m $rigtype -r $rigdevice -s $rigrate $cmd $outfreq M $rigm 0 2> $ENV{HOME}/.pskmail/hamliberrors`   ##PATCH IS0GRB 11092008
					or die "cannot use hamlib? $@\n";
					
				} else {
#					print $freq_corrections[$mins], "\n";

					if ($PTTCOMMAND){ # reset PTT
						$error = `rigctl -m $rigtype -r $rigdevice -s $rigrate T 0 2> $ENV{HOME}/.pskmail/hamliberrors`
					}

					setworkingfreq ($outfreq  - $freq_corrections[0]);
#					print "Scanfrequency:$outfreq\n";
					$error = `rigctl -m $rigtype -r $rigdevice -s $rigrate $cmd $outfreq 2> $ENV{HOME}/.pskmail/hamliberrors`   # No mode switch here
					or die "cannot use hamlib? $@\n";
					
					
				}
				$error = "";

				if (-e "$ENV{HOME}/.pskmail/.idcd") {
					`rm $ENV{HOME}/.pskmail/.idcd`;
				}

			};
			if ($@ !~ /cannot use/ && -e "$ENV{HOME}/.pskmail/debugscanner") {
				print "Freq set error?\n$@\n$error\n";
			}
			set_autotune(1); # Remember that a fq change has taken place
			

		}
		}

		$oldmins = $mins;
}


##############################################
# Check the beacons array and send a beacon if the current minute is 0-4 (array size)
# and that array segment is 1. Update the status array to reflect beacon sent status.
# 2007-01-19, SM0RWO/Pär Crusefalk
sub serverbeacons {
##############################################
	my $trigger = 0;				# set to 1 if its time for a beacon
	my $n = 0;					# Used to loop the arrays
	my $realmin = (time / 60 ) % $period;		# What minute is this ?	#/

	if ($realmin == 0 && $txID_off == 0) {
		$txID_off = 1;
		if (get_session() eq "none") {
			# reset txID
			settxrsid(0);
		}

	} elsif ($realmin == 1) {
		$txID_off = 0;
	}

	# Loop the array
	foreach (@Beaconarray)
	{
		if ($realmin == $n)
		{
			# If the beacon has not been sent and it should then enter here
			if ($Beacons_sent[$n] == 0 && $_ == 1)
			{
				$trigger=1;			# Time for a beacon
				$Beacons_sent[$n]=1;		# Set the current minute as done
			}
			$Beacons_sent[$n-1]=0;			# Set the minute before (or wrap around) as unsent
		}
		$n++;
	}
	
	if ($realmin == 0 && -e "$ENV{HOME}/.pskmail/.internet") {
		sendfreqs();
	}

	# Send the beacon if triggered and no session
	if ($trigger == 1 && (get_session() eq $nosession))
	{
		sleep 1+int(rand(25)); # settle the tx and allow multiple servers on qrg
		# send beacon
		logprint ("Sending beacon\n");
#		set_txstatus("TXBeacon");
#		send_frame();
#		sleep (5);
		set_txstatus("TXPositBeacon");
		send_frame;
	}
}



################################################
sub getbeacons {
################################################

my $ClientCall = shift @_;
`cat $ENV{HOME}/.pskmail/server.log | grep "$ClientCall" > $ENV{HOME}/.pskmail/.mylines`;

my $now = time();
my $then = time();
$then -= 60 * 60 * 12;
my %beaconhours = ();

my $j = int ($then/(3600) % 24);
my $k = int ($now/(3600) % 24);
my $m = 0;

if ($j > $k) {
	$k += 24;
	$m = 1;
}

for (my $i = $j; $i <= $k; $i += 1) {
	$beaconhours{$i} = 0;
}

open (LOG, "$ENV{HOME}/.pskmail/.mylines");
while (my $input = <LOG>) {
	if ($input =~ /((\d\d):(\d\d)\s\w*\s(\w\w\w)-(\d*)-(\d\d\d\d): <SOH>..u\S*:26 .\d\d\d\d\.\d\d\w)/ ||
		$input =~ /((\d\d):(\d\d)\s\w*\s(\w\w\w)-(\d*)-(\d\d\d\d): <SOH>..u\S*:26 &&)/) {
#print $input;
		my $month = monthnumber($4);
		my $hour = $2;
		my $mins = $3;
		my $epoch = timegm (0, $mins, $hour, $5, $month,  $6);
		if ($epoch > $then) {
			my $beaconhour = int ($epoch/(3600) % 24);
			if ($m && $beaconhour < 13) { $beaconhour += 24;}
			$beaconhours{$beaconhour}++;
		}
	}
}
close (LOG);

my $index;
my $b_outstring = "";

foreach my $beacon(sort {$a <=> $b} keys %beaconhours) {
	if ($beacon > 23) {
		$index = $beacon - 24;
	} else {
		$index = $beacon;
	}
	$b_outstring .= $beaconhours{$beacon};
}
	$b_outstring .= "|";
	$b_outstring .= $index;
	$b_outstring .= " UTC\n";

	return $b_outstring;
} # end

###############################################
sub monthnumber {
###############################################
	my $mon = shift @_;

	my $count = 0;
	my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
	foreach $month (@months) {
		if ($month =~ /$mon/i) {
			last;
		}
		$count++;
	}
	if ($count > 11) {
		$count = 0;
	}
	return $count;
} # end

#############################################################
sub catcher  {		# catch ctrl-c
#############################################################
	$SIG{INT} = \&catcher;
	sendmodemcommand ("normal");
	sleep 2;
	print "Exiting...\n";
	`killall rflinkserver.pl`;
	wait;
	exit;
}

#=head1
###############################################
sub bigearconnect {
###############################################

	if ($BigEarserverport && $BE) {
		eval {
			$BigEar = IO::Socket::INET->new(Proto     => "tcp",
										PeerAddr  => 'pskmail.org',
										PeerPort  => $BigEarserverport)
			   or die 
		};

		if ($@) {
#			print "$@\n";
			return 0;
		}

		if (defined $BigEar) {
			$BigEar->autoflush(1);
			print $BigEar "/nick $ServerCall\n";
		}

		return $BigEar;
	} else {
		return 0;
	}
}
#=cut

############################################################
sub getprotocol {
############################################################
	my $protocolstring = `cat $ENV{HOME}/.pskmail/.protocol`;
	chomp $protocolstring;
	return ord ($protocolstring) - 48;
}

################################################
sub telnetagent {
################################################
my $answer = "";
my $alarmtime = 120;
my $last_time = time();
my $rv;
my $telnetpid;
my $s = 0;
my ($host, $port, $isnode) = @_;
my $userid = "";
my $pass = "";

$Max_retries = 30;

eval {
	local $SIG{ALRM} = sub { die "Connection error"};
	alarm 30;
	eval {
	 $s = IO::Socket::INET->new(PeerAddr => $host,
							PeerPort => $port,
							Proto => "tcp",
							Type => SOCK_STREAM)
	or die "Could not connect to Remote Host: $@\n";

	$s->blocking(0);
	};
	alarm 0;
};
alarm 0;

if ($@) { print "error:" , $@, "\n";}

if ($s) {
	print "Connected to $host\n";
}

if ($@) {
	`echo "\n -- Connection error: $@ --\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
	return 2;
}

if ($s && $isnode) {
	`echo "\n -- Connecting packet radio node --\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
} else {
	`echo "\n -- PSKmail Telnet agent --\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
}

`touch $ENV{HOME}/.pskmail/telnetactive`;

my @telnetarray = ();
my @telnetcommands = ();
my @commands = ();

eval {
	if (-e "$ENV{HOME}/.pskmail/telnetusers") {
		@telnetarray = `cat $ENV{HOME}/.pskmail/telnetusers`;
	}
};

my $call = get_session();

if ($isnode) {
	foreach $commandline (@telnetarray) {
		if ($commandline =~ /^$call/) {
			print $commandline, "\n";
			push @telnetcommands, $commandline;
		}
	}

	foreach $telnetaddress (@telnetcommands) {
		if ($telnetaddress =~ /$host\,$port\,(.*)\n/) {
			@commands = split "," , $1;
		}
	}

	foreach my $output (@commands) {
		sleep 1;
		chomp $output;
		print $s $output, "\n";
	}
}

if ($telnetpid = fork) {
## parent code
	while ($s) {

		eval {
			local $SIG{ALRM} = sub {die "timeout"};
			alarm 30;
			eval {
				#########################
				$command = <$reader>;

				if ($command && $command =~ /~QUIT/ ) {
					print "::" . $command . "::\n";
					close $s;
					last;
				} elsif ($command && $command =~ /~PASSx(\d+)\,(.*)$/) {

					if (-e "$ENV{HOME}/.pskmail/.auth_ok") {
							$telnetpassw = make_secret ($1, $2);
					} else {
						`echo "sorry, wrong auth password\n" > $ENV{HOME}/.pskmail/TxInputfile`;
					}
					if ($isnode) { #needs CR
						print $s "$telnetpassw\r\n"
					} else {
						print $s "$telnetpassw\n";
					}
					$command = "";

				} elsif ($command) {
					chomp $command;
					print "|", $command, "|\n";

					if ($command) {
						if ($isnode) { #needs CR
							print $s "$command\r\n"
						} else {
							print $s "$command\n";
						}
					}
					$command = "";
				}
				#########################
			};
			alarm 0;
		};
		alarm 0;
		last if $@ && $@ =~ /timeout/;

		if (get_session() eq $nosession) {
			close $s;
			last;
		}


	}

	if (-e "$ENV{HOME}/.pskmail/telnetactive") {	#/
		unlink "$ENV{HOME}/.pskmail/telnetactive";
	}

} else {
## child code

	$s->blocking(0);

	open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/

	while ($s) {

		$rv = read ($s, $c, 1);

		while ($rv) {

			$answer .= $c;
			if ($answer =~ /(.*\n)/) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT $answer;
				close OUT;
				print $answer;
				$answer = "";
			} elsif ($answer =~ /Login: /) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT $answer, "\n";
				close OUT;
				print $answer;
				$answer = "";
			} elsif ($answer =~ /Password: /) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT $answer, "\n";
				close OUT;
				print $answer;
				$answer = "";
			} elsif ($answer =~ /^=>/) {
				open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
				print OUT "=>\n";
				close OUT;
				print $answer, "\n";
				$answer = "";
			}

			$rv = read ($s, $c, 1);
		}

		if ($answer) {
			open (OUT, ">>", "$ENV{HOME}/.pskmail/TxInputfile"); #/
			print OUT $answer;
			close OUT;
			$answer = "";
		}

		sleep 1;

		if (get_session() eq $nosession) {
			close $s;
			last;
		}

	}

	`echo "\nTelnet closed.\n" >> $ENV{HOME}/.pskmail/TxInputfile`;

#	close (OUT);
	exit;

}

if ($isnode) {
	`echo "\nDisconnecting from packet node.\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
} else {
	`echo "\nClosing telnet sesion.\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
}

$Max_retries = 8;

	return;
}

##################################################################
sub setp {
##################################################################
my $b = shift @_;
$b =~ s.([a-zA-Z]).ord $1<97?uc($1^$f[ord($1)-65]):lc($1^$f[ord($1)-97]).ge;
return $b;
}

#################################################################
sub getp {
##################################################################
	tie (%db, "DB_File", $dbfile)
		or die "Cannot open user database\n";

	my $key = "";
	my $Value = "";
	my $Values;
	my @values = ();

	$key = shift @_;
	my $pw = shift @_;
	return ("") unless $key;
	if (exists $db{$key}) {
		 $Value = $db{$key};
		 @values = split ",", $Value;
	}

	my $fp = pop @values;

	if ($fp eq "none") {
		push @values, $pw;
		$Values = join ",",@values;
		$db{$key} = $Values;
		untie (%db);
		return 1;
	} elsif ($fp eq $pw) {
		push @values, $fp;
		$Values = join ",",@values;
		$db{$key} = $Values;
		untie (%db);
		return 1;
	} else {
		push @values, $fp;
		$Values = join ",",@values;
		$db{$key} = $Values;
		untie %db;
		return 0;
	}
}
##########################################################
sub send_freq_table {
##########################################################
### send freq. table to pskmail.org
	if ($scanner ne ''){
		my $ua = LWP::UserAgent->new();
			my $freqURL = 'http://pskmail.org/postfreq.php';
			if ((defined $postfreqURL) && $postfreqURL && $MARS == 0) {
				$freqURL = $postfreqURL;
			}
			my $req = HTTP::Request->new(POST => $freqURL);
			$hour = (time / 3600) % 24;
			my $xml = $ServerCall . ":" . $freqtable[$hour];
			my $length = length($xml);
			$req->content($xml);
			$req->header('Content-length' => $length);

			my $res =  $ua->request($req);
			my $response_data = $res->content;
#			print $response_data . "\n";
	}
}
