#! /bin/bash

# Install script for pskmail_server

INSTALLDIR="/usr/local/share/pskmail_server"

PWDIR=$PWD

cd ..

BASEDIR=$PWD

cd config

CONFIGDIR=$PWD

cd $PWDIR

echo "Install pskmail_server in $INSTALLDIR for user $myuser"

echo "Copying start script to /usr/local/bin"

cp -f -p pskmail_server /usr/local/bin
cp -f -p startpskmail /usr/local/bin

echo "Copying fldigi_modem to /usr/local/bin"

sudo cp -f -p fldigi_pskmail /usr/local/bin

sudo cp ../lib/libfltk_images.so.1.1 /usr/lib/i386-linux-gnu

if [ -d $INSTALLDIR ]; then
	echo "$INSTALLDIR already exists"
else
	mkdir $INSTALLDIR
fi

echo "Copying scripts to $INSTALLDIR"

chmod 777 rflinkserver.pl

cp -f rflinkserver.pl $INSTALLDIR
cp -f -p arq.pm $INSTALLDIR

if [ -e $HOME/.pskmail/rflinkusers/rflink_users.db ]; then
	cp $HOME/.pskmail/rflinkusers/rflink_users.db $PWDIR/keepdb
fi

if [ -e $HOME/.pskmail/pskmailrc.pl ]; then
	cp $HOME/.pskmail/pskmailrc.pl $PWDIR/keepconfig
fi

if [ -e $HOME/.pskmail/.node_data ]; then
	cp $HOME/.pskmail/.node_data $PWDIR/keepnode
fi

if [ -e $HOME/.pskmail/telnetusers ]; then
	cp $HOME/.pskmail/telnetusers $PWDIR/keepusers
fi


if [ -e $HOME/.pskmail/qrg/freqs.txt ]; then
	cp $HOME/.pskmail/qrg/freqs.txt $PWDIR/keepfreqs
fi

if [ -e $HOME/.pskmail/qrg/memtable ]; then
	cp $HOME/.pskmail/qrg/memtable $PWDIR/keepmems
fi

if [ -e $HOME/.pskmail/qrg/scancalls ]; then
	cp $HOME/.pskmail/qrg/scancalls $PWDIR/keepcalls
fi

if [ -d $HOME/.pskmail/localmail ]; then
	cp -r $HOME/.pskmail/localmail $PWDIR/keepmail
fi

if [ -d $HOME/.pskmail/pskdownload ]; then
	cp -r $HOME/.pskmail/pskdownload $PWDIR/keepfiles
fi

if [ -e $HOME/.pskmail/routes ]; then
	cp $HOME/.pskmail/routes $PWDIR/keeproutes
fi

if [ -e $HOME/.pskmail/memories.pm ]; then
	cp $HOME/.pskmail/memories.pm $PWDIR/keepmems
fi

if [ -e $HOME/.pskmail/validfreqs.pm ]; then
	cp $HOME/.pskmail/routes $PWDIR/keepvalidfreqs
fi

if [ -e $HOME/.pskmail/stats.log ]; then
	cp $HOME/.pskmail/stats $PWDIR/keepstats
fi

rm -r $HOME/.pskmail

echo "check if /etc/pskmail directory is present"
if [ -d /etc/pskmail ]; then
	echo "pskmail directory is present"
else
	echo "Making pskmail dir /etc/pskmail"
	mkdir -m 777 /etc/pskmail
fi
	echo "copying admin web pages to pskmaildir"
	echo "$HOME/.pskmail/" > /etc/pskmail/pskmaildir
	chmod 666 /etc/pskmail/pskmaildir

echo "check if web directory is present"
if [ -d /var/www ]; then
	echo "web directory is present"
else
	echo "Making web dir /var/www"
	mkdir -m 777 /var/www
fi
	echo "copying admin web pages to /var/www"
	cp ../config/serveradmin.php /var/www
	cp ../config/process.php /var/www

cd $HOME

echo "Checking .fldigi directory"

if [ -d .fldigi ]; then
	echo ".fldigi directory is present"
else
	echo "Making web dir .fldigi"
	mkdir -m 777 .fldigi
	cp $CONFIGDIR/fldigi.prefs $BASEDIR/.fldigi
	cp $CONFIGDIR/fldigi_def.xml $BASEDIR/.fldigi
fi


echo "Making new ~/.pskmail directory"

mkdir .pskmail
chmod 777 .pskmail
cd .pskmail

mkdir uploads
mkdir pskdownload
mkdir rflinkusers
mkdir qrg
mkdir localmail
mkdir bulletins
mkdir pending

cd $HOME
chmod -R 777 .pskmail

echo "Setting ownership to $SUDO_USER"

cp -r $BASEDIR/rflinkusers $HOME/.pskmail
cp -r $BASEDIR/qrg $HOME/.pskmail
cp -f $CONFIGDIR/pskmailrc.pl .pskmail
cp -f $CONFIGDIR/testfile .pskmail
cp -f $CONFIGDIR/crapmail.txt .pskmail
cp -f $CONFIGDIR/messages.txt .pskmail
touch .pskmail/aprslog
touch .pskmail/routes

if [ -e $PWDIR/keepdb ]; then
	cp $PWDIR/keepdb $HOME/.pskmail/rflinkusers/rflink_users.db
	rm $PWDIR/keepdb
	echo "Restoring your old user database"
fi

if [ -e $PWDIR/keepconfig ]; then
	cp $PWDIR/keepconfig $HOME/.pskmail/pskmailrc.pl
	rm $PWDIR/keepconfig
	chmod 777 $HOME/.pskmail/pskmailrc.pl
	echo "Restoring your old config file"
fi

if [ -e $PWDIR/keepnode ]; then
	cp $PWDIR/keepnode $HOME/.pskmail/.node_data
	rm $PWDIR/keepnode
	echo "Restoring your old nodes file"
fi

if [ -e $PWDIR/keepusers ]; then
	cp $PWDIR/keepusers $HOME/.pskmail/telnetusers
	rm $PWDIR/keepusers
	echo "Restoring your old telnetusers file"
fi

if [ -e $PWDIR/keepfreqs ]; then
	cp $PWDIR/keepfreqs $HOME/.pskmail/qrg/freqs.txt
	rm $PWDIR/keepfreqs
	echo "Restoring your old freqs file"
	chmod 777 $HOME/.pskmail/qrg/freqs.txt
fi

if [ -e $PWDIR/keepmems ]; then
	cp $PWDIR/keepmems $HOME/.pskmail/qrg/memtable
	rm $PWDIR/keepmems
	echo "Restoring your old memtable file"
	chmod 777 $HOME/.pskmail/qrg/memtable
fi

if [ -e $PWDIR/keepcalls ]; then
	cp $PWDIR/keepcalls $HOME/.pskmail/qrg/scancalls
	rm $PWDIR/keepcalls
	echo "Restoring your old calls file"
fi

if [ -e $PWDIR/keeproutes ]; then
	cp $PWDIR/keeproutes $HOME/.pskmail/routes
	rm $PWDIR/keeproutes
	echo "Restoring your old routes file"
fi

if [ -e $PWDIR/keepstats ]; then
	cp $PWDIR/keepstats $HOME/.pskmail/stats.log
	rm $PWDIR/keepstats
	echo "Restoring your old statistics file"
fi

if [ -d $PWDIR/keepmail ]; then
	cp -r $PWDIR/keepmail/* $HOME/.pskmail/localmail
	rm -r $PWDIR/keepmail
	echo "Restoring your old mail file"
fi

if [ -d $PWDIR/keepfiles ]; then
	cp -r $PWDIR/keepfiles/* $HOME/.pskmail/pskdownload
	rm -r $PWDIR/keepfiles
	echo "Restoring your old file content"
fi

if [ -e $PWDIR/keepmems ]; then
	cp $PWDIR/keepmems $HOME/.pskmail/memories.pm
	rm $PWDIR/keepmems
	echo "Restoring your old memories file"
fi

if [ -e $PWDIR/keepvalidfreqs ]; then
	cp $PWDIR/keepvalidfreqs $HOME/.pskmail/validfreqs.pm
	rm $PWDIR/keepvalidfreqs
	echo "Restoring your old freqs filter file"
fi

echo "Setting permissions for config and freqs.txt"
sudo chown -R $SUDO_USER:$SUDO_USER .pskmail
sudo chmod 777 $HOME/.pskmail/qrg/freqs.txt
sudo chmod 777 $HOME/.pskmail/pskmailrc.pl

echo "Pskmail is now ready for use."
echo "Please restart the application after the first config."

exit 1
