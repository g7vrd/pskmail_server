#! /bin/sh

# lib installer for debian based systems (ubuntu).

echo "Installing necessary SSL libraries for pskmail server"

apt-get install libnet-ssleay-perl libdigest-crc-perl
apt-get install libio-socket-ssl-perl
apt-get install libdigest-hmac-perl
apt-get install libfile-path-expand-perl
apt-get install libemail-foldertype-perl
apt-get install liblockfile1
apt-get install libcrypt-dh-perl

dpkg -i ./libs/libnet-smtp-tls-perl_0.12-1_all.deb

echo "Installing utility programs for pskmail_server"

apt-get install libportaudio2 elinks lynx curl
apt-get install  libhamlib2  libhamlib-utils

echo "Installing network app modules"

apt-get install libmail-pop3client-perl
apt-get install libemail-localdelivery-perl
apt-get install libemail-folder-perl
apt-get install libmime-lite-perl
apt-get install libio-multiplex-perl
cpan install Email::Send::SMTP::Gmail

apt-get install apache2
apt-get install php5

echo ""
echo "done installing libraries..."

exit 1

