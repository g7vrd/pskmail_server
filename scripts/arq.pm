#! /usr/bin/perl -w

# ARQ module arq.pm by PA0R. This module is part of the PSK_ARQ suite of
# programs. PSK_ARQ adds an arq layer to keyboard oriented protocols like
# PSK31, PSK63, MFSK, MT63 etc.
# arq.pm includes the arq primitives common to server and client.
# This program is published under the GPL license.
#   Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013
#       Rein Couperus PA0R (rein@couperus.com)

# *    arq.pm is free software; you can redistribute it and/or modify
# *    it under the terms of the GNU General Public License as published by
# *    the Free Software Foundation; either version 2 of the License, or
# *    (at your option) any later version.
# *
# *    arq.pm is distributed in the hope that it will be useful,
# *    but WITHOUT ANY WARRANTY; without even the implied warranty of
# *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *    GNU General Public License for more details.
# *
# *    You should have received a copy of the GNU General Public License
# *    along with this program; if not, write to the Free Software
# *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


# Date: 241014
#########################################
# link layer spec for PSK_ARQ
#########################################

=header1 PSK_ARQ MODULE

generic block format:	   <SOH>dcl[info])12EF<EOT|SOH>
						   		||| |     +-checksum (4xAlphaNum)
						    	||| +-----block (1 ... 128 chars)
						    	||+-------block type
						    	|+--------stream id
						    	+---------protocol number

Frame:					<Block><Block>....<stat>

SendQueue:
	<     ><     ><Block><Block><Block><Block><Block><Block><Block>
				  |											|
		   		  Firstsent									Lastblock

ReceiveQueue:
	<     ><     ><     ><Block><      ><Block><Block><Block><Block>
						|									 |
		  				Goodblock	 						 EndBlock

listen --- connect       ---data ---data --- data --- statreq              data---statreq       disc
		|	|				  |              |             |      |  |
		connected 			data ---					statrprt             statrprt  ack
=cut

use Digest::CRC qw(crc16);
use Digest::MD5 qw(md5_base64 md5_hex);
use Crypt::DH;
use Time::Local;
use Fcntl;
use Fcntl qw/:flock/;
use Socket;
use Net::hostent;
use IO::Socket;
use Errno qw( EWOULDBLOCK );


#use DB_File;
use lib qw{./lib ../lib};
#use IPC::SysV qw(IPC_RMID IPC_CREAT S_IRWXU);

if (-e "$ENV{HOME}/.pskmail/memories.pm") {
	require "$ENV{HOME}/.pskmail/memories.pm";
}
if (-e "$ENV{HOME}/.pskmail/validfrequencies.pm") {
	require "$ENV{HOME}/.pskmail/validfrequencies.pm";
}

if (-e "$ENV{HOME}/.pskmail/.tosend") {
	eval {
		` rm $ENV{HOME}/.pskmail/.tosend`;
	};
}

# PROTOCOL
my $Protocol_level = 1;
$CTSTIA_mode = 0;

#Auto timing
use Time::HiRes qw(gettimeofday);

$Defaultmode = "PSK500R";

 my $newmode = "";
 my $transfermodes = "653";
 my $transferconnectmode = "3";
 my $transfermodelist = $transferconnectmode . $transfermodes;

my $timingdebug = 0;
my $squelch = 0;
my $squelchtime = 0;
my $proto = 0;
my %myservers = ();
my @knownservers = ();

my $eot = sprintf("%c", 0x04);
my $preamble = "-";
my $idcd = 0;
my $PTTstatus = 0;
my $Transfer = 0;

my $txrsidcfg = 0;		# Intermar patch
my $arqstatus = "";		# Intermar patch
my @aprsGroups = ();	# Intermar patch
my @accu_array = ();
my $cblock = 1;
my $fldigiport = 7322;
my $first_instance = 1;
my $Idle_init = -2;
my $st_check = "";
my $st_check2 = "";
my $Status_received = "";
my $getprediction = 0;
my $rspeed = "DominiEx22";

$Version = "2.3.5";

# $SIG{PIPE} = 'IGNORE';
$SIG{PIPE} = sub { warn "BROKEN PIPE\n" };
$txing = 0;

END {
	if ($fldigi) {
	    print $fldigi "<cmd>normal</cmd>\n";
	    sleep 1;
	     close $fldigi;
	} else {
		print "Fldigi closed its socket...";
	}
}

$sendbeacon = mkflag();

		if (-e "$ENV{HOME}/.pskmail/qrg/freqs.txt") {
			open ($fh, "$ENV{HOME}/.pskmail/qrg/freqs.txt") or die "Cannot open the scanner file!";
			@freqhrs = <$fh>;
			close ($fh);
my $storeline = "";

			for ($i = 0; $i < @freqhrs; $i++) {
				if ($freqhrs[$i] =~ /^\d.*/) {
					$freqtable[$i] = $freqhrs[$i];
					$storeline = $freqhrs[$i];
				} elsif ($freqhrs[$i]) {
					if ($freqhrs[$i] =~ /USB.*/ ||
						$freqhrs[$i] =~ /FM.*/) {
							$pskmodes = $freqhrs[$i];
							if ($i > 0) {
								$freqtable[$i] = $storeline;
							}
					} elsif ($freqhrs[$i] =~ /PSK.*/ ||
							$freqhrs[$i] =~ /THOR.*/ ||
							$freqhrs[$i] =~ /MFSK.*/) {
								$pskmodes = $freqhrs[$i];
							}
							if ($i > 0) {
								$freqtable[$i] = $storeline;
							}
					} else {
					if (!$freqhrs && $i > 0) {
						$freqtable[$i] = $storeline;
					}
				}
			}
			
			`echo $freqtable[0] > $ENV{HOME}/.pskmail/.band`;
			

		}



# initialyze cps values
	open CPSDATA, ">$ENV{HOME}/.pskmail/receivedCPS";
	print CPSDATA "2";
	close CPSDATA;

	if (-e "$ENV{HOME}/.pskmail/squelch.lk") {	# reset the squelch
		unlink "$ENV{HOME}/.pskmail/squelch.lk";
	}

# make sure there is a routes file
`touch $ENV{HOME}/.pskmail/routes`;


%Callindex = ();
%Routes = ();

getshortcalls();

# init the routes hash from the routes file

					my $routes = "";
					my @routelines = `cat $ENV{HOME}/.pskmail/routes`;
					foreach $line (@routelines) {
						if ($line =~ /(\S+) (\S+) (\d+)/) {
							my $pre = $2 . " " . $3;
							$Routes{$1} = $pre;
						}
					}

my $logfile = "$ENV{HOME}/.pskmail/server.log";
$Inputfile = "";			# Default Rx input from gmfsk

$debug2 = 0;

if (-e "$ENV{HOME}/.pskmail/debug"){
	$debug2 = 1;
}

 my ($Iamserver, $ServerCall, $monitor, $ShowBlock, $debug, $output, 
 $TxInputfile, $Inputfile, 	$Txdelay, $Sqldelay, $Framelength, 
 $Pingdelay, $rigptttune, $Aprs_connect, $Aprs_beacon, 	
 @Aprs_port, @Aprs_address, @prefixes, $positmessage, 
 $bulletinmode, $repeats, $PTTCOMMAND, $alt_forwarder, $NOINTERNET);

$Iamserver = 1;
$NOINTERNET = 0;
@prefixes = qw(); #deprecated 
$posit_time = 10; #deprecated 

$debug = 0;
$nosession = "none";
$Txdelay = 0;
$Sqldelay = 0;
$repeats = 2;

#$PTTCOMMAND = 0;
my $MARS = 0;
$alt_forwarder = "";

# read standardmessages
my @stdmessages = ();
if (-e "$ENV{HOME}/.pskmail/messages.txt"){
	open (MSGS, "$ENV{HOME}/.pskmail/messages.txt");
	while (my $line = <MSGS>) {
		(my $lnr, my $msg) = split "," , $line;
		$stdmessages[$lnr] = $msg;
	}
	close MSGS;
}

$scanner = "";

if (-e "$ENV{HOME}/.pskmail/.tx.lck") { unlink "$ENV{HOME}/.pskmail/.tx.lck"; }

if (-e "$ENV{HOME}/.pskmail/pskmailrc.pl" ) {	#must be server";
	eval `cat $ENV{HOME}/.pskmail/pskmailrc.pl`;
}
		if (-e "$ENV{HOME}/.pskmail/.internet" && $NOINTERNET == 0){
			my $result = query("~list ALL");
			open $fh1, ">", "$ENV{HOME}/.pskmail/.internetcalls";
			print $fh1 $result;
			close $fh1;	
			my @srvrlist = `cat "$ENV{HOME}/.pskmail/.internetcalls"`;
			my $dt = `date`;
			my $dy = "0";
#			print "DATE:", $dt, "\n";
			#Wed Nov 28 21:34:02 CET 2012
			if ($dt =~ /\w+\s\w+\s(\d+)\s/) {
				$dt = $1;
			} 
#			print "DATE:", $dt, "\n";
			
			foreach $sv (@srvrlist) {
				#28 21:00 9A1CRA
				if ($sv =~ /(\d+)\s\d+:\d+\s(\S+)/) {
#					if ($dt + 0 == $1) {
						push (@knownservers, $2);
#						print "Adding: ", $2, "\n";
#					}
				}
			}
		}


print "SERVERS:\n";
	foreach $findcall (@knownservers) {
		print $findcall, "\n";
	}


###############################################################
my @Amodes = qw(default CW CTSTIA THOR4 THOR8 MFSK16 DOMINOEX11 THOR22 MFSK32 DOMINOEX22 PSK250R PSK63RC5 PSK500R PSK250 PSK63RC10 PSK500 PSK250RC3 PSK1000);
my @Bmodes = qw(default CW CTSTIA THOR4 THOR8 MFSK16 DOMINOEX11 THOR22 MFSK32 DOMINOEX22 PSK125R PSK250R PSK63RC5 PSK63RC10 PSK250RC3 PSK250 PSK1000);
my @currentmodes = @Amodes;
my %modelist = ("0" => "default",
				"1" => "THOR8",
				"2" => "MFSK16",
				"3" => "THOR22",
				"4" => "MFSK32",
				"5" => "PSK250R",
				"6" => "PSK500R",
				"7" => "PSK500",
				"8" => "PSK250",
				"9" => "PSK125",
				"a" => "PSk63",
				"b" => "PSK125R",
				"c" => "MFSK64",
				"d" => "THOR11",
				"e" => "THOR4",
				"f" => "Contestia",	
				"g" => "PSK1000",
				"h" => "PSK63RC5",
				"i" => "PSK63RC10",
				"j" => "PSK250RC3",
				"k" => "PSK125RC4",
				"l" => "DOMINOEX22",
				"m" => "DOMINOEX11");

if ($MARS) {    # MARS stns do not have 300 Bd restriction....
	$NO500 = 0;
}

if ($NO500) { print "Modes limited to PSK250 downwards...\n"};

my $MAXMODENUM = 7;
my $alt_modetable = 0;
`echo "-1" > $ENV{HOME}/.pskmail/.servers2n`;
`echo "-1" > $ENV{HOME}/.pskmail/.clients2n`;
`touch $ENV{HOME}/.pskmail/pskmailstat.txt`;

	settable(0);
	if ($NO500) {

		print "NO500 set";

		if (-e "$ENV{HOME}/.pskmail/.rxid") {`rm $ENV{HOME}/.pskmail/.rxid`};
		`echo "PSK250" > $ENV{HOME}/.pskmail/.rxid`;
		`echo "PSK250" > $ENV{HOME}/.pskmail/.txmodem`;
		`echo "PSK250" > $ENV{HOME}/.pskmail/.rxmodem`;
		`echo "8" > $ENV{HOME}/.pskmail/.txmodenumber`;
		`echo "8" > $ENV{HOME}/.pskmail/.rxmodenumber`;
	} else {
		`echo "PSK500R" > $ENV{HOME}/.pskmail/.rxid`;
		`echo "PSK500R" > $ENV{HOME}/.pskmail/.txmodem`;
		`echo "PSK500R" > $ENV{HOME}/.pskmail/.rxmodem`;
		`echo "6" > $ENV{HOME}/.pskmail/.txmodenumber`;
		`echo "6" > $ENV{HOME}/.pskmail/.rxmodenumber`;
	}

#}

settxrsid(0);
setrxrsid(1);
######################## parameter dialog for puppy version ###########
if ($Iamserver) {

	print "\n### Server v. $Version. (C) 2013 PA0R\n";

	if ($ServerCall eq "N0CAL") {
		print "\n### Please configure the server by answering some questions\n";
		print "### You can make your parameters permanent by editing the file pskmailrc.pl\n";
		print "### Use the geany or gedit editor (or vi) for that.\n\n";
		while (1) {
			print "\nWhich callsign do you want to use? ";
			$ServerCall = uc(<STDIN>);
			chomp $ServerCall;
			print "What is the address of your SMTP server? ";
			$relay = <STDIN>;
			chomp $relay;
			print "Your latitude (decimal degrees): ";
			my $lat = <STDIN>;
			chomp $lat;
			print "Your longitude (decimal degrees): ";
			my $lon = <STDIN>;
			chomp $lon;
			print "Your beacon message: ";
			my $positmessage = <STDIN>;
			chomp $positmessage;

			my $lat_sign;
			my $lon_sign;
			if ($lat < 0) {
				$lat = abs $lat;
				$lat_sign = "S";
			} else {
				$lat_sign = "N";
			}
			if ($lon < 0) {
				$lon = abs $lon;
				$lon_sign = "W";
			} else {
				$lon_sign = "E";
			}

			$aprs_lat = (($lat - int($lat)) * 60) + int($lat)* 100;

			if (int($lat) != 0 && abs($lat) < 10.0) {
				$aprs_lat = "0" . $aprs_lat;
			} elsif (int($lat) == 0) {
				$aprs_lat= "000" . $aprs_lat;
			} else {
				$aprs_lat = $aprs_lat;
			}

			if ($aprs_lat =~ /(\d\d\d\d\.\d\d)/) {
				$aprs_lat = $1;
			} elsif ($aprs_lat =~ /(\d\d\d\d\.\d)/) {
				$aprs_lat = $1 . "0";
			} elsif ($aprs_lat =~ /(\d\d\d\d)/) {
				$aprs_lat = $1 . ".00";
			} else {
				$aprs_lat = $1 . "0000.00";
			}


			$aprs_lon = (($lon - int($lon)) * 60) + int($lon)* 100;
			if (int($lon) == 0) {
				$aprs_lon = "00000" . $aprs_lon;
			} elsif (int($lon) < 10) {
				$aprs_lon = "00" . $aprs_lon;
			} elsif (int($lon) < 100) {
				$aprs_lon = "0" . $aprs_lon;
			} else {
				$aprs_lon =  $aprs_lon;
			}

			if ($aprs_lon =~ /(\d\d\d\d\d\.\d\d)/){
				$aprs_lon = $1;
			} elsif ($aprs_lon =~ /(\d\d\d\d\d\.\d)/){
				$aprs_lon = $1 . "0";
			} elsif ($aprs_lon =~ /(\d\d\d\d\d)/) {
				$aprs_lon = $1 . ".00";
			} else {
				$aprs_lon = "00000.00";
			}
			$Aprs_beacon = $aprs_lat . $lat_sign . "/" . $aprs_lon . $lon_sign . "&" . $positmessage;
			print "\nUsing the following data:\n";
			print "Server call: ", $ServerCall, "\n";
			print "SMTP server: ", $relay, "\n";
			print "APRS beacon: ", $Aprs_beacon, "\n\n";
			print "Is that correct? (y/n) [y]\n";
			my $a = <STDIN>;
			chomp $a;
			if ($a eq "y" || $a eq "") {
				print "\n";
				last;
			}
		}
	}
	`echo "$ServerCall,$relay,$Aprs_beacon" > $ENV{HOME}/.pskmail/.manual_config`;
}

 $TxInputfile = "ENV{HOME}/.pskmail/TxInputfile";
 ############ end config #####################

if ($Iamserver && -e "$ENV{HOME}/.pskmail/id_defined") {
	unlink "$ENV{HOME}/.pskmail/id_defined";
}

 $TxInputfile = "$ENV{HOME}/.pskmail/TxInputfile";

my $ClientCall = "";
my $CallerCall = "";
my $Call = "";
my $hiscall = "";
my %Owned_list = ();

getlinks();

logprint("Program start\n");

###################### constants ###############################
my $Conreq = "c";
my $Conack = "k";
my $Statreq = "s"; # used to be 'p'
my $Statrprt = "s";
my $Conid = "i";
my $Disreq = "d";
my $Disack = "b";
my $Abort = 'a';
my $Unproto = 'u';

my $Streamid = "0";
my $Current_session = "0";
my $InputString = "";
my $Sessionnumber = 0;

my $HeaderStart = " ";
my $FrameEnd = " ";
my $Bufferlength = 64;
$Framelength = 17;
my @scancls = ();

###################### status variables #######################
my $Modem = "PSK500R";		#
my $newmodem = "";
my $oldmodem = "";
my $Blockindex = 5;			# Data block length (2^x)
my $MaxDataBlocks = 8;		# Max. number of data blocks to be sent
 $Lastblockinframe = 0;	# Flag for last block in frame
my $Idle_counter = $Idle_init;		# seconds from last <SOH>
my $olderror = "";			# stores first missing block index
my $Connect_time = 0;		# connect time in seconds
my $Interval_time = 0;		# 500 seconds interval
my $InputLine = "";			# Input from gmfsk
my $TxFlag = 0;				# TX on
my $ConnectFlag = 0;		# Station is connected
my $linkquality = 0;		# Nr. of missing blocks for link quality
my $payloadlength = 32;		# Average length of payload received
my $max_idle = 17;			# Dynamic timing slot initial value
#tie %memreceive, "DB_File", "$ENV{HOME}/.pskmail/.arq_memreceive.dbm" or die "Can't open .arq_memreceive:$!\n";
#tie %aprs_store, "DB_File", "$ENV{HOME}/.pskmail/.aprsposits.dbm" or die "Can't open .aprsposits:$!\n";
							# hashes for storing previous receive data and aprsposits
my $lastpolldata = "";		# last poll data received correctly
my $laststatusdata = "";	# last status data received
my $last_eot = 0;			# was last modem character an <eot> ?
my $prediction = "";		# predicted status string from client
my $garbage = "";			# last 64 bytes of raw data from the modem
my $reason = "Already connected";	# reason for reject
my $rejectcall = "";				# call for reject
my %Myroutes = ();			# stations linked to this server
#Auto timing
my $connect_cps = 0;		# Characters per second of connect frame as received (assume is constant during session)
#Asymmetric link
my $asymlink = 0;
my $rxs2n = 50;			#start with middle of the road number (will be averaged with new s2n measures)
my $hisrxs2n = 50;		#start with middle of the road number (will be averaged with new s2n measures)
my $workingfreq = 0;
my $cw_accu = "";


######### my status
my $Firstsent = 0;			# First block  I sent last turn
my $Lastblock = 0;			# Last block I sent last turn
my $Endblock = 0;			# Last  I received o.k.
my $Goodblock = 0;			# Last block I received conseq. o.k, 1st in send queue
my $Lastqueued = 0;			# Last block in my send queue
my $TXServerStatus= "";
my $ServerStatus = ""; 		# Listen, Connect_req, Disconnect_req, Abort_req
my @Missing = ();			# List of repeat requests
my $MissString = "";		# List of repeat requests
my $ReceivedLastBlock = 0;	# Flag for end of frame
my $b_array = "";
my $Current_session_sent = "0";
my $Lastsent = " ";
my $Hislastsent = " ";
##################

######### his status
my $HisGoodblock = 0;		# Other station's Good block
my $HisLastblock = 0;		# Other station's Block last sent
my $HisEndblock = 0;		# Other station's last received block
my @HisMissing = ();		# Other station's missing blocks
###################

######### status of my rig
$rigptttune = 0 unless $rigptttune;
my $AutoTune = 1;		# If 1 then fq has changed and ATU need to tune antenna
###################

##################### queues ##################################
my $TxTextQueue = "";		# Text in from mail engine
my @Sendqueue = ();			# Array of data ready for sending
my $RxTextQueue = "";		# Text string
my @ReceiveQueue = ();		# Array of msg received ok.
my $TextOut = "";

my @options = ();
my $Latitude = "";
my $Longitude = "";
my %pingdb;
my $inputbytes = 0;
my $answer = "";

##################### APRS unproto stuff ######################
my %servers = (); # known servers
my ($host, $port, $line);
#my $host_out = $Aprs_address;
#my $port_out = $Aprs_port;
my %Messagehash;
my $lastmessage = "";
my $Maxlinktime = 36000;
my $unattended = 0;
my $gpsnr = 0;

getoptions();

$StartHeader = sprintf("%c", '1');
$FrameEnd = sprintf("%c%c", '4','10');
$BlockLengthStr= sprintf("%c", 5);
$BlockLength = (2 ** $Blockindex) ;
#Adaptative timing: timeout depends on client's blocklength, not server.
$ClientBlockLength = (2 ** 5); #Assumes it is max 2 ^ 5.

$wait_a_second = 0;

$Call = $ClientCall;

my $Startmessage = "";
if ($Iamserver) {
	$Startmessage .= "Program start: " . getdate() . "\n";
}

open ($logfh, ">>", $logfile) or die "Can not write to logfile!\n";
print $logfh $Startmessage;
close ($logfh);

if ($Iamserver == 0) {	# remove gps file if present
	if (-e ".gps") {
		unlink ".gps";
	}
}

if ($Iamserver && $Aprs_connect) {	#connect to aprs backbone


# fork off a handler for aprs...

    die "can't fork: $!" unless defined($kidpid = fork);

    if ($kidpid) {
		# don't do anything here....
		sleep 1;
    }    else {
	    my %RFout_list = ();
	    my $connectcount = 0;
	    my $aprs_is_connect_time = time;
##debug

		while (1) {
			my $cntr = 0;
		    # create a tcp connection to the specified host and port
		    while (1) {
				$connectcntr++;
				my $maxaprs = $#Aprs_address + 1;
				$maxaprs = 1 unless $maxaprs; ## fix for client
				$aprsinx = $cntr % $maxaprs;

			    eval {
			    	$handle_out = IO::Socket::INET->new(Proto     => "tcp",
			                                    	PeerAddr  => $Aprs_address[$aprsinx],
			                                    	PeerPort  => $Aprs_port[$aprsinx])
			           or die "can't connect to port $Aprs_port[$aprsinx] on $Aprs_address[$aprsinx]:";
			    };
			    if ($@) {
			    	print "Cannot connect...\n";
			    	sleep 10;
					$cntr++;
			    	if ($Aprs_address[$aprsinx + 1]) {logprint ("Reconnecting $cntr ($Aprs_address[$aprsinx + 1])\n");}
			    } else {
			    	$cntr = 0;
					$connectcntr = 0;
			    	logprint ("Connected to $Aprs_address[$aprsinx] port $Aprs_port[$aprsinx]\n");
			    	last;
			    }
			    sleep 10 * $connectcntr;;
		    }

					$passw = dohash($ServerCall);

					sleep 1;

		           print $handle_out "user $ServerCall pass $passw vers $Version filter u/PSKAPR t/m\n";

					sleep 10;

		    # split the program into two processes, identical twins
		    die "can't fork: $!" unless defined($kidpid = fork());

		    # the if{} block runs only in the parent process
		    if ($kidpid) {

		        # copy the socket and handle the aprs message
		        while (defined ($line = <$handle_out>)) {
		            if ($line) {
#		            	print STDOUT $line; ##DEBUG
		 				$line = filter_aprs ($line);

				        if ($line) {handle_aprs ($line);}
				        $line = "";
		            }
		            select undef, undef, undef, 0.01;

		        } # end while

		        kill("TERM", $kidpid);                  # send SIGTERM to child
		    }
		    # the else{} block runs only in the child process
		    else {
		        # copy standard input to the socket
		        while (1) {
		        	select undef, undef, undef, 0.01; # take some rest...
		            if (-e "$ENV{HOME}/.pskmail/.aprsmessage") {
		            	open ($fh, "$ENV{HOME}/.pskmail/.aprsmessage");
		            	$MSG = <$fh>;
		            	close ($fh);

		            	unlink ("$ENV{HOME}/.pskmail/.aprsmessage");
						eval {
							if ($MSG) {
								print $handle_out $MSG or die "Cannot print to aprs: $@";
							}
						};
						if ($@ && $@ =~ /Cannot prin/) {
							logprint ($@);
							last;  # reconnect...
						} else {
							if ($MSG) {
								logprint ("Send>APRS-IS:$MSG\n");
							} else {
								logprint ("No aprs message in file\n");
							}
						}
		            	$MSG = "";

		            }

		        }

		    }

		    sleep 20;
		}
##end debug


	} # end child

}

    die "can't fork: $!" unless defined($msgqueuepid = fork);

    if ($msgqueuepid) {
 ## read the scan calls
			if (-e "$ENV{HOME}/.pskmail/qrg/scancalls") {
				my $scancalls = `cat $ENV{HOME}/.pskmail/qrg/scancalls`;
				@scancls = split (",", $scancalls);
			}

    } else {		# message queue child...
		if (-e "$ENV{HOME}/.pskmail/id_defined") {
			exit;
		}

#Auto timing
my $frame_length;
my $cps = 0;
my $frame_duration;
#END Auto timing

my $haveeot = 0;
my $newrxmode = "PSK500R";
my $DCDnow = gettimeofday();
my $DCDtime = $DCDnow;
my $DCDflag = 0;
my $DCDcurrent = 0;
my $Blockline = "";
my $Blockarray = ();
my $prediction = "";		# predicted status string from client
my $haves2n = 0;
my $last_eot = 0;
my $soh_time = gettimeofday();
my $eot_time = gettimeofday();
#my $PTTCOMMAND = 0;
my $PTTstatus = 0;
my $rxmodem = "PSK500R";
my $Sqldelay = 0;
my $bytes = "";
my $garbage = "";
my $fldigi;
my $firstbyte = -1;


`touch $ENV{HOME}/.pskmail/id_defined`; # only start 1 copy
$debug2 = 0;

while (1) {
	
	if ($fldigi){
		;
	} else {
		while (1) {
			print "waiting for the modem...\n";
			
			eval {
				
				$fldigi = new IO::Socket::INET (
			                                  PeerAddr => 'localhost',
			                                  PeerPort => $fldigiport,
			                                  Proto => 'tcp',
			                                 );
				die "Could not create socket: $!\n" unless $fldigi;
			
				$fldigi->autoflush();
				$fldigi->blocking(0);
				print "modem o.k., port is $fldigiport\n";
				
			};
			if ($fldigi) {
				last;
			}

			sleep 30;
		} 
	}

			if ($first_instance == 0) {
				print "Second instance\n";
			}
			eval { 
				print $fldigi "<cmd>server</cmd>\n";
			}; warn $@ if $@;
	

	sleep 1;


	#Control Receive RSID from the server (requires Fldigi version 3.13 and above).

	eval { 
		print $fldigi "<cmd><rsid>ON</rsid></cmd>\n";
	}; warn $@ if $@;
	

	sleep 1;

	print "initialized\n";

	sleep 1;
print "start of receive loop\n";
	my $rc = 0;
	my $CTSTIA = "";
	
$debug = 0;

	while ($fldigi) {
=head		
		if (-e "$ENV{HOME}/.pskmail/.ctstiamode") {
			$CTSTIA = `cat $ENV{HOME}/.pskmail/.ctstiamode`;
		}	
=cut		
		if (-e "$ENV{HOME}/.pskmail/.tosend") { 
			my $ts = "";
			my @tsnd = ();
			
			eval {
				open ($fhsnd, "$ENV{HOME}/.pskmail/.tosend") 
						or die "can't open sendfile: $!";
						
				@tsnd = <$fhsnd>;

				close $fhsnd;
				
				unlink "$ENV{HOME}/.pskmail/.tosend";
				
				$ts = join ('', @tsnd);
				
				if ($debug2) { print "in tosend:", $ts, "\n";}
			};
						
#			print "in tosend:", $ts, "\n";
		
			my $stxcnt = 0;
			
			while (-e "$ENV{HOME}/.pskmail/.stxflag") {
#				print "waiting for fldigi\n";
				select undef, undef, undef, 0.1;
				$stxcnt++;
				$rc = sysread($fldigi,$data,1);
				if ($rc && $rc == 1 && ord($data) == 2) {
#					print "got STX\n";
					`rm $ENV{HOME}/.pskmail/.stxflag`;
					$stxcnt = 0;
					last;
				}
				if ($stxcnt > 50) {  # wait max. 5 seconds
					$stxcnt = 0;
					print "this arq.pm is for fldigi-3.21.83 and up.... need to upgrade?\n";
					last;
				}
			}
			
			eval {
				$bw = syswrite $fldigi, $ts 
						or die "can't write to fldigi: $!";	
			};
			
			if ($ts =~ /<cmd><mode>/) {  ## <STX> handshake...
				`touch $ENV{HOME}/.pskmail/.stxflag`;
#				print "setting .stx flag\n";
			}			
			
					if ($@) {
			if ($debug2){			print ("fldigi write error:", $@,)  ;}
					} else {
			if ($debug2){print "Sending from rx loop: $ts\n";}
					}
		    if (defined $bw) {
if ($debug2) {							printf ("written $bw bytes\n");	}			
			} else {
				print ("fldigi write failed\n");
			}		

		}  # end .tosend handling
		
		my $rc = 0;
	
		if (length($b_array) > 0) {
			$data = substr($b_array, 0, 1);
			$rc = 1;
			$b_array = substr($b_array, 1);
#print "B_ARRAY:", $b_array, "\n";
		} else {
	
			eval { 
				$rc = sysread($fldigi,$data,1);
			
			}; warn $@ if $@;
			 
#			if ($rc) {print "DATA_IN:", $data, "\n";}			
			
			if ($rc) {
				$accu .= $data;
			
				if (length($accu) > 21) {
					$accu = substr($accu, -21);	

					$check_st = ".s([^a-z]{3})";
					$check_st2 = "([^a-z]{3})[0-9A-F]{4}<EOT>";
#print $accu, "\n";	
						
					if ($accu =~ /$check_st/) {
						$st_check = $1;
#						print "CAUGHT:|", $1, "|\n";
					}
					if ($accu =~ /$check_st2/) {
						$st_check = $1;
#						print "CAUGHT:|", $1, "|\n";
					}					
						if (length($st_check) > 2 && -e "$ENV{HOME}/.pskmail/.stream") {
							my $lsession = `cat $ENV{HOME}/.pskmail/.stream`;
							$lsession = substr($lsession, -1,1);
							chomp $lsession;
#							print "PREDICTION=|", $st_check, "|\n";	
							if (substr($st_check, 1,1) eq substr($st_check, 2,1)) {
#								print "SENDING:|", $st_check, "|\n";
#								open ($FG, ">$ENV{HOME}/.pskmail/.prediction4");
#								print $FG $st_check;
#								close $FG;
								`echo "100" > $ENV{HOME}/.pskmail/.rxs2n`;
								`echo "100" > $ENV{HOME}/.pskmail/.servers2n`;
							} 
							$st_check = "";						
							$accu = "";
						}							
					 						
				}
			}

			if ($rc) {
				
				my $c = ord($data);
				
=head
#print "CTSTIAMODEM:", getrxmodem(), "\n";
				
				if ($c != 6 && (getrxmodem() eq "Contestia" || $CTSTIA_mode || $CTSTIA eq "CTSTIA" )) {	
				
#					print $data;
#print "CHAR:", $c, "\n";
					if ($c > 64 && $c < 73) {  #first byte
						$firstbyte = $c - 65;
#print "FIRST:", $c + 0, " =", $firstbyte,"\n";						
						$data = "";

					}
					if ($c > 73 && $c < 91 && $firstbyte > -1) {  # second byte ok, first byte ok
							$c -= 74;
							$data = sprintf("%c", $c + $firstbyte * 16);
#print "SECOND:", $c + 0, " = ", $data, "\n";
							$firstbyte = -1;
					} elsif ($c > 73 && $c < 91 && $firstbyte == -1) {		# wrong first byte, wrong second byte
							$data = sprintf("%c", 24);
							$firstbyte = -1;										
					} elsif ( $firstbyte > -1 && $c < 73 && $c > 91) {		# right first byte, wrong second byte
							$data = sprintf("%c", 24);	
							$firstbyte = -1;
					} 
					print $data;					
				} elsif (getrxmodem() eq "CW"){
=cut					
				if (getrxmodem() eq "CW"){	

					if ($c > 64 && $c < 92 || $c > 46 && $c < 62 || $c == 32) {
						if (length($cw_accu) > 150) {
							$cw_accu = substr($cw_accu, -150);
						}
						$cw_accu .= sprintf("%c", $c);	

						if ($cw_accu =~ /([A-Z0-9]+)\/([A-Z]{2}[0-9]{2}[A-Z]{2})\/([A-Z0-9]+)\/(\d+)([A-Z]).*/){
							print "\nCALL:",$1, "\n";	
							print "POS:",$2, "\n";
							print "COMMENT:",$3, "\n";
							print "nr:", $4, "\n";
							print "ICON:", $5, "\n";
							$stringlength = length($1) + length($2) + length($3) + 3;
							if ($stringlength != $4) {
								$answer = $1 . " NN\n";
							} else {
								my $comment = lc($3);
								$comment = ucfirst($comment);
								$answer = $1 . " RR\n";
								sleep 1;
								if ($first_instance) {
									sendstuff($answer);
								}
								
								$icn = "Q";
								if ($5) {
									$icn = $5;
								}
								$cw_accu = "";
								$Message = "!" . extractLat($2) . "/" . extractLon($2) . $icn . $comment. "\n";
								$MSG = $1 . ">PSKAPR,TCPIP*:$Message";
								print $MSG;
								aprs_send ($MSG);
							}
							$MSG = "";							
						} elsif ($cw_accu =~ /([A-Z0-9]+)\/25\/([A-Z0-9]+)\/(\w+)\/([A-Z]+)\/(.*)\/(\d+)NNNN.*/) {
#									$RxTextQueue .= "~MSG " . $ucl . " " . $payload3 . "\n";
#									PA0R/25/PER/CRUSEFALK/SE/ALL OK ON BOARD/41NNNN
														
							print "\nCALL:",$1, "\n";	
							print "ADDR:",$2, "\n";
							print "ISP:",$3, "\n";
							print "ISP:", $4, "\n";
							print "MESSAGE:", $5, "\n";
							print "nr:", $6, "\n";
							if (length($1) + length($2) + length($3) + length($4) + length($5) + 8 == $6) {
								$RxTextQueue .= "~MSG " . $1 . " " . $2 . "@" . $3 . "." . $4 . " " . $5 . "\n";
								open (RMSG, ">$ENV{HOME}/.pskmail/.mailmessage");
								print RMSG $RxTextQueue;
								close RMSG;
							}
							$cw_accu = " ";
							
						}
					} elsif ($c == 45) {
						$cw_accu = "";
					}
				} else {
						$cw_accu = "";
				}
			}
		}
	

		if (defined $rc) { # non-error
#print "DEFINED\n";			
			if ($rc > 0) { # read successful
#DEBUG
my $cpdata = $data;
#print "DATA:" . $data;
$cpdata =~ s/(['"])/\\$1/g;
#print $data, $cpdata, "\n";

=head
				if ($rc > 0  && ord($data) == 7) {

					my $pred = "";
					if (-e "$ENV{HOME}/.pskmail/.prediction2"){
						$pred = `tail -n 1 ~/.pskmail/.prediction2`;
						`rm ~/.pskmail/.prediction2`;
					}
					chomp $pred;
#					print "PREDICTION:", $pred, "|\n";
			
					my $str = sprintf("v%s", $pred) ;
					my $check = checksum ($str);
					my $out = "<SOH>" . $str . $check . "<EOT>";
					$b_array = sprintf ("%cv%s%s%c", 1,  $pred, $check, 4);
				
					if ($PTTCOMMAND) {
						$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "T" 0`;
					}					

#					print "MYOUT:", $out, "\n";	
					$data = "";	
									
				} els
				
=cut			
## handle <STX> flag
				if ($rc > 0 && ord($data) == 2) {
#					print "<STX> received\n";
					if (-e "$ENV{HOME}/.pskmail/.stxflag") {
						`rm $ENV{HOME}/.pskmail/.stxflag`;
#						print "killing <STX>\n";
					}
				}
	
				if ($rc > 0  && ord($data) == 6) {
#print "\nACK!!\n";	
					#Asymmetric linkup - reset mode to receiving mode, (re)adjust timings
					my $rxmodem = getrxmodem();
#print "MYRXMODEM=" , $rxmodem, "\n";					
					if ($rxmodem ne "default" ) {
						if (-e "$ENV{HOME}/.pskmail/.tx.lck") {
							`rm "$ENV{HOME}/.pskmail/.tx.lck"`;
						}
						sendmode($rxmodem);
						
#print "SENDMODE ", $rxmodem	, "\n";		
			
					} else {
#print "NOSENDMODE ", $rxmodem	, "\n";						
					}
					
					if (-e "$ENV{HOME}/.pskmail/.tx.lck") {`rm "$ENV{HOME}/.pskmail/.tx.lck"`;}
									
#					if (-e "$ENV{HOME}/.pskmail/.tx.lck") { unlink "$ENV{HOME}/.pskmail/.tx.lck"; }

					if (-e "$ENV{HOME}/.pskmail/.txing") {
						unlink "$ENV{HOME}/.pskmail/.txing";
						`echo "%D" > $ENV{HOME}/.pskmail/.bigear`;
						$PTTstatus = 0;
					}
										
#					$txing = 0;


					if ($PTTCOMMAND) {
						sleep 1; # 5 second tail
						if ($rigtype ne "2") {
							$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "T" 0`;
						} else {
							$dummy = `rigctl -m $rigtype  "T" 0`;
						}
					}

					if (-e "$ENV{HOME}/.pskmail/.txing") {
						`rm $ENV{HOME}/.pskmail/.txing`;
						`echo "%D" > $ENV{HOME}/.pskmail/.bigear`;
						$PTTstatus = 0;
					}

					if (-e "$ENV{HOME}/.pskmail/.prediction") {
						$prediction = `cat $ENV{HOME}/.pskmail/.prediction`;
						chomp $prediction;
						`rm $ENV{HOME}/.pskmail/.prediction`;
					}

					if (-e "$ENV{HOME}/.pskmail/squelch.lk") { 
						unlink "$ENV{HOME}/.pskmail/squelch.lk";
					}
					#Adaptative timing, remove .soh and .eot files when returning to RX
					if (-e "$ENV{HOME}/.pskmail/.soh") { 
						unlink "$ENV{HOME}/.pskmail/.soh";
					}
					if (-e "$ENV{HOME}/.pskmail/.eot") { 
						unlink "$ENV{HOME}/.pskmail/.eot";
					}

				} else {
					# handle a successful read
#					$txing = 0;  #cannot be txing....
					if (ord($data) > 0) {
#						print "DAT:" . $data . "\n";
						$bytes .= $data;					
						$bytes = str_parser($bytes);
	
						$garbage .= $data;
						$garbage = handlegarbage ($garbage);
#print "\n", $garbage, "\n";
						$data = "";
					}
				}
			} else { # end of file close SOCK;
			print "Closing socket\n";
#				`echo "Closing fldigi socket\n" >> "$ENV{HOME}/.pskmail/fldigi_errortest.txt"`;
				close ($fldigi);
			}
		} elsif ($! == EWOULDBLOCK) { # got a would block error
			# handle blocking, probably just by trying again
#print "EWOULDBLOCK\n";
		} else { # unexpected error
			print "sysread() error: $!\n";
#DEBUG
			last;
#			die "sysread() error: $!";
		}

		if (-e "$ENV{HOME}/.pskmail/.input") {
#if ($debug2) {			print ".input present\n";}
		} elsif (scalar(@Blockarray) > 0) {
			my $outline = pop @Blockarray;
			my $to_input = "";
#print "OUTLINE=", $outline, "\n";
			if ($outline =~ /(<SOH>.+<EOT>)/s) {
				$to_input = $1;
				$squelch = 0;

			} elsif ($outline =~ /(<SOH>.+<SOH>)/s) {
				$to_input = $1;
			}
#print "1=", $to_input, "\n";
			if ($to_input){
				open (INP, ">". "$ENV{HOME}/.pskmail/.input");
				print (INP $to_input, "\n");
				close (INP);
			}

		}
=head
		if (-e "$ENV{HOME}/.pskmail/.tosend") {
			my $ts = "";
			my @tsnd = ();
			
			eval {
				open ($fhsnd, "$ENV{HOME}/.pskmail/.tosend") 
						or die "can't open sendfile: $!";
						
				@tsnd = <$fhsnd>;

				close $fhsnd;
				
				unlink "$ENV{HOME}/.pskmail/.tosend";
				
				$ts = join ('', @tsnd);
				
				if ($debug2) { print "in tosend:", $ts, "\n";}
			};
						

			
#			if ($txing == 0) {$txing = 1;}

			eval {
#				print $fldigi $ts 				
#					or die "can't write to fldigi: $!";
				$bw = syswrite $fldigi, $ts 
						or die "can't write to fldigi: $!";
			};
					if ($@) {
			if ($debug2){			print ("fldigi write error:", $@,)  ;}
					} else {
			if ($debug2){print "Sending from rx loop: $ts\n";}
					}
		    if (defined $bw) {
if ($debug2) {							printf ("written $bw bytes\n");	}			
			} else {
				print ("fldigi write failed\n");
			}
		

#					sleep 1;
		}
	
		while (-e "$ENV{HOME}/.pskmail/.tosend") {
			`rm $ENV{HOME}/.pskmail/.tosend`;
			select undef, undef, undef, 0.01;
		}
		
#		print "tosend killed\n";
=cut
		select undef, undef, undef, 0.01;
#print "END\n";
	}
	
	print "end of receive loop\n";
	close($fldigi);
=head
	sleep 1;
	print "start modem agn\n";
	if (-e "$ENV{HOME}/jpskmail/V2Modem.jar"){
		print "starting modem\n";
		system ("/usr/local/bin/startmodem.sh");
	}
=cut
}

print "exiting message queue\n";

 exit;

1;
}

###########################################################

###########################################################
sub newsession {	# new session number
###########################################################
	$SessionNumber = ord($Streamid) - 32;
	++$SessionNumber;
	if ($SessionNumber > 63) { $SessionNumber = 0;}
	$Streamid = sprintf ("%c", $SessionNumber + 32);
	$Current_session = $Streamid;
print "NEWSESSION:" . $Current_session, "\n";
	`echo $Streamid > $ENV{HOME}/.pskmail/.stream`;
	return $Streamid;
}

###########################################################
sub newblocknumber { # get new blocknumber
###########################################################

	$Lastqueued++;

	if ($Lastqueued >= $Bufferlength) {
		$Lastqueued -= $Bufferlength;
	}

	return $Lastqueued ;

}

###########################################################
sub makeindex { # make transmitable index from number
###########################################################
my $index = shift @_;
my $character = "";

$character = sprintf ("%c", ($index % $Bufferlength) + 32);

return $character;

}

###########################################################
sub contime {		# UTC time of this connect
###########################################################
#	(my $sec, my $min, my $hr) =gmtime;
#	$Contime = sprintf ("%02d:%02d", $hr, $min);

}
############################################################
sub checksum {		# Checksum of header + block
############################################################
# Time + password + header + block
############################################################

my $String = shift (@_);
my $Encrypted = "0000" . sprintf ("%X", crc16($String));

	return substr ($Encrypted, -4);

}

############################################################
sub newconnectblock {	# Connect (client:port, server:port)
# c block = Client:port Server:port <streamnr. (0)> <max. blocklen>
# e.g.: '00cEA3FG:1024 PI4TUE:24 4'
############################################################
	my $server = shift @_;
	my $Call = shift @_;

	if (exists $myservers{$server}) {
		$proto = 0;
	} else {
		$proto = 0;
	}

	$Blockindex = 6;
	$BlockLength = (2 ** $Blockindex) ;

	$Connect_time = 0; # start timer
	if ($proto == 0) {
		return ("0"
		. "0"		#Streamid
		. $Conreq
		. $Call
		. ":1024 "
		. $server
		. ":24 "
#		. $BlockLengthStr);  ## W.I.P transfer protocol...
		. "67653");
	} elsif ($proto == 2) {
		my $shortcall = "";
		my $shortserver = "";

		$shortcall = shortcall($ClientCall);
		$shortserver = shortcall($server);



		if ($shortcall && $shortserver) {
			return ("2"
			. "0"		#Streamid
			. $Conreq
			. $shortcall
			. $shortserver
			. $BlockLengthStr);
		} else {
			return ("0"
			. "0"		#Streamid
			. $Conreq
			. $ClientCall
			. ":1024 "
			. $server
			. ":24 "
			. $BlockLengthStr);
		}
	}

}

############################################################
sub newtransferconnectblock {	# Connect (client:port, server:port)
# c block = Client:port Server:port <streamnr. (0)> <max. blocklen>
# e.g.: '00cEA3FG:1027 PI4TUE:27 6653'
############################################################
	my $server = shift @_;
	my $Call = shift @_;
	my $adressee = shift @_;
	my $modelist = $transfermodelist;
	

	$proto = 0;

	$Blockindex = 6;
	$BlockLength = (2 ** $Blockindex) ;

	$Connect_time = 0; # start timer
	if ($proto == 0) {
		return ("0"
		. "0"		#Streamid
		. $Conreq
		. $Call
		. ":1027 "
		. $server
		. ":27 "
		. $adressee
		. " "
		. $modelist);
	} 
}

############################################################
sub ttyconnectblock {	# Connect (caller:port, my:port)
# c block = Caller:port My:port <streamnr. (0)> <max. blocklen>
# e.g.: '00cEA3FG:87 PI4TUE:87 4'
############################################################
	my $ServerCall = shift @_;
	my $Call = shift @_;

	$Connect_time = 0; # start timer
	return ("0"
	. "0"	#Streamid
	. $Conreq
	. $Call
	. ":87 "
	. $ServerCall
	. ":87 "
	. $BlockLengthStr)
}
###############################################################
sub newackblock {		# Connect acknowledge (server:port, client:port)
# k block = Server:port Client:port <streamnr.> <max. blocklen>
# e.g: '00kPI4TUE:24 EA3FG:1024 8'
###############################################################
	my $server = shift @_;
	my $Call = shift @_;
	my $ro = "";

		my $time = time();
		my $pre = $server . " " . $time . "\n";
		$Routes{$Call} = $pre;
		open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
		foreach $key (keys %Routes) {
			$value = $Routes{$key};
			$ro = $ro . $key . " " . $value . "\n";
		}
#		print $ro;
		print ROUTES $ro;
		close (ROUTES);

		$Connect_time = 0; # start timer
		$Current_session_sent = $Streamid;
#		print "STREAMID SENT:", $Current_session_sent, "\n";

		return ("1"
		. $Streamid
		. $Conack
		. $server
		. ":24 "
		. $Call
		. ":1024 "
		. $BlockLengthStr);

}
###############################################################
sub ttyackblock {		# Connect acknowledge (server:port, client:port)
# k block = Server:port Client:port <streamnr.> <max. blocklen>
# e.g: '00kPI4TUE:87 EA3FG:87 4'
###############################################################
	my $caller = shift @_;
	my $Call = shift @_;

	$Connect_time = 0; # start timer

	return ("0"
	. $Streamid
	. $Conack
	. $ClientCall
	. ":87 "
	. $CallerCall
	. ":87 "
	. $BlockLengthStr);
}
###############################################################
sub rejectblock {		# connect rejected
# r block = 00r<clientcall>:<reason>
# e.g.: '00rDJ0LN:already connected'
###############################################################
(my $call, my $reason) = @_;

return "00r" . $call . ":" . $reason;
}
###############################################################
sub pollblock {		# poll
#p frame = <last block tx><last block rx ok><last block rx> <missing blocks>
#e.g.: '00pXHCAB'
###############################################################

	my $modenumber = 0;
	my ($MyLast, $Good, $Last, @RptBlocks) =  @_;

	my $MyLastblock = sprintf ("%c", $MyLast + 0x20);

	my $thisgoodblock = sprintf("%c", $Goodblock + 32);
	my $thisLastblock = sprintf("%c", $Endblock + 32);

	my $misses = join('', @RptBlocks);

	$modenumber = getrxmodenumber();

	$modenumber += 0;



	my $mnr = sprintf ("%c", $modenumber + 48);



	return ($mnr   # set modenumber
	. $Streamid
	. $Statreq
	. $MyLastblock
	. $thisgoodblock
	. $thisLastblock
	. $misses
	);

}

################################################################
sub statreport {		# Status report (End, Good, Lastrx, Missing)
#p frame = <last block tx><last block rx ok><last block rx> <missing blocks>
#e.g.: '00sXHCAB'
################################################################
	my ($MyLast, $Good, $Last, @RptBlocks) =  @_;
	
	$Lastsent = sprintf ("%c", $MyLast + 0x20);
	$Hislastsent = sprintf("%c", $Last + 32);
	
	my $modenumber = 0;
	my $MyLastchar = sprintf ("%c", $MyLast + 0x20);
	my $Goodchar = sprintf ("%c", $Goodblock + 32);
	my $Lastchar = sprintf("%c", $Endblock + 32);

	my $misses = join('', @RptBlocks);

	my $HisLastchar = sprintf("%c", $HisLastblock + 32);
	$prediction = $Streamid . "s" . $HisLastchar . $MyLastchar . $MyLastchar;
	

	open (POUT, ">$ENV{HOME}/.pskmail/.prediction");
	print POUT $prediction, "\n";
	close POUT;
	open (POUT, ">$ENV{HOME}/.pskmail/.prediction2");
	print POUT $prediction, "\n";
	close POUT;	

#Remove the last created prediction file otherwise it will get used the first time there is a timeout
	if (-e "$ENV{HOME}/.pskmail/.stat") {
		`rm $ENV{HOME}/.pskmail/.stat`;
	}

	$modenumber = getrxmodenumber();

	$modenumber += 0;

	my $mnr;

	if (-e "$ENV{HOME}/.pskmail/filetransfer") {
		my $s2n = `cat $ENV{HOME}/.pskmail/.servers2n`;
		chomp $s2n;
		$s2n = sprintf "%.0f", $s2n;
		if (int ($s2n + 0) > 99) {
			$s2n = 100;
		}
		my $usedblocks = length($misses) + 16;
		my $quality = int (($s2n * 16 / $usedblocks) * 90/100) + 32;
		$mnr = sprintf ("%c", $quality);		
#		$mnr = "z";
	} else {
		$mnr = sprintf ("%c", $modenumber + 48);
	}


	return ($mnr   # set modenumber
	. $Streamid
	. $Statrprt
	. $MyLastchar
	. $Goodchar
	. $Lastchar
	. $misses
	);
}



###############################################################
sub identblock {		# Identify (mycall, hiscall)
#i frame = '00iPI4TUE de PA0R'
###############################################################
	my ($Call, $hiscall) = @_;

	if ($Iamserver == 1) {
		return (
			"0"
			. $Streamid
			. $Conid
			. $hiscall
			. " de "
			. $Call);
	}else {
		return (
			"0"
			. $Streamid
			. $Conid
			. $Call
			. " de "
			. $hiscall);

	}

}

##############################################################
sub disconnectblock {		# Disconnect session
#d frame = ""
#e.g.: '00d'
##############################################################
	my $cs = `cat $ENV{HOME}/.pskmail/.stream`;
	$Current_session = chomp $cs;
#print "DISC:CURRENT", $Current_session, "\n";
	return (
	$Current_session
	. $Streamid
#	. $Current_session
	. $Disreq);

}

##############################################################
sub abortblock {		# Abort session
#a frame = ""
#e.g.: '00a'
##############################################################
	sendmodemcommand("<txrsid>ON</txrsid>");

	return (
	"0"
	. $Streamid
	. $Abort);

}

##############################################################
sub pingblock {		# e.g. Ping frame
# u block = From:port
# e.g: '00uPA0R:7 '
##############################################################

	if ($Iamserver == 0) {
		return (
		"0"
		. $Streamid
		. $Unproto
		. $Call
		. ":7 ");
	} else {		# I am a server, send a 71
		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}
		my $s2n = `cat $ENV{HOME}/.pskmail/.servers2n`;
		chomp $s2n;
		$s2n = sprintf "%.0f", $s2n;

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. ":71 "
		. $s2n . " ");

	}

}
##############################################################
sub beaconblock {		# Beacon frame
# u block = From:port  data
# e.g: '00uPA0R:72 Beacon text '
##############################################################
		my $date = getgmt();
		my $servermessage = "";

		if ($date =~ /\s(\d\d:\d\d:\d\d)/) {
			$date = $1;
		}

		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}

		if ($arqstatus) {
			$servermessage = $arqstatus;
		} else {
			$servermessage = $Version;
		}

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. ":72 "
		. "$servermessage\n - "
		. $date
		. " ");

}
#$MSG = "$ServerCall" . ">PSKAPR:" . "@" . $mytime . $Aprs_beacon ."\n";

##############################################################
sub positbeaconblock {		# Beacon frame
# u block = From:port  data
# e.g: '00uPA0R:72 Beacon text '
##############################################################
		my $date = getgmt();
		my $servermessage = "";

			my ($month, $hour, $min) = (gmtime) [3, 2, 1];
			$month = substr (("0" . $month), -2, 2);
			$hour = substr (("0" . $hour), -2, 2);
			$min = substr (("0" . $min), -2, 2);

			my $mytime = $month . $hour . $min . "z";

		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}

		if ($arqstatus) {
			$servermessage = $arqstatus;
		} else {
			$servermessage = $Version;
		}

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. ":26 !" . substr($Aprs_beacon, 0, 19));

}
##############################################################
sub ui_messageblock {		# UI email frame
# u block = From:port  data
# e.g: '00uPA0R:25 Message'
##############################################################
		my $message = shift @_;
#		if (length ($message) > 67) {
#			$message = substr($message, 0 , 67);
#		}

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $Call
		. ":25 "
		. "$message\n");

}
##############################################################
sub ui_aprsblock {		# UI aprs frame
# u block = From:port  data
# e.g: '00uPA0R:26 Message'
##############################################################
		my $message = shift @_;
#		if (length ($message) > 67) {
#			$message = substr($message, 0 , 67);
#		}

	if ($Iamserver) {

		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. ":26 "
		. "$message");

	} else {
		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $Call
		. ":26 "
		. "$message");
	}
}

##############################################################
sub aprs_linkblock {		# UI link frame
# block = Call><ServerCall
# e.g: '00uPA0R><PI4TUE'
##############################################################

	getoptions();

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $Call
		. "><"
		. $ServerCall
		. " ");
}
##############################################################
sub aprs_linkackblock {		# UI link ack frame
# block = Call><ServerCall
# e.g: '00uPI4TUE<>PA0R '
##############################################################
		sleep 1;

		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}

		return (
		"0"
		. "0"	#Streamid
		. $Unproto
		. $ServerCall
		. "<>"
		. $Call
		. " ");
}

##############################################################
sub linkcheckblock {
##############################################################
# '00uPI4TUE:10 PA0R '
	my $checkcall = @_;
	my $out = sprintf ("00%s:10 %s", $ServerCall, $checkcall);
	$out = makeblock ($out);
	sendit ($out);	
}

###############################################################
sub checkown {
###############################################################
	my $check = shift @_;
	if (exists $ownedlist{$check}) {
		return 1;
	} else {
		return 0;
	}
}

##############################################################
sub Queue_txdata {
##############################################################

my $Inpt = shift @_;

if ($Inpt) {
	$TxTextQueue .= $Inpt;
}

my $newqueuedblock = 0;
	my $qlength = $Lastqueued - $HisGoodblock;

# wipe the send queue
		if ($qlength == 0) {
			for (my $ii = 0; $ii < $Bufferlength; $ii++){
				$Sendqueue [$ii] = "";
			}
		}

	if ($qlength < 0) { $qlength += $Bufferlength; }

	if ($qlength > $Framelength) { return }; # next time better....

	while (length $TxTextQueue > 0 ) {

		$newqueuedblock = newblocknumber();

		$Sendqueue[ $newqueuedblock ] =  substr ($TxTextQueue, 0, $BlockLength);

 		$Lastqueued = $newqueuedblock;
		$qlength = $Lastqueued - $HisGoodblock;
		if ($qlength < 0) {
			$qlength += 64;
		}

		if (length $TxTextQueue >= $BlockLength) {
			$TxTextQueue = substr($TxTextQueue, $BlockLength);
		} else {
			$TxTextQueue = "";
			last;
		}
		if ($debug == 5){
			print $TxTextQueue, "\n";
		}


		last if ($qlength > $Framelength) ;

	}


	if ($debug == 5) {
		printf ("Lastqueued=%d, Queuelength=%d\n", $Lastqueued, $qlength);
	}
}


##############################################################
sub packdata {		# Add header
# <0x20...0x5f><Data>
# e.g.: '00jThis is data for'
##############################################################

	my $Currentblock = shift @_;
	my $Data = shift @_;

if ($Data) {
	return (
	"0"
	. $Streamid
	. $Currentblock
	. $Data);
}

}

#############################################################
sub make_block { #Adds SOH and checksum
# e.g.: '<SOH>00jThis is data for'akj0
#############################################################
	my $info = shift @_;
		if ($info) {
			my $check = checksum ($info);

			return ( $StartHeader . $info . $check);
		}
}

#############################################################
sub sendit {		# send routine
#############################################################
	my $sendstring = shift @_;
	my $counter = 0;

#print "SENDSTRING=", $sendstring, "\n";

	my $index = index ($sendstring, sprintf("%c", 0x04));
#print "INDEX=", $index, "\n";
	if ($index > 0) {


#Handles TX RSID switching here. Use flag set elsewhere
		if (gettxrsid() == 0) {
#			sendmodemcommand("<txrsid>OFF</txrsid>");
		} else {
			sendmodemcommand("<txrsid>ON</txrsid>");
		}

		if ($Txdelay == 0) {
			Time::HiRes::sleep(0.2);	#Minimum for the TX RSID switching above
		} else {
			sleep ($Txdelay);
		}

		if ($rigptttune == 1) {
			ptttune();	# initiate an autotune if required
		}


  		if ($Iamserver) {
  			$sendstring = "\n" . "\037" . $sendstring . "-----" ;
  		} else {
  			$sendstring = "\037" . $sendstring . "-----";
  		}

		my $mysession = 0;

		open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
		my $session = <SESSION>;
		chomp $session;
		close SESSION;

		if (($session ne "none" && $session ne "beacon") || $TXServerStatus eq "TXDisconnect")	{
			$mysession = 1;
		}

  		my $dcdsleeptimer = 0;	##software squelch

  		#DCD routine, works when not connected.
  		if ($mysession == 0) {
		  	while ($dcdsleeptimer < 10) {
#				print "sleeping:" . $dcdsleeptimer . "\n";
				if (-e "$ENV{HOME}/.pskmail/.DCD") {
					unlink "$ENV{HOME}/.pskmail/.DCD";
#					print "busy...\n";
					sleep 1;
				} else {
					my $rand = int rand(3); # Persistence, one out of 4...
#					print "not busy, rand=" . $rand . "\n";
					if ($rand == 1) {
						print "Message_out:" . $sendstring . "\n";
						$dcdsleeptimer = 10;
					} else {
						sleep 1;
					}
				}
				$dcdsleeptimer++;
#				print "timer update to " . $dcdsleeptimer . "\n";
			}
		}


		while (-e "$ENV{HOME}/.pskmail/squelch.lk") {
  			sleep 1;
  			$sleeptimer++;
  			if ($sleeptimer > 3) {
  				unlink "$ENV{HOME}/.pskmail/squelch.lk";
  				last;
  			}
		}

		#Asymmetric linkup: change mode before TX without adjusting timings
		#But only if not unproto frame
		if ($mysession) {
			if ($TXServerStatus eq "TXDisconnect") {
				$TXServerStatus = "";
			}
			my $temptxmodem = gettxmodem();
			if ($temptxmodem ne "default") {
				sendmodeNT($temptxmodem);

			}
		}
		##Moved variable init here
		my $maxtxtime = 30;

		while (-e "$ENV{HOME}/.pskmail/.tx.lck") {			
			$maxtxtime--;
			if ($maxtxtime < 1) {
				`rm "$ENV{HOME}/.pskmail/.tx.lck"`;
				last;
			}
			sleep 1;
		}

		my $TXmodem = gettxmodem();

#print "TXMODEMTEST:", $TXmodem, "\n";

			sendmode($TXmodem);
			
 		if ($TXmodem eq "PSK500" || $TXmodem eq "PSK1000") {
			$sendstring = $preamble . $sendstring;		
		} elsif ($TXmodem =~ /THOR/) {
			$sendstring = "----" . $sendstring;
		}
		
		
		

		$JK = 0;
		while (-e "$ENV{HOME}/.pskmail/.idcd") {
			sleep 1;
			print "Waiting...\n";
			reset_idle();
			$JK++;
			last if $JK > 10;
		}
		if (-e "$ENV{HOME}/.pskmail/.idcd"){
			`rm $ENV{HOME}/.pskmail/.idcd`;
		}

		open (FRE, "$ENV{HOME}/.pskmail/.band");
		my $srvfreq = <FRE>;
		close FRE;
		chomp $srvfreq;
		`touch $ENV{HOME}/.pskmail/.txing`;
		`echo "%U$srvfreq" > $ENV{HOME}/.pskmail/.bigear`;
		$PTTstatus = 1;


#print "PTT on\n";

		if ($PTTCOMMAND) {
			if ($rigtype ne "2") {
				$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "T" 1`;
			} else {
				$dummy = `rigctl -m $rigtype "T" 1`;
			}
			select undef,undef,undef, 0.1
		}

#print "SENDSTUFF:", $sendstring, "\n";
		sendstuff ($sendstring);
		
#if ($debug2) {		print "SENDIT\n";}

		`touch $ENV{HOME}/.pskmail/.tx.lck`;

		$sendstring = "";

		$Idle_counter = $Idle_init; # Activity, reset counter

	}

}
#############################################################
sub sendbulletin {		# send routine
#############################################################
	my $sendstring = shift @_;
	my $counter = 0;
	my $count = 0;
	my $bulletinout = "";

		my @bulletinlines = split "\n", $sendstring;
		push @bulletinlines, "NNNN\n";
		sendmodemcommand("<txrsid>ON</txrsid>");

		foreach $bulletinline (@bulletinlines) {
			if ($bulletinline =~ /NNNN/) {
				$count = 4;
			}

			$bulletinline .= "\n";
			$bulletinline = make_block ($bulletinline);
			$bulletinline .= "\004";
			$bulletinout .= $bulletinline ;
			$count++;
			if ($count > 3) {

				select (undef,undef,undef,0.1);
				$bulletinout .= "\000";
				while (-e "$ENV{HOME}/.pskmail/.tx.lck") {
					select (undef,undef,undef,0.1);
				}

				open (FRE, "$ENV{HOME}/.pskmail/.band");
				$srvfreq = "0";
				$srvfreq = <FRE>;				
				close FRE;
				if (defined $srvfreq) { 
					chomp $srvfreq;
				} else {
					 $srvfreq = "0";
				 }
				`touch $ENV{HOME}/.pskmail/.txing`;
				`echo "%U$srvfreq" > $ENV{HOME}/.pskmail/.bigear`;
				$PTTstatus = 1;

						if ($PTTCOMMAND) {
#							print "PTT on\n";
							if ($rigtype ne "2") {
								$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "T" 1`;
							} else {
								$dummy = `rigctl -m $rigtype  "T" 1`;
							}
						}
						sendmode($bulletinmode);
						select (undef,undef,undef,0.1);
#				msgsnd($txid, pack("l! a*", $type_sent, $bulletinout), 0 ) or die "# msgsend failed: $!\n";
				sendstuff ($bulletinout);

				$bulletinout = "";
				`touch $ENV{HOME}/.pskmail/.tx.lck`;
				$count = 0;
			}
		}

		sendmode ($Defaultmode);
}
#############################################################
sub sendamp {		# send routine
#############################################################
	my $sendstring = shift @_;
	my $counter = 0;
	my $count = 0;
	my $bulletinout = "";
	
#		`echo "%D$srvfreq" > $ENV{HOME}/.pskmail/.bigear`;
		sendmodemcommand("<txrsid>ON</txrsid>");	
		`touch $ENV{HOME}/.pskmail/.txing`;	
					
		sendstuff ($sendstring);

		while (-e "$ENV{HOME}/.pskmail/.txing") {
			;
		}
				open (FRE, "$ENV{HOME}/.pskmail/.band");
				$srvfreq = "0";
				$srvfreq = <FRE>;				
				close FRE;
				if (defined $srvfreq) { 
					chomp $srvfreq;
				} else {
					 $srvfreq = "0";
				}
				 		
		`echo "%D$srvfreq" > $ENV{HOME}/.pskmail/.bigear`;
				
		sendmode ($Defaultmode);
}
#############################################################
sub sendlongbulletin {		# send routine
#############################################################
	my $sendstring = shift @_;
	my $counter = 0;
	my $count = 0;
	my $bulletinout = "";
	settxrsid(1);

	if ($rigptttune == 1) {
		ptttune();	# initiate an autotune if required
	}

	my @bulletinlines = split "\n", $sendstring;
	push @bulletinlines, "NNNN\n";
	my $size = scalar @bulletinlines;

	foreach $bulletinline (@bulletinlines) {
		if ($bulletinline =~ /NNNN/) {
			$count = $size+1;
		}
		$bulletinline .= "\n";
		$bulletinline = make_block ($bulletinline);
		$bulletinline .= "\004";
		$bulletinout .= $bulletinline ;
		$count++;
		if ($count > 9) {
			$bulletinout .= "\000";
			while (-e "$ENV{HOME}/.pskmail/.tx.lck") {
				select (undef,undef,undef,0.1);
			}
			if ($PTTCOMMAND) {
#				print "PTT on\n";
				if ($rigtype ne "2") {
					$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "T" 1`;
				} else {
					$dummy = `rigctl -m $rigtype "T" 1`;
				}
			}
			
			sendmode($bulletinmode);
			
			select (undef,undef,undef,0.1);

			sendstuff ($bulletinout);
			$bulletinout = "";
			`touch $ENV{HOME}/.pskmail/.tx.lck`;
			$count = 0;
		}
	}
}

############################################################
sub send_frame { #send blocks in queue
# frame = [<id>]<data><data><stat><EOT>
############################################################
my $Payload = shift @_;
my $charindex = 0;
my $outstring = "";

if ($Payload) {$Payload =~ s/\r/\n/g;}

Queue_txdata($Payload);	##debug

#print "DISCON3:", $TXServerStatus ,"\n";

	if ($TXServerStatus eq "TXAbortreq") {			# send abort frame
		my $finfo = abortblock();
		$outstring .= make_block($finfo);

	} elsif ($TXServerStatus eq "TXPing") {
		$AutoTune = 1; #
		my $info = pingblock ();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXBeacon") {
        	$AutoTune = 1; # Always tune before beacon
		my $info = beaconblock ();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXPositBeacon") {
        	$AutoTune = 1; # Always tune before beacon
		my $info = positbeaconblock ();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXUImessage") {

		my $info = ui_messageblock ($Payload);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXaprsmessage") {
print "Waiting to send aprs message \n";
		sleep 10;
print "ready...\n";		
		my $info = ui_aprsblock ($Payload);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXlinkreq") {

		my $info = aprs_linkblock();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXlinkack") {
		$AutoTune = 1; # Always tune before link
		my $info = aprs_linkackblock();
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXConnect_ack") {	# send connect ack
		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}
		my $info = newackblock ($ServerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
		$linkquality = 2;
		$AutoTune = 1; # Always tune before connect
		$session = $ClientCall;
		if ($Iamserver) {
			# tell APRS-IS we've got him....
			$Owned_list{$ClientCall} = time();
			logprint ("Added $ClientCall to link list\n");
			# send APRS-IS confirm
			if ($scancls[0]) {
				my $min = time() / 60 % 5;
				$ServerCall = $scancls[$min];
				chomp $ServerCall;
			}

			$MSG = "$ServerCall>PSKAPR,TCPIP*::PSKAPR   :GATING $ClientCall";
			aprs_send($MSG);
				print "$ClientCall->$ServerCall\n";
				my $time = time();
				$pre = $ServerCall . " " . $time . "\n";
				$Routes{$ClientCall} = $pre;

				my $ro = "";
				open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
				foreach $key (keys %Routes) {
					$value = $Routes{$key};
					$ro = $ro . $key . " " . $value . "\n";
				}
#				print $ro;
				print ROUTES $ro;
				close (ROUTES);

				settxrsid(1);

				## add status block to $outstring

				my $info = statreport($Lastblock, $Endblock, $Goodblock, @Missing);
				$Lastblockinframe = 1;
				$outstring .= make_block($info);

		}
	} elsif ($TXServerStatus eq "TTYConnect_ack") {	# send connect ack
		my $info = ttyackblock($CallerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
		$linkquality = 2;
		$wait_a_second = 2;

	} elsif ($TXServerStatus eq "TXConnect") {		# send connect request
		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}

		my $info = newconnectblock($ServerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
	} elsif ($TXServerStatus eq "TXtransferConnect") {		# send connect request
		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}

		my $info = newtransferconnectblock($ServerCall, $ClientCall, $adressee);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);
	} elsif ($TXServerStatus eq "TTYConnect") {		# send connect request
		if ($scancls[0]) {
			my $min = time() / 60 % 5;
			$ServerCall = $scancls[$min];
			chomp $ServerCall;
		}

		my $info = ttyconnectblock($ServerCall, $ClientCall);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXReject") {	# send reject frame

		my $info = rejectblock ($rejectcall, $reason);
		$Lastblockinframe = 1;
		$outstring .= make_block($info);

	} elsif ($TXServerStatus eq "TXDisconnect") {	# send disconnect request
#print "DISCON4\n";
		my $info = disconnectblock();
		$Lastblockinframe = 1;
		$outstring = "";
		$outstring .= make_block($info);
#print "DISCON5:", $outstring, "\n";

	} elsif ($TXServerStatus eq "TXPoll") {			# send poll


		my $info = pollblock($Lastblock, $Endblock, $Goodblock, @Missing);

		$Lastblockinframe = 1;

		$outstring .= make_block($info);

	} elsif ($TXServerStatus && $TXServerStatus eq "TXStat") {			# send status


		my $info = statreport($Lastblock, $Endblock, $Goodblock, @Missing);

		$Lastblockinframe = 1;

		$outstring .= make_block($info);
		
print "SERVERSTATUS:", $Serverstatus, "\n";

	} elsif ($TXServerStatus eq "RESET_TXQueue") {   ## ~STOP command
print "resetting queue...\n";

		$Payload = "";
		if (-e "$ENV{HOME}/.pskmail/TxInputfile") {
			`rm $ENV{HOME}/.pskmail/TxInputfile`;
		}

		$Lastblock = $HisGoodblock ;
		$Lastqueued = $Lastblock ;

		for (my $ie = $HisGoodblock ; $ie < $HisGoodblock + 16; $ie++) {
			$Sendqueue[$ie % 64] = "";
		}

		$TxTextQueue = "";

		$txqlen = gettxtqueue();
## status block
		$info = statreport($Lastblock, $Endblock, $Goodblock, @Missing);
		$outstring .= make_block($info);
		$Lastblockinframe= 1;

	if ($Lastblockinframe == 1) {
		$outstring .= $FrameEnd;
		$Lastblockinframe = 0;
	}
	`echo "\n-abort-\n" >> $ENV{HOME}/.pskmail/TxInputfile`;
#print "reset2\n";

	} elsif ($TXServerStatus eq "TXTraffic") {		# traffic
## id block
		if ($Interval_time > 500) {				# every 500 seconds
			$Interval_time = 0;
			my $info = identblock($ServerCall, $ClientCall);
			$outstring .= make_block($info);
		}
## Data
#$debug = 5;
		if ($debug == 5) { #debug
			printf ("Payload=%s\n", $Payload);
		}

#		Queue_txdata($Payload);

		my $queuelength = ($Lastqueued - $HisGoodblock) % $Bufferlength;	# lenght of payload queue
		if ($queuelength < 0) {
			$queuelength += $Bufferlength;
		}

		if ($debug == 5) {	#debug
			print "payloadqueue =", $queuelength, "\n";
		}

		my $blocks_sent = 0;
		my $text = "";
## missing data
		foreach $blockindex (@HisMissing) {
			$info = packdata($blockindex, $Sendqueue[(ord $blockindex) - 32]);
			if ($info) {
				$outstring .= make_block($info);
				$blocks_sent++;
			}
		}
## queued data
		my $hveretries = 0;
		if (-e "$ENV{HOME}/.pskmail/.retries") {
			$hveretries = 1;
		}
 		if ($hveretries == 0 && (
			(gettxmodem() eq "PSK500") 
			|| (gettxmodem() eq "PSK1000") 
			|| (gettxmodem() eq "PSK63RC10") 
			|| (gettxmodem() eq "PSK250RC3") 
			)
			) {
			$MaxDataBlocks = 16;
		} else {
			$MaxDataBlocks = 8;
		}

		if ($debug == 5) {	#debug
			printf ("HisEndblock=%d, Blocks_sent=%d, Last_queued=%d, Maxdata=%d\n", $HisEndblock, $blocks_sent, $Lastqueued, $MaxDataBlocks);
		}

		for ($runvar = $HisEndblock + 1;
				($queuelength > 0)
				&& ($runvar <= $HisEndblock + ($MaxDataBlocks - $blocks_sent));
				$runvar++) {

			if ($debug == 5 ) {	#debug
				printf ("Runvar=%d, Lastqueued=%d\n", $runvar, $Lastqueued);
			}

			if ($runvar > ($Bufferlength -1)) {
				$charindex = $runvar - $Bufferlength;
			} else {
				$charindex = $runvar;
			}
			$Lastblock = $charindex;

			if ($debug == 5) {	#debug
				printf ("Runvar=%d, lastqueued=%d\n", $runvar, $Lastqueued);
			}

			if (($Lastqueued - $runvar) < 0) {
				last if ($Lastqueued -$runvar + $Bufferlength) == 0;
			 } else {
				last if ($runvar > $Lastqueued) ;
			}
			my $char = sprintf("%c", $charindex + 32);

			if ($Sendqueue[$charindex]) {
				$info = packdata($char, $Sendqueue[ord ($char) - 32]);

				if ($debug == 5) {	#debug
					printf ("charindex=%d, char=%s", $charindex, $char);
				}

				$outstring .= make_block($info);

				last if ($runvar == $Lastqueued);

			} else { 
				last; 
			}
		}
## status block
		if (length($outstring) > 0) {
			$info = statreport($Lastblock, $Endblock, $Goodblock, @Missing);
			my $blkinfo = make_block($info);
			$outstring .= $blkinfo;
			$Lastblockinframe= 1;
		}
	}

	if ($Lastblockinframe == 1) {
		$outstring .= $FrameEnd;
		$Lastblockinframe = 0;
	}

	if ($debug == 0) {
		if ($TXServerStatus eq "TXDisconnect") {

			my $info = disconnectblock();
			$Lastblockinframe = 1;
			$outstring = "";
			$outstring .= make_block($info);
			$outstring .= sprintf("%c", 4);
			
#			print "SENDDISCON:", $outstring,"\n";			
#			sleep 5;
		} 		
		sendit("eeee" . $outstring);
		sleep 4;
	} else  {
		if (length($outstring) > 1) {		
		logprint ($outstring);
		logprint ("\n");

			sendit($outstring);
			sleep 4;
		}	
	}


}

####################### end send_frame ##############################

####################################################################
sub disconnect {
####################################################################
#print "DISCON2\n";
$TXServerStatus = "TXDisconnect";
$ServerStatus = "Abort";
@Sendqueue = ();
#send_frame() unless $Streamid eq "";
send_frame();
#print "DISCON6\n";
return;
}

#####################################################################
sub handle_rxqueue {		# Do we have consecutive good buffers?
#####################################################################

my $runvar = 0;
my $Endpoint = 0;
my $index = 0;
my $inx = 0;
my $cell = "";
my $Character = "";
my $runvarinit;
$MissString = "";
@Missing = ();
$Missing = "";

	if ($HisLastblock == $Goodblock) {
		$Endblock = $Goodblock;
		$Endpoint = $Goodblock; ##debug
		return;
	} elsif ($HisLastblock < ($Goodblock)) {
		$Endpoint = $HisLastblock + $Bufferlength;
	} else {
		$Endpoint = $HisLastblock;
	}


	if ($debug == 5) {
		printf ("Hislast=%d, Goodblock=%d, Endblock=%d, Endpoint=%d\n", $HisLastblock, 				$Goodblock, $Endblock, $Endpoint);
	}

	# set missing blocks

	my $Goodstuff = 1;
	my $missers = 0;

	if ($Goodblock == $Bufferlength -1) {
		$runvarinit = 0;
	} else {
		$runvarinit = $Goodblock +1;
	}

	my $cntruns = 0;

	for ($runvar = $runvarinit; $runvar <= $Endpoint; $runvar++) {


			if ($runvar > ($Bufferlength - 1)) {
				$index = $runvar - $Bufferlength;
			} else {
				$index = $runvar;
			}

		if ($ReceiveQueue[$index]) {

			if ($debug == 5) {	#debug
				printf ("%d - %s\n", $index, $ReceiveQueue[$index]);
			}

			$Character = "";
			$Endblock = $index; 	#set Endblock


			if ($Goodstuff == 1) {
				$Goodblock = $index;
				$RxTextQueue .= $ReceiveQueue[$index];
				$ReceiveQueue[$index] = "";
				$RxTextQueue = getlocale ($RxTextQueue);
			}

		} else {

			$cntruns++;

			if ($cntruns > 8) { last; }	##debug

			$Goodstuff = 0;
			$missers++;
#			if ($missers > $MaxDataBlocks){
#				$Endblock = $index; # safety net, something is wrong...
#				last;
#			}
			$Character = makeindex($index);
			push @Missing, $Character;
			$MissString = join ('', @Missing);
#			$MissString = substr ($MissString, 0, $MaxDataBlocks -1 );
			$MissString = substr ($MissString, 0, 8 );


			if ($debug == 5) {
				printf("Endpoint=%d, Index=%d, Info:%s", $Endpoint, $index,
					$ReceiveQueue[$index]);
				printf("\nMissString=%s\n", $MissString);
			}

		}
	}

#	if (length ($MissString) == 8 && $Iamserver) {
#		$MissString = "";
#		$HisLastblock = $Goodblock;
#	}

}

#####################################################################
sub unframe {		# get frame from modem and unpack
#####################################################################

my $TextFile = shift @_;
my $Block = "";
my $BlockIndex = 5;
my $ii = 0;
my $closure = "";
my $rxmodemnumber = 0;
#my $BigEarserverport = 0;

my $reqtxmodenumber = 0;

#$debug = 2;

	while ($closure ne "<EOT>") {
		if ($debug == 2) {
			printf ("Input=%s\n", $TextFile);
		}

		my $BlockStart = index ($TextFile, "<SOH>");
		if ($debug == 2) {
			printf ("Start=%d\n", $BlockStart);
		}
		if ($BlockStart < 0) { # not enough stuff yet
			 return $TextFile;
		}	else {
			$Idle_counter = $Idle_init; # but we have a block coming...

		}

		$Block = substr ($TextFile, $BlockStart);
		
		if ($debug == 2) {
			printf ("Block=%s\n", $Block);
		}
		
#DEBUGG
=head
		my $getprediction = 0;
		if (-e "$ENV{HOME}/.pskmail/.prediction4") {  ## status reconstruction
			my $myprediction = `cat $ENV{HOME}/.pskmail/.prediction4`;
			chomp $myprediction;
#			print "RECEIVING:|", $myprediction, "|\n";
			open ($FG, "$ENV{HOME}/.pskmail/.stream");
			my $lsession = <$FG>;
			close $FG;
			chomp $lsession;			
			my $str = "z" . $lsession . "s" .$myprediction; 
#		print "SESSION:", $lsession, "\n";
#		print "REPAIR INFO:|", $str, "|\n";
dprint ($str);
			my $check = checksum ($str);
			$Block = "<SOH>" . $str . $check . "<EOT>";
#					print "STATUS BLOCK REPAIRED:|", $Block, "\n";
			$Status_received = "";
			`rm $ENV{HOME}/.pskmail/.prediction4`;
			`echo "100" > $ENV{HOME}/.pskmail/.rxs2n`;
			`echo "100" > $ENV{HOME}/.pskmail/.servers2n`;
			$myprediction = "";
#			$TextFile = "";
			$getprediction = 1;
		} 		
=cut
		my $BlockEnd = index ($Block, "<SOH>", 5);
		if ($BlockEnd < 0 ) {
			$BlockEnd = index ($Block, "<EOT>");
		}

			if ($BlockEnd < 0) {  return $TextFile;  } # not enough stuff yet
		
	
		
		if ($BlockEnd < 0) {  return $TextFile;  } # not enough stuff yet

		if ($debug == 2) {
			printf ("End=%d\n", $BlockEnd);
		}
		if ($getprediction == 0){
			$Block = substr ($Block, 0, $BlockEnd + 5);
		} 
 		reset;
#logprint ("\nBLOCK:$Block\n");
		if ($Block =~ /<SOH>(<SOH>.*)/s) {
			$Block = $1;
			$TextFile = $1;
		}
		if ($Block =~ /<EOT>(<SOH>.*)/s) {
			$Block = $1;
			$TextFile = $1;
		}
		if ($Block =~ /QSL\s\S+\sde\s([A-Z0-9-]+).*/) {
			$servers{$1} = time();
			$myservers{$1} = time();
			log_ping ($1, 0);
#			print "Adding server $1 to list...\n";
		}

		if ($Block =~ /<SOH>((.)(.)(.)(.*))(....)(<EOT>)/s ||
			$Block =~ /<SOH>((.)(.)(.)(.*))(....)(<SOH>)/s) {
#logprint ("\nBLOCKCHECK:$Block\n");

			my $checkinfo = $1;
#Asymmetric linkup: proto byte from client in a status frame = Client's received (s2n average - s2n std dev) * 90 / 100 + 32 (all printable characters)
			my $protobyte = $2;
			my $Session_id = $3;
			my $operand = $4;
			my $payload = $5;
			my $check = $6;
			$closure = $7;
#logprint ("\nINFO:$1,$2,$3,$4,$5,$6,$7\n");
#logprint ("\nPAYLOAD:|$payload|\n");
			if ($debug == 5) {
				print "All   :",$1, "\n";
				print "Proto :", $2, "\n";
				print "Stream:", $3, "\n";
				print "Oper  :" ,$4, "\n";
				print "Data  :|", $5, "|\n";
				print "Check :", $6, "\n";
				print "Close :", $7, "\n";
			}

			$lengthlastframe = length($5);

			$Current_session = $3;

	## got it, remove from queue
#logprint ("\nQUEUE_IN:$TextFile\n");

			if ($getprediction == 0){
				$TextFile = substr ($TextFile, index($TextFile, $1) + 4 + length ($1) + 5);
			}
			
			$getprediction = 0;

#logprint ("\nQUEUE_OUT:$TextFile\n");
#$debug = 2;

			if ($debug == 2) {
				printf ("Txtfromfile_now:%s\n", $TextFile);
				print checksum($checkinfo), "\n";
				print $check, "\n";
			}

	## checksum o.k.?
			if ($check eq checksum($checkinfo)) {
				if ($debug == 2) {
					print "checksum o.k.\n" ;
					printf ("operand=%s\n", $operand);
				}

	## t.b.d: store checksum + data, you never know if you are going to need it...

	if ($4 eq "s") {
		open ($fh3, ">", "$ENV{HOME}/.pskmail/laststatusrcvd") ;
		print $fh3 $1;
		close $fh3;
	}
 
	## monitor
				if ($monitor) {
					if ($ShowBlock) {
						my $logtxt = $Block . "\n";
						logprint ($logtxt);
					}
					if (ord $4 > 95) {
						if (substr ($4, 0, 1) eq "p") {
							my $lastblockvalue = ord (substr ($5, 0, 1)) - 32;
							my $goodblockvalue = ord (substr ($5, 1, 1)) - 32;
							my $endblockvalue = ord (substr ($5, 2, 1)) - 32;
							my $missingvalue = substr ($5, 3);

							my $logtxt = sprintf ( "> Poll   : last=%d good=%d end=%d missing=%s\n",
								$lastblockvalue, $goodblockvalue, $endblockvalue, $missingvalue);
							logprint ($logtxt);
						}
#DEBUGG
#print "STRING=|", $5, "|\n";						
						if (substr ($4, 0, 1) eq "s") {
							my $lastblockvalue = ord (substr ($5, 0, 1)) - 32;
							my $goodblockvalue = ord (substr ($5, 1, 1)) - 32;
							my $endblockvalue = ord (substr ($5, 2, 1)) - 32;
							my $missingvalue = substr ($5, 3);

							my $logtxt = sprintf ( "> Status : last=%d good=%d end=%d missing=%s\n",
								$lastblockvalue, $goodblockvalue, $endblockvalue, $missingvalue);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "a") {
							my $logtxt = sprintf ( "> Abort \n\n");
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "c") {
							my $logtxt = sprintf ( "> Connect: %s\n\n",  $5);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "n") {
							my $logtxt = sprintf ( "> Summon: %s\n\n",  $5);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "d") {
							my $logtxt = sprintf ( "> Disconnect\n\n");
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "i") {
							my $logtxt = sprintf ( "> Ident  : %s\n\n", $5);
							logprint ($logtxt);
						}
						if (substr ($4, 0, 1) eq "u") {
							my $logtxt = sprintf ( "> Unproto: %s\n\n", $5);
							logprint ($logtxt);
						}
					} else {
						my $zahl = (ord $4) - 32;
						my $logtxt = sprintf ( "> Data: %2d\n%s\n", $zahl, $5);
						logprint ($logtxt);
					}
				}	# end monitor

				if ($closure eq "<EOT>") {

					$ServerStatus = "EOT";
					$ReceivedLastBlock = 1;

#Adaptative timing: no need to recalculate maxidle here. Now done just before usage using latest cps

				}

				if ($Iamserver == 1) {
					open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
					my $session = <SESSION>;
					chomp $session;
					close SESSION;
	
					if($session eq "none") {
						if ($operand eq "a" ||
							$operand eq "s" ||
							$operand eq "p" ||
							$operand eq "d" ||
							$operand eq "i" ) {
							return "";
						}

					}
				}
				
#	print "CHECK:", $operand, ",", $Current_session_sent, ",", $Streamid, ",", $session,"\n";

	## abort
				if ($operand eq 'a') {		#t.b.d.
					$ServerStatus = "Abort";
					return $TextFile;
	## unproto services
				} elsif ($operand eq 'u'){
						if ($payload =~ /(.*):(\d+) (.*)/s) {
		## ping service		
							$ucl = $1;
							$payload3 = $3;
							if ($2 == 72) {
								log_ping($1,$2);
							} elsif ($2 == 8) {
								log_ping($1,$2);
#						print "Inquire:" . $ServerCall . "," . $payload3 . "\n";
								if ($3 eq $ServerCall . " ") {
										sleep ($Pingdelay);
#						print "Inquire" . $ServerCall. "\n";
										$AutoTune = 1;	# autotune if required

										my $mlscall = $1;
										my $mls = 0;
										my $mlcount = 0;
										
										if (checklink($mlscall) == 1) {	
											print "call $mlscall found \n";
											$mls = just_count_mail($mlscall);
											
											
											if (-e "$ENV{HOME}/.pskmail/localmail/$mlscall") {	# see if there is local mail
												my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$mlscall");
												my $mails = "";
			
												for my $msg ($folder->messages) {
													$mlcount++;
												}
											} else {
												$mlcount = 0;
											}	
																						
										} else {
											print "call $mlscall not found \n";
										}	

										my $s2n = `cat $ENV{HOME}/.pskmail/.servers2n`;
										chomp $s2n;
										$s2n = sprintf "%.0f", $s2n;

										my $QSL = sprintf("%cQSL %s de %s %s %d:%d",0x01, $1, $ServerCall, $s2n, $mls, $mlcount);
										my $qslcheck = checksum ($QSL);
										$QSL .= " " . $qslcheck . $eot;
=head
										if (exists $servers{$1} || exists $myservers{$1}) {
											# do nothing, its a server.
										} elsif (exists $Myroutes{$1}) {
											sendit ($QSL . $eot);
										}
=cut										
										if (exists $Myroutes{$1}) {
											sendit ($QSL . $eot);
										}
										
										$payload = "";
								}
							} elsif ($2 == 9) { # who hears <CALL>?
								my $mheard = `cat $ENV{HOME}/.pskmail/pskdownloads/pings.log | grep :6`;
								if (length($mheard) > 0) {
									$mheard =~ /.*\s-\s\d+\s-\s(\d+)/;
									print $2, " beacon:" . $1 . "\n";	
								}
							} elsif ($2 == 7 || $2 == 71) {
								log_ping($1,$2);
								$myservers{$1} = time();
								if ($Iamserver == 1) {
									if ($2 == 7) {
										sleep ($Pingdelay);
										$AutoTune = 1;	# autotune if required
										sleep (int (rand(30)));
										$TXServerStatus = "TXPing";
										send_frame();
										$payload = "";
									}
								}
							} elsif ($2 == 25) {			# unproto email
									log_ping($ucl,$2);
									
									eval {
										if ($MARS) {
											@ping = `ping -c 1 "$alt_forwarder" 2>/dev/null`;
										} elsif ($NOINTERNET == 0) {
											@ping = `ping -c 1 pskmail.org 2>/dev/null`;
										}
									};
#print $ping[0], "\n";
#print $ping[1], "\n";									
									if($ping[1] && $ping[1] =~ /(time=\d*\.*\d* ms)/) {
										$pingout = "Ping " . $1;
										`touch $ENV{HOME}/.pskmail/.internet`;
									}	else {
										if ( -e "$ENV{HOME}/.pskmail/.internet") {
											`rm $ENV{HOME}/.pskmail/.internet`;
										}
									}								
								if ($Iamserver == 1 && -e "$ENV{HOME}/.pskmail/.internet") {
									$AutoTune = 1;	# autotune if required
									$RxTextQueue .= "~MSG " . $ucl . " " . $payload3 . "\n";

									$TXServerStatus = "TXPing";
									sleep (int (rand(10)));
									send_frame();
									$payload = "";
								}
							} elsif ($2 == 26) {			# aprs message
								if ($Iamserver == 1) {
									my $cont = $payload3;
#									print "BEACON HEARD:" . $payload3 . "\n";
									if (index ($cont, "!") > 0 && index ($cont, "&") > 0){
										$myservers{$ucl} = time();
										print "Server $ucl heard... $myservers{$ucl}\n";										
									}
									if (index ($cont, ">PSKAPR*::") < 0) {

										if ($rigptttune == 1) {
											$AutoTune = 1;	# autotune if required
											ptttune();
										}
										if ($scancls[0]) {
											my $min = time() / 60 % 5;
											$ServerCall = $scancls[$min];
											chomp $ServerCall;
										}

										my $s2n = `cat $ENV{HOME}/.pskmail/.servers2n`;
										chomp $s2n;
										$s2n = sprintf "%.0f", $s2n;
										$mlscall = $1;								

										my $QSL = sprintf("%cQSL %s de %s %s",0x01, $mlscall, $ServerCall, $s2n);
										
										my $qslcheck = checksum ($QSL);
										$QSL .= " " . $qslcheck . $eot;

										if (exists $Myroutes{$mlscall}) {
												sendit ($QSL . $eot);
										}

										my $msgcall = $ucl;

										if ($payload3 =~/^TWEET\s(.*)\{.*/) { # send tweet to identi.ca
											my $call = $msgcall;
											my $cmd = sprintf ("curl -u %s -d %cstatus=!pskmail %s:%s%c http://identi.ca/api/statuses/update.xml",  "pskmailposter:useCurl", 34, $call, $1, 34);
											my $result = `$cmd`;
											## add it to twitter as well
											$RxTextQueue .= "~MSG " . $ucl . " " . 'tweet@tweetymail.com ' . $1 . "\n";							
											
										} else {
											aprs_serv($1, $payload3); # $1 = Call, $3 = Message
											log_ping($msgcall,$2);
										}
									}
								} else {
									aprs_client($ucl, $payload3);
									log_ping($msgcall,$2);
								}
								$payload = "";
							} elsif ($2 == 6) {			# aprs message

									my $cont = $3;

									my @msg = split "", $cont;
									my $courseflg = 0;
									my $flg = ord ($msg[0]) - 32;
									my $latdegrees = ord($msg[1]) - 32;
									my $latminutes = ord($msg[2]) - 32;
									my $latrest =ord($msg[3]) - 32;
									my $londegrees =ord($msg[4]) - 32;
									my $lonminutes = ord($msg[5]) - 32;
									my $lonrest =  ord($msg[6]) - 32;
									my $course = (ord($msg[7]) -32) * 2;
									my $speed = ord($msg[8]) -32;
									my $symbol = $msg[9];
									my $statusmsginx = ord($msg[10]) - 32;
									my $statusmessage = "";
									if ($statusmsginx <= $#stdmessages) {
										$statusmessage = $stdmessages[$statusmsginx];
									}

									if (length($cont) > 11) {
										chomp $statusmessage;
										$statusmessage .= substr($cont, 11);
									}
									my $latstr = "S";
									my $lonstr = "W";
									if ($flg & 32) { $course += 180 };
									if ($flg & 16) { $speed += 90 } ;
									if ($flg & 8) { $latstr = "N" } ;
									if ($flg & 4) { $lonstr = "E" } ;
									if ($flg & 2) { $londegrees += 90 };



									my $aprsbeacon = sprintf("!%02d%02d.%02d%s/%03d%02d.%02d%s%s%03d/%03d/%s",
										$latdegrees,$latminutes,$latrest,$latstr,
										$londegrees,$lonminutes,$lonrest,$lonstr,$symbol,
										$course,$speed,$statusmessage);

										if ($rigptttune == 1) {
											$AutoTune = 1;	# autotune if required
											ptttune();
										}
										if ($scancls[0]) {
											my $min = time() / 60 % 5;
											$ServerCall = $scancls[$min];
											chomp $ServerCall;
										}
										
										my $mlscall = $1;
										my $mls = 0;
										my $mlcount = 0;
										
										if (checklink($mlscall) == 1) {	
											print "call $mlscall found \n";
											$mls = just_count_mail($mlscall);
											
											
											if (-e "$ENV{HOME}/.pskmail/localmail/$mlscall") {	# see if there is local mail
												my $folder = Email::Folder->new("$ENV{HOME}/.pskmail/localmail/$mlscall");
												my $mails = "";
			
												for my $msg ($folder->messages) {
													$mlcount++;
												}
											} else {
												$mlcount = 0;
											}	
																						
										} else {
											print "call $mlscall not found \n";
										}	

										my $s2n = `cat $ENV{HOME}/.pskmail/.servers2n`;
										chomp $s2n;
										$s2n = sprintf "%.0f", $s2n;

										my $QSL = sprintf("%cQSL %s de %s %s %d:%d",0x01, $1, $ServerCall, $s2n, $mls, $mlcount);
										my $qslcheck = checksum ($QSL);
										$QSL .= " " . $qslcheck . $eot;

										if ($symbol eq "&" || exists $servers{$1}) {
											# do nothing, its a server.
										} else {
											if ($mlcount) {
												sleep 15;
											}
											sendit ($QSL . $eot);
										}

										my $msgcall = $1;
										aprs_serv($1, $aprsbeacon); # $1 = Call, $3 = Message
										log_ping($msgcall,$2);

								$payload = "";
							}

				 		} elsif ($Iamserver == 1 && $payload =~ /(\S+)><(\S+)/){ # APRS link request
				 			# add station to owned_list
							if ($scancls[0]) {
								my $min = time() / 60 % 5;
								$ServerCall = $scancls[$min];
								chomp $ServerCall;
							}

				 			if ($2 eq $ServerCall) {
								$Owned_list{$1} = time();
								logprint ("Added $1 to list\n");

								# send APRS-IS confirm
								$MSG = "$ServerCall>PSKAPR,TCPIP*::PSKAPR   :GATING $1";
								aprs_send($MSG);
								my $time = time();
								$Myroutes{$1} = time();
print "NEW ROUTE:" . $1 . "\n";
print "TIME=" . $Myroutes{$1} . "\n";
								# add to routing table
								print "$1->$2\n";
								my $pre = $2 . " " . $time . "\n";
								$Routes{$1} = $pre;
								my $ro = "";
								open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
								foreach $key (keys %Routes) {
									$value = $Routes{$key};
									$ro = $ro . $key . " " . $value . "\n";
								}
#								print $ro;
								print ROUTES $ro;
								close (ROUTES);
								# tell the station we got him...
								$Call = $1;
								set_txstatus("TXlinkack");
								send_frame();
								# add call to data base

				 			}

				 		} elsif ($Iamserver == 0 && $payload =~ /(\w*)<>(\w*)/){		# APRS link ack
							# set linked flag (tbd)
						}
	## connect request ('PA0R:1024 PI4TUE:24 4')
				} elsif ($operand eq 'c') {
					my $validcall = 0;
					my $modetbl = "";

					print "CONNECT:protocol:" . $protobyte ."\n";
		# set protocol level of the caller
					$Protocol_level = ord ($protobyte) - 48;
					`echo "$protobyte" > $ENV{HOME}/.pskmail/.protocol`;

					if ($payload =~ /(.*):(.*)\s(.*):(.*)\s(\S+)/s) {
						if ($debug == 2) {
							print "CONNECT frame:\n";
							print $1, "\n";
							print $2, "\n";
							print $3, "\n";
							print $4, "\n";
							print $5, "\n";
						}

						my $checkcaller = $1;

						if ($checkcaller =~ m/([0-9]{1})([A-Z]{1})([0-9]{1})([A-Z]{1,3})(\-?)(\d*)/ ){
							$validcall = 1;
						} elsif ( $checkcaller =~  m/([A-Z]{1,3})([0-9]{1,2})([A-Z]{1,3})(\-?)(\d*)/ )  {
							$validcall = 1;
							print "Call valid\n";
						} 
					}

					if ($payload =~ /(.*):(.*)\s(.*):(.*)\s(\S+)/s) {


	## set rxmodem as per request from the client's modem byte
					my $modemrequest = "5";
					if (length($5) == 1) {  # payload for connect block, start modem
						$modemrequest = $5;
						if ($NO500) {
							$modemrequest = "8";
							$modetbl = "8";
						}

#						@currentmodes = @Amodes;
					} elsif (length($5) > 1) {
						$MAXMODENUM = 7;
						$modemrequest = substr($5, 0, 1);
#print $5, "\n";
#print $modemrequest, "\n";

						@currentmodes = ();

						$modetbl = substr($5,1);
#print "Modetbl:", $modetbl, "\n";
						my $modemchange = 0;
						if ($NO500) {	# 500 Bd not allowed....

							my @mds = split "", $modetbl;
							$modetbl = "";
							my $mdsize = @mds;
							for (my  $i = 0; $i < $mdsize; $i++) {
								if ($mds[$i] eq "7") {
									$modemchange = 1;
								} elsif ($mds[$i] eq "6"){
									$modemchange = 1;
								} else {
#print $modetbl, ",", $mds[$i], ",", "INDEX:", index($modetbl, $mds[$i]), "\n";
									$modetbl .= $mds[$i];
								}
							}
						}
#print "MODETBL:", $modetbl, "\n";

						if ($NO500) {	# 500 Bd not allowed in USA....
							if ($modemrequest eq "7" || $modemrequest eq "6") {
								$modemrequest = substr($modetbl,0,1);
							}
#print "Modemreq:", $modemrequest, "\n";

						}

						$MAXMODENUM = length($modetbl);
							$rxmodemnumber = $MAXMODENUM;

#							print $rxmodemnumber, "\n";
#							print $modetbl, "\n";
						my @indexes = split "", $modetbl;

						my $requestedmodestring = "";


							for (my $i = $MAXMODENUM; $i > -1; $i--) {
								if ($i == 0) {
									$currentmodes[$i] = "default";
								} else {
									$currentmodes[$i] = $modelist{$indexes[$MAXMODENUM - $i]};
								}
#								print "CUR:"  . $currentmodes[$i] . "," . $i . "\n";

#								print "INX:", $indexes[$MAXMODENUM - $i], ",", $modemrequest, ",", $i ,"\n";

								if ($i > 0 && $indexes[$MAXMODENUM - $i] eq $modemrequest) {
#									print "NOWMODE:",$currentmodes[$i], "\n";;
									$requestedmodestring = $currentmodes[$i];
									$rxmodemnumber = $i;
								}

							}
							if ($modemchange == 1 ) {
								$rxmodemnumber = $MAXMODENUM;
								$txmodemnumber = $MAXMODENUM;
							}

					}



#		print "MODEMREQ:" . $modemrequest . "\n";


#		print "rxmodemnr:" . $rxmodemnumber . "\n";
					setrxmodem ($rxmodemnumber);
					settxmodem ($rxmodemnumber);


						if ($Iamserver) {

							if ($validcall == 0) {
									$TXServerStatus = "TXReject";
									$rejectcall = $1;
									$reason = "Wrong call format\n";
									send_frame();

									return($TextFile);
							}

							if ($scancls[0]) {
								my $min = time() / 60 % 5;
								$ServerCall = $scancls[$min];
								chomp $ServerCall;
							}
#print $ServerCall, "\n";
							if ($3 ne $ServerCall) {
#								logprint ("Not my call...$ServerCall...\n");
								return($TextFile);
							} else{
#print "OK, my call\n";				
								#check if we are connected already
								open SESSIONDATA, "$ENV{HOME}/.pskmail/PSKmailsession";
								my $session = <SESSIONDATA>;
								chomp $session;
								close SESSIONDATA;

								if ($session ne "beacon" &&
									$session ne "none" &&
									$session ne $1
									&& $session ne "connecting") {
									logprint ("Already connected to $session\n");

									$TXServerStatus = "TXReject";
									$rejectcall = $1;
									$reason = "Already connected\n";
									send_frame();

									return($TextFile);
								}

								$AutoTune = 1;	# initiate an autotune if required
								newsession();
								$ServerStatus = "Connect_req";
								$Call = $1;				# set the call
								$ClientCall = $1;
								log_ping($Call, $2);		# > mheard list


								#Logic: the block size byte sent by the client in the connect frame carries the
								#  requested mode number to be used by the server. If zero this means that we stay in
								#  variable mode but symmetrical linkup. If it is > zero, we check that it is within boundaries.
								#  If yes, we use this as the TX mode of the server. If outside boundaries, we reset to variable
								#  symmetric mode. This should ensures backward compatibility too.

								# For the server RX mode, we use the current connect mode RSID (or current default mode for
								#   compatibilty if no RSID) and check that it is in the list. If yes, we use it as and sent
								#   it in the first byte of the status frames to the client for it to TX in that mode.
								#   The proto byte is the method for changing the server RX/Client TX mode. If the mode is not
								#   in the list, we revert to symmetrical mode.


								$reqtxmodenumber = 	$rxmodemnumber;

#print "Requested Server Tx modenumber =" . $reqtxmodenumber . "\n";

								if ($reqtxmodenumber == 0 ) {
									#Then we stay in symmetric mode. Compatible with older versions too.
									$asymlink = 0;
								} else {
									$asymlink = 1;
									#Check that this mode is in the list, otherwise stay in symmetric mode


#									print "TXMODEMNUMBER:", $rxmodemnumber, "\n";
									settxmodem($rxmodemnumber);
									if ($modemchange) {
										$lastrxid = gettxmodem();
										if ("-e $ENV{HOME}/.pskmail/.rxid") {`rm -e $ENV{HOME}/.pskmail/.rxid`};
									}
								}
#print "txmode now ", gettxmodem(), "\n";

#									if ($modemchange) {
										$lastrxid = gettxmodem();
										if ("-e $ENV{HOME}/.pskmail/.rxid") {`rm $ENV{HOME}/.pskmail/.rxid`};
#									}

#print "LASTRXID=", $lastrxid, "\n";


								#Check that this mode is in the list, otherwise reset to symmetric mode
								#One option here would be to have an equivalent list for mode outside the list (e.g. for psk250)

								my $asymrxmode = tomodenumber($lastrxid);
#print "Converted to modenumber" . $asymrxmode . "=\n";
								if ($asymrxmode > 0) {
									setrxmodem($asymrxmode);
								} else {
									#We don't know this mode, use it, but stay in symmetric mode
									$asymlink = 0;
#print "We don't know that mode - asymlink reset to 0\n";
								}

								#If in symmetric mode, txmode defines rx mode as well, rx mode = 0 then
								if ($asymlink == 0) {
									settxmodem(tomodenumber($lastrxid));
									setrxmodem(0);
								}



print "Final connect status:  Asymmetric link:", $asymlink, " , tx mode: " , gettxmodem() , " , rx mode: " , getrxmodem() , "\n";

								$Blockindex = 5;	# set default blocklength
								$BlockLengthStr= sprintf("%c", $Blockindex + 48);
								$BlockLength = (2 ** $Blockindex) ;

								if ($debug == 2) {
									logprint ($ServerStatus . "\n");
									logprint ("Called by $ClientCall\n");
									logprint ("Max block length = $5\n");
								}
							}
						} else {	# I am a client
								log_ping($Call, $2);		# > mheard list
							if ($3 ne $options[0]) {
								logprint ("Not my call...\n");
								return($TextFile);
							} else {
				# client connect request
								$CallerCall = $1;
								$Mode = $2;

								$Moderequest = $4;

								if ($Moderequest == 87) {
									$ServerStatus ="TTY_req";
									`echo "$CallerCall" > $ENV{HOME}/.pskmail/.mastercall`;

								} else {
									$ServerStatus = "Connect_req";
								}

								$Maxlen = 5;

								$Blockindex = $Maxlen;	# set max blocklength
								$BlockLengthStr= sprintf("%c", $Blockindex + 48);
								$BlockLength = (2 ** $Blockindex) ;

#Adaptative timing: no need to recalculate maxidle here. Now done just before usage using latest cps



								if ($debug == 2) {
									logprint ($ServerStatus . "\n");
									logprint ("Called by $CallerCall\n");
									logprint ("Max block length = $5\n");
								}

							}
						}

					}
				} elsif ($operand eq 'n') {

	## set rxmodem as per request from the client's protocol byte
					if ($protobyte eq "") {$protobyte = "6"};

					my $modemrequest = $protobyte;
					my $rxmodenumber = 6;
					$rxmodemnumber = ord ($protobyte) - 48 ;
					setrxmodem ($rxmodenumber);
					
					if ($payload =~ /(.*):(.*)\s(.*):(.*)\s(\d+)\s(\S)/s) {
						if ($debug == 2) {
							print "CONNECT frame:\n";
							print $1, "\n";
							print $2, "\n";
							print $3, "\n";
							print $4, "\n";
							print $5, "\n";
							print $6, "\n";
						}

						my $checkcaller = $1;

						if ($checkcaller =~ m/([0-9]{1})([A-Z]{1})([0-9]{1})([A-Z]{1,3})(\-?)(\d*)/ ){
							$validcall = 1;
						} elsif ( $checkcaller =~  m/([A-Z]{1,3})([0-9]{1,2})([A-Z]{1,3})(\-?)(\d*)/ )  {
							$validcall = 1;
							print "Call valid\n";
						} 
					}					

#################################################################################
					if ($payload =~ /(.*):(.*)\s(.*):(.*)\s(\d+)\s(\S)/s) {

	## set rxmodem as per request from the client's modem byte
					my $modemrequest = "5";
					if (length($6) == 1) {  # payload for connect block, start modem
						$modemrequest = $6;
						if ($NO500) {
							$modemrequest = "8";
							$modetbl = "8";
						}

#						@currentmodes = @Amodes;
					} elsif (length($6) > 1) {
						$MAXMODENUM = 7;
						$modemrequest = substr($6, 0, 1);
print $6, "\n";
print $modemrequest, "\n";

						@currentmodes = ();

						$modetbl = substr($6,1);
print "Modetbl:", $modetbl, "\n";
						my $modemchange = 0;
						if ($NO500) {	# 500 Bd not allowed....

							my @mds = split "", $modetbl;
							$modetbl = "";
							my $mdsize = @mds;
							for (my  $i = 0; $i < $mdsize; $i++) {
								if ($mds[$i] eq "7") {
									$modemchange = 1;
								} elsif ($mds[$i] eq "6"){
									$modemchange = 1;
								} else {
#print $modetbl, ",", $mds[$i], ",", "INDEX:", index($modetbl, $mds[$i]), "\n";
									$modetbl .= $mds[$i];
								}
							}
						}
#print "MODETBL:", $modetbl, "\n";

						if ($NO500) {	# 500 Bd not allowed....
							if ($modemrequest eq "7" || $modemrequest eq "6") {
								$modemrequest = substr($modetbl,0,1);
							}
#print "Modemreq:", $modemrequest, "\n";

						}

						$MAXMODENUM = length($modetbl);
							$rxmodemnumber = $MAXMODENUM;

							print $rxmodemnumber, "\n";
							print $modetbl, "\n";
						my @indexes = split "", $modetbl;

						my $requestedmodestring = "";


							for (my $i = $MAXMODENUM; $i > -1; $i--) {
								if ($i == 0) {
									$currentmodes[$i] = "default";
								} else {
									$currentmodes[$i] = $modelist{$indexes[$MAXMODENUM - $i]};
								}
#								print "CUR:"  . $currentmodes[$i] . "," . $i . "\n";

#								print "INX:", $indexes[$MAXMODENUM - $i], ",", $modemrequest, ",", $i ,"\n";

								if ($i > 0 && $indexes[$MAXMODENUM - $i] eq $modemrequest) {
									print "NOWMODE:",$currentmodes[$i], "\n";;
									$requestedmodestring = $currentmodes[$i];
									$rxmodemnumber = $i;
								}

							}
							if ($modemchange == 1 ) {
								$rxmodemnumber = $MAXMODENUM;
								$txmodemnumber = $MAXMODENUM;
							}

					}



#		print "MODEMREQ:" . $modemrequest . "\n";


#		print "rxmodemnr:" . $rxmodemnumber . "\n";
					setrxmodem ($rxmodemnumber);
					settxmodem ($rxmodemnumber);


						if ($Iamserver) {

							if ($validcall == 0) {
									$TXServerStatus = "TXReject";
									$rejectcall = $1;
									$reason = "Wrong call format\n";
									send_frame();

									return($TextFile);
							}

							if ($scancls[0]) {
								my $min = time() / 60 % 5;
								$ServerCall = $scancls[$min];
								chomp $ServerCall;
							}
#print $ServerCall, "\n";
							if ($3 ne $ServerCall) {
#								logprint ("Not my call...$ServerCall...\n");
								return($TextFile);
							} else{
#print "OK, my call\n";				
								#check if we are connected already
								open SESSIONDATA, "$ENV{HOME}/.pskmail/PSKmailsession";
								my $session = <SESSIONDATA>;
								chomp $session;
								close SESSIONDATA;

								if ($session ne "beacon" &&
									$session ne "none" &&
									$session ne $1
									&& $session ne "connecting") {
									logprint ("Already connected to $session\n");

									$TXServerStatus = "TXReject";
									$rejectcall = $1;
									$reason = "Already connected\n";
									send_frame();

									return($TextFile);
								}

								$AutoTune = 1;	# initiate an autotune if required
								newsession();
								$ServerStatus = "Connect_req";
								$Call = $1;				# set the call
								$ClientCall = $1;
								log_ping($Call, $2);		# > mheard list


								#Logic: the block size byte sent by the client in the connect frame carries the
								#  requested mode number to be used by the server. If zero this means that we stay in
								#  variable mode but symmetrical linkup. If it is > zero, we check that it is within boundaries.
								#  If yes, we use this as the TX mode of the server. If outside boundaries, we reset to variable
								#  symmetric mode. This should ensures backward compatibility too.

								# For the server RX mode, we use the current connect mode RSID (or current default mode for
								#   compatibilty if no RSID) and check that it is in the list. If yes, we use it as and sent
								#   it in the first byte of the status frames to the client for it to TX in that mode.
								#   The proto byte is the method for changing the server RX/Client TX mode. If the mode is not
								#   in the list, we revert to symmetrical mode.


								$reqtxmodenumber = 	$rxmodemnumber;

#print "Requested Server Tx modenumber =" . $reqtxmodenumber . "\n";

								if ($reqtxmodenumber == 0 ) {
									#Then we stay in symmetric mode. Compatible with older versions too.
									$asymlink = 0;
								} else {
									$asymlink = 1;
									#Check that this mode is in the list, otherwise stay in symmetric mode


									print "TXMODEMNUMBER:", $rxmodemnumber, "\n";
									settxmodem($rxmodemnumber);
									if ($modemchange) {
										$lastrxid = gettxmodem();
										if ("-e $ENV{HOME}/.pskmail/.rxid") {`rm -e $ENV{HOME}/.pskmail/.rxid`};
									}
								}
#print "txmode now ", gettxmodem(), "\n";

#									if ($modemchange) {
										$lastrxid = gettxmodem();
										if ("-e $ENV{HOME}/.pskmail/.rxid") {`rm $ENV{HOME}/.pskmail/.rxid`};
#									}

#print "LASTRXID=", $lastrxid, "\n";


								#Check that this mode is in the list, otherwise reset to symmetric mode
								#One option here would be to have an equivalent list for mode outside the list (e.g. for psk250)

								my $asymrxmode = tomodenumber($lastrxid);
#print "Converted to modenumber" . $asymrxmode . "=\n";
								if ($asymrxmode > 0) {
									setrxmodem($asymrxmode);
								} else {
									#We don't know this mode, use it, but stay in symmetric mode
									$asymlink = 0;
#print "We don't know that mode - asymlink reset to 0\n";
								}

								#If in symmetric mode, txmode defines rx mode as well, rx mode = 0 then
								if ($asymlink == 0) {
									settxmodem(tomodenumber($lastrxid));
									setrxmodem(0);
								}



print "Final connect status:  Asymmetric link:", $asymlink, " , tx mode: " , gettxmodem() , " , rx mode: " , getrxmodem() , "\n";

								$Blockindex = 5;	# set default blocklength
								$BlockLengthStr= sprintf("%c", $Blockindex + 48);
								$BlockLength = (2 ** $Blockindex) ;

								if ($debug == 2) {
									logprint ($ServerStatus . "\n");
									logprint ("Called by $ClientCall\n");
									logprint ("Max block length = $5\n");
								}
							}
						} 

#					}



		print "MODEMREQ:" . $modemrequest . "\n";


		print "rxmodemnr:" . $rxmodemnumber . "\n";
					setrxmodem ($rxmodemnumber);
					settxmodem ($rxmodemnumber);
####################################################################
						if ($Iamserver) {
							if ($scancls[0]) {
								my $min = time() / 60 % 5;
								$ServerCall = $scancls[$min];
								chomp $ServerCall;
							}

							if ($3 ne $ServerCall) {
								logprint ("Not my call...\n");
								return($TextFile);
							} else{
								#check if we are connected already
								open SESSIONDATA, "$ENV{HOME}/.pskmail/PSKmailsession";
								my $session = <SESSIONDATA>;
								chomp $session;
								close SESSIONDATA;

								if ($session ne "beacon" &&
									$session ne "none" &&
									$session ne $1
									&& $session ne "connecting") {
									logprint ("Already connected to $session\n");
									return($TextFile);
								}

								# send the server to the summoning frequency...
								my $sumcall = $4;
								my $requestedfreq = $5;
								my $rband = int ($requestedfreq/1000000);
								my $validband = 0;
								my $sfreq = 0;

								@scanfreqs = memtofreq ($hour);

								if ($scanner eq "M") {
									for ($i = 0; $i < 5; $i++) {
										if ($scanfreqs[$i] == $requestedfreq) {
											$validband = 1;
										}
									}
								} elsif ($scanner eq "F") {

									for ($i=0; $i < 5; $i++) {
										if (int($scanfreqs[$i]/1000000) == $rband) {
											$validband = 1;
										}
									}
								}

								if ($validband) {
									my $validfreq = 0;
									$sfreq = $requestedfreq + $freq_corrections[0];

									if ($rband == 1) {
										if ($sfreq >= 1839500 && $sfreq <= 1842500) {
											$validfreq = 1;
										}
									} elsif ($rband == 3) {
										if ($sfreq >= 3580500 && $sfreq <= 3599500) {
											$validfreq = 1;
										}
									} elsif ($rband == 7) {
										if ($sfreq >= 7040500 && $sfreq <= 7049500) {
											$validfreq = 1;
										}
									} elsif ($rband == 10) {
										if ($sfreq >= 10140500 && $sfreq <= 10149500) {
											$validfreq = 1;
										}
									} elsif ($rband == 14) {
										if ($sfreq >= 14070500 && $sfreq <= 14099500) {
											$validfreq = 1;
										}
									} elsif ($rband == 18) {
										if ($sfreq >= 18095500 && $sfreq <= 18119500) {
											$validfreq = 1;
										}
									} elsif ($rband == 21) {
										if ($sfreq >= 21070500 && $sfreq <= 21109500) {
											$validfreq = 1;
										}
									} elsif ($rband == 24) {
										if ($sfreq >= 24915500 && $sfreq <= 24928500) {
											$validfreq = 1;
										}
									} elsif ($rband == 28) {
										if ($sfreq >= 28070500 && $sfreq <= 28189500) {
											$validfreq = 1;
										}
									}

									# handling of servers with memory scan
									if ($scanner eq "M" && $validfreq) {
										$sfreq = freqtomem ($sfreq);
									}

									#handling of servers with frequency list
									if (-e "$ENV{HOME}/.pskmail/validfrequencies.pm") {
										$validfreq = check_frequency ($sfreq);
									}
									
									$workingfreq = $sfreq;

									#handling of users on blacklist
									if (-e "$ENV{HOME}/.pskmail/blacklist") {
										open BL, "$ENV{HOME}/.pskmail/blacklist";
										@lines = <BL>;
										while (my $blcall = pop @lines) {
											chomp $blcall;
											if ($blcall eq $sumcall) {
												$validfreq = 0;
												print "User ", $sumcall, " blacklisted\n";
												last;
											}
										}
										close BL;
									}

									if ($validfreq && $scanner eq "F") {
										if ($rigtype ne "2"){
											$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "F" $sfreq`;
										} else {
											$dummy = `rigctl -m $rigtype "F" $sfreq`;
										}
										`echo "$requestedfreq" > $ENV{HOME}/.pskmail/.band`;

print "FREQ scan:", $sfreq, "\n";
									} elsif ($validfreq && $scanner eq "M"){
print "MEM scan:", $sfreq, "\n";
										$sfreq = freqtomem ($sfreq);
										if ($rigtype ne "2") {
											$dummy = `rigctl -m $rigtype -r $rigdevice -s $rigrate "E" $sfreq`;
										} else {
											$dummy = `rigctl -m $rigtype "E" $sfreq`;
										}

										`echo "$requestedfreq" > $ENV{HOME}/.pskmail/.band`;
									}
								}

								$AutoTune = 1;	# initiate an autotune if required
								newsession();
								$ServerStatus = "Connect_req";
								$Call = $1;				# set the call
								$ClientCall = $1;
								log_ping($Call, $2);		# > mheard list


								#Logic: the block size byte sent by the client in the connect frame carries the
								#  requested mode number to be used by the server. If zero this means that we stay in
								#  variable mode but symmetrical linkup. If it is > zero, we check that it is within boundaries.
								#  If yes, we use this as the TX mode of the server. If outside boundaries, we reset to variable
								#  symmetric mode. This should ensures backward compatibility too.

								# For the server RX mode, we use the current connect mode RSID (or current default mode for
								#   compatibilty if no RSID) and check that it is in the list. If yes, we use it as and sent
								#   it in the first byte of the status frames to the client for it to TX in that mode.
								#   The proto byte is the method for changing the server RX/Client TX mode. If the mode is not
								#   in the list, we revert to symmetrical mode.


								$reqtxmodenumber = (ord (substr($payload, -1))) - 48;

#print "Requested Server Tx modenumber =" . $reqtxmodenumber . "\n";

								if ($reqtxmodenumber == 0 ) {
									#Then we stay in symmetric mode. Compatible with older versions too.
									$asymlink = 0;
								} else {
									$asymlink = 1;

								}
#print "txmode now ", gettxmodem(), "\n";

								#Get rxid data if any
								my $lastrxid = "default";
								if (-e "$ENV{HOME}/.pskmail/.rxid") {
									open RXID, "$ENV{HOME}/.pskmail/.rxid";
									$lastrxid = <RXID>;
									chomp $lastrxid;
									close RXID;
#print "read from .rxid =" . $lastrxid . "=\n";
								} else {
#print "No file .rxid =\n";
								}
#print "default modem =", "\n";
								if ($lastrxid eq "default") {
									#We must have connected in the default mode then
									my $lastrxid = $Modem;
									if (tomodenumber($lastrxid) != 0) {
										print "Using default mode" . $lastrxid . "=\n";
									}
								}

								#Check that this mode is in the list, otherwise reset to symmetric mode
								#One option here would be to have an equivalent list for mode outside the list (e.g. for psk250)
								my $asymrxmode = tomodenumber($lastrxid);
#print "Converted to modenumber " , $asymrxmode , "\n";
								if ($asymrxmode > 0) {
									setrxmodem($asymrxmode);
								} else {
									#We don't know this mode, use it, but stay in symmetric mode
									$asymlink = 0;
#print "We don't know that mode - asymkink reset to 0\n";

								}

								#If in symmetric mode, txmode defines rx mode as well, rx mode = 0 then
								if ($asymlink == 0) {
									settxmodem(tomodenumber($lastrxid));
									setrxmodem(0);
								}



print "Final connect status:  Asymmetric link:", $asymlink, " , tx mode: " , gettxmodem() , " , rx mode: " , getrxmodem() , "\n";

								$Blockindex = 5;	# set default blocklength
								$BlockLengthStr= sprintf("%c", $Blockindex + 48);
								$BlockLength = (2 ** $Blockindex) ;

#Adaptative timing: no need to recalculate maxidle here. Now done just before usage using latest cps


								if ($debug == 2) {
									logprint ($ServerStatus . "\n");
									logprint ("Called by $ClientCall\n");
									logprint ("Max block length = $6\n");
								}
							}
						} else {	# I am a client
								log_ping($Call, $2);		# > mheard list
							if ($3 ne $options[0]) {
#								logprint ("Not my call...\n");
								return($TextFile);
							} else {
				# client connect request
								$CallerCall = $1;
								$Mode = $2;

								$Moderequest = $4;

								if ($Moderequest == 87) {
									$ServerStatus ="TTY_req";
									`echo "$CallerCall" > $ENV{HOME}/.pskmail/.mastercall`;

								} else {
									$ServerStatus = "Connect_req";
								}

								$Maxlen = 5;

								$Blockindex = $Maxlen;	# set max blocklength
								$BlockLengthStr= sprintf("%c", $Blockindex + 48);
								$BlockLength = (2 ** $Blockindex) ;

#Adaptative timing: no need to recalculate maxidle here. Now done just before usage using latest cps



								if ($debug == 2) {
									logprint ($ServerStatus . "\n");
									logprint ("Called by $CallerCall\n");
									logprint ("Max block length = $5\n");
								}

							}
						}

					}
	## Connect acknowledge	(PI4TUE:24 PA0R:1024 4)
				} elsif ($operand eq 'k') {
					
#print "RCVDACK\n";
#print $payload, "\n";
#print $ServerCall, "\n";
					if ($payload =~ /(.*):\d+\s(.*):\d+\s\d/ && $2 eq $ServerCall){ #my  call
						
						$Streamid = $Current_session;
#print "STREAM2:", $Current_session, "\n";
						open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
						print SESSIONDATA $1;
						close SESSIONDATA;
#print "SESSION:", $1, "\n";	
						
						$Transfer = 1;
					
#print "TX=" . gettxmodem() . " , RX=" . getrxmodem() . " , T=" . $trouble . " LQ=" . $linkquality . " BL=" . $Blockindex . "\n";
						
						$Idle_counter = $Idle_init;
						$ServerStatus = "Transfer_connect";

						if ($payload =~ /(.*)\s.*\s(.)/s){
							my $pingcall = $2;
							$pingcall =~ /(.*):(\d*)/;
							log_ping ($1, $2);
						}
						
						print "SENDSTATUS\n";
						$TXServerStatus = "TXStat";
						send_frame();
						$TXServerStatus = "none";
						
						return $TextFile;
					}
	## disconnect
				} elsif ($operand eq 'd' && $Current_session eq $Streamid) {
					$ServerStatus = "Disconnect_req";
					if ($Iamserver == 0) {
###try to kill the session#######################################
						open CLIENTOUT, ">>$ENV{HOME}/.pskmail/clientout";
						print CLIENTOUT "\n==Disconnected", "\n";
						close CLIENTOUT;
						open SESSIONDATA, ">$ENV{HOME}/.pskmail/PSKmailsession";
#						print SESSIONDATA $nosession;
						print SESSIONDATA "none";
						close SESSIONDATA;
						set_txstatus("TXDisconnect");
						send_frame();
						$mystring = "";
						$session = $nosession;
						`killall rflinkclient.pl`;
#################################################################

					}
					return $TextFile;
					
## status block ('abcdef')
				} elsif ($operand eq 's' && $Current_session_sent eq $Streamid) {
					
#						handle_status ();
					my $modemnr;
					if (-e "$ENV{HOME}/.pskmail/filetransfer") {
						# file transfer
						my $modetbl = $transfermodes;
						my $MAXMODENUM = length($modetbl);

						my @indexes = split "", $modetbl;
#			print $transfermodes, "\n";			
						$modemnr = ord($protobyte) - 48;
#			print $modemnr, "\n";
						my @modes = split "", $transfermodes;
						my @rmodes = reverse (@modes);
						my $txmodestr = $rmodes[$modemnr - 1];
#			print $txmodestr, "\n";
#			print "MAX:", $MAXMODENUM, "\n";
							for (my $i = $MAXMODENUM; $i > 0; $i--) {
								if ($i == 0) {
									$currentmodes[$i] = "default";
								} else {
									$currentmodes[$i] = $modelist{$indexes[$MAXMODENUM - $i]};
								}
#			print "LIST:", $i, ",", $currentmodes[$i], "\n";					
#			print "INX:", $indexes[$MAXMODENUM - $i], "\n";
								if ($i > 0 && $indexes[$MAXMODENUM - $i] eq $txmodestr) {
									$requestedmodestring = $currentmodes[$i];
#			print "MODEM:", $requestedmodestring, "\n";
									settxrsid(1);
									settxmodem($i);
								}
							}							
					}
					
					if ($payload =~ /(.)(.)(.)(.*)/s) {
						$HisLastblock = ord ($1) - 32;
						my $HisGoodblockchar = $2;
						my $HisEndblockchar = $3;
						$HisMissString = $4;
#print "PAYLOAD in status:", $payload, "\n";
						if ($HisGoodblockchar eq $HisEndblockchar){
							$HisMissString = "";
						}

#Disable tx rsid on receipt of a good status block
						if ($Transfer == 0) {
							settxrsid(0);
						}

						#recalibrate received s2n info back to 100
						# values '0' and ' ' are invalid, set to 100%
						# '0' for backword compatibility and ' ' for clients
						# which have not set S/N reporting

						if ($protobyte eq "0" || $protobyte eq " ") {
							$protobyte = "z";
						}

						$protobyte = ((ord $protobyte) - 32) * 100 / 90;

						set_blocklength ($HisMissString, $protobyte);

						@HisMissing = split('', $HisMissString);

						$ServerStatus = "Status_rx";

						$HisGoodblock = (ord $HisGoodblockchar) - 32;
						$HisEndblock = (ord $HisEndblockchar) - 32;

						############### purge send queue #############
						my $ij;
						my $jj;
						for ($ij = $HisGoodblock; $ij > ($HisGoodblock - 17); $ij--) {
							if ($ij < 0) {
								$jj =$ij + 64;
							} else {
								$jj = $ij;
							}
							$Sendqueue[$jj] = "";
						}


						##############################################

						if ($debug == 2) {
							print $HisLastblock, "\n";
							print $HisGoodblock, "\n";
							print $HisEndblock, "\n";
							print $HisMissString, "\n";
						}

						handle_rxqueue();

						$ReceivedlastBlock = 1;
						
						if (length($HisMissString) == 0) {
							set_idle (get_maxidle(15,2.5,5) -4);
						}
=head						
						if (gettxtqueue() == 0) {						
							print "SENDSTATUS_2\n";
							sleep 2;
							$TXServerStatus = "TXStat";
							send_frame();
							$TXServerStatus = "none";			
						}			
=cut						
					}
						return $TextFile;

## data block
				} else {
					if ($Current_session ne $Streamid) {
						return $TextFile;
					}
					$ServerStatus = "Data";
					my $Current = (ord $operand) - 32;

					if ($debug == 2) {		##debug
						printf ("Current=%d\n", $Current);
						printf ("Payload=%s\n", $payload);
					}

#					if ($payload =~ /~PROFILE\s*(\S+)\s*(\S+)/) {
#						$MODE[0] = $2; #lowmode
#						$MODE[1] = $1; #himode
#						`echo "$2" > $ENV{HOME}/.pskmail/.backoff_modem`;
#					}

					$payloadlength *= 7;
					$payloadlength += length ($payload);

					$payloadlength /= 8;

#Adaptative timing: no need to recalculate maxidle here. Now done just before usage using latest cps


					$ReceiveQueue[$Current] = $payload;

					if ($debug == 2) {			##debug
						for (my $ij = 0; $ij < $Bufferlength ; $ij++) {
							if ($ReceiveQueue[$ij]) {
								printf ("%d-%s\n", $ij, $ReceiveQueue[$ij]);
							}
						}
					}

				}
			} else {	# we received <EOT> but no valid status, so poll for a repeat...
				if ($closure eq "<EOT>" && $Current_session eq $Streamid) {
					open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
					my $session = <SESSION>;
					chomp $session;
					close SESSION;
					if ($session ne "none" && $session ne "beacon") {
						# send poll block
						$Serverstatus = "Poll_rx";
						set_txstatus("TXStat");
						send_frame();
						dprint ("SENDSTATUS");
						print "SENT POLLBLOCK\n";
					}
				}
			} # end checksum

		}
	} # end while
return $TextFile;
}

#####################################################################
sub handle_status {
#####################################################################
					my $modemnr;
					if (-e "$ENV{HOME}/.pskmail/filetransfer") {
						# file transfer
						my $modetbl = $transfermodes;
						my $MAXMODENUM = length($modetbl);

						my @indexes = split "", $modetbl;
#			print $transfermodes, "\n";			
						$modemnr = ord($protobyte) - 48;
#			print $modemnr, "\n";
						my @modes = split "", $transfermodes;
						my @rmodes = reverse (@modes);
						my $txmodestr = $rmodes[$modemnr - 1];
#			print $txmodestr, "\n";
#			print "MAX:", $MAXMODENUM, "\n";
							for (my $i = $MAXMODENUM; $i > 0; $i--) {
								if ($i == 0) {
									$currentmodes[$i] = "default";
								} else {
									$currentmodes[$i] = $modelist{$indexes[$MAXMODENUM - $i]};
								}
#			print "LIST:", $i, ",", $currentmodes[$i], "\n";					
#			print "INX:", $indexes[$MAXMODENUM - $i], "\n";
								if ($i > 0 && $indexes[$MAXMODENUM - $i] eq $txmodestr) {
									$requestedmodestring = $currentmodes[$i];
#			print "MODEM:", $requestedmodestring, "\n";
									settxrsid(1);
									settxmodem($i);
								}
							}							
					}
					
					if ($payload =~ /(.)(.)(.)(.*)/s) {
						$HisLastblock = ord ($1) - 32;
						my $HisGoodblockchar = $2;
						my $HisEndblockchar = $3;
						$HisMissString = $4;

						if ($HisGoodblockchar eq $HisEndblockchar){
							$HisMissString = "";
						}

#Disable tx rsid on receipt of a good status block
						if ($Transfer == 0) {
							settxrsid(0);
						}

						#recalibrate received s2n info back to 100
						# values '0' and ' ' are invalid, set to 100%
						# '0' for backword compatibility and ' ' for clients
						# which have not set S/N reporting

						if ($protobyte eq "0" || $protobyte eq " ") {
							$protobyte = "z";
						}

						$protobyte = ((ord $protobyte) - 32) * 100 / 90;

						set_blocklength ($HisMissString, $protobyte);

						@HisMissing = split('', $HisMissString);

						$ServerStatus = "Status_rx";

						$HisGoodblock = (ord $HisGoodblockchar) - 32;
						$HisEndblock = (ord $HisEndblockchar) - 32;

						############### purge send queue #############
						my $ij;
						my $jj;
						for ($ij = $HisGoodblock; $ij > ($HisGoodblock - 17); $ij--) {
							if ($ij < 0) {
								$jj =$ij + 64;
							} else {
								$jj = $ij;
							}
							$Sendqueue[$jj] = "";
						}


						##############################################

						if ($debug == 2) {
							print $HisLastblock, "\n";
							print $HisGoodblock, "\n";
							print $HisEndblock, "\n";
							print $HisMissString, "\n";
						}

						handle_rxqueue();

						$ReceivedlastBlock = 1;
						
						print "SENDSTATUS\n";
						$TXServerStatus = "TXStat";
						send_frame();
						$TXServerStatus = "none";
						
					}
}


#####################################################################
sub gettxinput {
#####################################################################
#print "GETTX\n";
#my $content = `cat $ENV{HOME}/.pskmail/TxInputfile`;
#print length($content), "\n";
#print $content;

if (-s "$ENV{HOME}/.pskmail/TxInputfile") {
	my $txinfile = `cat $ENV{HOME}/.pskmail/TxInputfile`;
	unlink "$ENV{HOME}/.pskmail/TxInputfile";
#print "::$txinfile\n";
	my @strcharacters = split //, $txinfile;

	foreach my $badchar (@strcharacters) {
			if (ord ($badchar) < 5) {
				$badchar = sprintf ("|Ctl-%c|", ord ("A") + ord	($badchar));
			}
	}

	$txinfile = join "", @strcharacters;

	$txinfile =~ s/=?ISO-8859-1?Q?//g ;				# get rid of this strange stuff

	while($txinfile =~ /=([89ABCDEF][0-9A-F])/) { 	#quoted printables?
			$zahl = hex($1);
			$zahlstring = sprintf("&%d;", $zahl);	# use html coding of ISO 8859-1
			reset
			$txinfile =~ s/=[89ABCDEF][0-9A-F]/$zahlstring/ ;
	}

	$txinfile =~ s/<SOH>/<SOHSign>/g;
	$txinfile =~ s/<EOT>/<EOTSign>/g;

	my @characters = split (//, $txinfile);		# anything left?
	foreach my $char (@characters) {
		if (ord ($char) > 127) {
			$char = "&" . ord ($char) . ";";	# html encoding of ISO 8859-1
		}
	}

	$txinfile = (join "", @characters);

	$txinfile =~ tr/\200-\377/./;	# just in case....

#print "$txinfile\n";

	return $txinfile;
}

Queue_txdata("");

return "";

}
#########################################################
sub listening {
#########################################################
#		my $string = getinput();
my $debug = 0;
my $string = "";
my $dummy;
#my $bigearreader = shift @_;

	if ($debug == 1) {	##DEBUG

								my $debugv = get_idle();

								open (DOUT, ">>" , "testtxt");
								print DOUT ">>>>", "Listening,$debugv", "\n";
								close DOUT;
	}					##DEBUG end

	if (-e "$ENV{HOME}/.pskmail/.input") {
		open (INPT, "<", "$ENV{HOME}/.pskmail/.input");
		@instring = <INPT>;
		close INPT;
		unlink "$ENV{HOME}/.pskmail/.input";
		$string = join "", @instring;
		
		

		if ($string =~ /<EOT>/) {
			if (-e "$ENV{HOME}/.pskmail/squelch.lk"){
				unlink "$ENV{HOME}/.pskmail/squelch.lk";
			}
		}

		if ($debug == 2) {	##DEBUG
								open (DOUT, ">>" , "testtxt");
								print DOUT ">>>>", $string, "\n";
								close DOUT;
		}					##DEBUG end

	} else {
		# BigEar experimental, removed form main stream
		return;
	}
	
#	print "STRING to unframe:", $string, "\n";
		$TextFromFile .= $string;

		my $count = 0;
		$dummy = unframe($string);

#print "LISTENING\n";
		return;
}

#####################################################################
sub initialize {
#####################################################################

	$Call = $ClientCall;

	for (my $i = 0; $i < $Bufferlength; $i++) {
		$ReceiveQueue[$i] = "";
	}

#	sysopen (INFH, $Inputfile, O_NONBLOCK|O_RDWR);

}

#####################################################################
sub get_rxstatus {
#####################################################################

	return $ServerStatus;
}
#####################################################################
sub reset_rxstatus {
#####################################################################

	$ServerStatus = "Listening";
}
#####################################################################
sub set_rxstatus {
#####################################################################

	$ServerStatus = "Transfer_req";
}
#####################################################################
sub get_call {
#####################################################################

	return $ClientCall;
}

#####################################################################
sub set_txstatus {
#####################################################################

	$TXServerStatus = shift @_;
}
#####################################################################
sub set_connectstatus {
#####################################################################

	$ConnectedFlag = "Connected";

}
#####################################################################
sub reset_connectstatus {
#####################################################################

	$ConnectedFlag = "Disconnected";

}
#####################################################################
sub get_connectstatus {
#####################################################################

	return $ConnectedFlag ;

}

#####################################################################
sub reset_arq  {
#####################################################################
$Firstsent = 0;
$Lastblock = 0;
$Endblock = 0;
$Goodblock = 0;
$Lastqueued = 0;
@Missing = ();
$MissString = "";
$HisGoodblock = 0;		# Other station's Good block
$HisLastblock = 0;		# Other station's Block last sent
$HisEndblock = 0;		# Other station's last received block
@HisMissing = ();		# Other station's missing blocks
@Sendqueue = ();
$TxTextQueue = "";
$RxTextQueue = "";
@ReceiveQueue = ();
$Idle_counter = $Idle_init;		# seconds from last <SOH>
$Connect_time = 0;		# connect time in seconds
$Interval_time = 0;		# 500 seconds interval
$Tx_time = 0;
if (-e $TxInputfile) { unlink $TxInputfile; }

}

#####################################################################
sub get_rxqueue  {
#####################################################################

return $RxTextQueue;

}

#####################################################################
sub reset_rxqueue  {
#####################################################################

	$RxTextQueue = "";
}

#####################################################################
sub get_rxqueue_status  {
#####################################################################
my @outlist = ();
	push @outlist, $Lastblock;
	push @outlist, $Endblock;
	push @outlist, $Goodblock;
	push @outlist, $MissString;
	return @outlist;

}
#####################################################################
sub check_lastblock  {
#####################################################################

	my $flag = $ReceivedLastBlock;
	$ReceivedLastBlock = 0;
	return $flag;
}

#####################################################################
sub inc_idle  {
#####################################################################
sleep (1);
if ($Iamserver && -e "$ENV{HOME}/.pskmail/.tx.lck") {
	$Idle_counter = $Idle_init;
	$Tx_time++;
	if ($Tx_time > 30){
		$Tx_time = 0;
		`rm $ENV{HOME}/.pskmail/.tx.lck`;
	}
} else {
	$Idle_counter++;
}
#print $Idle_counter, "\n";
$Connect_time++;
$Interval_time++;

}

#####################################################################
sub get_idle  {
#####################################################################

return $Idle_counter;

}
#####################################################################
sub get_maxidle  { #Argumens are:
#	1. Data size of frame (e.g. blocklength or 3 to 11 for status frame)
#	   Frame size excludes soh, eot, crc, and the first 3 bytes of each frame
#	2. Safety factor for timing
#	3. Tournaround time for client (TYpically total of TX delay, DCD, response time)
# Latest CPS data (received or calculated) is used for calculating timeout
#####################################################################
my $framesize = 32;
my $safetyfactor = 2;
my $turnaroundtime = 5;
my $cps = 10;

$framesize = shift @_;
$safetyfactor = shift @_;
$turnaroundtime = shift @_;
$max_idle = 4;

	if (-e "$ENV{HOME}/.pskmail/receiedCPS") {
		open CPSDATA, "$ENV{HOME}/.pskmail/receivedCPS";
		$cps = <CPSDATA>;
		close CPSDATA;
	}

	$cps += 0;

	if ($cps > 0) {
		$max_idle = (($framesize + 9) / $cps ) * $safetyfactor + $turnaroundtime;
	}

	if ($max_idle > 58) {
		$max_idle = 58;
	}

#print "In get_maxidle size: $framesize, safety: $safetyfactor, Turnarond: $turnaroundtime, cps: $cps, ret maxidle: $max_idle \n";

	return $max_idle;
}

#####################################################################
sub reset_idle  {
#####################################################################

$Idle_counter = $Idle_init;

}

#####################################################################
sub set_idle  {
#####################################################################

my $Idle = shift @_;
$Idle_counter = $Idle;

}

#####################################################################
sub get_connect_time  {
#####################################################################

return $Connect_time;

}
#####################################################################
sub get_sendqueue  {
#####################################################################

	my $qlength = $Lastqueued - $HisGoodblock;

	if ($qlength < 0) { $qlength += $Bufferlength };

return $qlength;

}

#####################################################################
sub set_blocklength {
# 1st parameter = list pf missing blocks, 2nd parameter = protobyte from client = (his received s2n - std dev) * 90 / 100
#####################################################################
my $errors = shift @_;
my $hislatestrxs2n = shift @_;
my $trouble = length $errors;
#print "ERRORS:$errors\n";

############### constants.... ##############

my $LQ_down = 2;
my $LQ_up1 = 2;
my $LQ_up2 = 3;
my $LQ_up3 = 6;
my $LQ_preset = 3;


my $Min_index = 3;
my $Max_index = 6;
############################################

#	if ($Lastqueued == $HisGoodblock) { return };

my $latestrxs2n = -1;

#Client TX speed adjustment
	if (-e "$ENV{HOME}/.pskmail/.rxs2n") {
		$latestrxs2n = `cat $ENV{HOME}/.pskmail/.rxs2n`;
		$latestrxs2n += 0; #convert to numeric
		`rm $ENV{HOME}/.pskmail/.rxs2n`;
	}
	
	if (-e "$ENV{HOME}/.pskmail/.ctstiamode") {
		$latestrxs2n = 100 *  8 / 8 + $trouble;
#print "CONTESTIACORRECTION:", $latestrxs2n, "\n";
	}

	my $currentrxmode = getrxmodenumber();

	if ($latestrxs2n > 0) {
		$rxs2n = decayavg($rxs2n, $latestrxs2n, 3); #This is 10% old, 90% new for fast convergence
	}
print "Server's rx s2n: " . $latestrxs2n . " , averaged: " . $rxs2n . " \n";
`echo "$latestrxs2n" > $ENV{HOME}/.pskmail/.servers2n`;


	if ($currentrxmode ne 0) {
		if (upgradable(getrxmodenumber(), $rxs2n)) {
			print "Upgrading RX speed \n";
			adjustrxmodem(+1);
			$rxs2n = 50; # reset average s2n value
		}
	}


#Server TX speed adjustment
	if (-e "$ENV{HOME}/.pskmail/.retries") {
		$Haveretries = `cat $ENV{HOME}/.pskmail/.retries`;
		`rm $ENV{HOME}/.pskmail/.retries`;
	} else {
		$Haveretries = "";
	}

	if ($Haveretries) {
##VK2ETA: I don't understand the logic of fixing the linkquality and block length here?
		$linkquality = 8;
		$Blockindex = $Max_index - 1;
	}

	if (substr($errors,0,1) eq $olderror && $trouble > 0) {
			$linkquality += $LQ_up2;
	} else {
			$olderror = substr($errors,0,1);
	}

	my $currenttxmode = gettxmodenumber();

	if ($hislatestrxs2n > 0) {
		$hisrxs2n = decayavg($hisrxs2n, $hislatestrxs2n, 3); #This is 10% old, 90% new for fast convergence
	}

print "Client's rx s2n: " . $hislatestrxs2n . " , averaged: " . $hisrxs2n . " \n";
`echo "$hislatestrxs2n" > $ENV{HOME}/.pskmail/.clients2n`;

	#If no trouble, and client s2n feedback is top, then we upgrade ASAP
	if ($trouble == 0 && $currentmodes[$currenttxmode] ne "PSK500R") {
		if (upgradable($currenttxmode, $hisrxs2n) == -1) {
			$linkquality = 0;  #directly to the minimum
			print "Try Turbo upgrade of TX mode\n";
		} else {
			$linkquality -= $LQ_down; #Otherwise just improve LQ to let blocksize grow
		}
	} elsif (upgradable($currenttxmode, $hisrxs2n) == 1 && $trouble < 2) {

		$linkquality = 0;  #directly to the minimum

	} elsif ($trouble < 2 ) {

		$linkquality -= $LQ_down;

	} elsif ($trouble == 2) {
			$linkquality += 0;
	} else {
			$linkquality += $LQ_up2;

			if (gettxmodem() eq "PSK500") {  # get out of PSK500 quickly...
				$linkquality += $LQ_up2;
				$linkquality += $LQ_up2;
				$linkquality += $LQ_up2;
			}
	}


	if ($linkquality < 0) {
		$linkquality = 0;
	}
	if ($linkquality > 9) {
		$linkquality = 9;
	}

print "TX=" . gettxmodem() . " , RX=" . getrxmodem() . " , T=" . $trouble . " LQ=" . $linkquality . " BL=" . $Blockindex . "\n";


	if ($linkquality > 8) {
		if ($Blockindex > $Min_index) {	# minimum 16 chars
			$Blockindex--;
			$linkquality = 4;
			#If we are in symetric mode RX will follow TX
			if ($Blockindex le $Min_index && gettxmodenumber() ne 0) {

				if (adjusttxmodem(-1)) {	#Downgrade TX Speed by one step
					Time::HiRes::sleep(0.2);
					settxrsid(1);
					print "Backoff to TX:", gettxmodem(), " , RX: ", getrxmodem(), "\n";
					$Blockindex = $Max_index;
					$hisrxs2n = 50; # reset average s2n value
				}
			}
		}
	} elsif ($linkquality < 2) {

			if ($Blockindex < $Max_index) {
				$Blockindex++;
##VK2ETA Debugging: speed up block & mode inc				$linkquality = 6;
				$linkquality = 4;
			}

			#If we are in symetric mode RX will follow TX, but also check for server's s2n
			# Do not upgrade if client's s2n is not high enough
			if ($Blockindex ge $Max_index && $currenttxmode ne 0 && upgradable($currenttxmode, $hisrxs2n)) {
				#If symmetric modes, check server's rx s2n as well
				if ($currentrxmode != 0 || upgradable($currenttxmode, $rxs2n)) {
					if (adjusttxmodem(+1)) {	#Upgrade TX Speed by one step
						Time::HiRes::sleep(0.2);
						print "Up-speed to TX:", gettxmodem(), " , RX: ", getrxmodem(), "\n";
						settxrsid(1);
						$rsidon = 1;
						reset_idle();
##Reduce block size...will grow again after
						$Blockindex--;
						$hisrxs2n = 50; # reset average s2n value
						if (-e "$ENV{HOME}/.pskmail/.ctstiamode") {  #get out of Contestia
							unlink "$ENV{HOME}/.pskmail/.ctstiamode";
							$CTSTIA = "";
							$CTSTIA_mode = 0; 
						}
					}
				}
			}

	}

	$BlockLength = (2 ** $Blockindex) ;
	`echo "$BlockLength" > $ENV{HOME}/.pskmail/.blocklength`;
	
	set_idle(get_maxidle(15, 2.5, 5) - 2);
}

################################################################
sub send_ping {
################################################################

$TXServerStatus = "TXPing";
send_frame();

}
################################################################
sub send_uimessage {
################################################################
my $message = shift @_;
$TXServerStatus = "TXUImessage";
send_frame($message);

}
################################################################
sub send_aprsmessage {
################################################################
my $message = shift @_;
$message =~ s/\r//g;
$TXServerStatus = "TXaprsmessage";
send_frame($message);

}
###############################################################
sub sendtransfer {
###############################################################
			my $info = shift @_;
					$Lastblockinframe = 1;
					my $outstring .= make_block($info);
#					print "OUTSTRING:", $outstring, "\n";
					$Transfer = 1;
					$TXServerStatus = "TXtransferConnect";	
					sendit($outstring . $eot);
}

################################################################
sub send_linkreq {
################################################################

$TXServerStatus = "TXlinkreq";
send_frame();

}
################################################################
sub get_positmsg {
################################################################

return $positmessage;

}

################################################################
sub log_ping {
################################################################
my $pingcall = shift @_;
my $service = shift @_;
#if ($service =~ /\d*/) { $service = $1;}
$pingcall .= ":";
$pingcall .= $service;

my $S2N = `cat $ENV{HOME}/.pskmail/.servers2n`;
chomp $S2N;

my $Qcall = $pingcall;
$Qcall =~ /(.*)\-*:.*/;
my $qualitylog = $1 . "," . sprintf "%s,%d", getdate(), $S2N;
`echo $qualitylog >> $ENV{HOME}/.pskmail/rxquality`;  # log quality value

if (exists($pingdb{$pingcall})) {
	my $pingvalue = $pingdb{$pingcall};
	my @values = split ",", $pingvalue;
	
	$S2N = ($values[2] + $S2N) / 2;

	$values[1]++;
	$pingdb{$pingcall} = sprintf "%s,%d,%d", getdate(), $values[1], $S2N;
} else {
	$pingdb{$pingcall} = sprintf "%s,%d,%d", getdate(), 1, $S2N;
}


if ($Iamserver) {
	open ($lg, ">$ENV{HOME}/.pskmail/pskdownload/pings.log");
	print $lg $Startmessage;
	while ((my $key, my $pvalue) = each (%pingdb)) {
		my $logcall = substr ($key . "        ", 0, 13);
		$pvalue =~ s/,/ - /g;
		my $logline = $logcall . $pvalue;
		print $lg $logline, "\n";
	}
	close ($lg);
} else {
	open ($lg, ">$ENV{HOME}/.pskmail/pings.log");
	print $lg $Startmessage;
	while ((my $key, my $pvalue) = each (%pingdb)) {
		my $logcall = substr ($key . "        ", 0, 13);
		$pvalue =~ s/,/ - /;
		my $logline = $logcall . $pvalue;
		print $lg $logline, "\n";
	}
	close ($lg);

}
}

################################################################
sub getgmt {
################################################################
my @array_month = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
my @array_week = qw(Sun Mon Tue Wed Thu Fri Sat);
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime time;
my $fixyear = 1900 + $year;
my $abbmonth = $array_month[$mon];
my $abbweek = $array_week[$wday];
my $fixmday = substr(" " . $mday, -2);
my $fixhr = substr("0" . $hour, -2);
my $fixmin = substr("0" . $min, -2);
my $fixsec = substr("0" . $sec, -2);
my $outtime = $abbweek . " " . $abbmonth . " " . $fixmday . " " . $fixhr
		. ":" . $fixmin . ":" . $fixsec . " UTC " . $fixyear . "\n";


return $outtime;


}

################################################################
sub getdate {
################################################################
my $timedate = getgmt();
$timedate =~ /(\w*)\s*(\w*)\s*(\d*)\s*(\d\d:\d\d):\d\d\s(.*)\s*(\d\d\d\d)/;
my $outtime = sprintf "%s", "$4 UTC $2-$3-$6";

return $outtime;

}
################################################################
sub getlocale {
################################################################
	my $txt = shift @_;

	while ($txt =~ /\&(\d\d\d);/) {
		my $char = chr ($1);
		reset
		$txt =~ s/\&\d\d\d;/$char/;
	}
return $txt;
}

################################################################
sub logprint {
################################################################
	my $txt = shift @_;
	my $dt = getdate();
$logfile = "$ENV{HOME}/.pskmail/server.log";
open ($logfh, ">>", $logfile) or die "Cannot open logfile";
flock ($logfh, LOCK_EX);	# lock logfile for write
print $dt . ": " . $txt;
print $logfh $dt . ": " . $txt;
close ($logfh);

}


###############################################################
sub aprs_send {	#message, port
###############################################################
		my $MSG = shift @_ ;

$isaprs = 0;

if ($MSG =~ /.*TCPIP..:[A-Z]*[0-1][A-Z]+/) {
	$isaprs = 1;
} elsif ($MSG =~ /.*TCPIP..[!@]/) {
	$isaprs = 1;
} elsif ($MSG =~ /.*TCPIP..:PSKAPR/) {
	$isaprs = 1;
} elsif ($MSG =~ />PSKAPR/) {
	$isaprs = 1
}

		if ($Aprs_connect & $isaprs) {
			$MSG .= "\n";

			my $aprstime = `date -u +"%d %H:%M"`;
			$aprstime =~ /(\d+)\s+(\d\d:\d\d)/;
	#		$aprstime =~ /\w+\s+\w+\s+(\d+)\s+(\d\d:\d\d)/;
			$error = `echo "$1 $2> $MSG" >> $ENV{HOME}/.pskmail/aprslog`;

			open ($fh, ">$ENV{HOME}/.pskmail/.aprsmessage");
			flock ($fh, LOCK_EX);
			print $fh $MSG;
			close ($fh);
		}


}
###############################################################
sub getoptions {
###############################################################
	if ($Iamserver == 0 && -e "$ENV{HOME}/.pskmail/.PSKoptions") {
		open (OPTIONS, "$ENV{HOME}/.pskmail/.PSKoptions") or die "No options file\n";
		@options = <OPTIONS>;
		close (OPTIONS);

		chomp $options[0];
		chomp $options[1];
		chomp $options[2];
		chomp $options[3];
		chomp $options[4];

		$ClientCall = $options[0];
		$Call = $ClientCall;
		$ServerCall = $options[1];
		$Blockindex = $options[2];
		$Latitude = $options[3];
		$Longitude = $options[4];
	}

}

###############################################################
sub checklink {
###############################################################

my $linkcall = shift @_;

	if (exists($Owned_list{$linkcall})) {

		my $linktime = time - $Owned_list{$linkcall};

		if($linktime <= $Maxlinktime ){
				$Owned_list{$linkcall} = time ();
				print "setting link record: $linkcall,$linktime\n";
				return 1;
		} else {
			return 0;
		}
	}
0;
}
###############################################################
sub deletelink {
###############################################################
my $deletecall =    @_;

delete ($Owned_list{$deletecall});
}

###############################################################
sub aprs_serv {
###############################################################
my $AprsCall = shift @_;
my $Message = shift @_;

#	if (checklink ($AprsCall) == 1) {
	if (1){	#always on receive...

		$Call = $AprsCall;

		if (checklink ($AprsCall)) {
			$Owned_list{$AprsCall} = time();
		}

		if ($Message =~ /&&/) {
			if (exists($aprs_store{$AprsCall})) {
				$Message = $aprs_store{$AprsCall};
				my $MSG = $AprsCall . ">PSKAPR,TCPIP*:$Message";
				aprs_send ($MSG);
			}

		} elsif (index ($Message, "!") == 0 ||
			index ($Message, "@") == 0 ||
			index ($Message, "=") == 0 ) {

			my $MSG = $AprsCall . ">PSKAPR,TCPIP*:$Message";
			aprs_send ($MSG);
			$aprs_store{$AprsCall} = $Message;
			if (checklink ($AprsCall)) {
				$Owned_list{$AprsCall} = time();
			}

		} elsif ($Message =~ m/(\w+\-*\d*)\s+(.*)/) {
			my $To_call = $1;
			my $message = $2;

			if ($message !~ />PSKAPR/) {
				if (checklink ($To_call)) {
					$Owned_list{$To_call} = time();
				}

				if (checklink($To_call)) {

					if (substr($message, 0, 0) ne ":"){
						$To_call .= "     ";
						$To_call = substr ($To_call , 0, 9);
						$message = $AprsCall . ">PSKAPR*::$To_call:$message";
						$TXServerStatus = "TXaprsmessage" ;
						send_frame( $message);
					}
				} else {

					$To_call .= "     ";
					$To_call = substr ($To_call , 0, 9);
					my $MSG = $AprsCall . ">PSKAPR,TCPIP*::$To_call:$2" ;

					if (substr($message, 0, 0) ne ":"){
						aprs_send ($MSG) ;
					}
				}
			} # end if

		}

	} else {
#		logprint ("$AprsCall not linked...\n");

	}
}

###############################################################
sub aprs_client {
###############################################################
my ($Call, $Message) = @_;
#":PE1FTV   :Message test...."
#":PE1FTV   :Message test....{003"
#":PE1FTV   :ack003"
#":PE1FTV   :rej003"
#":BLN1     :Bulletin...."
#":BLNA     :Announcement...."
#":BLN3WXGRP:Group bulletin...."
#PE1FTV>PSKAPR::PA0R :ack31
#PE1FTV>PSKAPR::PA0R :[AA] Sorry ... out of office .. 73, Ad{49

my $msgdate = getdate();
$msgdate =~ /(\d*:\d*)\s.*/;
$msgdate = $1;
my $displaymessage;
my $To;
my $From;
my $Aprsmessage;

	if ($Message =~ m/(\w*\-*\d*).*::(\w*\-*\d*)\s*:(.*)/) {
		$Message .= "\n";
		$From = $1;
		$To = $2;
		$Aprsmessage = $3;
		$displaymessage = $msgdate . "-" . $1 . "-" . $3;

		#`echo ">$Message >> $ENV{HOME}/.pskmail/clientout`;
		if ($To eq $ClientCall || $To eq "APRPACK  ") {

			if ($Message =~ m/:ack\d*/) {
				# don't display acks
			} elsif ($Message =~ m/(\w*\-*\d*).*{(\d*|\w)/ ) {

				my $MSG = "$1 ack$2\n";

				$TXServerStatus = "TXaprsmessage";
				send_frame($MSG);

				`echo $displaymessage >> $ENV{HOME}/.pskmail/clientout`;
			} else {

				`echo $displaymessage >> $ENV{HOME}/.pskmail/clientout`;
			}
	} elsif ($Message =~ m/(\w*\-*\d*)\s(.*)/) {
		$To = $1;
		if ($To eq $ClientCall || $To eq "APRPACK  ") { # only messages to myself
				$displaymessage = $msgdate . "-" . $1 . "-" . $2;
				`echo $displaymessage >> $ENV{HOME}/.pskmail/clientout`;
		}
	} else {
		# don't know what to do.
	}
}

}
###############################################################
sub aprs_connect {
###############################################################
my ($host_out, $port_out) = @_;
my $handle_out;

   # create a tcp connection to the specified host and port
	eval {
		$handle_out = IO::Socket::INET->new(Proto     => "tcp",
	                                    PeerAddr  => $host_out,
	                                    PeerPort  => $port_out)
	           or die "can't connect to port $port_out on $host_out: $!";

	};

	if ($@) {
		logprint ($@)  ;
	} else {
		logprint ("Connected to $host_out:$port_out\n");
	}
	return $handle_out;
}


###############################################################
# dohash
#
# Steve Dimse, K4HG, released this algorithm to the public domain
# April 11, 2000.  He posted it to the APRSSIG mailing list in
# the form of C source code.
#
# This function takes a callsign as input and returns the password.
# SSID is stripped from the callsign before computing the password,
# so any SSID will result in the same password.
#
#
sub dohash
{
  my $call = $_[0];

  my $kKey = 0x73e2;

  my $short_call = $call;
  $short_call =~ tr/a-z/A-Z/;           # Convert to uppercase
  $short_call =~ s/(\w+)\-*.*/$1/g;     # If SSID, remove it

  my $hash = $kKey;                     # Initialize with the key value

  my $i = 0;
  my $len = length( $short_call );
  $short_call = $short_call . "\0";     # Add 0x00 to make sure we don't run off the end

  while ($i < $len)                     # Loop through the string two bytes at a time
  {
    my $char = substr($short_call,$i,1);
    #printf( "%s\n", $char );

    $hash = $hash ^ ( ord($char) <<8 ); # Xor high byte with accumulated hash
    $i++;

    $char = substr($short_call,$i,1);
    #printf( "%s\n", $char );

    $hash = $hash ^ ord( $char );       # Xor low byte with accumulated hash
    $i++;
  }
  $hash = $hash & 0x7fff;               # Mask off the MSB so number is always positive
#  print "Hash = $hash\n";
  return($hash);
}

#########################################################
sub filter_aprs {
#########################################################
my $line = shift @_;

#print "APRS:", $line, "\n";

#00uPI4TUE:26 PA0R>PSKAPR*::PA0R     :test{087852
#
my $pre;
my $tocall;

if ($scancls[0]) {
	my $min = time() / 60 % 5;
	$ServerCall = $scancls[$min];
	chomp $ServerCall;
}


#DJ0LN>APRS,TCPIP*,qAC,THIRD::PA0R :3rd message test
#print "FIL0:", $line, "\n";
#`echo "FIL0:$line" >> $ENV{HOME}/.pskmail/filtest`;
#
		if (index ($line, "PSKAPR") > 0) {# server->server
#			print "FIL::". $line, "\n";
			if ($line =~ /^(\S+)>.*GATING\s(\S+)_*/) {
				print "$2->$1\n";
				$tocall = $2;
				if ($1 ne $ServerCall) {
#					print "dropping call from list 1\n";
#					foreach my $key (keys %Owned_list) {
#						print $key, "\n";
#					}
#					print $2, ":\n";
					if (checklink($2)) {
						print "dropping call from list 2\n";
						deletelink();
						delete $RFout_list{$2};
					}
				}
				my $time = time();
				$pre = $1 . " " . $time . "\n";
				$Routes{$2} = $pre;
				my $ro = "";
				open (ROUTES, ">$ENV{HOME}/.pskmail/routes");
				foreach $key (keys %Routes) {
					$value = $Routes{$key};
					$ro = $ro . $key . " " . $value . "\n";
				}
#				print $ro; ##DEBUG
				print ROUTES $ro;
				close (ROUTES);
			}


		}



#DJ0LN>APRS,TCPIP*,qAC,FOURTH::PA0R     :ggggg
		if ($line =~ /(\S+)>APRS.*::(\S+)\s*:(.*)/) {
#			print $line;
#			print "DEB:", $1, "," , $2, "," , $3, "\n";
			my $myroute = `cat $ENV{HOME}/.pskmail/routes | grep $ServerCall | grep $2`;
						if ($myroute) {
							my $tocall = $2 . substr("         ", 0, 9 - length($1));
							my $sendmessage = $1 . ">PSKAPR::" . $tocall . " :" . $3 . "{01";

							send_aprsmessage($sendmessage);
							sleep 20;
						}
			return "";
		}

		if ($line =~ /(\S+)>.*::(\S+)\s*:.*{(.*)}.*/ && $1 ne "") {
			my $srvr = "none" ;
			if ($2) {$srvr = $2;}
#			print "2:", $2, "\n";
				if ($srvr =~ /[A-Z0-9\-]/) {
					eval {
						my $myroute = `cat $ENV{HOME}/.pskmail/routes | grep $srvr | grep $ServerCall`;
						if ($myroute && length($1) > 2) {
											my $toCall = $1 . substr("         ", 0, 9 - length($1));
											$MSG = $2 . ">PSKAPR,TCPIP*::" . $toCall . ":ack" .  $3 . "}";
											aprs_send($MSG);

							return $line;
						}

					};
				}


		}

		return "";

}
###############################################################
sub handle_aprs {
###############################################################

my $line = shift @_;

if ($line) {
	print "HANDLE:$line";
#	$error = `echo "< $line" >> $ENV{HOME}/.pskmail/aprslog`;
}

my $fromcall ;
my $groupcall;
my $path;
my $type;
my $tocall;
my $message;
my $mesgnumber;

if ($scancls[0]) {
	my $min = time() / 60 % 5;
	$ServerCall = $scancls[$min];
	chomp $ServerCall;
}

	my @tailout = `tail -n 4 $ENV{HOME}/.pskmail/aprslog | grep GATING`;
	foreach my $rec (@tailout) {

#print "FIL4:$rec\n";
		if (index ($rec, $ServerCall) > 0) {
			$rec =~ /(\d*) (\d\d):(\d\d)> .*GATING\s(\w*)/;
			my $minutes = $3;
			my $hours = $2;
			my $mday = $1;
			my ($mon, $year, $sec) = (localtime)[4,5,0];

			my $gatetime = timelocal ($sec, $minutes, $hours, $mday, $mon, $year);
			my $gatingcall = $4;
			$gatingcall =~ tr/_//;	# remove '_'

			$RFout_list{$gatingcall} = $gatetime;

			push (@prefixes, $gatingcall); # add to prefixlist if needed
			my %seen = ();
			foreach my $ccall (@prefixes) {
				$seen{$ccall}++;
			}
			@prefixes = keys %seen;


		}else {
			if ($rec =~ /< (\w*\d*\w*\-*\d*)>PSKAPR.*GATING\s(\S+)/)        {
				my $gatingcall = $2;
				$gatingcall =~ tr/_//;	# remove '_'
				if (exists ($RFout_list{$gatingcall})) {
					delete $RFout_list{$gatingcall};
					delete $Owned_list{$gatingcall};
				}
			}
     	}
	}

	$line =~ tr/\r\b\f/_/;

		if ($line =~ /^(\w*\-*\d*)>(\w*),*(.*?):GATING (\w*\d*\w*\-*\d*)_*/) {	# Gating message

		if ($1 ne $ServerCall) {
			my $dropcall = $4;
			chop $dropcall ;
			if (exists ($Owned_list{$dropcall})) {
				delete ($Owned_list{$dropcall});
			}
			delete ($RFout_list{$dropcall});

			print "Dropping $dropcall from record\n";
		}
	} elsif ($line =~ /^(\w*\-*\d*)>(\w*),*(.*?):(>|=|!|\?|@)(.*)/) {	# posits, no gating at present
		;	# do nothing (yet)
#		print "Alt data:$line\n";
	} elsif ($line =~ /^(\w*\-*\d*)>(\w*),*(.*?):(.)(\w*\-*\d*)\s*:(.*)(\{*.*)/) {	# message
		$fromcall = $1;
		$groupcall = $2;
		$path = $3;
		$type = $4;
		$tocall = $5;
		$message = $6;
		if  ($message =~ /(.*)(\{\d*)/) {
			$message = $1;
			$mesgnumber = $2;
		} elsif ($message =~ /(.*)(\{\w)/){
			$message = $1;
			$mesgnumber = $2;
		} else {
			$mesgnumber = "";
		}

		if ($type eq ":") {
			if ($message =~ /(ack\d*)/) {		# clean ack message
				$message = $1;
			} elsif ($message =~ /(ack\w)/) {	# clean ack message
				$message = $1;
			}
			print "From: $fromcall To: $tocall Mesg: $message Nr: $mesgnumber\n"; # gate message > RF

			my $sendmessage = $fromcall . ">PSKAPR::" . $tocall . " :" . $message . $mesgnumber;

			foreach my $messagetime (keys %Messagehash) {	# reset the message array
				if (time - $Messagehash{$messagetime} > 30 ){
					delete $Messagehash{$messagetime};
				}
			}

			my $messagekey = $fromcall . $mesgnumber;

			if (exists($Messagehash{$messagekey})) {
													# send ack via aprs?? how?
			} else {
				$Messagehash{$messagekey} = time;	# add message to table

#				if (exists($Owned_link{$tocall})) {
print "Waiting to send aprs message...\n";
						sleep 30;
						send_aprsmessage($sendmessage);
						sleep 20;					# allow time for ack...
#				}
			}

		} elsif ($type eq "{") {
			if ($message =~ /GATING (\w*\d*\w*\-*\d*)/) {
				my $dropcall = $1;
				$dropcall =~ tr/_//;
				print "Drop link $dropcall\n";
				if (exists ($Owned_link{$dropcall})) {
					delete ($Owned_link{$dropcall}); # drop link
				}
				foreach my $ownedcall (keys %Owned_list) {
					print "OWNED:",$ownedcall, "\n";
				}
			}
		}
	}


}
###############################################################
sub queue_aprs_message {
###############################################################
my $fromcall = shift @_;
my $message = shift @_;
my $mesgnumber = shift @_;

my $mesgkey = $fromcall . $mesgnumber;

# dump key/message into hash
$Messagehash{$mesgkey} = $message;
# add key to FIFO

 }


################################################
sub settxrsid {  #Sets TX RSID for handling in sendit(): 0 or 1.
################################################
	my $rsidflag =shift @_;
	open (CMD, ">$ENV{HOME}/.pskmail/.txrsid");
	print CMD $rsidflag;
	close (CMD);
}

################################################
sub gettxrsid {  #Gets TX RSID required state
################################################
	open (CMD, "$ENV{HOME}/.pskmail/.txrsid");
	my $rsidflag = <CMD>;
	chomp $rsidflag;
	close (CMD);
	return $rsidflag;
}


################################################
sub setrxrsid {  #Sets RX RSID for handling in receiving task: 0 or 1.
################################################
	my $rsidflag =shift @_;
	open (CMD, ">$ENV{HOME}/.pskmail/.rxrsid");
	print CMD $rsidflag;
	close (CMD);
}

################################################
sub getrxrsid {  #Gets RX RSID required state
################################################
	my $rsidflag = 1;
	eval {
		open (CMD, "$ENV{HOME}/.pskmail/.rxrsid") or die "no txrsid flag file";
		$rsidflag = <CMD>;
		chomp $rsidflag;
		close (CMD);
	};
	return $rsidflag;
}
################################################
sub gettable {  #Gets mode table number (0 or 1)
################################################
	my $table = 0;
	eval {
		open (CMD, "$ENV{HOME}/.pskmail/.modetable") or die "no modetable flag file";
		$table = <CMD>;
		chomp $table;
		close (CMD);
	};
	return $table;
}
################################################
sub settable {  #Sets mode table for handling in receiving task: 0 or 1.
################################################
	my $table =shift @_;
	open (CMD, ">$ENV{HOME}/.pskmail/.modetable");
	print CMD $table;
	close (CMD);
}

################################################
sub setmode {  #Sets both the TX and RX mode to the same value
################################################
	$newmodem =shift @_;
	$rspeed = $newmodem;
	
if ($debug2) {print "SETMODE\n";}
	my $newmodenumber = tomodenumber($newmodem);

	if ($newmodenumber ne 0) {
		#Save new mode and number
		open (MODEM, ">$ENV{HOME}/.pskmail/.rxmodem");
		if ($alt_modetable) {
			print MODEM $Bmodes[$newmodenumber];
		} else {
			print MODEM $currentmodes[$newmodenumber];
		}
		close (MODEM);

		open (MOD, ">$ENV{HOME}/.pskmail/.rxmodenumber");
		print MOD $newmodenumber;
		close (MOD);

		open (MODEM, ">$ENV{HOME}/.pskmail/.txmodem");
		if ($alt_modetable) {
			print MODEM $Bmodes[$newmodenumber];
		} else {
			print MODEM $currentmodes[$newmodenumber];
		}
		close (MODEM);

		open (MOD, ">$ENV{HOME}/.pskmail/.txmodenumber");
		print MOD $newmodenumber;
		close (MOD);

		#send to the modem
		sendmode($newmodem);

	} else {
#		print "Mode not in list: ", $newmodem, "\n";
	}


}

##############################################################
sub tomodenumber {
##############################################################
	my $textmodem = "";
	$textmodem = shift @_ ;
	my $m_index = 0;
# print "TEXTMODEM:", $textmodem, "\n";
		for ($i = $#currentmodes; $i > 0; $i--){
#			print "CURRENTMODES:", $currentmodes[$i], ",", $i, "\n";
		}
		for ($i = $#currentmodes; $i > 0; $i--) {
			if ($textmodem eq $currentmodes[$i]) {
				$m_index = $i;
				$i = 0;
			}
		}

	return $m_index;
}

##############################################################
sub gettxmodem {
##############################################################
	if (-e "$ENV{HOME}/.pskmail/.txmodem") {
		open (SPD, "$ENV{HOME}/.pskmail/.txmodem");
		my $speed = <SPD>;
		chomp $speed;
		close (SPD);
		return $speed;
	} else {
		return "PSK250R";
	}
}

##############################################################
sub getrxmodem {
##############################################################
	if (-e "$ENV{HOME}/.pskmail/.rxmodem") {
		open (RSPD, "$ENV{HOME}/.pskmail/.rxmodem");
		my $rspeed1 = <RSPD>;
		 eval { 
			 chomp $rspeed1; 
#		  warn $@ if $@;
			close (RSPD);
			$rspeed = $rspeed1;
			return $rspeed1;
		};
		
	} 
		return $rspeed;
}

##############################################################
sub gettxmodenumber {
##############################################################
	open (SPD, "$ENV{HOME}/.pskmail/.txmodenumber");
	my $speed = <SPD>;
	chomp $speed;
	close (SPD);
	return $speed;
}

##############################################################
sub getrxmodenumber {
##############################################################
	open (RSPD, "$ENV{HOME}/.pskmail/.rxmodenumber");
	my $rspeed = <RSPD>;
	chomp $rspeed;
	close (RSPD);
	return $rspeed;
}

##############################################################
sub adjusttxmodem { #Argument is step size in + or - from current mode
##############################################################
	my $adjustment = shift @_;
	my $newtxmodenumber = gettxmodenumber() + $adjustment;

	#Beyond boundaries or default, we leave at previous mode
	if ($newtxmodenumber < 1 || $newtxmodenumber > $MAXMODENUM) {
		$newtxmodenumber = gettxmodenumber();
		$adjustment = 0;
	} else {
		#Save new mode and number
		open (MODEM, ">$ENV{HOME}/.pskmail/.txmodem");
		if ($alt_modetable) {
			print MODEM $Bmodes[$newtxmodenumber];
		} else {
			print MODEM $currentmodes[$newtxmodenumber];
		}
		close (MODEM);

		open (MOD, ">$ENV{HOME}/.pskmail/.txmodenumber");
		print MOD $newtxmodenumber;
		close (MOD);
	}

	return $adjustment;   #zero if failed, adjustment if ok
}

##############################################################
sub adjustrxmodem { #Argument is step size in + or - from current mode
##############################################################
	my $adjustment = shift @_;
	my $newrxmodenumber = getrxmodenumber() + $adjustment;


	#Beyond boundaries or default, we leave at previous mode
	if ($newrxmodenumber < 1 || $newrxmodenumber > $MAXMODENUM) {
		$newrxmodenumber = getrxmodenumber();
		$adjustment = 0;
	} else {

#print "newrxmodenumer=", $newrxmodenumber, "\n";
#print "Amodes:", $currentmodes[$newrxmodenumber], "\n";

		#Save new mode and number
		open (MODEM, ">$ENV{HOME}/.pskmail/.rxmodem");
		if ($alt_modetable) {
			print MODEM $Bmodes[$newrxmodenumber];
		} else {
			print MODEM $currentmodes[$newrxmodenumber];
		}
		close (MODEM);

		open (MOD, ">$ENV{HOME}/.pskmail/.rxmodenumber");
		print MOD $newrxmodenumber;
		close (MOD);
	}

	return $adjustment;   #zero if failed, adjustment if ok
}





##############################################################
sub settxmodem { #Argument is mode number: 1 to MAXMODENUM
##############################################################
	my $newtxmodenumber = shift @_;
	my $successcode = $newrxmodenumber;
	#Beyond boundaries, we leave at previous mode. But 0=default is ok (disables mode switching)
	if ($newtxmodenumber < 0 || $newtxmodenumber > $MAXMODENUM) {
		$successcode = -1;
	} else {
		#Save new mode and number
		open (MODEM, ">$ENV{HOME}/.pskmail/.txmodem");
		if ($alt_modetable) {
			print MODEM $Bmodes[$newtxmodenumber];
		} else {
			print MODEM $currentmodes[$newtxmodenumber];
		}
		close (MODEM);

		open (MOD, ">$ENV{HOME}/.pskmail/.txmodenumber");
		print MOD $newtxmodenumber;
		close (MOD);
	}

	return $successcode;   #-1 if failed, passed modenumber if ok
}


##############################################################
sub setrxmodem { #Argument is mode number: 1 to MAXMODENUM
##############################################################
	my $newrxmodenumber = shift @_;

#print "NEW:" , $newrxmodenumber, "\n";
#print "RXMODEM:", $currentmodes[$newrxmodenumber], "\n";

	my $successcode = $newrxmodenumber;
	#Beyond boundaries, we leave at previous mode but 0=default is ok (disables mode switching)
	if ($newrxmodenumber < 0 || $newrxmodenumber > $MAXMODENUM) {
		$successcode = -1;
	} else {
		#Save new mode and number
print "done saving rxmodem\n";
		open (MODEM, ">$ENV{HOME}/.pskmail/.rxmodem");
		if ($alt_modetable) {
			print MODEM $Bmodes[$newrxmodenumber];
		} else {
			print MODEM $currentmodes[$newrxmodenumber];
		}
		close (MODEM);

		open (MOD, ">$ENV{HOME}/.pskmail/.rxmodenumber");
		print MOD $newrxmodenumber;
		close (MOD);
	}

	return $successcode;   #-1 if failed, passed modenumber if ok
}


##############################################################
sub upgradable { #1st Argument is mode number: 1 to MAXMODENUM, 2nd arg is s2n ratio
## required to harmonize the differences in reported signal quality between modes
##############################################################
	my $currentmodenumber = shift @_;
	my $currents2n =  shift @_;
	my $oktoupgrade = 0;

# print "in upgradable() Current mode and s2n : " . $currentmodenumber . "  ,  " . $currents2n . " \n";

	if ($currentmodes[$currentmodenumber] eq "PSK500R") {  #upgrade to PSK500 only if link is stable
		if ($currents2n > 95) {
			$oktoupgrade = -1;
		}
		return $oktoupgrade;
	}elsif ($currentmodenumber < 2 && $currents2n > 95 && $lengthlastframe > 32) {
		$oktoupgrade = 1;
	} else {
		if ($currents2n > 85) {
			$oktoupgrade = -1;
		}
		return $oktoupgrade;
	}
	return $oktoupgrade;
}


##############################################################
sub needupgrade { #1st Argument is mode number: 1 to MAXMODENUM, 2nd arg is s2n ratio
## required to harmonize the differences in reported signal quality between modes
##############################################################
	my $currentmodenumber = shift @_;
	my $currents2n =  shift @_;
	my $oktoupgrade = 0;

print "in needupgrading() Current mode and s2n : " . $currentmodenumber . "  ,  " . $currents2n . " \n";

	if ($currents2n < 25 ) {
		$oktoupgrade = -1;
	}
	return $oktoupgrade;

}


##############################################################
sub decayavg { #Arguments are old average, then new value, then avg factor
# Returns the new averaged value
##############################################################
	my $oldaverage = shift @_;
	my $newvalue =  shift @_;
	my $factor = shift @_;
	my $newaverage = $oldaverage;

	if ($factor > 1) {
		$newaverage = ($oldaverage * (1 - 1 / $factor)) + ($newvalue / $factor);
	}
	return $newaverage;

print "in decayavg:  $oldaverage , $newvalue , $factor , $newaverage \n";

}


##############################################################
sub mkflag {	# closure making simple flags (0/1)
#				use: 	$myflag = mkflag();
#						$yourflag = mkflag();
#						$myflag->{SET}->();
##############################################################
	my $var = 0;
	my $bundle = {
		"SET" => sub { $var = 1 },
		"GET" => sub { return $var },
		"RESET" => sub { $var = 0 },
	};
	return $bundle;
}
###############################################################
sub getshortcalls {
###############################################################


	if (-e "$ENV{HOME}/.pskmail/calls.txt") {

		open (CALLS, "$ENV{HOME}/.pskmail/calls.txt");
		my @indexcalls = <CALLS>;
		close (CALLS);

		foreach my $callinx (@indexcalls) {
		chomp $callinx;
			if ($callinx) {
				my $index =shortcall($callinx);
				$Callindex{$callinx} = $index;
			}
		}
		$proto = 2;
	}
}
###############################################################
sub shortcall {
###############################################################

my $call = shift @_;
my $fingerprint = md5_base64($call);
my $shortprint =  substr($fingerprint, -3);
$shortprint  =~ tr/0123456789\/\+/abcdefghijXX/;

return $shortprint;
}
###############################################################
sub gettxtqueue {
###############################################################
	return length ($TxTextQueue);

}
 ##################################################
 sub getarqconfig {
 ##################################################
 	my @conf = ();
 	my $configdata;
 	if (-e "$ENV{HOME}/.pskmail/.pskmailconf") {
 		open (CONFIG, "$ENV{HOME}/.pskmail/.pskmailconf");
 		my $configdata = <CONFIG>;
 		close (CONFIG);
 		@conf = split (",", $configdata);
# logprint ($configdata, "\n");
 		return $configdata;
 	} else {
 		$conf[0] = 0;
 		$conf[1] = 0;
 		$conf[2] = 0;
 		$conf[3] = "$ENV{HOME}/.pskmail/gMFSK.log";	# Inputfile
 		$conf[4] = "$ENV{HOME}/.pskmail/gmfsk_autofile"; # commandfile
 		$conf[5] = "$ENV{HOME}/.pskmail/server.log";
 		$conf[6] = 16;
 		$conf[7] = 15;
 		$conf[8] = 0;
 		$conf[9] = "@";
 		$conf[10] = "pskmail $Version";
 		$conf[11] = 0;
 		$conf[12] = 20;

 	}
 	return $configdata;
 }


#####################################################################
# Set status for need to handle autotuner
sub set_autotune {
#####################################################################
	$AutoTune = shift @_;
	#print "AutoTune:$AutoTune.\n";
}

###############################################################
# Send a PTTTUNE command to fldigi, this triggers an autotune for
# ICOM autotuners (when the rig is set to "PTT TUNE" /SM0RWO
sub ptttune {
###############################################################
	if ($AutoTune != 0) {
		sendcmd("PTTTUNE");
		sleep (2);
		$AutoTune = 0;
	}
}

################################################
# Send an XML coded command to fldigi, "mode" used here will change
# later when fldigi is done. /SM0RWO
sub sendcmd {
################################################

	my $command =shift @_;
	my $cmdstring = "\<cmd\>\<mode\>" . $command . "\</mode\>\</cmd\>";

#	msgsnd($txid, pack("l! a*", $type_sent, $cmdstring), 0 ) or die "# msgsend failed: $!\n";
	sendstuff ($cmdstring);
	print "SENDCMD\n";
	#print $cmdstring;

#	open (CMDOUT, $output);
#	print CMDOUT $cmdstring;
#	close (CMDOUT);
}
################################################
sub getgpstime {
################################################

	my $gpstime = `cat .gps`;
	if ($gpstime =~ /.*\s.*\s.*\s(\d+)/) {
		(my $sec, my $min, my $hr) = gmtime($1 + 1);
		my $out = sprintf("%02s:%02s:%02s", $hr, $min, $sec);
		$out;
	}

}


################################################
sub getgpspos {
################################################
		my $lat;
		my $lon;
		my $gpstime = `cat .gps`;
		if ($gpstime =~ /(\d+)\s(.*)\s(.*)\s\d+/) {
			if ($gpsnr != $1) {
				$gpsnr = $1;
				($lat,$lon) = ($2, $3);
			} else {
				($lat,$lon) = ("Nofix", "Nofix");
			}
		}

}
#################################################
sub point_handler {
#################################################
  my $last_return=shift()||1; #the return from the last call or undef if first
  my $point=shift(); #current point $point->fix is true!
  my $config=shift();
	  if ($point->fix) {
		open (OUTGPS, ">.gps");
	  	print OUTGPS $last_return, " ", $point->lat, " ", $point->lon, " ", $point->time, "\n";
	  	close (OUTGPS);
	  } else {
		open (OUTGPS, ">.gps");
	  	print "No fix\n";
	  	close (OUTGPS);
	  }
  return $last_return + 1; #Return a true scalar type e.g. $a, {}, []
                           #try the interesting return of $point
}

#############################################################
sub sendmode {		# send mode to modem
#############################################################
	my $modemstring = shift @_;

#Auto timing
	my $newmode = $modemstring; #Save it for later

#	$modemstring = sprintf("<cmd><mode>%s</mode></cmd>%c",$modemstring, 0x00);
	$modemstring = "<cmd><mode>". $modemstring . "</mode></cmd>";

	if ($modemstring ne "CTSTIA") {
			if (-e " $ENV{HOME}/.pskmail/.ctstiamode") {
				`rm $ENV{HOME}/.pskmail/.ctstiamode`;
			}
				$CTSTIA_mode = 0;
				$CTSTIA = "";
	} 

	sleep 1; # no overlapping mode change commands...

	$type_sent = 1;


##Added copy of Rein's patch for TX overlap

            my $maxtxtime = 30;

		while (-e "$ENV{HOME}/.pskmail/.tx.lck") {
			sleep 1;
			$maxtxtime--;
			if ($maxtxtime < 1) {
				`rm "$ENV{HOME}/.pskmail/.tx.lck"`;
				last;
			}
#print "TXLOCK=", $maxtxtime, "\n";			
		}
		
		

#	msgsnd($txid, pack("l! a*", $type_sent, $modemstring), 0 ) or die "# msgsend failed: $!\n";
	sendstuff ($modemstring);
#	print "SENDMODE:",$modemstring, "\n";

#Auto timing, adjust cps and timing (critical for downgrading of speed)

if ($timingdebug) {print "====> in sendmode using newspeed of:  $newmode \n";}

	if ($newmode =~ /PSK500R/) {
		$connect_cps = 31;
	} elsif ($newmode =~ /PSK500/) {
		$connect_cps = 52;
	} elsif ($newmode =~ /PSK1000/) {
		$connect_cps = 70;
	} elsif ($newmode =~ /PSK250RC3/) {
		$connect_cps = 40;
	} elsif ($newmode =~ /PSK63RC10/) {
		$connect_cps = 32;		
	} elsif ($newmode =~ /PSK63RC5/) {
		$connect_cps = 15;
	} elsif ($newmode =~ /PSK250R/) {
		$connect_cps = 15;
	} elsif ($newmode =~ /PSK250/) {
		$connect_cps = 26;
	} elsif ($newmode =~ /PSK125R/) {
		$connect_cps = 7;
	} elsif ($newmode =~ /PSK125/) {
		$connect_cps = 13;
	} elsif ($newmode =~ /PSK63/) {
		$connect_cps = 6.5;
	} elsif ($newmode =~ /MFSK64/) {
		$connect_cps = 22;
	} elsif ($newmode =~ /MFSK32/) {
		$connect_cps = 7;
	} elsif ($newmode =~ /MFSK22/) {
		$connect_cps = 6;
	} elsif ($newmode =~ /MFSK16/) {
		$connect_cps = 3;
	} elsif ($newmode =~ /THOR22/) {
		$connect_cps = 4;
	} elsif ($newmode =~ /THOR8/) {
		$connect_cps = 2;
	} elsif ($newmode =~ /DOMINOEX22/) {
		$connect_cps = 14;
	} elsif ($newmode =~ /DOMINOEX11/) {
		$connect_cps = 8;
	} elsif ($newmode =~ /Contestia/) {
		$connect_cps = 2;	} else {
		$connect_cps = 1; #don't know this mode so have extended timouts
	}

#Adaptative timing. Store for usage by get_maxidle()
	open CPSDATA, ">$ENV{HOME}/.pskmail/receivedCPS";
	print CPSDATA "$connect_cps";
	close CPSDATA;

if ($timingdebug) {print "====> in sendmode setting new connect_cps to:  $connect_cps \n";}

if ($timingdebug) {print "====> in sendmode using payloadlength of:  $payloadlength \n";}

}


#############################################################
sub sendmodeNT {		# send mode to modem, No Timeout adjustement. Used when switching to TX mode
#No change in timing since this is for TX only
#############################################################
	my $modemstring = shift @_;

#	$modemstring = sprintf("<cmd><mode>%s</mode></cmd>%c",$modemstring, 0x00);
	$modemstring = "<cmd><mode>" . $modemstring . "</mode></cmd>";
	$type_sent = 1;

           my $maxtxtime = 30;

	while (-e "$ENV{HOME}/.pskmail/.tx.lck") {
		sleep 1;
		$maxtxtime--;
		if ($maxtxtime < 1) {
			`rm "$ENV{HOME}/.pskmail/.tx.lck"`;
			last;
		}
	}

	sendstuff ($modemstring);
#	print "SENDMODENT\n";

}


#############################################################
sub sendmodemcommand {		# send command to modem
#############################################################
	my $cmdstring = shift @_;

#	$cmdstring = sprintf("<cmd>%s</cmd>%c",$cmdstring, 0x00);
	$cmdstring = "<cmd>" . $cmdstring . "</cmd>";
	$type_sent = 1;

        my $maxtxtime = 30;

		while (-e "$ENV{HOME}/.pskmail/.tx.lck") {			
			$maxtxtime--;
			if ($maxtxtime < 1) {
				`rm "$ENV{HOME}/.pskmail/.tx.lck"`;
				last;
			}
			sleep 1;
		}


	sendstuff ($cmdstring);
		
if ($debug2){	print "SENDMODEMCOMMAND: $cmdstring\n";}

}

###############################################################
sub handle_statistics {
###############################################################
my $Queue = "";
$Queue = shift @_;
my $missed = 0;
my $success = 100;
my $bl = 0;
my  $session = "none";
my $md = "M";

sleep 1;

open SESSION, "$ENV{HOME}/.pskmail/PSKmailsession";
$session = <SESSION>;
chomp $session;
close SESSION;

if ($session ne "none") {
	$md = "C";
	if ($Queue =~ /<SOH>\S\Ss\S\S\S(.*)\S{4}<EOT>.*/) {
		$success = 8 - length($1);
		$success = ($success / 8) * 100;
		`echo $success > $ENV{HOME}/.pskmail/.servers2n`;
		`echo $success > $ENV{HOME}/.pskmail/.rxs2n`;
	} else {
		$success = 100;
	}
}

$servers2n = `cat $ENV{HOME}/.pskmail/.servers2n`;
chomp $servers2n;
$clients2n = `cat $ENV{HOME}/.pskmail/.clients2n`;
chomp $clients2n;

if ($Queue =~ /<SOH>00u(\S+\-*\d*):.*\S{4}<EOT>.*/) {
	$session = $1;
	print "beacon:", $1, "\n";
} elsif ($Queue =~ /<SOH>00u(\S+\-*\d*)[<>].*/) {
	$session = $1;
	print "link req:", $1, "\n";
} elsif ($Queue =~ /<SOH>QSL (\S+\-*\d*) de (\S+)\s.*/) {
	$session = $2;
}

my $band = `cat $ENV{HOME}/.pskmail/.band`;
chomp $band;
my $freq = 

$frame_epoch = gettimeofday();
($seconds, $minutes, $hours, $day, $month, $year, $wday, $yday, $isdst) = localtime($frame_epoch);

my $timestr = sprintf("%04d%02d%02d,%02d:%02d:%02d", $year+1900, $month+1, $day, $hours, $minutes, $seconds);

if (-e "$ENV{HOME}/.pskmail/.blocklength") {
	$bl = `cat $ENV{HOME}/.pskmail/.blocklength`;
	chomp $bl;
	`rm $ENV{HOME}/.pskmail/.blocklength`;
}

if ($md ne "C") {
	$clients2n = 0;
	$bl = 0;
	$success = 100;
}

if (!gettable()) {
	$txmode = $Bmodes[gettxmodenumber()];
	$rxmode = $Bmodes[getrxmodenumber()];
} else {
	$txmode = $currentmodes[gettxmodenumber()];
	$rxmode = $currentmodes[getrxmodenumber()];
}
	my $txmode = gettxmodem();
	my $rxmode = getrxmodem();
	
	eval {
		my $mystat = sprintf("%s,%s,%s,%s,%d,%d,%d,%d,%s,%s,%d\n", $timestr, $md, $ServerCall, $session, $clients2n, $servers2n, $bl, $success,  $txmode, $rxmode, $band);
		open (STATI, ">>","$ENV{HOME}/.pskmail/pskmailstat.txt");
		print STATI $mystat unless $session eq "none";
		close (STATI);
	};
#	print "MYSTAT:", $mystat;
}

#####################################################
sub test_user {
#####################################################
	tie (%db, "DB_File", $dbfile)
		or die "Cannot open user database\n";

	my $key = "";
	my $Value = "";

	$key = shift @_;
	return (0) unless $key;
	if (exists $db{$key}) {
		 untie %db;
		 return (1);
	}
	untie %db;

}

#############################################################################
 sub str_parser {
 #############################################################################
 my $byte_array = shift @_;
 my $blen = length($byte_array);
 my $c = "";

 if ($newrxmode) {
	 ;
 } else {
	 $newrxmode = "PSK500R";
 }

 if ($Blockline) {
	 ;
 } else {
	 $Blockline = "";
 }

 if ($soh_time) {

 } else {
	 $soh_time = gettimeofday();
 }

	if ($blen) {
		$c = substr($byte_array, 0,1);

	}
	if ($blen > 1){
		$byte_array = substr($byte_array, 1);
		
	}

	if (ord($c) == 0) {
		;
	} elsif (ord($c) == 1) {
		
#		print "BLOCK:\n";
		push @accu_array, $c;
		$cblock = 1;
				
		#Adaptative timing: mark reception of <SOH>
		open (SOUT, ">$ENV{HOME}/.pskmail/.soh");
		close (SOUT);

		if ($last_eot == 1) {
			$last_eot = 0;
		}
		


		#Auto timing - Get latest SOH time
		$soh_time = gettimeofday();

		if (-e "$ENV{HOME}/.pskmail/.tx.lck") { unlink "$ENV{HOME}/.pskmail/.tx.lck"; }
		
			if ($squelch == 1) {

				$Blockline .= "<SOH>\n";

				unshift @Blockarray, $Blockline;

				$Blockline = "<SOH>";
				$squelchtime = time();
			} else {
				$Blockline = "<SOH>";
				$squelch = 1;
				`touch $ENV{HOME}/.pskmail/squelch.lk`;
				$squelchtime = time();
				 
			}
	} elsif (ord ($c) == 2) {	## <STX>
		;# ignore
	} elsif (ord ($c) == 4) {
		
		push @accu_array, $c;
		my $arraycontent = join "", @accu_array;
=head		
		if (length($arraycontent) > 5) {
			print "\nACCU:", $arraycontent, "\n";
		}
=cut
		
		@accu_array = ();
		
		if ($last_eot == 0) {		## only 1 <EOT>
			#save time at this point
			$eot_time = gettimeofday();
			#Adaptative timing: mark reception of <EOT>
			#<EOT> not used at present
#			open (SOUT, ">$ENV{HOME}/.pskmail/.eot");
#			close (SOUT);


			##Asymmetric linkup: store received RSID into file (must have been sent just before that frame)
			open (SOUT, ">$ENV{HOME}/.pskmail/.rxid");
			print SOUT $newrxmode, "\n";
			close (SOUT);

			## Asymmetric modes: delete RxID information to ensure it was not for a previous frame
			$newrxmode = "default";

			#Auto timing
			$frame_length = length ($Blockline) -3;  ##SOH expanded to "<SOH>" plus last EOT character
			$frame_duration = ($eot_time - $soh_time);
			if ($frame_duration < 0.25) {
			     $frame_duration = 0.25;
			}
			$cps = $frame_length/$frame_duration;
			if ($timingdebug) {print "length-> $frame_length ,  cps-> $cps \n";}

### Assumes maximum number for frame length is 32 + overhead
			if (($frame_length > 7) && ($frame_length < 50)) {
				open CPSDATA, ">$ENV{HOME}/.pskmail/receivedCPS";
				print CPSDATA "$cps";
				close CPSDATA;
			}

			$Blockline .= "<EOT>\n";
			unshift @Blockarray, $Blockline;
print $Blockline, "\n";
			if (-e "$ENV{HOME}/.pskmail/squelch.lk"){
					`rm $ENV{HOME}/.pskmail/squelch.lk`;
			}
			$Blockline = "";

			#Delay writing of frame to .input file until we have processed the s2n data or waited 0.25sec
			$haveeot = -1;

		}
		$last_eot = 1;
		
		push @accu_array, $c;
		my $accucontent = join "", @accu_array;
#		print "\nACCU:", $accucontent, "\n";
		@accu_array = ();
		$cblock = 0;
	} elsif (ord ($c) == 6) {
		;
	} elsif (ord ($c) == 31) {	## <US> opens squelch
		`touch $ENV{HOME}/.pskmail/squelch.lk`;
		$squelchtime = time();
	} else {
		if ($cblock) {
			push @accu_array, $c;
		}
	
		
		#Only write complete frame to .input file when we have
		#  stored the s2n OR after 0.25 sec
		if (($haveeot && $haves2n) && ((-e "$ENV{HOME}/.pskmail/.rxs2n") || (gettimeofday() > $eot_time + 0.50))) {
##DEBUGG
print $Blockline, "\n";
			handle_statistics($Blockline);
			if (length($Blockline) > 5) {
				unshift @Blockarray, $Blockline;
			}

			$haveeot = 0;
			$haveeots2n = 0;
			## delay the squelch release ?
			if ($Sqldelay) {
				select undef, undef, undef, $Sqldelay;
			}

			$squelch = 0;
			if (-e "$ENV{HOME}/.pskmail/squelch.lk") {
				unlink "$ENV{HOME}/.pskmail/squelch.lk";
			}

			$Blockline = "";
		} else {
			# In case there is not more data after the s2n data structure
			# Only write complete frame to .input file when we have
			#  stored the s2n OR after 0.25 sec

			if (($haveeot && $haves2n) && ((-e "$ENV{HOME}/.pskmail/.rxs2n") || (gettimeofday() > $eot_time + 0.25))) {
				if (length($Blockline) > 5) {
					unshift @Blockarray, $Blockline;
				}
				$haveeot = 0;
				$haveeots2n = 0;
				## delay the squelch release ?
				if ($Sqldelay) {
					select undef, undef, undef, $Sqldelay;
				}
				$squelch = 0;
				if (-e "$ENV{HOME}/.pskmail/squelch.lk") {
					unlink "$ENV{HOME}/.pskmail/squelch.lk";
				}
				$Blockline = "";
			}
		}
		if ($squelch == 1) {
			if (length(Blockline)> 128){
				$Blockline = substr($Blockline, 30);
			}
			$Blockline .= $c;
			my $time_now = time();

#Changed from 10 to 30 seconds to capture slow modes packets
			if ($time_now - $squelchtime > 30) {	# reset squelch after 30 secs.
				$squelch = 0;
				$squelchtime = $time_now;
				if (-e "$ENV{HOME}/.pskmail/squelch.lk") {
					unlink "$ENV{HOME}/.pskmail/squelch.lk";
				}
			}
		}

		my $time_now = time();

#Changed from 10 to 30 seconds to capture slow modes packets
		if ($time_now - $squelchtime > 30) {	# reset squelch after 30 secs.
			$squelch = 0;
			$squelchtime = $time_now;
			if (-e "$ENV{HOME}/.pskmail/squelch.lk") {
				unlink "$ENV{HOME}/.pskmail/squelch.lk";
			}
		}
		select (undef , undef, undef, 0.001);

	}

	select (undef , undef, undef, 0.001);

	return $byte_array;
}

#############################################################################
sub handlegarbage {
#############################################################################
	$garbage_array .= shift @_;
		

	if (length($garbage_array) > 32) {
		$garbage_array = substr($garbage_array, -32);
	}

	my $statindex = index ($garbage_array, $prediction);

	if ($statindex > 0) {
		$pstat = substr($garbage_array, $statindex, length($prediction)) ;
		$garbage_array = "";
		open (SOUT, ">$ENV{HOME}/.pskmail/.stat");
		print SOUT $pstat, "\n";
		close (SOUT);
		$pstat = "";
	}

	my $modeindex = index ($garbage_array, chr(18)."<Mode:"); # ascii 18 = DC2, send by Fldigi as prefix

	if ($modeindex == 1) {

		my $endmodepos = index ($garbage_array, ">");
		if ($endmodepos > 10) {
			##Store rsid mode for validation by <SOH>, reset by <EOT>
			$newrxmode = substr($garbage_array, 8, $endmodepos - 8) ;
			#Remove space or dash from mode string sent by Fldigi
			$newrxmode =~ s/[- ]//;
			#Change BPSK into PSK
			$newrxmode =~ s/BPSK/PSK/;
#print "TEST:", $newrxmode, "\n";
#			$CTSTIA_mode = 0;
			# Remove that mode data from the string, but keep the rest
			$garbage_array = substr($garbage_array, $endmodepos + 1, length($garbage_array) - $endmodepos);
		}
	}

	my $s2nindex = index ($garbage_array, chr(18)."<s2n:"); # ascii 18 = DC2, send by Fldigi as prefix

	#Synchronize s2n with frame being received, process as soon as complete sentence is visible
	if ($s2nindex > 0 && $s2nindex < 14) {
		if ($garbage_array =~ /.{1}\<s2n\:(.*),(.*),(.*)\>.*/) {
		##Store rsid mode for validation by <SOH>, reset by <EOT>
#print $garbage_array, "\n";
			if ($1 > 11) {	#minimum block size for status
				open (RSOUT, ">$ENV{HOME}/.pskmail/.rxs2n");
				print RSOUT $2 - $3, "\n";
				close (RSOUT);
				open (RSOUT, ">$ENV{HOME}/.pskmail/.servers2n");
				print RSOUT $2 - $3, "\n";
				close (RSOUT);
			}
			# Remove that mode data from the string
			$garbage_array = "";
			$haves2n = -1;
		}


	
}

	return $garbage_array;

	} #end handlegarbage

###############################################################
sub sendstuff {
###############################################################
my $stuff = shift @_;
#print "STUFF=", $stuff, "\n";
my $cnt = 0;

while (-e "$ENV{HOME}/.pskmail/.tosend") {
	select undef, undef, undef, 0.1;
	$cnt++;
	if ($cnt > 20) {  # wait max. 2 seconds
		$cnt = 0;
		last;
	}
}

#DEBUG
#select undef, undef, undef, 0.1;
#print "STUFF2=", $stuff, "\n";
open (ST, ">>", "$ENV{HOME}/.pskmail/.tosend");
print ST $stuff;
close ST;

#sleep 1;

}

{
##################### security ###############################
my $prime = 0;  # reference to dh object
my $base = 5;
my $my_pub_key = 0;

##############################################################
sub getkeys {
##############################################################

	my $sc = `cat $ENV{HOME}/.pskmail/.connectsecond`;
	chop $sc;

	my $index = $sc / 2;

	my @nrs = (563,587,719,839,863,887,983,1019,
		1187,1283,1307,1319,1367,1439,1487,1523,1619,1823,
		1907,2027,2039,2063,2099,2207,2447,2459,2579,2819,
		2879,2903);

	$prime = $nrs[$index];

    $dh = Crypt::DH->new;
    $dh->g($base);
    $dh->p($prime);

    ## Generate public and private keys.

    $dh->generate_keys;

    $my_pub_key = $dh->pub_key;
    $my_priv_key = $dh->priv_key;

    open (PAR, ">$ENV{HOME}/.pskmail/.dhpar");
	print PAR $base . "," . $prime . "," . $my_priv_key;
	close PAR;

    return ($my_pub_key);
}

##############################################################
sub make_secret {
##############################################################
    ($other_pub_key, $pass) = @_;

    $mypar = `cat $ENV{HOME}/.pskmail/.dhpar`;
    ($base, $prime, $my_priv_key) = split ",", $mypar;

    $dh = Crypt::DH->new;
    $dh->g($base);
    $dh->p($prime);
    $dh->priv_key($my_priv_key);

    ## Now compute shared secret from "other" public key.
    my $shared_secret = $dh->compute_secret( $other_pub_key );


 $sharedkey = md5_hex($shared_secret . "");
 $sharedkey .= $sharedkey;
 $sharedkey .= $sharedkey;
 $sharedkey .= $sharedkey;

 @in_pass = split "", $pass;

 @shared = split "", $sharedkey;

 @outbytes = ();

 for (my $i = 0; $i < scalar(@in_pass); $i++) {
	 my $sh = str_to_hex($in_pass[$i]) ^ str_to_hex($shared[$i]);
	 push (@outbytes, $sh);
 }

my @str = ();
my $j = 1;
my $k = 0;

for (my $i = 0; $i < scalar(@outbytes); $i++) {
	if ($j) {
		$k = $outbytes[$i] * 16;
		$j = 0;
	} else {
		$k += $outbytes[$i];
		push @str, chr($k);
		$k = 0;
		$j = 1;

	}
}

$ostr = join "", @str;

#print "PASSWORD:", $ostr, "\n";

return ($ostr);
}

#########################################################
 sub str_to_hex {
##########################################################
$inchar = shift @_;

my @hex = ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

	for (my $i = 0; $i < 16; $i++) {
		if ($hex[$i] eq $inchar) {
			return $i;
		}
	}
	return undef;

}
} #end security

############################################################
sub memtofreq {  # hour
############################################################;
	my $hour = shift @_;
	my @memfreqs = ();

	if (($scanner eq "F" || $scanner eq "M") && -e "$ENV{HOME}/.pskmail/qrg/freqs.txt") {
			open ($fh, "$ENV{HOME}/.pskmail/qrg/freqs.txt") or die "Cannot open the scanner file!";
			@freqhrs = <$fh>;
			close ($fh);
			my $storeline = "";
			my $freqline = $freqhrs[$hour];
			@memfreqs =  split "," , $freqline;

			for ($i = 0; $i < (0 + @freqs); $i++) {
				if ($freqhrs[$i] =~ /^\d.*/) {
					$freqtable[$i] = $freqhrs[$i];
					$storeline = $freqhrs[$i];
				}
			}

			return @memfreqs;
	}

	if (-e "$ENV{HOME}/.pskmail/qrg/memtable") {
		open ($fh1, "$ENV{HOME}/.pskmail/qrg/memtable");
		my @memtable = <$fh1>;
		close $fh1;

		foreach $mline (@memtable) {
			chomp $mline;
				my ($nr,$fr) = split "," , $mline;
				if ($fr && $fr =~ /^\d.*/){
					$memories[$nr] = $fr;
				}
		}
		my @freqs = split ",", $freqhrs[$hour];
		for ($i = 0; $i < 5; $i++) {
			my $mem = $freqs[$i];
			if ($memories[$mem]) {
				$memfreq = $memories[$mem];
			}
			push @memfreqs, $memfreq ;
		}
		return @memfreqs;

	} else {
		return @memfreqs;
	}

	return @memfreqs;
}

###############################################################
sub freqtomem { # frequency
###############################################################
my $frq = shift @_;
my $mem = 0;

	if (-e "$ENV{HOME}/.pskmail/qrg/memtable") {
		open ($fh1, "$ENV{HOME}/.pskmail/qrg/memtable");
		my @memtable = <$fh1>;
		close $fh1;

		foreach $mline (@memtable) {
			chomp $mline;
				my ($nr,$fr) = split "," , $mline;
				if ($fr && $fr =~ /^\d.*/){
					$memories[$nr] = $fr;
				}
		}
	}
	for ($i = 0; $i < @memtable; $i++) {
		if ($memtable[$i] == $frq) {
			$mem = $i;
			last;
		}
	}

	return $mem;
}

###############################################################
sub getlinks {
###############################################################
my $tm = time();

	if (-e "$ENV{HOME}/.pskmail/routes") {
		open ($fhr, "$ENV{HOME}/.pskmail/routes");
		my @routes = <$fhr>;
		my @cls;
		close $fhr;
		foreach my $rt (@routes) {
			if ($rt =~ /(\S+)\s(\S+)\s(\d+)/) {
				if ($2 eq $ServerCall) {
					if ($tm - $3 < 2419200) {
						print "Link:" . $1 . "\n";
						$Owned_list{$1} = $3;
					}
				}
			}
		}
	}
}

################################################
sub setworkingfreq {
###############################################
	my $outfreq = shift @_;
	if ($outfreq != 0) {
		$workingfreq = $outfreq;
		`echo "$outfreq" > $ENV{HOME}/.pskmail/.band`;
#		print "wf:", $outfreq, "\n";		
	}
	 
 }
 ###############################################
################################################
sub checkserver {
################################################
	my $server = shift @_;
	my $s = 0;
	my $serverlist = getheard();
	@srvrs = split "\n", $serverlist;
	for (my $i = 0; $i < @srvrs; $i++){
		print "LIST=$srvrs[$i]", "\n";
		if ($srvrs[$i] =~ /([A-Z0-9\-]*)/) {
			$thisserver = $1;
		}
		print "THISSERVER:", $thisserver, "\n";
		if ($thisserver eq $server) {
			$s = 1;
		} else {
#			print "Cannot find $server\n";
		}	
	}
	
	foreach $findcall (@knownservers) {
#print $findcall, "," , $server, "\n";
		if ($findcall eq $server) {
			$s = 1;
		} else {
			print "Cannot find $server\n";
		}
	}
		
	
	return $s;
}
################################################
sub getheard {
#################################################
	my $lst = "";
	my $mheard = `cat $ENV{HOME}/.pskmail/pskdownload/pings.log | sort`;
	my @mh = split "\n", $mheard;
	my $previous = "";
	for ($i = 0; $i < @mh; $i++) {
		if ($mh[$i] =~ /([A-Z0-9\-]*):(\d+).*/){
#					print "ARRAY:", $mh[$i], "\n";
			if ($2 == 26 || $2 == 71 || $2 == 0) {
				print $1, "\n";
				if ($1 ne $previous) { 
					$lst .= $1;
					$lst .= "\n";
				}
				$previous = $1;
			}
		}
	}

	`echo "$lst" >> $ENV{HOME}/.pskmail/TxInputfile`;
	return $lst;	
}
###############################################################
sub send_document {
###############################################################
	my $traffic = shift @_;
	my $content = `tail -n +1 $traffic`;
	`echo "$content" >> $ENV{HOME}/.pskmail/TxInputfile`;
	`echo "-end-" >> $ENV{HOME}/.pskmail/TxInputfile`;
	`rm $ENV{HOME}/.pskmail/transfer_ok`;
	$TXServerStatus = "TXTraffic";
	send_frame();
}

###############################################
sub listall {
#############################################

}

###############################################
sub query { # query pskmail.org or $alt_forwarder
###############################################
#print "QUERY\n";
	my $query = shift @_;
	my $out = "";
	my $locmail = connectserver(0);
	$query .= "\n";
	
	if ($locmail) {
		print $locmail $query;
	} else {		
		return "not connected\n";
	}
#print "query sent\n";	
	while (1) {
		if ($locmail) {
				eval {
				local $SIG{ALRM} = sub {die "alarm"};
				alarm 5;
				eval {
					$bigearline = <$locmail>;
					if ($bigearline) {
#						print "BIGE:", $bigearline;
					}
				};
				alarm 0;
			};
			alarm 0;
			if ($@ && $@ =~ /alarm/) {
				print "Timeout reading BigEar\n";
			} elsif ($@) {
				print "$@,\n";
			}
		} 

		if ($bigearline) {
#				print $bigearline;
				$out .= $bigearline;
			if ($bigearline =~ /-end-/mg) {
				$bigearline = "";
				close $locmail;
#				print 	"OUT:\n", $out;
				return $out;
			} else {
				$bigearline = "";
			}
		}
	}
	return "";		
}

###############################################
sub storemail {
#############################################
 my $mail = shift @_;
 @mails = split "\n", $mail;
 $dummy = shift @mails;
 
 if (length($mail) > 0) {
	 return 0;
 }
 
 my $from = "";
 my $to = "";
 my $subject = "";
 
 #print "IMAIL:\n", $mail;
 #print "\nEND\n";
 
 $dummy = pop @mails; # remove -end- 
 $cmail = join "\n", @mails;
 


	my $cpcmail = $cmail;

	$cmail = decode_base64 ($cmail);

	open (CMAIL, ">", "$ENV{HOME}/.pskmail/compressed.gz");
	print CMAIL $cmail;
	close CMAIL;

	$error = `gunzip -f $ENV{HOME}/.pskmail/compressed.gz`;

my $testvar = `cat $ENV{HOME}/.pskmail/compressed`;

	open (CMAIL, "<", "$ENV{HOME}/.pskmail/compressed");

	my @cmsg = ();
	@cmsg = <CMAIL>;
	$dummy = shift @cmsg;

	push @cmsg, ".\n";

	foreach $readline (@cmsg){

		if ($readline) {

				reset_idle();
				chomp($readline);
				if ($monitor) {
					print $readline, "\n";
				}
	
				if ($readline =~ /^\.$/) {
					$mailstatus = "end_of_mail";
					@message = ();
	
					push @message, $to;
					push @message, $address;
					push @message, $subject;
					push @message, @body;
					my $bd = join "\n", @body;
=head	
					print "TO:", $to, "\n";
					print "FROM:", $from, "\n";
					print "MYCALL=", $ServerCall, "\n";
					print "SUBJECT:", $subject, "\n";
					print "BODY:\n", $bd, "\n";
=cut
	
	#					`echo "Receiving local mail" > $ENV{HOME}/.pskmail/TxInputfile`;
					print "Local mbox $to\n";
					if (-e "$ENV{HOME}/.pskmail/localmail/$to") {
						print "Local mbox $to exists\n";
					} else {
						`touch "$ENV{HOME}/.pskmail/localmail/$to"`;
						print "Local mbox $to made\n";
	#						`echo "Local mailbox made" > $ENV{HOME}/.pskmail/TxInputfile`;
					}
					my $mydate = `date`;
					if ($body[0] eq "~SEND") { 
						$error = shift @body;
					}
					
					my $bd2 = join ("\n", @body);

					my $msg =
						"To: $to" . "\n" .
						"From: $from" . "\n" .
						"Subject: " . $subject . "\n"  . 
						"Date: $mydate" . "\n" .
						$bd2 . "\n";
	
					@delivered_to = ();
					@boxes = ("$ENV{HOME}/.pskmail/localmail/$to");
					@delivered_to = Email::LocalDelivery->deliver($msg, @boxes);
					print "Mail delivered\n";	
					@message = ();
					$mailstatus = "end_of_mail";
					@boxes = ();
					@body = ();
					$msg = "";
					last;
						
				} elsif ($readline =~ /From: (.*)/) {
					$from = $1;
				} elsif ($readline =~ /Date: (.*)/) {
					$mydate = $1;
				} elsif ($readline =~ /Subject: /) {
					# add subject
					my $subjectline = substr($readline, 9);
					$subject = $subjectline ;
				} elsif ($readline =~ /To: ([A-Z0-9\-]+)\@.*/) {
	
					# write  To:
	
					$to = $1;
	
				} elsif ($readline =~ /Your attachment: filename=/) {
	
					$attachment = substr($readline, 26);
	#							print $attachment, "\n";
					push @body, $readline;
	
				} else {
					push @body, $readline;
				}
			}
		}

	
	return 0;
}

###############################################
sub sendlocalmail {
#############################################
my ($transitmsg, $addr) =  @_;
print $addr, "\n";
my $tries = 0;
my $packet = "";

		while (1) {
			$locmail = localmailconnect();
			if ($locmail) { last;}
			$tries++;
			sleep int(rand(5));
			if ($tries > 5) {
#				print "Too many tries\n";
				return 0;
			}
		}

		while (1) {
			if ($locmail) {
					eval {
					local $SIG{ALRM} = sub {die "alarm"};
					alarm 1;
					eval {
						$bigearline = <$locmail>;
					};
					alarm 0;
				};
				alarm 0;
				if ($@ && $@ =~ /alarm/) {
					print "Timeout reading BigEar\n";
				} elsif ($@) {
					print "$@,\n";
				}
			} 

			if ($bigearline) {
#				print $bigearline;
				if ($bigearline =~ /closing/) {
#					print "done\n";
					return 1;
				}
				$bigearline = "";
			}
			
			$packet = $dir;
#print $packet, "\n";
#			$content = `cat $packet`;
			$content = $transitmsg;
#print "PACKET:", $content;
			if ($locmail && length($content) > 0) {	
				print $locmail "FREQ:$ServerCall:Version $Version\n";	
						
				print $locmail $content or die "noprint";
				$content = "";
				print $locmail "-end-\n";
			} else {
					sleep 1;
					$locmail = localmailconnect();
					if ($locmail) {
						$locmail->autoflush(1);
					} else {
						return 0;
					}
			}
			sleep 1;

		}
#	`echo "Sent to server > $ENV{HOME}/.pskmail/TxInputfile`;
}
###############################################
sub sendfreqs {
###############################################
my $tries = 0;
my $packet = "";
my $dir = "$ENV{HOME}/.pskmail/qrg/freqs.txt";
#my $ServerCall = "PI4TUE";
#my $Version = "2.0.0b";

		while (1) {
			$locmail = localmailconnect();
			if ($locmail) { last;}
			$tries++;
			sleep int(rand(5));
			if ($tries > 5) {
#				print "Too many tries\n";
				return 0;
			}
		}

		while (1) {
			if ($locmail) {
					eval {
					local $SIG{ALRM} = sub {die "alarm"};
					alarm 1;
					eval {
						$bigearline = <$locmail>;
					};
					alarm 0;
				};
				alarm 0;
				if ($@ && $@ =~ /alarm/) {
					print "Timeout reading BigEar\n";
				} elsif ($@) {
					print "$@,\n";
				}
			} 

			if ($bigearline) {
#				print $bigearline;
				if ($bigearline =~ /closing/) {
#					print "done\n";
					return 1;
				}
				$bigearline = "";
			}
			
			$packet = $dir;
#print $packet, "\n";
			$content = `cat $packet`;
#print "PACKET:", $content;
			if ($locmail && length($content) > 0) {	
				print $locmail "FREQ:$ServerCall:Version $Version\n";	
						
				print $locmail $content or die "noprint";
				$content = "";
				print $locmail "-end-\n";
			} else {
					sleep 1;
					$locmail = localmailconnect();
					if ($locmail) {
						$locmail->autoflush(1);
					} else {
						return 0;
					}
			}
			sleep 1;

		}
}

###############################################
sub connectserver {
###############################################
		my $dummy = shift @_;
		my $locmail = 0;
		my $tries = 0;
		
		while (1) {
			$locmail = localmailconnect();
			if ($locmail) { 
				last;
			}
			$tries++;
			sleep int(rand(5));
			if ($tries > 5) {
#				print "Too many tries\n";
				
				last;
			}
		}
		
#	print $locmail, "\n";
		return $locmail;
}

###############################################
sub localmailconnect {
###############################################
	if (-e "$ENV{HOME}/.pskmail/.internet" && $NOINTERNET == 0) {
		eval {
			if ($MARS && $alt_forwarder) {
				$locmail = IO::Socket::INET->new(Proto     => "tcp",
											PeerAddr  => "$alt_forwarder",
											PeerPort  => "8888")
				   or die		
			} else {
				$locmail = IO::Socket::INET->new(Proto     => "tcp",
											PeerAddr  => 'pskmail.org',
											PeerPort  => "8888")
				   or die 
			 }
		};


		if ($@) {
#			print "$@\n";
			return 0;
		}

		if (defined $locmail) {
			$locmail->autoflush(1);
		}

		return $locmail;
	} else {
		return 0;
	}
} 
	
###############################################################
sub get_streamid {
###############################################################
	
	return $Current_session;
}

###############################################################
sub extractLat {
###############################################################
my $maidenhead = shift @_;
my $nschar = "N";

my $lat = -90 + 10 * (ord(substr($maidenhead, 1)) - 65)
	+ (ord(substr($maidenhead, 3)) - 48) + 2.5 / 60
	* (ord(substr($maidenhead, 5)) - 65) + 2.5 / 60 / 2;
		
	if ($lat < 0) {
		$lat = abs ($lat);
		$nschar = "S";
	}	
	
	my $degrees = int ($lat);	 	
	my $minutes = int (($lat - $degrees) * 6000) / 100;
	
	$minutes = "0" . $minutes . "0";
	$minutes =~ /.*(\d\d\.\d\d).*/;

	my $latstring = substr ("0" . $degrees, -2) . $1;	
	$latstring = sprintf("%.2f", $latstring);
	$latstring = substr("0" . $latstring, -7) . $nschar;

	return ($latstring);
}
###############################################################

###############################################################
sub extractLon {
###############################################################
my $maidenhead = shift @_;
my $ewchar = "E";

my $lon = -180 + 20 * (ord(substr($maidenhead, 0)) - 65) + 2 
	* (ord(substr($maidenhead, 2)) - 48) + 5 / 60
	* (ord(substr($maidenhead, 4)) - 65) + 5 / 60 / 2;
	
	if ($lon < 0) {
		$lon = abs ($lon);
		$ewchar = "W";
	}
	
	my $degrees = int ($lon);
		
	my $minutes = int (($lon - $degrees) * 6000) / 100;
	
	$minutes = "0" . $minutes . "0";
	$minutes =~ /.*(\d\d\.\d\d).*/;
	
	my $lonstring = sprintf("%.2f", substr ("0000" . $degrees, -3) . $1);
	$lonstring = substr("00" . $lonstring, -8) . $ewchar;
	
	return ($lonstring);
}

###############################################################
sub getbulletin {
###############################################################
my $fname = shift @_;
my $file = "";
my $ftime = "";

#print $fname, "\n";

open(INFO, $fname) or die("Could not open  file.");

foreach $line (<INFO>)  {   
    if (index ($line,  "<PROG") > -1){    
		my $cat = `cat $fname`;
		close(INFO);
		`rm $fname`;
		return ($cat, 0);
	}
}
close(INFO);

my $fl = "";
my ($sec,$min,$hour,$mday,$mon,$year) = gmtime(time);

$year += 1900;
$mon += 1;
$mon = substr("0" . $mon, -2);
$mday = substr("0" . $mday, -2);
$hour = substr("0" . $hour, -2);
$min = substr("0" . $min, -2);
$sec = substr("0" . $sec, -2);

if ($fname =~ m#^(.*?)([^/]*)$#) {
	$file = $2;
	if ($file =~ m#(\d{14})(\S+)#){
		$ftime = $1;
		$file = $2;	
		$fl = sprintf("%s%s", $ftime,$file);	
	} else {
		$fl = sprintf("%s%s%s%s%s%s%s", $year,$mon,$mday,$hour,$min,$sec,$file);		
	}
}



my $cat = `cat $fname`;
my $catlength = length($cat);

#print "File:", $file, "\n";
print "CAT:\n", $cat, "\n";


my $nrofnonprints = 0;
for ($i = 0; $i < $catlength; $i++) {
	my $c = ord(substr($cat, $i, $i + 1));
	if ($c > 127 || ($c < 32 && $c != 10   && $c != 13  && $c != 27  && $c != 9)) {
		 $nrofnonprints += 1;
	}
}

my $b64file = $cat;
print "B64:\n", $b64file, "\n";

if ($nrofnonprints > 0 || $catlength > 20) {  # make base 64
print "FNAME=", $fname, "\n";
`mv $fname tozip`;
	$dummy = `gzip tozip`;	
	print $dummy, "\n";
	my $gzname = "tozip.gz";
	$xx = `ls -l "$ENV{HOME}/.pskmail/bulletins"`;
	print $xx, "\n";
print "GZNAME=", $gzname, "\n";	
	$size_file = `cat $gzname`;	
	$b64file = encode_base64 ($size_file);
	`rm $gzname`;
} else {
	$b64file = $cat;
}

print "Z_B64:\n", $b64file, "\n";

my $ft = "";
my $filetime = "";
my $filesize = -s $fname;
my $comp = "0";
my $type = "b64";
my $blocksize = 77;

#if ($ftime) {
if (1) {	
	$ft = sprintf("%s:%s%s%d", $ftime,$file,$comp,$blocksize);
	$filetime = $ftime;	
}

my $hash = checksum($ft);
my $prog = "pskmail_server-". $Version;
my $ln = 0;
my $prgdata = "{" . $hash . "}" . $prog;
my $cs_in = checksum($prgdata);
$ln = length($prgdata);
my $prg = sprintf("<PROG %d %s>{%s}%s\n", $ln, $cs_in, $hash, $prog);
#print $prg, "\n";
my $idcs = checksum($ServerCall);
my $id = sprintf("<ID %d %s>%s\n", length($ServerCall), $idcs, $ServerCall);
#print $id, "\n";

my $fllen = length($fl) + 6;
my $flin = "{" . $hash . "}" . $fl;
my $flcs = checksum($flin);
my $flstr = sprintf("<FILE %d %s>%s\n",$fllen,$flcs,$flin ); 

#print $flstr, "\n";

$filesize = length($b64file);

#print "SIZE:", $filesize, "\n";
#print "BLOCKS:", $blocksize, "\n";


my $blocks = int ($filesize/$blocksize);
if ($filesize - ($blocks * $blocksize) > 0) { $blocks += 1};
#print "BLOCKS:", $blocks, "\n";
my $szdata = sprintf ("{%s}%d %d %d", $hash, $filesize, $blocks, $blocksize);
my $sizecs = checksum($szdata);
my $szlen = length($szdata);
my $sizestring = sprintf("<SIZE %d %s>%s\n",$szlen, $sizecs, $szdata);
#print $sizestring, "\n";

my $datastring = "";
my $datalen = 0;
my $datacs = "";
my $dataccu = "";
my $cntlstring = "";
my $cntlcs = checksum("{" . $hash . ":EOT}");
my $cntllen = length("{" . $hash . ":EOT}");
#my $fh = "";

#open ($fh, $fname) or die "cannot open file $fname\n";

my $bulletinoutput = "";

$bulletinoutput .= $prg;
$bulletinoutput .= $id;
$bulletinoutput .= $flstr;
$bulletinoutput .= $sizestring;

for (my $k = 0; $k < $repeats; $k++) {
	my $copy = $b64file;
	for ($i = 1; $i <= $blocks; $i++){
		my $buffer = substr($b64file, 0, $blocksize);
		if (length($b64file) >= $blocksize) {
			$b64file = substr($b64file, $blocksize);
		}
		if (length($buffer) > 0) {
			$dataccu = sprintf("{%s:%d}%s", $hash,$i,$buffer);
			$datacs = checksum($dataccu);
			$datalen = length($dataccu);
			$datastring = sprintf("<DATA %d %s>%s\n",$datalen, $datacs, $dataccu );
			$bulletinoutput .=  $datastring;		
		} else {
			last;
		}
	}
	$b64file = $copy;
}

#close $fh;

$zipper = "tozip.gz";

if (-e "$ENV{HOME}/.pskmail/bulletins/$zipper"){
`rm $ENV{HOME}/.pskmail/bulletins/$zipper`;
};

$cntlstring = sprintf("<CNTL %d %s>%s\n", $cntllen, $cntlcs, "{" . $hash . ":EOT}");
$bulletinoutput .= $cntlstring;

#print $bulletinoutput;

open $fh3, ">$ENV{HOME}/.pskmail/repeats/$file";
print $fh3 $bulletinoutput;
close $fh3;

return ($bulletinoutput, $filetime);
	
}


###############################################################
sub dprint { # print debug info to debug.txt
###############################################################
my $out = shift @_;

`echo $out >> $ENV{HOME}/debug.txt`;
print $out , "\n";

}
###################### END ####################################
1;
