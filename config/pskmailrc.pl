#! /usr/bin/perl -w
#PSKmail server config data
#
### Call of this server
$ServerCall = "N0CALL";
### Wait x seconds before every ping
$Pingdelay = 0;
### Which minutes to send beacon? (1 for ON, 0 for OFF)
@Beaconarray = qw (0 0 0 1 0);
### Call for switching off the beacon remotely
$commandcall = "PA0R";
### Address of the SMTP server used to send the mail
$relay = "smtp.gmail.com";
	### Authorization level required for SMTP
	# 0 = no auth
	# 1 = password only
	# 2 = TLS
	# tls, ssl, none

	### The next parameters are needed for use with TLS:
	# You have to use singel quotes ' ' instead of double quotes " "
	# or use escape character \ with double quotes "user\@gmail.co"                                                                                                            
$smtptlsuser =  'userid@gmail.com';
$smtptlspass = 'password';
	### Secure layer required for SMTP
	# Parameter: tls (default), ssl, none
	# smtp.googlemail.com and smtp.mail.yahoo.com use the following secure layers
	# SSL Port 465
	# TLS Port 587 
	# TLS Port 25                                                                                  
$smtpseclayer = 'tls';
$smtptlsport = 587;
	### Authentication method for SMTP                                                             
	# Paramater: ANONYMOUS, CRAM-MD5, DIGEST-MD5, EXTERNAL, GSSAPI, LOGIN (default), PLAIN, none              
$smtpauthlevel = 'LOGIN';

### Display verbosity (1 = ON, 0 = OFF)
$monitor = 1;
$ShowBlock = 1;
$debug = 1;
### Max number of retries for connect acknowledge
$Max_retries = 7;
### Nr. of seconds to wait for an answer from the client
$Maxidle = 17;
### Max. size of downloaded webpage 
$Maxwebpage = 30000;
### Waiting time before sending a packet
$Txdelay = 1;
### Beacon period
$period = 60;
### Enable email and web download function?
$Emails_enabled = 1;
$Web_enabled = 1;
### Port for BigEar function
$BigEarserverport = 10148; # reserved 
## Enable connection to APRS Tier2 server
$Aprs_connect = 1;
### Position DDMM.MM, DDDMM.MM
$latitude = "0000.00N";
$longitude = "00000.00E";
### Beacon text for status
$serverstatus = "PSKmail 2.3.1 ";
### Beacon text for APRS beacon
$Aprs_beacon = "0000.00NP00000.00E& 2.3.1 ";
### Connection to APRS server (3 alternatives)
@Aprs_address = qw (euro.aprs2.net germany.aprs2.net italys.aprs2.net);
@Aprs_port = qw (14580 14580 14580);
### Use scheduler file?
$scheduler = 0; 
### Hamlib Rigctl data for scanning
$rigtype = 229; 
$rigrate = 19200; 
$rigdevice = "/dev/ttyUSB0";
$scanner = "";
$qrgfile  = "$ENV{HOME}/.pskmail/qrg/freqs.txt";
$freq_offset = 0; 
@freq_corrections = qw (-1000 -1000 -1000 -1000 -1000);
@Beaconarray = qw (0 1 0 0 0); 
$traffic_qrg = "10148000"; 
### Default mode
$Defaultmode = "PSK500R";
### Use CAT PTT?
$PTTCOMMAND = 0;
### Parameters for sending bulletins
$bulletinmode = "PSK500R";
$repeats = 2;
### Which servers for local mail transfer?
@knownservers = qw ( DJ0LN 9A1CRA DL3YCS-3 DL4OAH-8 OE5RTL SM0RWO-1 );
### Am I a MARS server?
$MARS = 0; 
### Disallow 500 baud speed? (1 to block)
$NO500 = 0;
### Disallow internet connection?
$NOINTERNET = 0;
1;
