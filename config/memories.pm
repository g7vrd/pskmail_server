# memories.pm is a config file for those pskmail
# scanningservers which use memory scan.
#
# This file sits in ~/.pskmail, and has 2 functions:
# * Cross reference summoning frequency to memory number
# * Provide input for the '~CHANNELS' command
#
# This example provides 4 frequencies:
# 7047000, 10147000, 14107000 and 18105000.

sub convert_to_memory {
	$sfreq = shift (@_);

	if ($sfreq == 10145000) {	#center frequency
		$sfreq = 6;		#memory number
		$validfreq = 1;
	} elsif ($sfreq == 10147000) {	#center frequency
		$sfreq = 15;		#memory number
		$validfreq = 1;
	} elsif ($sfreq == 14107000) {	#center frequency
		$sfreq = 20;		#memory number
		$validfreq = 1;
	} elsif ($sfreq == 18105000) {	#center frequency
		$sfreq = 3;		#memory number
		$validfreq = 1;
	} else {
		$validfreq = 0;
	}
	return ($sfreq, $validfreq);
}
1;
