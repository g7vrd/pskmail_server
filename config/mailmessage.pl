#! /usr/bin/perl -w

$num_args = $#ARGV + 1;

if ($num_args != 1) {
  print "\nUsage: mailmessage.pl SERVERCALL\n";
  exit;
}

$servercall = $ARGV[0];

$cmd = "xmessage -button ok -center \"You've got mail on the server\"";

$dirname = "$ENV{HOME}/.pskmail/localmail/$servercall/";

while(1) {

	if (-d $dirname) {
		opendir DIR, $dirname;
		if(scalar(grep( !/^\.\.?$/, readdir(DIR))) != 0){
			system($cmd);
			closedir DIR;
			exit;
		} else {
			sleep 60;
		}
	}  else {
		sleep 60;
	}
}
