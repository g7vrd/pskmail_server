#! /usr/bin/perl -w

use DB_File;

$dbfile = "rflink_users.db";
my @values = ();
my $deleted = 0;
my $tempkey = "TEMPLATE";

tie (%db, "DB_File", $dbfile) or die "cannot connect to db\n";

################################## values ##################################################
my $key = "";

my $Pop_host = 	"";		# pop3 provider
my $Pop_user = 	"";		# pop user
my $Pop_pass = 	"";		# pop password
my $relay=	"";	# SMTP relay used (non-authorized!!)
my $address = 	""; 	# set return address for SMTP....
my $mailuser = 	""; 	# data store where the mail is received with fetchmail...
my $findupasswd = "";			# password for APRS
############################################################################################

print "\n";
foreach $key (keys %db) {
	print "$key ";
}

print "\n\n", "Station callsign: ";

$key = <STDIN>;
$key =~ tr/a-z/A-Z/;
chomp $key;

	$Value = $db{$key};
	@values = split (/,/ , $Value);

	$Pop_host = shift @values;
	$Pop_user = shift @values;
	$Pop_pass = shift @values;
	$relay = shift @values;
	$address = shift @values;
	$mailuser = shift @values;
	$findupasswd = shift @values;

	while (1) {
		print "\nCommands for $key:";
		if ($Pop_host) {
			print "\n1: Pop host = $Pop_host\n";
		} else {
			print "\n1: Pop host = \n";	
			$Pop_host = "\n";	
		}
		if ($Pop_user) {
			print "2: Pop user = $Pop_user\n";
		} else {
			print "2: Pop user = \n";
			$Pop_user = "\n";
		}
		if ($Pop_pass) {
			print "3: Pop password = $Pop_pass\n";
		} else {
			print "3: Pop password = \n";
			$Pop_pass = "\n";
		}
		if ($relay) {
			print "4: SMTP server = $relay\n";
		}else {
			
			@temparray = split "," , $db{$tempkey};
			$relay = $temparray[3];
			if ($relay) {
				print "4: SMTP server = $relay\n";
			} else {
				print "4: SMTP server = \n";
				$relay = "\n";
			}
		}
		if ($address) {
			print "5: From address = $address\n";
		} else {
			print "5: From address = \n";
			$address = "\n";
		}
		if ($mailuser) {
			print "6: Mail file = $mailuser\n";
		} else {
			@temparray = split "," , $db{$tempkey};
			$mailuser = "/var/mail/$key";
			print "6: Mail file = $mailuser\n";
		}
		if ($findupasswd) {
			print "7: APRS password = $findupasswd\n";
		} else {
			print "7: APRS password = \n";
			$findupasswd = "\n";
		}
		print "9: Delete callsign\n";
		print "0: Exit\n";
		print "\nCommand nr: ";
		my $cmdnr = <STDIN>;
		chomp $cmdnr;
		if ($cmdnr lt "0" || $cmdnr gt "9")
			{	untie %db;
				print "Input must be a number from 0...9, exiting.\n";
				exit;
			}
			
		if ($cmdnr == 1) {
			print "\nNew value for Pop host: ";
			$Pop_host = <STDIN>;
			$Pop_host =~ tr/\,/_/;
			chomp $Pop_host;
		} elsif ($cmdnr == 2) {	
			print "\nNew value for Pop userid: ";
			$Pop_user = <STDIN>;
			$Pop_user =~ tr/,/_/;
			chomp $Pop_user;
		} elsif ($cmdnr == 3) {	
			print "\nNew value for Pop password: ";
			$Pop_pass = <STDIN>;
			$Pop_pass =~ tr/,/_/;
			chomp $Pop_pass;
		} elsif ($cmdnr == 4) {	
			print "\nNew value for SMTP host: ";
			$relay = <STDIN>;
			$relay =~ tr/,/_/;
			chomp $relay;
		} elsif ($cmdnr == 5) {	
			print "\nNew value for From address: ";
			$address = <STDIN>;
			$address =~ tr/,/_/;
			chomp $address;
			$address = " " unless $address;
		} elsif ($cmdnr == 6) {	
			print "\nNew value for Mail file: ";
			$Pop_host =~ tr/,/_/;
			$mailuser = <STDIN>;
			$mailuser =~ tr/,/_/;
			chomp $mailuser;
		} elsif ($cmdnr == 7) {	
			print "\nNew value for APRS password: ";
			$findupasswd = <STDIN>;
			$findupasswd =~ tr/,/_/;
			chomp $findupasswd;
		} elsif ($cmdnr == 9) {	
			print "\nDeleted callsign $key!\n ";
			 delete $db{$key};
			 $deleted = 1;
			 last;
		} elsif ($cmdnr == 0) {
			last;
		}
	} # end while
	
	if ($deleted == 0 && $Pop_host) {
		@values = ();
		push @values, $Pop_host ;
		push @values, $Pop_user ;
		push @values, $Pop_pass ;
		push @values, $relay ;
		push @values, $address;
		push @values, $mailuser;
		push @values, $findupasswd;

		$value = join ("," , @values);

		$db{$key} = $value;
	}

untie %db;

print "\nReady, thank you....\n";

